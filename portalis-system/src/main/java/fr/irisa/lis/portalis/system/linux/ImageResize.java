package fr.irisa.lis.portalis.system.linux;


public class ImageResize {

	// convert
	// http://www.pays-arles.org/e-patrimoine/IMG/jpg/monuments-morts-1.jpg
	// -resize 100x100 - | convert - -gravity center -crop 75x75+0+0
	// cropped-75.jpg
	private static final String convertTo75x75 = "convert %s -resize 100x100 - | convert - -gravity center -crop 75x75+0+0 %s";

	
	String input1 = "http://www.pays-arles.org/e-patrimoine/IMG/jpg/Facade-2---St-Pierre.jpg";
	String input2 = "http://www.pays-arles.org/e-patrimoine/IMG/jpg/monuments-morts-1.jpg";
	public static void main(String[] args) {
		String input = "";
		String output = "test.jpg";
		imageResize(input, output);

	}
	
	public static void imageResize(String input, String output) {
		String cp_cmd = String.format(convertTo75x75, input, output);
		try {
			LinuxInteractor.executeCommand(cp_cmd, true);
		} catch (PortalisSystemException e) {
			e.printStackTrace();
		}
	}



}
