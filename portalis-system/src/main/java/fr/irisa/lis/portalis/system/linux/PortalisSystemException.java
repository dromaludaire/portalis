package fr.irisa.lis.portalis.system.linux;

@SuppressWarnings("serial")
public class PortalisSystemException extends Exception {

	public PortalisSystemException(String mess) {
		super(mess);
	}

	public PortalisSystemException() {
		super();
	}

	public PortalisSystemException(String mess, Throwable e) {
		super(mess, e);
	}

	public PortalisSystemException(Throwable e) {
		super(e);
	}
}
