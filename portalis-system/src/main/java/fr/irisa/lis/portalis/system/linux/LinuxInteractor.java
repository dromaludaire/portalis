package fr.irisa.lis.portalis.system.linux;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class LinuxInteractor {private static final Logger LOGGER = LoggerFactory.getLogger(LinuxInteractor.class.getName());

	public static String executeCommand(String command, boolean waitForResponse)
			throws PortalisSystemException {
		return executeCommand(command, waitForResponse, true);
	}

	public static String executeCommand(String command,
			boolean waitForResponse, boolean checkExitCode)
			throws PortalisSystemException {

		String response = "";

		ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
		pb.redirectErrorStream(true);

		String mess = "\nPortalisCtx execute Linux command " + command + "," + waitForResponse + ","
				+ checkExitCode + ")";
		LOGGER.debug(mess);

		try {
			Process shell = pb.start();

			// To capture output from the shell
			InputStream shellIn = shell.getInputStream();

			if (waitForResponse) {

				// Wait for the shell to finish and get the return code
				int shellExitStatus = shell.waitFor();
				response = convertStreamToStr(shellIn);
				if (checkExitCode && shellExitStatus != 0) {
					if (shellIn != null)
						shellIn.close();
					errorExit(command, response, shellExitStatus);
				}
			} else {
				int exitCode = 99999;
				int i = 5;
				while (exitCode == 99999 & i > 0) {
					try {
						Thread.sleep(100);
						i--;
						exitCode = shell.exitValue();
					} catch (IllegalThreadStateException e) {
					}
				}
				if (exitCode != 99999) {
					response = convertStreamToStr(shellIn);
					if (shellIn != null)
						shellIn.close();
					errorExit(command, response, exitCode);
				}

			}
			if (shellIn != null)
				shellIn.close();

		}

		catch (IOException e) {
			String message = "Error occured while executing Linux command '"
					+ command + "'\nError Description: " + e.getMessage();
			throw new PortalisSystemException(message);
		}

		catch (InterruptedException e) {
			String message = "Error occured while executing Linux command '"
					+ command + "'\nError Description: " + e.getMessage();
			throw new PortalisSystemException(message);
		}

		return response;
	}

	private static void errorExit(String command, String response, int exitCode)
			throws IOException, PortalisSystemException {
		String errMess = "Le processus n'a pas été lancé correctement, les informations sont :"
				+ "\nexitCode = "
				+ exitCode
				+ "\ncommande = "
				+ command
				+ "\nmessage  = " + response;
		LOGGER.error(errMess);
		throw new PortalisSystemException(errMess);
	}

	/*
	 * To convert the InputStream to String we use the Reader.read(char[]
	 * buffer) method. We iterate until the Reader return -1 which means there's
	 * no more data to read. We use the StringWriter class to produce the
	 * string.
	 */

	public static String convertStreamToStr(InputStream is) throws IOException {

		if (is != null) {
			LOGGER.debug("convertStreamToStr() InputStream not null");
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				int i = 1;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
					LOGGER.debug("convertStreamToStr() buf" + i + " = \n"
							+ new String(buffer, 0, n));
					i++;
				}
			} finally {
				is.close();
			}
			String reponse = writer.toString();
			if (reponse.contains("Unix.Unix_error(50, \"bind\", \"\")")) {
				reponse += " (ndrl : Most probably, the required port is already in use)";
			}
			return reponse;
		} else {
			return "";
		}
	}



}
