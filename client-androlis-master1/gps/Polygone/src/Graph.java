import java.util.ArrayList;

public class Graph {

	// Ici, un graphe a pour sommets les segments de la carte, et pour aretes les intersections entre les sommets.
	public ArrayList<Segment> sommets;
	public ArrayList<Arete> aretes;
	
	public Graph(ArrayList<Segment> sommets, ArrayList<Arete> aretes) {
		this.sommets = sommets;
		this.aretes = aretes; 
	}
	
	public Graph(ArrayList<ArrayList<ArrayList<Double>>> carte) {
		this.sommets = new ArrayList<Segment>();
		this.aretes = new ArrayList<Arete>();
		Segment s1;
		Segment s2;
		for (int i=0;i<carte.size();i++) {
			s1 = new Segment(new Point(carte.get(i).get(0).get(0),carte.get(i).get(0).get(1)),new Point(carte.get(i).get(1).get(0),carte.get(i).get(1).get(1)));
			sommets.add(s1);
		}
		for (int i = 0; i < sommets.size(); i++) {
			for (int j = 0; j < i; j++) {
				if ((sommets.get(i).p1.equals(sommets.get(j).p1))||(sommets.get(i).p1.equals(sommets.get(j).p2))
						||(sommets.get(i).p2.equals(sommets.get(j).p1))||(sommets.get(i).p2.equals(sommets.get(j).p2))){
					aretes.add(new Arete(sommets.get(i),sommets.get(j),sommets.get(i).longueur(), new ArrayList<Segment>()));
					aretes.add(new Arete(sommets.get(j),sommets.get(i),sommets.get(j).longueur(),new ArrayList<Segment>()));
				}
			}
		}
	}
	
	// Hypoth�se, v�rifi�e par la suite : g contient l'ar�te a.
	
	public Graph supprimer_segment(Graph g, Segment s) {	
		System.out.println("Segment � supprimer " + s);
		Graph resultat = new Graph(g.sommets,g.aretes);
		ArrayList<Arete> aretes_entrantes = new ArrayList<Arete>();
		ArrayList<Arete> aretes_sortantes = new ArrayList<Arete>();
		Arete a;
		ArrayList<Segment> aux;
		ArrayList<Arete> auxa = new ArrayList(resultat.aretes);
		for (int i = 0; i < resultat.aretes.size(); i++) {
			if (resultat.aretes.get(i).s2.equals(s)) {
				aretes_entrantes.add(resultat.aretes.get(i));
				auxa.remove(resultat.aretes.get(i));
			}
			else if (resultat.aretes.get(i).s1.equals(s)) {
				aretes_sortantes.add(resultat.aretes.get(i));
				auxa.remove(resultat.aretes.get(i));	
			}
		}
		resultat.aretes = new ArrayList(auxa);
		for (int i = 0; i < aretes_entrantes.size();i++) {
			for (int j = 0; j < aretes_sortantes.size();j++) {
				if (!(aretes_entrantes.get(i).s1.equals(aretes_sortantes.get(j).s2))) {
					aux = new ArrayList(aretes_entrantes.get(i).chemin);
					aux.add(s);
					aux.addAll(new ArrayList(aretes_sortantes.get(j).chemin));
					//System.out.println("Arete entrante " + aretes_entrantes.get(i).s2 + " Arete sortante " + aretes_sortantes.get(j).s1);
					a = new Arete(aretes_entrantes.get(i).s1,aretes_sortantes.get(j).s2,
							aretes_entrantes.get(i).dist+aretes_sortantes.get(j).dist,
							aux);
					resultat.aretes.add(a);
				}
			}
		}
		// Elagage des "mauvaises" ar�tes
		auxa = new ArrayList(resultat.aretes);
		ArrayList<Arete> auxb = new ArrayList();
		ArrayList<Arete> auxc;
		while (auxa.size() > 0) {
			a = maj(auxa.get(0));
			auxa.remove(0);
			auxc = new ArrayList(auxa);
			for (int i = 0; i < auxa.size();i++) {
				if (auxa.get(i).egal(a)) {
					if (auxa.get(i).dist < a.dist) {
						a = maj(auxa.get(i));
					}
					auxc.remove(i);
				}
			}
			auxa = auxc;
			auxb.add(a);
		}
		resultat.aretes = auxb;
		resultat.sommets.remove(s);
		return resultat;
		
	}
	
	public Graph reduire(ArrayList<Segment> segments_choisis) {
		Graph g = new Graph(this.sommets,this.aretes);
		int i = 0;
		int j = 0;
		int s = this.sommets.size();
		while (i < s) {
			Boolean cond = false;
			for (int k = 0; k < segments_choisis.size();k++) {
				cond = cond || (segments_choisis.get(k).equals(this.sommets.get(j)));
				if (cond) {break;}
			}
			if (!cond) {
				g = supprimer_segment(g,this.sommets.get(j));
				System.out.println(g);
			}
			else {
				j++;
			}
			i++;
		}
		return g;
	}
	public String toString() {
		String resultat = "";
		int compteur = 1;
		for (int i=0; i < aretes.size();i++) {
			resultat = resultat + "Arete " + compteur + " : " + aretes.get(i).toString() + "\n";
			compteur++;
		}
		return resultat;
	}
	
	public int sum(int n) {
		if (n == 0) {
			return 0;
		}
		else {
			return n + sum(n-1);
		}
		
	}
	private Arete maj(Arete a) {
		return new Arete(new Segment(new Point(a.s1.p1.x,a.s1.p1.y),new Point(a.s1.p2.x,a.s1.p2.y)),new Segment(new Point(a.s2.p1.x,a.s2.p1.y),new Point(a.s2.p2.x,a.s2.p2.y)),a.dist,new ArrayList(a.chemin));
	}
}
