import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;



public class MapReader2 {

	private ArrayList<ArrayList<Point>> tabSectors;
	private ArrayList<ArrayList<Point>> tabPolygones;
	private ArrayList<ArrayList<Point>> tabSegments;
	private String map;
	private String currentForm;
	
	public MapReader2(String fileI, String fileO, String mapFile) {

		
		tabSectors = new ArrayList<ArrayList<Point>>();
		tabPolygones = new ArrayList<ArrayList<Point>>();
		tabSegments = new ArrayList<ArrayList<Point>>();
		read();
		
	}
	
	/**
	 * lit le fichier kml (map) et appelle transform pour stocker les infos
	 */
	private  void read(){
		Scanner scanner = null;
		String str = null;
		try {
			scanner = new Scanner(new FileReader(map));
			while (scanner.hasNextLine()) {
				str = scanner.nextLine();
				transform(str);
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void transform(String str) {
		if(str.contains("<[/]?name>")){
			String sstr[] = str.split(">");
			for(int i = 0 ; i < sstr.length; i++)
				System.out.println(sstr[i]);
		}
	}
	

	

	/**
	 * m�thode qui calcule si le point se situe dans le polygone
	 * @author Tristan CHARRIER
	 * @param tab tableau de points du polygone
	 * @param M point recherch�
	 * @return true si le point est situ� dans le polygone
	 */
	public static boolean appartient_polygone(ArrayList<Point> tab,Point M) {
	  int compteur = 0;
	  double y;
	  int n = tab.size();
	  double coefdir;
	  double ordori;
	  for (int i = 0; i < n ;i++) {

	   if (i < (n-1)) {
		   coefdir = ((tab.get(i+1).y-tab.get(i).y)/(tab.get(i+1).x-tab.get(i).x));
		   ordori = tab.get(i).y-coefdir*tab.get(i).x;
		  // System.out.println("Coef directeur" + coefdir + " ordonn�e � l'origine : " +ordori);
	       y =  (coefdir*M.x) + ordori; 
	       //System.out.println(y);
	    
	    if ((M.x < max(tab.get(i+1).x,tab.get(i).x)) && (M.x > min(tab.get(i+1).x,tab.get(i).x)) && (y >= M.y)) {
	     compteur++;
	    }
	   }
	   else {
		   coefdir = ((tab.get(0).y-tab.get(i).y)/(tab.get(0).x-tab.get(i).x));
		   ordori = tab.get(i).y-coefdir*tab.get(i).x;
		  // System.out.println("Coef directeur" + coefdir + " ordonn�e � l'origine : " +ordori);
	       y =  (coefdir*M.x) + ordori; 
	     //  System.out.println(y);
	    if ((M.x < max(tab.get(0).x,tab.get(i).x)) && (M.x > min(tab.get(0).x,tab.get(i).x)) && (y >= M.y)) {
	     compteur++;
	    }
	   }

	  }
	  return ((compteur % 2) == 1);

	 }

	
	public static double min(double a, double b) {
		if (a<b) {
			return a;
		}
		else {
			return b;
		}
	}
	
	public static double max(double a, double b) {
		if (b<a) {
			return a;
		}
		else {
			return b;
		}
	}
	

/* ************************************************CLASSES INTERNES*************************************************************************/
	
	private class Point{
		
		public double y;
		public double x;

		Point(double x,double y){
			
			this.x = x;
			this.y = y;
			
		}
		
	}
}
