package com.androlis.fr.smartphone.modele.data;

public class Application implements DataModel{

	private String name;
	private int image;
	
	public static final int NO_PICTURE = -1;
	
	public Application(String name, int imageRessources){
		this.name = name;
		this.image = imageRessources;
	}
	
	public Application(String name){
		this.name = name;
		this.image = -1;
	}

	public String getName() {
		return name;
	}

	public boolean hasPicture(){
		return image != NO_PICTURE;
	}
	
	public int getImage() {
		return image;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setImage(int image) {
		this.image = image;
	}
	
}
