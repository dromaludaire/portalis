package com.androlis.fr.smartphone.modele.data.lis;

import java.util.ArrayList;
import java.util.Iterator;

import com.androlis.fr.smartphone.modele.data.memento.Memento;
import com.androlis.fr.smartphone.modele.data.memento.RequeteMemento;
import com.androlis.fr.smartphone.modele.data.observer.Observer;
import com.androlis.fr.smartphone.modele.data.observer.Subject;

import android.util.Log;

public class Requete implements Memento, Subject {

	private ArrayList<String> requete;
	private ArrayList<Observer> observers;

	public Requete() {
		requete = new ArrayList<String>();
		observers = new ArrayList<Observer>();
	}

	public Requete(String encodedRequete){
		requete = new ArrayList<String>();

		String[] axiomes = encodedRequete.split("%20");
		for(String ax : axiomes ){
			requete.add(ax);
		}

		observers = new ArrayList<Observer>();
	}

	public void andAxiome(String axiome){
		if( !requete.isEmpty() ){
			requete.add("and");
		}
		requete.add(axiome);
		notifyObservers();
	}

	public void orAxiome(String axiome){
		if( !requete.isEmpty() ){
			requete.add("or");
		}
		requete.add(axiome);
		notifyObservers();
	}

	public void andNotAxiome(String axiome){
		if( !requete.isEmpty() ){
			requete.add("and");
		}
		requete.add("not");
		requete.add(axiome);
		notifyObservers();
	}

	public void orNotAxiome(String axiome){
		if( !requete.isEmpty() ){
			requete.add("or");
		}
		requete.add("not");
		requete.add(axiome);
		notifyObservers();
	}

	public void remove(String axiome){
		int indexPrevious = -1;

		int index_delete0 = -1;
		int index_delete1 = -1;
		int index_delete2 = -1;

		int cpt = 0;
		for(String str : requete){
			if( str.equals(axiome) ){
				if( indexPrevious != -1 && requete.get(indexPrevious).equals("not") ){
					index_delete1 = indexPrevious;
					index_delete2 = indexPrevious-1;
				}else{
					index_delete2 = indexPrevious;
				}
				index_delete0 = cpt;
				break;
			}
			indexPrevious = cpt;
			cpt++;
		}
		requete.remove(index_delete0);
		if( index_delete1 != -1 ) requete.remove(index_delete1);
		if( index_delete2 != -1 ) requete.remove(index_delete2);

		if(  !requete.isEmpty() ){
			if( requete.get(0).equals("and") || requete.get(0).equals("or") ){
				requete.remove(0);
			}
		}
		notifyObservers();
	}

	public String getRequete(){
		String req = "";

		Iterator<String> it = requete.iterator();
		while( it.hasNext() ){
			String str = it.next();
			req += str;
			if( it.hasNext() ){
				req += "%20";
			}
		}
		return req.equals("")?"all":req;
	}

	public void clear(){
		this.requete.clear();
		notifyObservers();
	}

	public String[] getAxiomes(){
		String req = getRequete();
		req = req.replace("and%20", "");
		req = req.replace("or%20", "");
		req = req.replace("not%20", "");
		//req = req.replace("%20", "<split>");
		Log.d("",req);
		return req.split("%20");
	}

	public ArrayList<String> getSource(){
		return requete;
	}

	public Memento getState(){
		return this;
	}

	public void setState(Memento memento){
		RequeteMemento reqM = ( RequeteMemento ) memento;
		this.requete = new ArrayList<String>(reqM.getState());
		notifyObservers();
	}

	@Override
	public void register(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void unregister(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObservers() {
		for(Observer o : observers){
			o.notifyFromSuject(this);
		}
	}

}
