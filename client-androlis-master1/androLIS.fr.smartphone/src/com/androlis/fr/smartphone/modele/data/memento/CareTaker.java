package com.androlis.fr.smartphone.modele.data.memento;

import java.util.ArrayList;

import android.util.Log;

import com.androlis.fr.smartphone.modele.data.lis.AxiomeLoader;

public class CareTaker {

	private ArrayList<Paire> historique;
	private static CareTaker careTaker;

	private int index;

	private CareTaker(){
		historique = new ArrayList<Paire>();
		index = 0;
	}

	public static CareTaker getInstance(){
		if( careTaker == null ){
			careTaker = new CareTaker();
		}
		return careTaker;
	}

	public void addMemento(Paire paire){
		index++;
		for( int i = index ; i < historique.size() ; i++ ){
			historique.remove(historique.size()-1);
		}
		index = historique.size();
		historique.add(paire);
		//Log.d("add", ((Requete)paire.getMemento()).getRequete().replace("%20", " "));
		Log.d("size", historique.size()+" : " + index );
		
	}

	public void clear(){
		historique.clear();
	}
	
	public boolean previous(){
		if(index > 0 && !historique.isEmpty()){
			Paire p = historique.get(index-1);
			((AxiomeLoader) p.getCommande()).setMemento(p.getMemento());
			//Log.d("Restore_requete", ((Requete)p.getMemento()).getRequete().replace("%20", " "));
			Log.d("Restore_index", index+" => " + formate((RequeteMemento)p.getMemento()));
			p.getCommande().execute();
			index--;
			return true;
		}else{
			index = 0;
			return false;
		}
	}

	private String formate(RequeteMemento rq){
		String r = "";
		for(String s : rq.getState()){
			r += s+ " ";
		}
		return r;
	}
	
	public Memento getLastIndex(){
		if( historique.size() < index ){
			return historique.get(index).getMemento();
		}else if( !historique.isEmpty() ){
			return historique.get(historique.size() - 1 ).getMemento();
		}else{
			return null;
		}
	}
	
}
