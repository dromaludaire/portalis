package com.androlis.fr.smartphone.modele.data.lis;

import java.util.ArrayList;

import com.androlis.fr.smartphone.modele.data.DataModel;

public class Groupe implements DataModel{

	private String nom;
	private ArrayList<DataModel> axiomes;
	
	public Groupe(String nom){
		this.nom = nom;
		axiomes = new ArrayList<DataModel>();
	}
	
	public void addAxiome(DataModel axiome){
		this.axiomes.add(axiome);
	}
	
	public void removeAxiome(int index){
		this.axiomes.remove(index);
	}
	
	public void removeAxiome(DataModel axiome){
		this.axiomes.remove(axiome);
	}
	
	public void removeAxiomes(ArrayList<DataModel> axiomes){
		this.axiomes.removeAll(axiomes);
	}

	public DataModel getAxiome(int index){
		return axiomes.get(index);
	}
	
	@Override
	public String getName() {
		return nom;
	}

	public ArrayList<DataModel> getAxiomes() {
		return axiomes;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setAxiomes(ArrayList<DataModel> axiomes) {
		this.axiomes = axiomes;
	}
	
	public String toString(){
		return nom;
	}
	
}
