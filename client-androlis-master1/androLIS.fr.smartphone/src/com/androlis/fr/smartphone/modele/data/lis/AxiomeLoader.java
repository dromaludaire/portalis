package com.androlis.fr.smartphone.modele.data.lis;

import java.util.ArrayList;

import android.os.Message;

import com.androlis.fr.core.Core;
import com.androlis.fr.smartphone.controleur.communication.CommunicationServeur;
import com.androlis.fr.smartphone.modele.data.Client;

public class AxiomeLoader extends DataLoader{
	
	public AxiomeLoader(Requete requete){
		super(requete);
	}

	@Override
	public void run() {
		CommunicationServeur cs = Client.getInstance().getHandler();
		Core core = Client.getInstance().getCore();

		//Log.d("","Requete envoyée : " + requete.getRequete());
		String[] stt = core.recupAxiomes(requete.getRequete().replace("objet:", "").replace("properties:", ""));
		//Log.d("", "Tableau reçu : " + (stt == null));

		Message msg = new Message();
		msg.setTarget(cs);

		if( stt != null ){
			ArrayList<Groupe> groupes = new ArrayList<Groupe>();
			if( stt.length == 0 ){
				String[] axiomes = requete.getAxiomes();

				for(String s : axiomes){ 

					if( !s.startsWith("objet:") && !s.startsWith("properties:")){
						//Log.d("", "load objet : "+s);
						loadObjets(core, groupes, s);
					}else{
						if( !s.startsWith("properties:") ){
							//Log.d("", "load properties : "+s);
							loadProperties(core, groupes, s);
						}
					}
				}
				//Log.d("","axiomes : "+aff);
			}else{
				for(String s : stt){
					Groupe g = new Groupe(s);

					//Log.d("","groupe : "+s);
					String[] ax = core.recupAxiomes( s );
					if( ax == null){
						continue;
					}
					if( ax.length == 0 ){
						//loadObjets(core, groupes, s); => /!\ Cette ligne fait planter le Camelis
						groupes.add(g); // /!\ Solution temporaire pour afficher la ligne
					}else{
						for(String axio : ax){
							//Log.d("","axiome : "+axio);
							g.addAxiome(new Axiome(g, axio));
						}
						groupes.add(g);
					}
				}
			}
			msg.what = groupes.size();
			msg.obj = groupes;
		}
		else{
			msg.what = IS_OBJECT;
		}
		msg.sendToTarget();
	}

	private void loadObjets(Core core,ArrayList<Groupe> groupes, String axiome){

		//DEBUG : 
		Groupe g = new Groupe(axiome);
		groupes.add(g);
		int[] objs = core.recupObjets(axiome);
		if( objs != null ){
			for( int o : objs ){
				g.addAxiome(new Axiome(g, String.valueOf("objet:"+o) ));
			}
		}
	}

	private void loadProperties(Core core,ArrayList<Groupe> groupes, String objet){

		//DEBUG : 
		Groupe g = new Groupe(objet);
		objet = objet.replace("objet:", "");
		try{
			int obj = Integer.parseInt(objet);
			String[] props = core.getProperties(obj);
			for( String o : props ){
				g.addAxiome(new Axiome(g, String.valueOf("properties:"+o) ));
			}
			groupes.add(g);
		}catch(Exception e){e.printStackTrace();return;}		
	}
	
}
