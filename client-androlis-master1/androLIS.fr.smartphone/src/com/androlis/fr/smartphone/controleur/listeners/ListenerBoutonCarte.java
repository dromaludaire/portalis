package com.androlis.fr.smartphone.controleur.listeners;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;

import com.androlis.fr.smartphone.cartographie.map.point.Point;
import com.androlis.map.LocalisationMap;

public class ListenerBoutonCarte implements OnClickListener{

	private Context activityRecherche;
	private ArrayList<Parcelable> parcelableList;
	
	public ListenerBoutonCarte(Activity activity)
	{
		this.activityRecherche = activity;
		this.parcelableList = new ArrayList<Parcelable>();
	}

	public ListenerBoutonCarte(Context ctx){
		this.activityRecherche = ctx;
		this.parcelableList = new ArrayList<Parcelable>();
	}
	
	public void add(double longitude, double latitude){
		parcelableList.add(new Point(longitude, latitude));
	}
	
	public void add(Point point){
		parcelableList.add(point);
	}
	
	@Override
	public void onClick(View v) {
		Intent intent = new Intent(activityRecherche, LocalisationMap.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

//		parcelableList.add(new Point(48.100f * 1E6, -1.667 * 1E6));
//		parcelableList.add(new Point(48.000f * 1E6, -4.100 * 1E6));
//		parcelableList.add(new Point(48.383f * 1E6,-4.500 * 1E6));
//		parcelableList.add(new Point(49.183 * 1E6, -0.367 * 1E6));
//		parcelableList.add(new Point(49.1466 * 1E6, 0.2293 * 1E6));
//		parcelableList.add(new Point(48.900f * 1E6, 48.900f * 1E6));
//		parcelableList.add(new Point(48.1138420401155*1E6, -1.67004669713748*1E6));
		
		intent.putParcelableArrayListExtra("points", parcelableList);
	    activityRecherche.startActivity(intent);
	}
	
	
	
}
