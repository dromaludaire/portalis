package com.androlis.fr.smartphone.gui.expendableList;

import com.androlis.fr.smartphone.gui.Viewer;

public interface ViewExpendableList extends Viewer {

	public Viewer getAxiomeView(int index);
	public int getNbAxiomes();
	
}
