package com.androlis.fr.smartphone.gui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.androlis.fr.smartphone.gui.Viewer;
import com.androlis.fr.smartphone.modele.data.Application;
import com.androlis.fr.smartphone.modele.data.DataModel;
import com.fr.lis.androlis.smartphone.main.R;

public class ImageGalerie implements Viewer{

	private static final int LAYOUT = R.layout.image_gallerie;
	private Application application;
	
	public ImageGalerie(Application app){
		this.application = app;
	}
	
	@Override
	public View getView(LayoutInflater inflater, View convertView) {
		if( convertView == null ){
			convertView = inflater.inflate(LAYOUT, null);
		}
		ImageView imgView = new ImageView(convertView.getContext());
		imgView.setImageResource(application.getImage());
		imgView.setScaleType(ImageView.ScaleType.CENTER);
		return imgView;
	}

	@Override
	public int getLayout() {
		return 0;
	}

	@Override
	public DataModel getDataModel() {
		return application;
	}

	@Override
	public void onCreate(ListViewer list) {
		// TODO Auto-generated method stub
		
	}

}
