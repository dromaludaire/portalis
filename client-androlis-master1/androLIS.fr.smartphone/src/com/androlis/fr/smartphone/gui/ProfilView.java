package com.androlis.fr.smartphone.gui;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.modele.data.DataModel;
import com.androlis.fr.smartphone.modele.data.ElementProfil;
import com.fr.lis.androlis.smartphone.main.R;

public class ProfilView implements Viewer{

	private static final int LAYOUT = R.layout.liste_adaptater;

	private ElementProfil element;
	
	public ProfilView(ElementProfil element){
		this.element = element;
	}
	
	@Override
	public View getView(LayoutInflater inflater, View convertView) {
		
		TextView entete;
		TextView corps;
		
		if(convertView == null) {
			convertView = inflater.inflate(LAYOUT, null);
		}
		 
		entete = (TextView) convertView.findViewById(R.id.entete);
		entete.setText(element.getEntete());
		
		corps = (TextView) convertView.findViewById(R.id.corps);
		corps.setText(element.getContenu());
		 
		return convertView;
	}

	@Override
	public int getLayout() {
		return LAYOUT;
	}

	@Override
	public DataModel getDataModel() {
		return element;
	}

	@Override
	public void onCreate(ListViewer list) {
		// TODO Auto-generated method stub
		
	}

}
