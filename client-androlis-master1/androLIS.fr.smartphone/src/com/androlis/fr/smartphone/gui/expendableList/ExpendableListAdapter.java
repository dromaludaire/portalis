package com.androlis.fr.smartphone.gui.expendableList;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.androlis.fr.smartphone.gui.Viewer;

public class ExpendableListAdapter extends BaseExpandableListAdapter{

	private ArrayList<ViewExpendableList> groupes;
	private LayoutInflater inflate;

	public ExpendableListAdapter(Context context){
		this.groupes = new ArrayList<ViewExpendableList>();
		inflate = LayoutInflater.from(context);
	}
	
	public ExpendableListAdapter(Context context, ArrayList<ViewExpendableList> groupes){
		this.groupes = groupes;
		inflate = LayoutInflater.from(context);
	}
	
	public void addGroupe(ViewExpendableList groupe){
		groupes.add(groupe);
	}
	
	public void clear(){
		groupes.clear();
	}
	
	@Override
	public Object getChild(int indexGroupe, int indexChild) {
		ViewExpendableList groupe = groupes.get(indexGroupe);
		return groupe.getAxiomeView(indexChild);
	}

	@Override
	public long getChildId(int indexGroupe, int indexChild) {
		return indexChild;
	}

	@Override
	public View getChildView(int groupePosition, int childPosition, boolean isExpended
			, View convertView, ViewGroup parent) {
		
		//Log.d("com.androlis", "["+groupePosition+","+childPosition+"]");
		
		ViewExpendableList groupe = groupes.get(groupePosition);
		Viewer axiome = groupe.getAxiomeView(childPosition);
		View view = axiome.getView(inflate, convertView);
		return view;
	}

	@Override
	public int getChildrenCount(int indexGroupe) {
		ViewExpendableList gr = groupes.get(indexGroupe);
		return gr.getNbAxiomes();
	}

	@Override
	public Object getGroup(int index) {
		return groupes.get(index);
	}

	@Override
	public int getGroupCount() {
		return groupes.size();
	}

	@Override
	public long getGroupId(int index) {
		return index;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		Viewer groupe = groupes.get(groupPosition);
		View view = groupe.getView(inflate, convertView);
		return view;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
