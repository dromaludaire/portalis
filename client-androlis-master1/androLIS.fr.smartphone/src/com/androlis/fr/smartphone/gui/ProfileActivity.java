package com.androlis.fr.smartphone.gui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.modele.data.Client;
import com.androlis.fr.smartphone.modele.data.ElementProfil;
import com.fr.lis.androlis.smartphone.main.R;

public class ProfileActivity extends Activity {
	public static boolean estadmin=false;
	public static final int INVISIBLE =4;

	List<ElementProfil> profil = new ArrayList<ElementProfil>();
	private ListViewer profileAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profil_activity);
		
		profileAdapter = new ListViewer(getApplicationContext());
		ListView listProfil = (ListView)findViewById(R.id.listElemProfil);
		listProfil.setAdapter(profileAdapter);
		
		//Bundle bundle = getIntent().getExtras();
		//String pseudo = (String) bundle.get("pseudo");
		
		String pseudo = Client.getInstance().getLogin();
		
		remplirProfil(pseudo);
		//listProfil = (ListView)findViewById(R.id.listElemProfil);
		
		//	ProfilAdapter adapterProfil = new ProfilAdapter(ProfileActivity.this, profil); 
		//	listProfil.setAdapter(adapterProfil);
		
		ImageButton buttonok=(ImageButton)findViewById(R.id.imageout);
			
		buttonok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		
	}

	private void remplirProfil(String pseudo)
	{
		Viewer v = new ProfilView(new ElementProfil ("NOM",""));
		profileAdapter.add(v);
		
		v = new ProfilView(new ElementProfil ("EMAIL",""));
		profileAdapter.add(v);
		
		v = new ProfilView(new ElementProfil ("PSEUDO",Client.getInstance().getLogin()));
		profileAdapter.add(v);
		
		if (estadmin){
			v = new ProfilView(new ElementProfil ("STATUT"," ADMINISTRATEUR"));
			profileAdapter.add(v);
		}
		else{
			v = new ProfilView(new ElementProfil ("STATUT"," UTILISATEUR"));
			profileAdapter.add(v);
		}
		v = new ProfilView(new ElementProfil ("EMAIL",""));
		profileAdapter.add(v);

		v = new ProfilView(new ElementProfil ("PASSWORD","*********"));
		profileAdapter.add(v);
	}

}
