package com.lis.map;

import java.util.Iterator;
import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.lis.activity.R;
import com.lis.map.circle.Circle;
import com.lis.map.point.CustomItemizedOverlay;
import com.lis.map.point.PointGraphique;


public class LocalisationMap extends MapActivity{

	private MapController mapController;
	private MapView mapView;
	
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.map);
	        
	        mapView = (MapView) findViewById(R.id.mapView);
	        mapController = mapView.getController();
	        
	        double longitude, latitude;
	        
	        latitude = 48.100f * 1E6;
	        longitude = -1.667 * 1E6;
	        GeoPoint rennes = new GeoPoint((int)latitude,(int) longitude);
	        
	        latitude = 48.000f * 1E6;
	        longitude = -4.100 * 1E6;
	        GeoPoint quimper = new GeoPoint((int)latitude,(int) longitude);
	        
	        latitude = 48.383f * 1E6;
	        longitude = -4.500 * 1E6;
	        GeoPoint brest = new GeoPoint((int)latitude,(int) longitude);
	       
	        latitude = 49.183 * 1E6;
	        longitude =  -0.367 * 1E6;
	        GeoPoint caen = new GeoPoint((int)latitude,(int) longitude);
	       
	        latitude = 49.1466 * 1E6;
	        longitude = 0.2293 * 1E6;
	        GeoPoint lisieux = new GeoPoint((int)latitude,(int) longitude);
	       
	        latitude = 48.900f * 1E6;
	        longitude = -0.200 * 1E6;
	        GeoPoint falaise = new GeoPoint((int)latitude,(int) longitude);
	       
	        List<Overlay> mapOverlays = mapView.getOverlays();
	        
	        Drawable rennes_icone = this.getResources().getDrawable(R.drawable.punaise2);
	        CustomItemizedOverlay items = new CustomItemizedOverlay(rennes_icone, this);
	        
	        /*PointGraphique item_rennes = new PointGraphique(this , rennes, R.drawable.punaise2);
	        PointGraphique item_quimper = new PointGraphique(this , quimper, R.drawable.punaise2);
	        PointGraphique item_brest = new PointGraphique(this , brest, R.drawable.punaise2);
	        
	        PointGraphique item_caen = new PointGraphique(this , caen, R.drawable.punaise2);
	        PointGraphique item_lisieux = new PointGraphique(this, lisieux, R.drawable.punaise2);
	        PointGraphique item_falaise = new PointGraphique(this, falaise, R.drawable.punaise2);*/
	        
	        OverlayItem item_rennes = new OverlayItem(rennes,"Rennes", "Rennes");
	        OverlayItem item_quimper = new OverlayItem(quimper, "Quimper", "Quimper");
	        OverlayItem item_brest = new OverlayItem(brest, "Brest", "Brest");
	        
	        OverlayItem item_caen = new OverlayItem(caen, "Caen", "Caen");
	        OverlayItem item_lisieux = new OverlayItem(lisieux, "Lisieux", "Lisieux");
	        OverlayItem item_falaise = new OverlayItem(falaise, "Falaise", "Falaise");
	        
	        items.addOverlay(item_rennes);
	        items.addOverlay(item_quimper);
	        items.addOverlay(item_brest);
	        items.addOverlay(item_caen);
	        items.addOverlay(item_lisieux);
	        items.addOverlay(item_falaise);
	        
	        mapOverlays.add(items);
	        
	        /*List<Overlay> overlays = mapView.getOverlays();
	        overlays.add(item_rennes);
	        overlays.add(item_quimper);
	        overlays.add(item_brest);
	        overlays.add(item_caen);
	        overlays.add(item_lisieux);
	        overlays.add(item_falaise);*/
	        	        
	        //centrer sur les items :
	        mapController.zoomToSpan(items.getLatSpanE6(), items.getLonSpanE6());
	        mapController.setCenter( items.getCenter( ));
	        
	        Circle c = new Circle(getApplicationContext(), rennes.getLatitudeE6(), rennes.getLongitudeE6(), (float)32.3);
	        mapView.getOverlays().add(c);
	        
	        mapView.invalidate();
	 }

	@Override
	protected boolean isRouteDisplayed() {
		return true;
	}
	
	public GeoPoint getCenter(List<Overlay> mapOverlays) {
		if (mapOverlays.size() == 0) {
			return new GeoPoint(0, 0);
		}

		int minLatitude = Integer.MAX_VALUE;
		int maxLatitude = Integer.MIN_VALUE;
		int minLongitude = Integer.MAX_VALUE;
		int maxLongitude = Integer.MIN_VALUE;

		Iterator<Overlay> overlayItems = mapOverlays.iterator();
		
		while( overlayItems.hasNext() ){
			PointGraphique overlay = (PointGraphique) overlayItems.next();
			int lat = overlay.getPoint().getLatitudeE6();
			int lon = overlay.getPoint().getLongitudeE6();
			maxLatitude = Math.max(lat, maxLatitude);
			maxLongitude = Math.max(lon, maxLongitude);
			minLatitude = Math.min(lat, minLatitude);
			minLongitude = Math.min(lon, minLongitude);
		}
		
		return new GeoPoint((maxLatitude + minLatitude) / 2,
				(maxLongitude + minLongitude) / 2);
	}
	
}
