package com.lis.map.point;

import com.google.android.maps.GeoPoint;

public class MapPoint extends GeoPoint{
	
	private int icone;
	private String titre;
	private String texte;
	
	public MapPoint(int arg0, int arg1, int icone, String titre, String texte) {
		super(arg0, arg1);
		this.icone = icone;
		this.titre = titre;
		this.texte = texte;
	}

	public int getIcone() {
		return icone;
	}

	public String getTitre() {
		return titre;
	}

	public String getTexte() {
		return texte;
	}

	public void setIcone(int icone) {
		this.icone = icone;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

}
