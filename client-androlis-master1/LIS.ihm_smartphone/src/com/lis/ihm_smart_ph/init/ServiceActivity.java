package com.lis.ihm_smart_ph.init;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.lis.activity.R;
import com.lis.map.LocalisationMap;
import com.lis.modele.data.Service;
import com.lis.modele.listAdapter.ListAdapterService;

public class ServiceActivity extends Activity {

	private ListAdapterService lstServices;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.services);
        
        ListView listeServices = (ListView) findViewById(R.id.listeservices);
        lstServices = new ListAdapterService(this);
        lstServices.add(new Service("Planet", R.drawable.planet));
        lstServices.add(new Service("Application Rennes", R.drawable.rennes));
        lstServices.add(new Service("Application PACA", R.drawable.paca));
        listeServices.setAdapter(lstServices);
        lstServices.notifyDataSetChanged();
        listeServices.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Service service = (Service)lstServices.getItem(arg2);
				if( service.getNom().equals("Application Rennes")){
					Intent intent = new Intent(ServiceActivity.this, LocalisationMap.class);
					startActivityForResult(intent, 0);
				}
				Toast.makeText(ServiceActivity.this, "Service choisi : " + service.getNom(), Toast.LENGTH_LONG).show();
			}
		});	  
    }
}
