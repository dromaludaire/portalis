package com.androlis.fr.smarpthone.data.lis.tests;

import android.os.Message;
import android.test.AndroidTestCase;

import com.androlis.fr.smartphone.controleur.communication.CommunicationServeur;
import com.androlis.fr.smartphone.controleur.communication.UpdateView;
import com.androlis.fr.smartphone.modele.data.Client;
import com.androlis.fr.smartphone.modele.data.lis.AxiomeLoader;

public class CoreTest extends AndroidTestCase implements UpdateView {
	
	private String login;
	private String passWord;
	
	private static final int TEST_HANDLER = 0;
	private boolean isReveived;
	
	@Override
	protected void setUp() throws Exception {
		login = "mathieu.rolland2705@gmail.com";
		passWord = "mathieu";
		isReveived = false;
		super.setUp();
	}
	
	public void testSingleton() throws Exception {
		Client c1 = Client.getInstance();
		Client c2 = Client.getInstance();
		
		assertEquals(c1, c2);
	}
	
	public void testLogin() throws Exception {
		Client c = Client.getInstance();
		assertTrue(c.getCore().login(login, passWord));
	}
	
	public void testHandler() throws Exception{
		Client c = Client.getInstance();
		CommunicationServeur h = c.getHandler();
		h.setView(this);
		
		Thread th = new Thread(new Runnable() {
			
			@Override
			public void run() {
				Client c = Client.getInstance();
				CommunicationServeur h = c.getHandler();

				Message msg = new Message();
				msg.setTarget(h);
				msg.what = TEST_HANDLER;
				msg.obj = "HANDLER OKAY";
				msg.sendToTarget();
				
				try {
					Thread.sleep(5 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
					assertTrue("Echec de la mise en attente du Thread", false);
				}
				assertTrue(isReveived);
				isReveived = false;
			}
		});
		
		th.start();
	}

	@Override
	public void onUpdateReceive(Message msg) {
		isReveived = true;
	}
	
}
