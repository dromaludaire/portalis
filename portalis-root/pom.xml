<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>fr.irisa.lis.portalis</groupId>
	<artifactId>portalis-root</artifactId>
	<packaging>pom</packaging>
	<name>Portalis root</name>

	<version>0.1.3-SNAPSHOT</version>

	<description>Portail d'accès aux systèmes d'information logique Lis</description>
	<inceptionYear>2012</inceptionYear>
	<organization>
		<name>IRISA - équipe LIS</name>
		<url>http://www.irisa.fr/LIS/home_html-fr</url>
	</organization>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<versioned.portalis.application.name>${portalis.application.name}-${project.version}</versioned.portalis.application.name>
		<portalis.application.path>${maven.local.repository}/fr/irisa/lis/portalis/server/${portalis.application.name}/${project.version}</portalis.application.path>


		<portalis.sphinx.autodoc>autodoc</portalis.sphinx.autodoc>
		<portalis.remote.sphinx.target>${portalis.home}/portalis-site/site-sphinx/src/main/sphinx/${portalis.sphinx.autodoc}</portalis.remote.sphinx.target>
		<portalis.remote.sphinx.webapp.target>${portalis.home}/portalis-site/site-webapp/src/main/webapp</portalis.remote.sphinx.webapp.target>

		<make.exe>/usr/bin/make</make.exe>

		<launcher.webapp>portalis-launcher-webapp</launcher.webapp>
		<jdk.version>1.7</jdk.version>

		<portalis-directory-path>/srv</portalis-directory-path>
		<portalis-log-directory-path>${portalis-directory-path}/logs</portalis-log-directory-path>

		<admin.lisServerChecker.logFilePath>${portalis-log-directory-path}/lisServersChecker.log</admin.lisServerChecker.logFilePath>
		<admin.portalisServer.logFile.prefixe>portalisServer.</admin.portalisServer.logFile.prefixe>
		<admin.client.logFile.prefixe>portalisClient.</admin.client.logFile.prefixe>
		<admin.camelis.logFilePath>${portalis-log-directory-path}/camelisServers.log</admin.camelis.logFilePath>

		<portalis-core>${portalis.home}/portalis-client-core</portalis-core>
		<admin.portalis.application.name>${versioned.portalis.application.name}</admin.portalis.application.name>
		<admin.portalis.server.name>localhost</admin.portalis.server.name>
		<admin.portalis.server.port>8080</admin.portalis.server.port>

		<camelis.application.name>xml-lis-server</camelis.application.name>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>2.6</version>
				<inherited>true</inherited>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>3.0</version>
				<inherited>true</inherited>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-antrun-plugin</artifactId>
				<version>1.7</version>
				<inherited>true</inherited>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-install-plugin</artifactId>
				<version>2.4</version>
				<inherited>true</inherited>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.5</version>
				<inherited>true</inherited>
				<configuration>
					<source>${jdk.version}</source>
					<target>${jdk.version}</target>
					<inherited>true</inherited>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.12.4</version>
				<inherited>true</inherited>
				<configuration>
					<!-- To ignore test failure durin maven build -->
					<testFailureIgnore>false</testFailureIgnore>
				</configuration>
			</plugin>

			<!-- to generate sources test-jar on top of test-jar -->
			<!-- To generate sources jar on top of jar files -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.2.1</version>
				<inherited>true</inherited>
				<executions>
					<execution>
						<goals>
							<!-- To produce sources's jar together with jar -->
							<goal>jar</goal>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- to be able to access the version number from java, with the properties 
				of jar files -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.4</version>
				<inherited>true</inherited>
				<configuration>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							<addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
						</manifest>
					</archive>
				</configuration>
			</plugin>
			<!-- generating properties' graphs -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.8</version>
				<inherited>true</inherited>
			</plugin>
			<!-- to copy resources (such as properties files) -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.6</version>
				<inherited>true</inherited>
			</plugin>
			<!-- delete log files -->
			<plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<filesets>
						<fileset>
							<directory>${portalis.logs.dir.path}</directory>
							<includes>
								<include>**/*</include>
							</includes>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<distributionManagement>
		<repository>
			<id>portalis-public-release</id>
			<url>http://maven.inria.fr/artifactory/portalis-public-release</url>
			<uniqueVersion>true</uniqueVersion>
		</repository>
		<snapshotRepository>
			<id>portalis-public-snapshot</id>
			<url>http://maven.inria.fr/artifactory/portalis-public-snapshot</url>
			<uniqueVersion>true</uniqueVersion>
		</snapshotRepository>
	</distributionManagement>

	<modules>
		<module>../portalis-admin-db</module>
		<module>../portalis-server/server-root</module>
		<module>../camelis-server</module>
		<module>../portalis-client-core</module>
		<module>../portalis-system</module>
	</modules>

	<profiles>
		<!-- profile to generate graph of dependencies -->
		<profile>
			<id>graph</id>
			<pluginRepositories>
				<pluginRepository>
					<id>mvnplugins.fusesource.org</id>
					<url>http://mvnplugins.fusesource.org/repo/release</url>
					<releases>
						<enabled>true</enabled>
					</releases>
				</pluginRepository>
			</pluginRepositories>
			<build>
				<plugins>
					<plugin>
						<groupId>org.fusesource.mvnplugins</groupId>
						<artifactId>maven-graph-plugin</artifactId>
						<version>1.0</version>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>