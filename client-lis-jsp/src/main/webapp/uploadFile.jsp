<%@ page import="java.io.File"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.apache.commons.fileupload.FileItem"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page
	import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ServiceCore"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Constants"%>
<%@ page import="fr.irisa.lis.portalis.shared.http.admin.AdminHttp"%>

<%@ page import="java.util.logging.Logger"%>
<jsp:include page="header.html" />

<%!private static final String jspName = "upLoadFile.jsp";
static final private LoggerProvider log = LoggerProvider.getDefault("fr.irisa.lis.portalis.admin." +jspName);%>
<%
	LOGGER.fine("\n================================================= "
			+ jspName
			+ " =================================================");

	File file;
	int maxFileSize = 5000 * 1024;
	int maxMemSize = 5000 * 1024;
	ServletContext context = pageContext.getServletContext();
	String filePath = null;

	String serviceName = null;

	out.println("<html>");
	out.println("<head>");
	out.println("<title>JSP File upload</title>");
	out.println("</head>");
	out.println("<body>");
	// Verify the content type
	String contentType = request.getContentType();
	LOGGER.fine("contentType = " + contentType);
	String testPortalisPath = request.getServletPath()+"/testPortalis.jsp";

	if (contentType.indexOf("multipart/form-data") >= 0) {

		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File("/tmp"));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);
		try {
			// Parse the request to get file items.
			@SuppressWarnings("unchecked")
			List<FileItem> fileItems = (List<FileItem>) upload
					.parseRequest(request);
			LOGGER.fine("nb items = " + fileItems.size());


			// processing multiparts
			for (FileItem item : fileItems) {
				// Get the uploaded file parameters
				String fieldName = item.getFieldName();
				// 					boolean isInMemory = item.isInMemory();
				// 					long sizeInBytes = item.getSize();
				LOGGER.fine("processing item " + fieldName
						+ " isFormField = " + item.isFormField());

				if (item.isFormField()) {
					// processFormField
					if (fieldName.equals(XmlIdentifier.SERVICE_NAME())) {
						serviceName = item.getString();
						String appliId = ServiceCore
								.extractAppliName(serviceName);
						String serviceId = ServiceCore
								.extractServiceName(serviceName);
						filePath = AdminHttp.WEBAPP_PATH + "/"
								+ appliId + "/" + serviceId + "/";
						LOGGER.fine("filePath = " + filePath);
					}
					break;
				}
			}
			if (serviceName == null) {
				out.println("<p>No service name is given !</p>");
			} else {
				for (FileItem item : fileItems) {
					if (!item.isFormField()) {
						// processUploadedFile
						// Write the file
						String fileName = item.getName();
						if (fileName.lastIndexOf("/") >= 0) {
							file = new File(filePath
									+ fileName.substring(fileName
											.lastIndexOf("/")));
						} else {
							file = new File(filePath
									+ fileName.substring(fileName
											.lastIndexOf("/") + 1));
						}
						item.write(file);
						out.println("<h3>Uploaded Filename: " + filePath
								+ fileName + "</h3>");
					}

				}
			}

		} catch (Exception ex) {
			System.out.println(ex);
		}
	} else {
		out.println("<h2>Error : No multipart HTTP request received</h2>");
	}
	 %>
	 <form action="<%= testPortalisPath %>">
		<input type="submit" value="retour � Test Portalis Admin" />
	 </form>

	 <%
	out.println("</body>");
	out.println("</html>");
%>