open Utils

let mimetype_of_extension = fun ext ->
  match (String.lowercase ext) with
  (* images *)
  | "jpg" -> "image/jpeg"
  | "gif" -> "image/gif"
  | "png" -> "image/png"
  | "svg" -> "image/svg+xml"
  (* text *)
  | "html" | "htm" -> "text/html"
  | "txt" -> "text/plain"
  | "xml" -> "text/xml"
  (* music *)
  | "mp3" -> "audio/mpeg"
  | "ogg" -> "audio/ogg"
  | "wav" -> "audio/vnd.wave"
  (* video *)
  | "avi" -> "video/avi"
  | "flv" -> "video/x-flv"
  | "mkv" -> "video/x-matroska"
  | "mp4" -> "video/mp4"
  | "mpg" | "mpeg" -> "video/mpeg"
  | "webm" -> "video/webm"
  (* office *)
  | "odt" -> "application/vnd.oasis.opendocument.text"
  | "ods" -> "application/vnd.oasis.opendocument.spreadsheet"
  | "odp" -> "application/vnd.oasis.opendocument.presentation"
  | "odg" -> "application/vnd.oasis.opendocument.graphics"
  | "doc" -> "application/vnd.ms-word"
  | "xls" -> "application/vnd.ms-excel"
  | "ppt" -> "application/vnd.ms-powerpoint"
  | "docx" -> "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  | "xlsx" -> "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  | "pptx" -> "application/vnd.openxmlformats-officedocument.presentationml.presentation"
  (* other *)
  | "pdf" -> "application/pdf"
  | "zip" -> "application/zip"
  | "gz" -> "application/gzip"
  | "js" -> "application/javascript"
  (* default *)
  | _ -> "application/octet-stream"


let extension_of_filename filename =
  try
    let pos = (String.rindex filename '.') + 1 in
    let len = (String.length filename) - pos in
    String.sub filename pos len
  with not_found -> ""

let mimetype_of_filename f = mimetype_of_extension (extension_of_filename f)
