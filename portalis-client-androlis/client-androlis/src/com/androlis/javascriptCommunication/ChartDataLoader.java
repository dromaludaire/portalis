package com.androlis.javascriptCommunication;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.androlis.R;
import com.androlis.component.Request;
import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.AttributesGetZoom;

import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

/**
 * Create data readable from Javascript
 */
public class ChartDataLoader{
	public String lastAttributeFocused = "/";
	public Context ctx=null;
	private Request request = null;

	public ChartDataLoader(Context ctx, Request request) {
		this.ctx=ctx;
		this.request=request;
	}

	@JavascriptInterface
	public JSONArray getArray() {
		JSONArray table = new JSONArray();

		LisIncrement[] increments = Memory.getIncrements();

		for(LisIncrement increment : increments) {
			JSONArray wordcloudRow = new JSONArray();
			wordcloudRow.put(" "+increment.getName()+" ");
			wordcloudRow.put(increment.getCard());

			table.put(wordcloudRow);
		}
		System.out.println(table);
		return table;
	}

	@JavascriptInterface
	public boolean doRequest() {
		if(!Memory.getAttributesMemorizator().getState().equals(lastAttributeFocused)) {
			if(updateIncrements(lastAttributeFocused)) {
				Memory.getAttributesMemorizator().add(lastAttributeFocused);
				return true;
			}
		}
		return false;
	}


	@JavascriptInterface
	public String getTitle() {
		ArrayList<String> attributes = Memory.getAttributesMemorizator().getAll();
		String path = "/";

		for(String attribute : attributes)
			path=path+" > "+attribute;

		return path;
	}

	@JavascriptInterface
	public void setFocus(String attribute) {
		lastAttributeFocused=attribute;

		String[] param = {lastAttributeFocused};
		Memory.setParam(param);
	}

	@JavascriptInterface
	public String getSelectionText1() {
		return ctx.getResources().getString(R.string.Javascript_attributes_selection_1);
	}

	@JavascriptInterface
	public String getSelectionText2() {
		return ctx.getResources().getString(R.string.Javascript_attributes_selection_2);
	}

	@JavascriptInterface
	public String getButtonText() {
		return ctx.getResources().getString(android.R.string.ok);
	}

	@JavascriptInterface
	public String getAttribute() {
		return Memory.getAttributesMemorizator().getState();
	}

	/**
	 * Update the adapter through the request memorizator and the last attribute in the attribute memorizator
	 * if the last attribute has at least one child
	 */
	public boolean updateIncrements(String attribute) {
		AttributesGetZoom attributesGetZoom = new AttributesGetZoom();
		ZoomReponse zoomReponse=null;
		String[] params = {request.getCurrentRequest(),attribute};

		try {
			zoomReponse = attributesGetZoom.execute(params).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LisIncrementSet receivedIncrs = zoomReponse.getIncrements();
		LisIncrement[] increments = receivedIncrs.toArray();

		if(increments.length!=0){
			Memory.setIncrements(increments);
			return true;
		}

		return false;
	}
}