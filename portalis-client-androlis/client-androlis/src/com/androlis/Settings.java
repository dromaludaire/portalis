package com.androlis;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;

/**
 * Settings activity
 * 
 * We want to show a preference's value as its summary, but the Android guys
 * didn't implement it. So we use the recipe posted on this StackOverflow
 * question:
 * 
 * http://stackoverflow.com/questions/531427/how-do-i-display-the-current-value-
 * of-an-android-preference-in-the-preference-su
 * 
 * @author Benjamin Sigonneau
 */

public class Settings extends PreferenceActivity implements
		OnSharedPreferenceChangeListener {

	private EditTextPreference portalisServerNamePref;
	private EditTextPreference portalisServerPortPref;
	private EditTextPreference objsPerPagePref;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		portalisServerNamePref = (EditTextPreference) getPreferenceScreen()
				.findPreference("pref_server_name");
		portalisServerPortPref = (EditTextPreference) getPreferenceScreen()
				.findPreference("pref_server_port");
		objsPerPagePref = (EditTextPreference) getPreferenceScreen()
				.findPreference("pref_objs_per_page");

	}

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		// Setup the initial values
		portalisServerNamePref.setSummary(sharedPrefs.getString(
				"pref_server_name", ""));
		portalisServerPortPref.setSummary(sharedPrefs.getString(
				"pref_server_port", ""));
		objsPerPagePref.setSummary(sharedPrefs.getString("pref_objs_per_page",
				""));
		// Set up a listener whenever a key changes
		sharedPrefs.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes
		PreferenceManager.getDefaultSharedPreferences(this)
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		// Let's do something a preference value changes
		if (key.equals("pref_server_name")) {
			portalisServerNamePref.setSummary(sharedPrefs.getString(key, ""));
			int serverPort = Integer.parseInt(sharedPrefs.getString("pref_server_port", ""));		
			PortalisService.getInstance().init(key, serverPort, "portalis");			
		} else if (key.equals("pref_server_port")) {
			portalisServerPortPref.setSummary(sharedPrefs.getString(key, ""));
			String serverName = sharedPrefs.getString("pref_server_name", "");	
			int serverPort = Integer.parseInt(key);
			PortalisService.getInstance().init(serverName, serverPort, "portalis");			
		} else if (key.equals("pref_objs_per_page")) {
			objsPerPagePref.setSummary(sharedPrefs.getString(key, ""));
			int n = Integer.parseInt(key);
			Memory.setExtentPageSize(n);
		}
	}

}
