package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;

public class LoginPortalis extends AsyncTask<String, Void, LoginReponse> {
	private static final Logger logger = LoggerFactory.getLogger(LoginPortalis.class.getName());

	@Override
	protected LoginReponse doInBackground(String... params) {
		logger.info("Start LoginPortalis Task");
		LoginReponse loginReponse = AdminHttp.login(params[0], params[1]);
		return loginReponse;
	}
	
	@Override
	protected void onPostExecute(LoginReponse result) {
		super.onPostExecute(result);
	}
}
