package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;

public class ServicesGetCamelisSessions extends AsyncTask<ActiveUser, Void, PingReponse> {
	private static final Logger logger = LoggerFactory.getLogger(ServicesGetCamelisSessions.class.getName());

	protected PingReponse doInBackground(ActiveUser... params) {
		logger.info("Start ServicesGetCamelisSessions Task");
		
		PingReponse pingReponse = AdminHttp.getActiveLisServicesOnPortalis(params[0]);
		return pingReponse;
	}
	
	@Override
	protected void onPostExecute(PingReponse result) {
		super.onPostExecute(result);
	}
}
