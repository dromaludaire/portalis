//package com.androlis.networkCommunication;
//
//import android.os.AsyncTask;
//
//import com.androlis.memorizator.Memory;
//
//import fr.irisa.lis.portalis.log.LoggerProvider;
//import fr.irisa.lis.portalis.shared.admin.data.CamelisService;
//import fr.irisa.lis.portalis.shared.admin.data.Session;
//
//public class ServicesStartCamelisSession extends AsyncTask<CamelisService, Void, Void> {
//	private final LoggerProvider logger = LoggerProvider.getDefault(ServicesStartCamelisSession.class);
//
//
//	@Override
//	protected Void doInBackground(CamelisService... params) {
//		logger.info("Start ServicesStartCamelisSession Task");
//		//TODO : afin de pouvoir se connecter à un service déjà lancé et faire des requêtes dessus, on doit pour l'instant se mettre en créateur
//		//de la session camelis
//		Session.createCamelisSession(Memory.getPortalisSession().getActiveUser(), params[0]);
//		return null;
//	}
//}
