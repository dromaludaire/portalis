package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;

import com.androlis.Timeline;
import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;

public class LogoutPortalis extends AsyncTask<Void, Void, Void> {
	private static final Logger logger = LoggerFactory.getLogger(Timeline.class.getName());


	@Override
	protected Void doInBackground(Void... params) {
		logger.info("Start LogoutPortalis Task");
		AdminHttp.logout(Memory.getActiveUser());
		return null;
	}
}
