package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

public class AttributesGetZoom extends AsyncTask<String, Void, ZoomReponse> {
	private static final Logger logger = LoggerFactory.getLogger(AttributesGetZoom.class.getName());

	@Override
	protected ZoomReponse doInBackground(String... params) {
		logger.info("Start AttributesGetZoom Task");
		ZoomReponse zoomReponse = null;
		
		if (params.length == 2) {
			String request = params[0];
			if (request == null || request.length() == 0)
				request = "all";

			CamelisHttp camelisHttp = Memory.getCamelisHttp();
			zoomReponse = camelisHttp.zoom(request, params[1]);
		}
		return zoomReponse;
	}

	@Override
	protected void onPostExecute(ZoomReponse zoomReponse) {
		super.onPostExecute(zoomReponse);
	}
}
