package com.androlis.memorizator;

import java.util.HashMap;
import java.util.List;

import com.androlis.list.Doublet;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

public class Memory {
	private static int currentPage = 0;
	// extentPageSize is intialized on app create, ie. Login.onCreate
	// it is updated when the user changes the corresponding preference
	private static int extentPageSize;		
	
	private static TextViewState requestMemorizator =  new TextViewState();
	private static TextViewState attributesMemorizator = null;
	
	private static ActiveUser activeUser = null;
	private static ActiveLisService activeLisService = null;
	// camelisHttp is updated when activeUser or activeLisService is modified
	private static CamelisHttp camelisHttp = null;
	
	
	//Contain the array of the previous objects load on the Objects activity
	private static LisObject[] arrayObjects = null;
	
	//Contain camelis services
	//(key : shared camelis services name, value : list of camelis services)
	public static HashMap<String, List<ActiveLisService>> activeLisServices = new HashMap<String, List<ActiveLisService>>();
	
	//Contain attributes data
	public static LisIncrement[] increments = null;
	
	//Contain the focused Object for details
	public static LisObject focusedObject = null;
	
	//Contain the current camelis name
	public static String camelisName=null;
	
	//Contain the kmls names
	public static Doublet[] kmlsNames = {new Doublet("massifs arbustifs",true),new Doublet("pelouses",true),new Doublet("surfaces minerales",true),new Doublet("massifs floraux",true),new Doublet("rosiers",true)};
	
	public static String[] param = {};
	
	public static int[]waypointOrder = {};
	
	private Memory() {}
	
	public static TextViewState getRequestMemorizator() {
		return requestMemorizator;
	}
	
	public static TextViewState getAttributesMemorizator() {
		return attributesMemorizator;
	}

	public static ActiveUser getActiveUser() {
		return activeUser;
	}

	public static void setActiveUser(ActiveUser activeUser) {
		Memory.activeUser = activeUser;
		try {
			camelisHttp = new CamelisHttp(activeUser, activeLisService);
		} catch (PortalisException e) {
			// this happens when activeUser or srv is null
			// best thing to do seem to fail silently, since it should only happen
			// at application init, when only one of the two is set.
			camelisHttp = null;
		}
	}

	public static ActiveLisService getActiveLisService() {
		return activeLisService;
	}

	public static void setActiveLisService(ActiveLisService srv) {
		Memory.activeLisService = srv;
		try {
			camelisHttp = new CamelisHttp(activeUser, srv);
		} catch (PortalisException e) {
			// this happens when activeUser or srv is null
			// best thing to do seem to fail silently, since it should only happen
			// at application init, when only one of the two is set.
			camelisHttp = null;
		}
	}

	public static CamelisHttp getCamelisHttp() {
		return camelisHttp;
	}
	
	public static HashMap<String, List<ActiveLisService>> getActiveLisServices() {
		return activeLisServices;
	}

	public static void setActiveLisServices(HashMap<String, List<ActiveLisService>> camelisServices) {
		Memory.activeLisServices = camelisServices;
	}

	public static LisObject[] getArrayObjects() {
		return arrayObjects;
	}

	public static void setArrayObjects(LisObject[] arrayObjects) {
		Memory.arrayObjects = arrayObjects;
	}

	public static LisIncrement[] getIncrements() {
		return increments;
	}

	public static void setIncrements(LisIncrement[] increments) {
		Memory.increments = increments;
	}

	public static LisObject getFocusedObject() {
		return focusedObject;
	}

	public static void setFocusedObject(LisObject lisObject) {
		Memory.focusedObject = lisObject;
	}
	
	public static String getCamelisName() {
		return camelisName;
	}
	
	public static void setCamelisName(String camelisName) {
		Memory.camelisName = camelisName;
	}
	
	public static Doublet[] getKmlsNames() {
		return kmlsNames;
	}
	
	public static void setKmlsNames(Doublet[] kmlsNames) {
		Memory.kmlsNames = kmlsNames;
	}
	
	public static void initKmls() {
		Doublet[] kmlsArray = {new Doublet("massifs arbustifs",true),new Doublet("pelouses",true),new Doublet("surfaces minerales",true),new Doublet("massifs floraux",true),new Doublet("rosiers",true)};
		Memory.kmlsNames = kmlsArray;
	}
	
	public static void setParam(String[] param) {
		Memory.param = param;
	}
	
	public static String[] getParam() {
		return param;
	}
	
	public static void serviceInitFactory(){
		requestMemorizator = new TextViewState();
		attributesMemorizator = new TextViewState();
		arrayObjects=null;
		increments = null;
		focusedObject = null;
		camelisName=null;
		//TODO : initialiser kmlsNames
		
	}
	
	public static int getExtentPageSize() {
		return extentPageSize;
	}
	
	public static void setExtentPageSize(int n) {
		Memory.extentPageSize = n;
	}
	
	public static int getCurrentPage() {
		return currentPage;
	}
	
	public static void setCurrentPage(int currentPage) {
		Memory.currentPage = currentPage;
	}

	public static void setwaypointOrder(int[] waypointOrder) {
		Memory.waypointOrder=waypointOrder;
	}
	
	public static int[] getWaypointOrder() {
		return waypointOrder;
	}
}
