package com.androlis.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androlis.R;
import com.androlis.memorizator.ButtonState;
import com.androlis.memorizator.Memory;

public class Request extends RelativeLayout  {
	private enum State {SHOW, HIDE};
	private State state = State.HIDE;
	private Button and = null;
	private Button or = null;
	private Button not = null;
	private final ButtonState requestAnd = new ButtonState();
	private final ButtonState requestOr = new ButtonState();
	private final ButtonState requestNot = new ButtonState();
	private TextView request = null;
	
	public String currentRequest = "";

	public Request(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.comp_request, this);
		
		and = (Button) findViewById(R.id.request_and);
		or = (Button) findViewById(R.id.request_or);
		not = (Button) findViewById(R.id.request_not);
		
		request = (TextView) findViewById(R.id.request_text);
		request.setText(Memory.getRequestMemorizator().getState());
		currentRequest=Memory.getRequestMemorizator().getState();
		
		findViewById(R.id.request_header).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState();
			}
		});
		
		findViewById(R.id.request_expand).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState();
			}
		});
		
		findViewById(R.id.request_reduce).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState();
			}
		});
		
		//navigation buttons listeners
		findViewById(R.id.request_double_left).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeReload();
				beginning();
			}
		});
		
		findViewById(R.id.request_left).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeReload();
				previous();
			}
		});
		
		findViewById(R.id.request_right).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeReload();
				next();
			}
		});
		
		findViewById(R.id.request_double_right).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeReload();
				ending();
			}
		});
		
		//buttons and, or and not listeners
		and.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				andButton();
			}
		});
		
		or.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				orButton();
			}
		});
		
		not.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				notButton();
			}
		});
	}

	private void changeReload() {
		((ImageButton) findViewById(R.id.request_reload)).setImageResource(R.drawable.reload_green);
	}
	
	public void changeState() {
		switch(state){
		case  SHOW:
			findViewById(R.id.request_reduce).setVisibility(View.GONE);
			findViewById(R.id.request_content).setVisibility(View.GONE);
			findViewById(R.id.request_expand).setVisibility(View.VISIBLE);
			state=State.HIDE;
			break;

		case HIDE:
			findViewById(R.id.request_reduce).setVisibility(View.VISIBLE);
			findViewById(R.id.request_content).setVisibility(View.VISIBLE);
			findViewById(R.id.request_expand).setVisibility(View.GONE);
			state=State.SHOW;
			break;
		}
	}

	public void hideButtons(){
		findViewById(R.id.request_line_connectors).setVisibility(View.GONE);
		and.setVisibility(View.GONE);
		or.setVisibility(View.GONE);
		not.setVisibility(View.GONE);
	}

	private void beginning() {
		while(Memory.getRequestMemorizator().hasPrevious())
			Memory.getRequestMemorizator().undo();

		request.setText(Memory.getRequestMemorizator().getState());
	}
	private void previous() {
		Memory.getRequestMemorizator().undo();
		request.setText(Memory.getRequestMemorizator().getState());
	}

	private void next() {
		Memory.getRequestMemorizator().redo();
		request.setText(Memory.getRequestMemorizator().getState());
	}

	private void ending() {
		while(Memory.getRequestMemorizator().hasNext())
			Memory.getRequestMemorizator().redo();

		request.setText(Memory.getRequestMemorizator().getState());
	}

	private void andButton() {
		and.setSelected(requestAnd.getState());
		requestAnd.changeState();
		changeColor(requestAnd, and);

		if(requestOr.getState()){
			requestOr.changeState();
			changeColor(requestOr, or);
		}
	}

	private void orButton() {
		or.setSelected(requestOr.getState());
		requestOr.changeState();
		changeColor(requestOr, or);

		if(requestAnd.getState()){
			requestAnd.changeState();
			changeColor(requestAnd, and);
		}
	}

	private void notButton() {
		not.setSelected(requestNot.getState());
		requestNot.changeState();
		changeColor(requestNot, not);
	}


	private void changeColor(ButtonState buttonState, Button button) {
		if (buttonState.getState()){
			button.setBackgroundResource(R.drawable.button_selected);
		} else {
			button.setBackgroundResource(R.drawable.button_normal);
		}
	}
	
	public void reload() {
		request.setText(Memory.getRequestMemorizator().getState());
		updateCurrentRequest();
		((ImageButton) findViewById(R.id.request_reload)).setImageResource(R.drawable.reload);
		initConnectors();
	}

	public void changeRequest(String[] properties) {
		if(properties.length>0) {
			//get the current request
			String state = currentRequest;
			boolean isStateEmpty = (state.length()==0);

			if(!isStateEmpty) {
				//add the connector with the previous request
				if(requestOr.getState()) 
					state="("+state+") or ";
				else 
					state+=" and ";
			}

			if(requestNot.getState())
				state+="not ";


			//open a parenthesis if it is necessary
			if(requestOr.getState() & isStateEmpty || requestNot.getState())
				state+="(";

			//add all the properties to the request with an and connector 
			state=state+properties[0];
			for(int i=1; i<properties.length; i++)
				state+=" and "+properties[i];

			//close a parenthesis if it is necessary
			if(requestOr.getState() & isStateEmpty || requestNot.getState())
				state+=")";

			initConnectors();
			
			//add the new request to the memory
			Memory.getRequestMemorizator().add(state);
			
			//change the textView
			request.setText(state);
		}
	}
	
	/**
	 * initialize connectors states
	 */
	private void initConnectors() {
		if(requestOr.getState()) {
			requestOr.changeState();
			changeColor(requestOr, or);
		}
		if(requestAnd.getState()){
			requestAnd.changeState();
			changeColor(requestAnd, and);
		}

		if(requestNot.getState()) {
			requestNot.changeState();
			changeColor(requestNot, not);
		}
	}
	
	public void updateCurrentRequest() {
		this.currentRequest = Memory.getRequestMemorizator().getState();
	}

	public String getCurrentRequest() {
		return currentRequest;
	}
}
