package com.androlis;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.ObjectsIntent;

import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.data.Property;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;

public class ObjectDetail extends Activity {
	private static final Logger logger = LoggerFactory.getLogger(ObjectDetail.class);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("ObjectDetail activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_object_detail);
		
		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());

		//Retrieve the focused object
		LisObject lisObject = Memory.getFocusedObject();

		if(lisObject==null)
			System.err.println("Error on object focused");
		else
			System.out.println(lisObject.getName());



		//Get IntentReponse
		ObjectsIntent objectsIntent = new ObjectsIntent();
		IntentReponse intentReponse=null;
		try {
			intentReponse = objectsIntent.execute(String.valueOf(lisObject.getOid())).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		LisIntent receivedIntent = intentReponse.getIntent();
		Property[] arrayIntent = receivedIntent.toArray();

		//Name
		TextView name = (TextView) findViewById(R.id.object_detail_name);
		name.setText(lisObject.getName());

		//		ImageView photo = (ImageView) findViewById(R.id.object_detail_photo);
		//		photo.setImageResource(R.drawable.earth);


		//		TextView date = (TextView) findViewById(R.id.object_detail_date);
		//		date.setText(R.string.object_detail_mock_date);

		//GPS
		int gpsPos=0;
		while(gpsPos<arrayIntent.length && !arrayIntent[gpsPos].getName().startsWith("gps is"))
			gpsPos++;

		if(gpsPos<arrayIntent.length) { 
			TextView location = (TextView) findViewById(R.id.object_detail_location);
			location.setVisibility(View.VISIBLE);
			location.setText(arrayIntent[gpsPos].getName());
		}

		//Properties
		StringBuilder stringBuilder = new StringBuilder();
		for(int i=0; i<arrayIntent.length; i++)
			if(i!=gpsPos)
				stringBuilder.append(arrayIntent[i].getName()+"\n");

		if(stringBuilder.length()>0){
			TextView properties = (TextView) findViewById(R.id.object_detail_properties);
			properties.setVisibility(View.VISIBLE);
			properties.setText(stringBuilder.toString());
		}



		//		TextView description = (TextView) findViewById(R.id.object_detail_description);
		//		description.setText(R.string.object_detail_mock_description);


	}
}
