package com.androlis.list;

public class List_Item {
	private String item;
	private int itemType;
	private int id;
	private int color;
	
	
	public List_Item(String item, int itemType, int id, int color) {
		this.item = item;
		this.itemType = itemType;
		this.id = id;
		this.color = color;
	}

	public List_Item(String item, int itemType, int id) {
		this.item = item;
		this.itemType = itemType;
		this.id = id;
		this.color = 0;
	}

	public String getItem() {
		return item;
	}

	public int getItemType() {
		return itemType;
	}
	
	public int getId() {
		return id;
	}
	
	public int getColor() {
		return color;
	}
}
