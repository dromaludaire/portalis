package com.androlis.list;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.androlis.R;

public class List_MyCustomAdapter extends BaseAdapter implements Filterable {
	private static final int TYPE_ITEM = 0;
	private static final int TYPE_SEPARATOR = 1;
	private static final int TYPE_CHECKABLE_ITEM = 2;
	private static final int TYPE_MAX_COUNT = 3;

	private Filter customFilter;

	//Contain separator position to acces their view in the ListView create from this current adapter
	private List<Integer> _separatorPosition = new ArrayList<Integer>();
	//Contain all items of the list
	private List<List_Item> _originalData = new ArrayList<List_Item>();
	//Contain only visible items
	private List<List_Item> _filteredData = new ArrayList<List_Item>();
	//Indicate if the checkboxs of all data are checked
	private List<Boolean>  _originalCheckBox = new ArrayList<Boolean>();
	//Indicate if a visible checkbox is checked
	private List<Boolean>  _filteredCheckBox = new ArrayList<Boolean>();

	private LayoutInflater _inflater;
	private Context _ctx;
	private CheckBox checkbox = null;

	private int nbItems=0;

	public List_MyCustomAdapter(Context ctx) {
		_inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		_ctx=ctx;
	}

	
	public void addItem(String item, int color) {
		List_Item itemList = new List_Item(item, TYPE_ITEM, nbItems, color);
		addItemAux(itemList);
	}
	
	public void addItem(String item) {
		List_Item itemList = new List_Item(item, TYPE_ITEM, nbItems);
		addItemAux(itemList);
	}

	private void addItemAux(List_Item itemList) {
		nbItems++;

		_originalCheckBox.add(false);
		_filteredCheckBox.add(false);

		_originalData.add(itemList);
		_filteredData.add(itemList);

		notifyDataSetChanged();
	}
	
	public void addSeparatorItem(String item) {
		_separatorPosition.add(_originalData.size());

		List_Item itemList = new List_Item(item, TYPE_SEPARATOR,nbItems);
		nbItems++;

		_originalCheckBox.add(false);
		_filteredCheckBox.add(false);

		_originalData.add(itemList);
		_filteredData.add(itemList);

		notifyDataSetChanged();
	}

	public void addCheckableItem(String item) {
		List_Item itemList = new List_Item(item, TYPE_CHECKABLE_ITEM, nbItems);
		nbItems++;

		_originalCheckBox.add(false);
		_filteredCheckBox.add(false);

		_originalData.add(itemList);
		_filteredData.add(itemList);

		notifyDataSetChanged();
	}

	/** Return names of checked items */
	public ArrayList<Integer> getChecked() {
		ArrayList<Integer> objectsSelected = new ArrayList<Integer>();

		for(int i=0; i<_originalCheckBox.size(); i++)
			if(_originalCheckBox.get(i))
				objectsSelected.add(i);

		return objectsSelected;	
	}

	/** Uncheck all items */
	public void unCheck() {
		for(int i=0; i<_originalCheckBox.size(); i++)
			if(_originalCheckBox.get(i)) {
				checkbox = (CheckBox) getView(i, checkbox, null).findViewById(R.id.list_checkable_item_checkbox); 
				checkbox.performClick();
			}

		//Notify changes to update the view otherwise checkboxs remain checked
		notifyDataSetChanged();
	}

	public void clean() {
		_separatorPosition = new ArrayList<Integer>();
		_originalData = new ArrayList<List_Item>();
		_filteredData = new ArrayList<List_Item>();
		_originalCheckBox = new ArrayList<Boolean>();
		_filteredCheckBox = new ArrayList<Boolean>();
		
		nbItems=0;
	}

	@Override
	public int getItemViewType(int position) {
		return _originalData.get(position).getItemType();
	}

	@Override
	public int getViewTypeCount() {
		return TYPE_MAX_COUNT;
	}

	public int getCount() {
		return _filteredData.size();
	}

	public long getItemId(int position) {
		return position;
	}

	public List_Item getItem(int position) {
		return _filteredData.get(position);
	}

	public List<Integer> getSeparatorPosition() {
		return _separatorPosition;
	}

	/** Manage the visibility of each item by implicit call */
	public View getView(final int position, View convertView, ViewGroup parent) {
		final List_Item item = _filteredData.get(position);
		int type = item.getItemType();
		int color = item.getColor();
		
		//Will contain information about how to display item according to his type
		TextView textView = new TextView(_ctx);

		//Select the right view according to item type
		switch (type) {
		case TYPE_ITEM:
			convertView = _inflater.inflate(R.layout.list_item, null);
			textView = (TextView)convertView.findViewById(R.id.list_item_text);
			break;

		case TYPE_SEPARATOR:
			convertView = _inflater.inflate(R.layout.list_separator, null);
			textView = (TextView)convertView.findViewById(R.id.textSeparator);
			break;

		case TYPE_CHECKABLE_ITEM:
			convertView = _inflater.inflate(R.layout.list_checkable_item, null);
			textView = (TextView)convertView.findViewById(R.id.list_checkable_item_text);

			//getView is called when the item becomes visible (after a scroll on the list for example)
			//we have to display the CheckBox in the right state
			checkbox = (CheckBox) convertView.findViewById(R.id.list_checkable_item_checkbox); 
			checkbox.setChecked(_filteredCheckBox.get(position));

			//Is called when the state of the CheckBox changes (We don't need to implement OnClick...)
			checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				Boolean checked = _filteredCheckBox.get(position);

				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					_filteredCheckBox.set(position, (!checked));
					int originalposition =_filteredData.get(position).getId();
					_originalCheckBox.set(originalposition, (!checked));
				}
			});

			break;
		}

		convertView.setTag(textView);
		textView.setText(item.getItem());
		if (color != 0) textView.setTextColor(color);
		return convertView;
	}

	/** Add the possibility to filter items from list items */
	public Filter getFilter()
	{
		if(customFilter == null)
			customFilter=new CustomFilter();

		return customFilter;
	}

	private class CustomFilter extends Filter
	{
		/** Select items which contains the charSequence filter */
		@Override
		protected FilterResults performFiltering(CharSequence charSequence)
		{
			FilterResults results = new FilterResults();

			//If there is nothing to filter on, return the original data for your list
			if(charSequence == null || charSequence.length() == 0)
			{
				results.values =new Doublet(_originalData, _originalCheckBox);
				results.count = 2;
			}
			//Else we have to calculate the new list displayed and the checkbox list associated with it
			else
			{
				List<List_Item> filterResultsData = new ArrayList<List_Item>();
				List<Boolean> filterResultsCheckBox = new ArrayList<Boolean>();

				//Test each item from the original list with the charSequence filter
				for(int i=0; i<_originalData.size();i++){
					List_Item item =_originalData.get(i);

					if(filterConditions(item.getItem(), charSequence))
					{
						filterResultsData.add(item);
						filterResultsCheckBox.add(_originalCheckBox.get(item.getId()));
					}
				}            

				results.values = new Doublet(filterResultsData, filterResultsCheckBox);
				results.count = 2;
			}
			return results;
		}


		/** Update the filtered list data and checkbox, and notify it for displaying it thanks to getView method */
		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence charSequence, FilterResults filterResults)
		{ 
			Doublet doublet = (Doublet) filterResults.values;
			_filteredData=(List<List_Item>)doublet.arg0;
			_filteredCheckBox=(List<Boolean>)doublet.arg1;
			notifyDataSetChanged();
		}

		/** 
		 *  Non case-sensitive filter which return true if item contains 
		 * a word starting by the charSequence filter 
		 * */
		private boolean filterConditions(String item, CharSequence filter) {
			String delims = "[ ]+";
			//Turns listItem to lower-case in the local alphabet and parses it to filter each word from it
			String[] tokens = item.toLowerCase(Locale.getDefault()).split(delims);

			//Turns filter to lower-case in the local alphabet
			String sFilter = filter.toString().toLowerCase(Locale.getDefault());


			for(String token : tokens){
				if(token.startsWith(sFilter))
					return true;
			}

			return false;
		}
	};


}
