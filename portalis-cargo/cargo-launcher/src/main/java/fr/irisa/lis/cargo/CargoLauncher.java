package fr.irisa.lis.cargo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.cargo.container.ContainerType;
import org.codehaus.cargo.container.InstalledLocalContainer;
import org.codehaus.cargo.container.configuration.ConfigurationType;
import org.codehaus.cargo.container.configuration.LocalConfiguration;
import org.codehaus.cargo.container.deployable.Deployable;
import org.codehaus.cargo.container.deployable.DeployableType;
import org.codehaus.cargo.container.tomcat.Tomcat7xInstalledLocalContainer;
import org.codehaus.cargo.generic.DefaultContainerFactory;
import org.codehaus.cargo.generic.configuration.ConfigurationFactory;
import org.codehaus.cargo.generic.configuration.DefaultConfigurationFactory;
import org.codehaus.cargo.generic.deployable.DefaultDeployableFactory;
import org.codehaus.cargo.util.log.FileLogger;

public class CargoLauncher {


	private static InstalledLocalContainer container;
	private static final String containerId = Tomcat7xInstalledLocalContainer.ID;

	private static String servletPort = CargoProprietes.cargoProp
			.getProperty("cargo.servlet.port"); // default to 8080
	private static String hostname = CargoProprietes.cargoProp
			.getProperty("cargo.hostname"); // default to localhost
	
	
	private static final String containerOutput = CargoProprietes.cargoProp
			.getProperty("cargo.container.output");
	private static final String containerLogDirPath = CargoProprietes.cargoProp
			.getProperty("cargo.log.dir.path");
	private static final String containerLog = containerLogDirPath + "/"
			+ CargoProprietes.cargoProp.getProperty("cargo.log");

	private static final String containerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String containerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String containerHome = containerPath + containerName;

	private static String deployablePath = System
			.getenv("portalis.deployablePath");
	private static String webapp = CargoProprietes.cargoProp
			.getProperty("webapp.name");

	private static String WAR_FILE;

	private static String hosturl;

	public static String getHostUrl() {
		return hosturl;
	}

	/**
	 * <p>
	 * Start the container prior to running the tests.
	 * </p>
	 * <p>
	 * The following System properties are used:
	 * <ul>
	 * <li>cargo.container.id - ID of the container to use. [tomcat5x]</li>
	 * <li>cargo.container.home - Full path to a local installation of the
	 * container. If not set, uses the value of the TOMCAT_HOME environment
	 * variable. REQUIRED</li>
	 * <li>cargo.deployable - Full path to the war file to deploy. REQUIRED</li>
	 * <li>cargo.container.output - Full path to a file to use for output.
	 * [none]</li>
	 * <li>cargo.container.log - Full path to a file to use for logging. [none]</li>
	 * <li>cargo.servlet.port - The port on which the container should listen.
	 * [8080]</li>
	 * </ul>
	 * </p>
	 * 
	 * @throws FileNotFoundException
	 * @throws CargoLauncherException
	 * @throws MalformedURLException 
	 * 
	 * @throws Exception
	 *             if an error occurs.
	 */
	public static void start() throws FileNotFoundException, CargoLauncherException, MalformedURLException {
		if (deployablePath == null) {
			deployablePath = CargoProprietes.cargoProp
					.getProperty("webapp.path");
		}

		WAR_FILE = deployablePath + "/" + webapp + ".war";
		
		if (containerLogDirPath==null ){
			String mess = "Il manque le chemin vers le répertoire de log";
			throw new FileNotFoundException(mess);
		}
		
		File containerdir = new  File(containerLogDirPath);
		
		if (containerdir.exists()) {
			if (!containerdir.isDirectory()) {
				String mess = "'"+containerLogDirPath+"' n'est pas un répertoire";
				throw new CargoLauncherException(mess);
			}
		} else {
			if (!containerdir.mkdirs()) {
				String mess = "Impossible de créer le répertoire '"+containerLogDirPath+"'";
				throw new CargoLauncherException(mess);
			}
		}
		
		if (!new File(WAR_FILE).exists()) {
			String mess = "L'application web '" + WAR_FILE
					+ "' n'a pas été trouvée";
			throw new CargoLauncherException(mess);
		}

		// Construct the war, using the container id and the path to the .war
		// file
		Deployable war = new DefaultDeployableFactory().createDeployable(
				containerId, WAR_FILE, DeployableType.WAR);

		// Container configuration
		ConfigurationFactory configurationFactory = new DefaultConfigurationFactory();

		LocalConfiguration configuration = (LocalConfiguration) configurationFactory
				.createConfiguration(containerId, ContainerType.INSTALLED,
						ConfigurationType.STANDALONE);
		
		// Find and (if provided) set the port to use for the container.
		// default 8080
		if (servletPort != null) {
			configuration.setProperty("cargo.servlet.port", servletPort);
		}
		
		int portNumber;
		try {
			portNumber = Integer.parseInt(configuration.getPropertyValue("cargo.servlet.port"));
		} catch (Exception e) {
			throw new CargoLauncherException(servletPort + " n'est pas un numéro de port correct");
		}

		// Find and (if provided) set the hostname to use for the container.
		// default localhost
		if (hostname != null) {
			configuration.setProperty("cargo.hostname", hostname);
		}
		
		hosturl = new URL("http", configuration.getPropertyValue("cargo.hostname"), 
				portNumber , "").toString();
		

		// On ajoute le war du server.launcher
		configuration.addDeployable(war);

		container = (InstalledLocalContainer) new DefaultContainerFactory()
				.createContainer(containerId, ContainerType.INSTALLED,
						configuration);

		container.setHome(containerHome);

		// Find and (if provided) set the path to a log file
		if (containerLog != null) {
			container.setLogger(new FileLogger(containerLog, false));
		}

		// Find and (if provided) set the path to an output file
		// default stdout
		if (containerOutput != null) {
			container.setOutput(containerOutput);
		}

		if (lsof(portNumber).size()>0) {
			throw new CargoLauncherException("Impossible de démarrer Tomcat, le port "+portNumber+" n'est pas libre");
		} else {
			container.start();			
		}

	}

	public static void stop() {
		container.stop();
	}

	private static final String bin_lsof = "/usr/bin/lsof";
	private static final String sbin_lsof = "/usr/sbin/lsof";
	
	public static Set<Integer> lsof(int port) throws CargoLauncherException {
		String lsof = null;
		File lsofFile1 = new File(bin_lsof);
		File lsofFile2 = new File(sbin_lsof);
		if (lsofFile1.exists()) {
			lsof = bin_lsof;
		} else if (lsofFile2.exists()) {
			lsof = sbin_lsof;
		} else {
			throw new CargoLauncherException("lsof is unknown on this system");
		}

		String cmd = lsof+" -i tcp:" + port + " -Fp";
		Set<Integer> pidList = new HashSet<Integer>();
		String reponse = UtilCargo.executeCommand(cmd, true, false);
		if (reponse != null && reponse.length() > 0) {
			String[] lines = reponse.split("\n");
			int javaPid = getJavaPid();
			for (String line : lines) {
				try {
					// On enlève le 'p' de 'p3456'
					int pid = Integer.parseInt(line.substring(1));
					if (pid != javaPid) {
						pidList.add(pid);
					}
				} catch (NumberFormatException e) {
					String mess = "Impossible de lire le numéro de processus : "
							+ line.substring(1);
					throw new CargoLauncherException(mess);
				}
			}
		}
		return pidList;
	}

	public static int getJavaPid() throws CargoLauncherException {
		String pid = ManagementFactory.getRuntimeMXBean().getName()
				.replaceFirst("@.*$", "");
		int n = 0;
		try {
			n = Integer.parseInt(pid);
		} catch (Exception e) {
			String mess = "Erreur interne : impossible de calculer le N° de processus Java, la chaîne d'entrée est '"
					+ pid + "'";
			throw new CargoLauncherException(mess);
		}
		return n;
	}

	public static boolean tomcatIsActif() throws CargoLauncherException {
		try {
			String host = (hostname != null) ? hostname : "localhost";
			int port = (servletPort != null) ? Integer.parseInt(servletPort) : 8080;
			Socket sock = new Socket(host, port);
			// if we get here, the tomcat port was opened
			sock.close();
			return true;
		} catch (IOException e) {
			// uh oh, socket creation failed
			// that means tomcat port is not open (ie tomcat not active) 
			return false;
		} catch (Exception e) {
			String mess = e.getClass().getName()
					+ " : impossible d'obtenir les ports actifs de tomcat, le message est :\n   "
					+ e.getMessage();
			throw new CargoLauncherException(mess);
		}
	}


}
