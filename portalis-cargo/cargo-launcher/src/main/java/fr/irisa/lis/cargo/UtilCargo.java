package fr.irisa.lis.cargo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public class UtilCargo {

	private static String webapp = CargoProprietes.cargoProp
			.getProperty("webapp.name");

	public static String executeCommand(String command,
			boolean waitForResponse, boolean checkExitCode)
			throws CargoLauncherException {

		String response = "";

		ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
		pb.redirectErrorStream(true);

		try {
			Process shell = pb.start();

			// To capture output from the shell
			InputStream shellIn = shell.getInputStream();

			if (waitForResponse) {

				// Wait for the shell to finish and get the return code
				int shellExitStatus = shell.waitFor();
				response = convertStreamToStr(shellIn);
				if (checkExitCode && shellExitStatus != 0) {
					if (shellIn != null)
						shellIn.close();
					UtilCargo.errorExit(command, response, shellExitStatus);
				}
			} else {
				int exitCode = 99999;
				int i = 5;
				while (exitCode == 99999 & i > 0) {
					try {
						Thread.sleep(100);
						i--;
						exitCode = shell.exitValue();
					} catch (IllegalThreadStateException e) {
					}
				}
				if (exitCode != 99999) {
					response = convertStreamToStr(shellIn);
					if (shellIn != null)
						shellIn.close();
					UtilCargo.errorExit(command, response, exitCode);
				}

			}
			if (shellIn != null)
				shellIn.close();

		}

		catch (IOException e) {
			String message = "Error occured while executing Linux command '"
					+ command + "'\nError Description: " + e.getMessage();
			throw new CargoLauncherException(message);
		}

		catch (InterruptedException e) {
			String message = "Error occured while executing Linux command '"
					+ command + "'\nError Description: " + e.getMessage();
			throw new CargoLauncherException(message);
		}

		return response;
	}

	public static String convertStreamToStr(InputStream is) throws IOException {

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}


	public static void errorExit(String command, String response, int exitCode)
			throws IOException, CargoLauncherException {
		String errMess = "Le processus n'a pas été lancé correctement, les informations sont :"
				+ "\nexitCode = "
				+ exitCode
				+ "\ncommande = "
				+ command
				+ "\nmessage  = " + response;
		throw new CargoLauncherException(errMess);
	}

	public static String buildURL(String command) {
		return CargoLauncher.getHostUrl() + "/" + webapp + "/" + command;
	}

	public static String buildURL(String command, String[] parameters) {
		StringBuffer buff = new StringBuffer(buildURL(command));
		for (int i = 0; i < parameters.length; i += 2) {
			buff.append(i == 0 ? "?" : "&").append(parameters[i]).append("=")
					.append(parameters[i + 1]);
		}
		return buff.toString();
	}

	/**
	 * String representation of an Exception's stack
	 * 
	 * @param e
	 *            the Exception
	 * @return the string representation of the exception stack
	 */
	public static String stack2string(Exception e) {
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return "------\r\n" + sw.toString() + "------\r\n";
		} catch (Exception e2) {
			return "bad stack2string";
		}
	}
	

}
