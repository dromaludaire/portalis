package fr.irisa.lis.portalis.shared.camelis.reponse;

import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;

@SuppressWarnings("serial")
public class SetRoleReponse extends CamelisUserReponse {

	public SetRoleReponse() {
		super();
		elemName = CamelisXmlName.SET_ROLE_REPONSE;	}

	public SetRoleReponse(CamelisUser user2) {
		this();
		setUser(user2);
	}
	
	public SetRoleReponse(Err err) {
		super(err);
		elemName = CamelisXmlName.SET_ROLE_REPONSE;	}


}
