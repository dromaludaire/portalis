package fr.irisa.lis.portalis.shared.camelis.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class LisObject implements CamelisDataObject {

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private int oid;
	private String name;
	private String picture;
	private String file;

	public LisObject() {}

	public LisObject(int oid, String name, String picture, String file) {
		this.oid = oid;
		this.name = name;
		this.picture = picture;
		this.file = file;
	}

	public LisObject(int oid, String name) {
		this.oid = oid;
		this.name = name;
		this.picture = "";
		this.file = "";
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}


	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisObject) ) return false;

		  final LisObject that = (LisObject)aThat;
		  return new EqualsBuilder()
			//.appendSuper(super.equals(aThat))
			.append(this.name, that.name)
			.append(this.oid, that.oid)
			.append(this.picture, that.picture)
			.append(this.file,  that.file)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31) // two randomly chosen prime numbers
			//.appendSuper(super.hashCode())
			.append(file)
			.append(picture)
			.append(name)
			.append(oid)
			.toHashCode();
	}

}
