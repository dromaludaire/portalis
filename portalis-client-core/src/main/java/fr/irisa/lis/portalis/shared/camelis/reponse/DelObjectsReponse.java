package fr.irisa.lis.portalis.shared.camelis.reponse;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;

@SuppressWarnings("serial")
public class DelObjectsReponse extends LisReponse implements CamelisReponseObject {

	private final String xmlName = CamelisXmlName.DEL_OBJECTS_REPONSE;
	private LisExtent extent  = new LisExtent();

	public DelObjectsReponse () {
		super();
	}

	public DelObjectsReponse(Err err) {
		super(err);
	}

	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public String getXmlName() {
		return xmlName ;
	}

	public LisExtent getExtent() {
		return extent;
	}

	public void setExtent(LisExtent extent) {
		this.extent = extent;
	}

	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

}
