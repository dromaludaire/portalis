package fr.irisa.lis.portalis.shared.camelis.http;

import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActivePortalisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AbstractHttpUrlConnection;
import fr.irisa.lis.portalis.shared.admin.http.HttpPortalisUrlConnection;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataObject;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.reponse.AddAxiomReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisUserReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelFeatureReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelObjectsReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.GetValuedFeaturesReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportLisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCamelisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCreatorReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.SetRoleReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

@SuppressWarnings("serial")
public class CamelisHttp implements CamelisDataObject {

	private static final Logger LOGGER = LoggerFactory.getLogger(CamelisHttp.class.getName());
	
	private ActiveUser activeUser;
	private ActiveLisService activeService;
	
	private HttpStatelessUrlConnection httpStatelessUrlConnection = new HttpStatelessUrlConnection();
	
	public class HttpStatelessUrlConnection extends AbstractHttpUrlConnection {


		public Element doConnection(String command) throws PortalisException {
			URL url = buildUrl(activeService.getHost(), activeService.getPort(), command, new String[0][2]);
			return parseXMLFromUri(url.toString());
		}

		public Element doConnectionWithoutBip(String command) throws PortalisException {
			URL url = buildUrl(activeService.getHost(), activeService.getPort(), command, new String[0][2]);
			return parseWithoutBip(url.toString());
		}

		public Element doConnection(String command,
				String[][] httpReqArgs) throws PortalisException {
			URL url = buildUrl(activeService.getHost(), activeService.getPort(), command, httpReqArgs);
			return parseXMLFromUri(url.toString());
		}

		public Element doConnectionWithoutBip(String command,
				String[][] httpReqArgs) throws PortalisException {
			URL url = buildUrl(activeService.getHost(), activeService.getPort(), command, httpReqArgs);
			return parseWithoutBip(url.toString());
		}

		/** 
		 * Attention : cette lecture est faite sans session client.
		 * Toutes les lectures sont indépendantes les unes des autres
		 * @param uri
		 * @return
		 * @throws PortalisException
		 */
		private Element parseXMLFromUri(String uri) throws PortalisException {
			if (activeUser==null) {
				String mess = "Null ActiveUser, it "+ErrorMessages.TEMOIN_IS_MISSING;
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}
			HttpPortalisUrlConnection.bip(activeUser);
			return parseWithoutBip(uri);
		}




	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	private static Element parseWithoutBip(String uri) throws PortalisException {
		if (ClientConstants.isPrintHttpRequest()) {
			System.out.println("\n"+uri+"\n");
		}

		LOGGER.info("\nPortalisCtx   @parseXMLFromUri : "+uri);
		Document doc = null;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			try {
				dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(uri);
			} catch (Exception e) {
				String mess = ErrorMessages.TEMOIN_ERROR_DURING_HTTP_REQUEST;
				LOGGER.error(mess, e);
				throw new PortalisException(mess, e);
			} 
		Element elemResult = doc.getDocumentElement();
		String ok = elemResult.getAttribute(XmlIdentifier.STATUS());
		LOGGER.info("\nPortalisCtx   @fin parseXMLFromUri "+elemResult.getNodeName()+
				(ok.equals(XmlIdentifier.ERROR) ? elemResult.getAttribute(XmlIdentifier.MESSAGE()) : "") );
		return elemResult;
	}

	public static Element doConnectionWitoutBip(String host, int port, String command) throws PortalisException {
		URL url = HttpStatelessUrlConnection.buildUrl(host, port, command, new String[0][2]);
		return parseWithoutBip(url.toString());
	}
	
	public CamelisHttp(ActiveUser activeUser, ActiveLisService activeService) throws PortalisException {
		if (activeUser == null) {
			String mess = "ActiveUser "+ErrorMessages.TEMOIN_IS_MISSING;
			LOGGER.error(mess);
			throw new PortalisException(mess);
		} 
		this.activeUser = activeUser;
		this.activeService = activeService;
	}

	/*
	 * ------------------------------ ping
	 *
	 * PingReponse ping (String hostName , (String port | int port))
	 */

	/*
	 * ------------------------------ setRole
	 *
	 * SetRoleReponse setRole(String sessionId, ActiveLisService service , String adminKey, String userKey,
	 * (String | RightValue) role )
	 */

   	public LoadContextReponse loadContext() {
		LoadContextReponse reponse = new LoadContextReponse();

		String[][] httpReqArgs = { { CamelisXmlName.ACTIF_USER_KEY,
			activeUser.getPortalisSessionId() } };

		String cmd = "load";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getLoadContextReponse(elem);
			setFeatures();
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new LoadContextReponse(new Err(e));
		}

		return reponse;
	}

	public void setFeatures() throws Exception {
		ZoomReponse zoomReponse = zoom(ClientConstants.REQUEST_ALL,
				ClientConstants.REQUEST_ALL);
		if (!zoomReponse.isOk()) {
			throw new Exception(zoomReponse.getMessagesAsString());
		}
		LisIncrementSet increments = zoomReponse.getIncrements();
		for (LisIncrement increment : increments.getIncrements()) {
			String pattern = "(.*) \\?$";
			String feature = increment.getName().replaceAll(pattern, "$1");
			activeService.addFeature(feature);
		}
	}
	
	public static void main (String[] args) {
		String pattern = "(.*) \\?$";
		String test1 = "toto";
		String test2 = "titi ?";
		System.out.println(test1.replaceAll(pattern, "$1"));
		System.out.println(test2.replaceAll(pattern, "$1"));

	}

	public ImportCtxReponse importCtx(String ctxFile) {
		ImportCtxReponse reponse = new ImportCtxReponse();
		try {
			String[][] httpReqArgs = { { CamelisXmlName.CTX_FILE, ctxFile },
					{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() } };
			String cmd = "importCtx";
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getImportCtxReponse(elem);
			setFeatures();
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			return new ImportCtxReponse(new Err(e));
		}
	
		return reponse;
	}

	public ImportCtxReponse importCtx() {
		
		String serviceName = activeService.getFullName();
		String[] ids = serviceName.split(":");
		String ctxFile = new StringBuffer(ids[1]).append(".ctx").toString();
		return importCtx(ctxFile);
	}

	public ImportLisReponse importLis(String lisFile) {
		ImportLisReponse reponse = new ImportLisReponse();
		try {
			String[][] httpReqArgs = {
					{ CamelisXmlName.LIS_FILE, lisFile },
					{ CamelisXmlName.ACTIF_USER_KEY,
							activeUser.getPortalisSessionId() } };
			String cmd = "importLis";
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getImportLisReponse(elem);
			setFeatures();
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			return new ImportLisReponse(new Err(e));
		}
		return reponse;
	}

	public ImportLisReponse importLis() {
		// derive .lis filename from service name
		String serviceName = activeService.getFullName();
		String[] ids = serviceName.split(":");
		String lisFile = new StringBuffer(ids[1]).append(".lis").toString();
		return importLis(lisFile);
	}
	
	/*
	 * ------------------------------------------ Extent
	 */

	public ExtentReponse extent(String query, int page, int pageSize) {
		ExtentReponse reponse = new ExtentReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.QUERY, query },
				{ CamelisXmlName.PAGE, Integer.toString(page) },
				{ CamelisXmlName.PAGE_SIZE, Integer.toString(pageSize) } };
		String cmd = "extent";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getExtentReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ExtentReponse(new Err(e));
		}

		return reponse;
	}

	public ExtentReponse extent(String query) {
		ExtentReponse reponse = new ExtentReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.QUERY, query }, };
		String cmd = "extent";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getExtentReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ExtentReponse(new Err(e));
		}

		return reponse;
	}

	public ExtentReponse extent() {
		return extent("all");
	}

	public IntentReponse intent(String[] oids) {
		IntentReponse reponse = new IntentReponse();

		String[][] httpReqArgs = new String[1 + oids.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = activeUser.getPortalisSessionId();
		for (int i = 0; i < oids.length; i++) {
			httpReqArgs[i + 1][0] = CamelisXmlName.OID;
			httpReqArgs[i + 1][1] = oids[i];
		}

		String cmd = "intent";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getIntentReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new IntentReponse(new Err(e));
		}

		return reponse;
	}


	public ZoomReponse zoom(String wq, String feat, int page, int pageSize, boolean showLeaf) {
		ZoomReponse reponse = new ZoomReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.WQ, wq },
				{ CamelisXmlName.FEATURE, feat },
				{ CamelisXmlName.PAGE, Integer.toString(page) },
				{ CamelisXmlName.PAGE_SIZE, Integer.toString(pageSize) },
				{ CamelisXmlName.SHOW_LEAF, Boolean.toString(showLeaf) }
			};
		String cmd = "zoom";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getZoomReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ZoomReponse(new Err(e));
		}

		return reponse;
	}
	
	public ZoomReponse zoom(String wq, String feat, int page, int pageSize) {
		ZoomReponse reponse = new ZoomReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.WQ, wq },
				{ CamelisXmlName.FEATURE, feat },
				{ CamelisXmlName.PAGE, Integer.toString(page) },
				{ CamelisXmlName.PAGE_SIZE, Integer.toString(pageSize) }
			};
		String cmd = "zoom";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getZoomReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ZoomReponse(new Err(e));
		}

		return reponse;
	}
	
	public ZoomReponse zoom(String wq, String feat, boolean showLeaf) {
		ZoomReponse reponse = new ZoomReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.WQ, wq },
				{ CamelisXmlName.FEATURE, feat },
				{ CamelisXmlName.SHOW_LEAF, Boolean.toString(showLeaf) }
			};
		String cmd = "zoom";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getZoomReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ZoomReponse(new Err(e));
		}

		return reponse;
	}
	

	public ZoomReponse zoom(String wq, String feat) {
		ZoomReponse reponse = new ZoomReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.WQ, wq },
				{ CamelisXmlName.FEATURE, feat }
			};
		String cmd = "zoom";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getZoomReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ZoomReponse(new Err(e));
		}

		return reponse;
	}


	public ResetCreatorReponse resetCreator(String creatorEmail) {
		ResetCreatorReponse reponse = new ResetCreatorReponse();
		String[][] httpReqArgs = { { CamelisXmlName.EMAIL, creatorEmail },
				{ CamelisXmlName.NEW_KEY, activeUser.getPortalisSessionId()} };

		String cmd = "resetCreator";
		try {
			Element elem = httpStatelessUrlConnection.doConnectionWithoutBip(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getResetCreatorReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ResetCreatorReponse(new Err(e));
		}

		return reponse;
	}

	static public PingReponse ping(ActivePortalisService activeService) {
		return ping(activeService.getHost(), activeService.getPort());
	}
	

	static public PingReponse ping(String host, int port) {
		PingReponse reponse = null;
		try {
			String cmd = "ping";

			Element elem = doConnectionWitoutBip(host, port, cmd);

			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getPingReponse(elem);
			if (reponse.isOk()) {
				ActiveLisService camelisService = reponse
						.getFirstService();
				if (camelisService == null) {
					String mess = "Internal error : unable to retreive camelisService on port "
							+ port;
					LOGGER.error(mess);
					new PingReponse(new Err(mess));
				} else {
					camelisService.setHost(host);
					camelisService.setPort(port);
					
				}
			}
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			return new PingReponse(new Err(e));
		}
		return reponse;
	}

	public DelKeyReponse delKey(String userKey) {
		DelKeyReponse reponse = new DelKeyReponse();
		try {
			String[][] httpReqArgs = {
					{ CamelisXmlName.ACTIF_ADMIN_KEY,
						activeUser.getPortalisSessionId() },
					{ CamelisXmlName.ACTIF_USER_KEY, userKey } };

			String cmd = "delKey";
			Element elem = httpStatelessUrlConnection.doConnectionWithoutBip(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelKeyReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			return new DelKeyReponse(new Err(e));
		}

		return reponse;
	}

	public DelObjectsReponse delObjects(int[] oids) {
		DelObjectsReponse reponse = new DelObjectsReponse();

		String[][] httpReqArgs = new String[1 + oids.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = activeUser.getPortalisSessionId();
		for (int i = 0; i < oids.length; i++) {
			httpReqArgs[i + 1][0] = CamelisXmlName.OID;
			httpReqArgs[i + 1][1] = Integer.toString(oids[i]);
		}

		String cmd = "delObjects";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelObjectsReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new DelObjectsReponse(new Err(e));
		}

		return reponse;
	}

	public DelFeatureReponse delFeature(String query,
			String[] features) {
		DelFeatureReponse reponse = new DelFeatureReponse();

		String[][] httpReqArgs = new String[2 + features.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = activeUser.getPortalisSessionId();
		httpReqArgs[1][0] = CamelisXmlName.QUERY;
		httpReqArgs[1][1] = query;
		for (int i = 0; i < features.length; i++) {
			httpReqArgs[i + 2][0] = CamelisXmlName.FEATURE;
			httpReqArgs[i + 2][1] = features[i];
		}

		String cmd = "delFeature";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelFeatureReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new DelFeatureReponse(new Err(e));
		}

		return reponse;
	}

	public AddAxiomReponse addAxiom(String premise, String conclusion) {
		AddAxiomReponse reponse = new AddAxiomReponse();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.AXIOM_PREMISE, premise },
				{ CamelisXmlName.AXIOM_CONCLUSION, conclusion }
			};
		String cmd = "addAxiom";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getAddAxiomReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new AddAxiomReponse(new Err(e));
		}

		return reponse;
	}

	public ResetCamelisReponse resetCamelis() {
		ResetCamelisReponse reponse = new ResetCamelisReponse();

		String[][] httpReqArgs = { { CamelisXmlName.ACTIF_USER_KEY,
			activeUser.getPortalisSessionId() } };

		String cmd = "resetCamelis";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getResetCamelisReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new ResetCamelisReponse(new Err(e));
		}

		return reponse;

	}

   public GetValuedFeaturesReponse getValuedFeatures(int[] oids, String[] feats) {
		GetValuedFeaturesReponse reponse = new GetValuedFeaturesReponse();

		String[][] httpReqArgs = new String[1 + oids.length + feats.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = activeUser.getPortalisSessionId();
		for (int i = 0; i < oids.length; i++) {
			httpReqArgs[i + 1][0] = CamelisXmlName.OID;
			httpReqArgs[i + 1][1] = Integer.toString(oids[i]);
		}
		for (int i = 0; i < feats.length; i++) {
			httpReqArgs[i + 1 + oids.length][0] = "featname";
			httpReqArgs[i + 1 + oids.length][1] = feats[i];
		}

		String cmd = "getValuedFeatures";
		try {
			Element elem = httpStatelessUrlConnection.doConnection(cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getGetValuedFeaturesReponse(elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string(e));
			reponse = new GetValuedFeaturesReponse(new Err(e));
		}

		return reponse;
	}

  	public URL getResource(String resourceName)
   			throws PortalisException {
  		try {
			return new URL("http", activeService.getHost(), activeService.getPort(),
					"/getResource" +
					"?" + CamelisXmlName.ACTIF_USER_KEY + "=" + activeUser.getPortalisSessionId() +
					"&" + CamelisXmlName.RESOURCE_NAME + "=" + URLEncoder.encode(resourceName, "UTF-8"));
		} catch (Exception e) {
			String errMess = String.format(
					"build URL failure during http request\nCause = %s",
					e.getMessage());
			LOGGER.warn(errMess);
			throw new PortalisException(errMess, e);
		}

   	}

  	public URL getThumbnail(String resourceName, String width, String height)
   			throws PortalisException {
  		try {
			return new URL("http", activeService.getHost(), activeService.getPort(),
					"/getThumbnail"	+
					"?" + CamelisXmlName.ACTIF_USER_KEY + "=" + activeUser.getPortalisSessionId() +
					"&" + CamelisXmlName.THUMBNAIL_NAME + "=" + URLEncoder.encode(resourceName, "UTF-8") +
					(width == "" ? "" : "&width=" + width) +
					(height == "" ? "" : "&height=" + height)
				);
		} catch (Exception e) {
			String errMess = String.format(
					"build URL failure during http request\nCause = %s",
					e.getMessage());
			LOGGER.warn(errMess);
			throw new PortalisException(errMess, e);
		}
   	}

   	public URL getThumbnail(String resourceName, int width, int height)
   			throws PortalisException {
   		return getThumbnail(resourceName, Integer.toString(width), Integer.toString(height));
   	}

   	public URL getThumbnail(String sessionId, String resourceName)
   			throws PortalisException {
   		return getThumbnail(resourceName, "", "");
   	}

   	
   	/* =========================================================================================================*/
   	

	public RegisterKeyReponse registerKey(String userKey,
			RightValue role) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		roleAction(reponse, userKey, role, "registerKey");
		return reponse;
	}

	public RegisterKeyReponse registerKey(String userKey,
			RightValue role, int timeout) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		try {
			
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string( e));
			return new RegisterKeyReponse(new Err(e));
		}
		roleAction(reponse, userKey, role.name(), timeout, "registerKey");
		return reponse;
	}

	public SetRoleReponse setRole(String userKey,
			String role) {
		SetRoleReponse reponse = new SetRoleReponse();
		try {
			
			reponse = new SetRoleReponse();
			roleAction(reponse, userKey, role, "setRole");
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string( e));
			return new SetRoleReponse(new Err(e));
		}
		return reponse;
	}

	public SetRoleReponse setRole(String userKey,
			RightValue role) throws PortalisException {
		SetRoleReponse reponse = new SetRoleReponse();
		try {
			
			roleAction(reponse, userKey, role, "setRole");
		} catch (Exception e) {
			LOGGER.warn(CommonsUtil.stack2string( e));
			return new SetRoleReponse(new Err(e));
		}
		return reponse;
	}

   	/* =========================================================================================================*/
   		
	
	public RegisterKeyReponse registerKey(String userKey,
			String role, int timeout) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		roleAction(reponse, userKey, role, timeout, "registerKey");
		return reponse;
	}

	public RegisterKeyReponse registerKey(String userKey,
			String role) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		roleAction(reponse, userKey, role, "registerKey");
		return reponse;
	}

	private void roleAction(CamelisUserReponse reponse,
			String userKey, String roleParam, String cmd)
			throws PortalisException {
		RightValue role = null;
		try {
			role = RightValue.valueOf(roleParam);
		} catch (Exception e) {
			role = RightValue.NONE;
			String mess = String.format(
					"parameter %s is incorrect, its value is %s",
					CamelisXmlName.ROLE, roleParam);
			LOGGER.warn(CommonsUtil.stack2string( e));
			reponse.addMessage(mess);
		}
		if (reponse.isOk())
			roleAction(reponse, userKey, role, cmd);
	}

	private <T extends CamelisUserReponse> T roleAction(T reponse,
			String userKey, RightValue role, String cmd)
			throws PortalisException {

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_ADMIN_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.ROLE, role == null ? null : role.toString() },
				{ CamelisXmlName.ACTIF_USER_KEY, userKey } };

		roleAction(reponse, cmd, httpReqArgs);
		return reponse;
	}

	private <T extends CamelisUserReponse> T roleAction(T reponse, 
			String userKey, String role, int timeout,
			String cmd) throws PortalisException {

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_ADMIN_KEY, activeUser.getPortalisSessionId() },
				{ CamelisXmlName.ROLE, role },
				{ CamelisXmlName.ACTIF_USER_KEY, userKey },
				{ CamelisXmlName.TIMEOUT, String.valueOf(timeout) } };

		roleAction(reponse, cmd, httpReqArgs);
		return reponse;
	}

	private void roleAction(CamelisUserReponse reponse,
			String cmd, String[][] httpReqArgs) {
		try {
			Element elem = httpStatelessUrlConnection.doConnectionWithoutBip(cmd, httpReqArgs);
			ClientConstants.CAMELIS_REPONSE_XML_READER.setCamelisUserReponse(
					reponse, elem);
		} catch (Exception e) {
			LOGGER.error(CommonsUtil.stack2string( e));
			reponse.addMessage(e.getMessage());
		}
	}

	public ActiveUser getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(ActiveUser activeUser) {
		this.activeUser = activeUser;
	}

	public ActiveLisService getActiveService() {
		return activeService;
	}

	public void setActiveService(ActiveLisService activeService) {
		this.activeService = activeService;
	}


}
