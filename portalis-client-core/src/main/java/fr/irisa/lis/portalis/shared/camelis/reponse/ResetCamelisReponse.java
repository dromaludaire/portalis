package fr.irisa.lis.portalis.shared.camelis.reponse;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class ResetCamelisReponse extends LisReponse implements CamelisReponseObject {

	private String xmlName = CamelisXmlName.RESET_CAMELIS_REPONSE;

	public ResetCamelisReponse() {
		super();
	}

	public ResetCamelisReponse(Err err) {
		super(err);
	}
	
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public String getXmlName() {
		return xmlName;
	}

}
