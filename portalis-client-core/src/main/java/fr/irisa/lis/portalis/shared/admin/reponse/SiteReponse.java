package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.SiteInterface;

@SuppressWarnings("serial")
public class SiteReponse extends VoidReponse implements Serializable,
		AdminReponseVisitedObject {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SiteReponse.class.getName());

	private Site site = new Site();

	public SiteReponse() {
		super();
	}

	public SiteReponse(SiteInterface site1) throws RequestException,
			PortalisException {
		super();
		try {
			site = Site.clone(site1);
		} catch (RequestException e) {
			throw new PortalisException(e);
		}
	}

	public SiteReponse(Err err) {
		super(err);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof SiteReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final SiteReponse that = (SiteReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.site, that.site).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(site).toHashCode();
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

}
