package fr.irisa.lis.portalis.shared.admin.data;

public enum RequestLanguage {
	CAMELIS, SEWELIS, SPARQL, XQUERY, NONE;
}