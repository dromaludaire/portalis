package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.validation.ActiveServiceIdValidator;

@SuppressWarnings("serial")
public abstract class ActivePortalisService extends ActiveService {

	static final Logger LOGGER = LoggerFactory
			.getLogger(ActivePortalisService.class.getName());

	protected int port;
	protected String host;
	protected String creator;
	protected Date activationDate;

	static final ActiveServiceIdValidator activeServiceIdValidator = ActiveServiceIdValidator
			.getInstance();

	public ActivePortalisService() {
		super();
	}

	public ActivePortalisService(String creator, String host1, int port,
			Date dateCreation) {
		this.creator = creator;
		this.host = host1;
		this.port = port;
		this.activationDate = dateCreation;
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ActivePortalisService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ActivePortalisService that = (ActivePortalisService) aThat;
		return new EqualsBuilder()
				.
				// if deriving: appendSuper(super.equals(obj)).
				append(this.port, that.port).append(this.host, that.host)
				.append(this.activationDate, that.activationDate)
				.append(this.creator, that.creator).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				. // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(port).append(host).append(activationDate)
				.append(creator).toHashCode();
	}

	public void setActivationDate(Date dateCreation) {
		this.activationDate = dateCreation;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String hostName) {
		this.host = hostName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	@Override
	public String getActivePartOfId() {
		return this.host + ":" + this.port;
	}


	public static int extractPort(String actifId) {
		String[] parts = actifId.split("::");
		int port = 0;
		try {
			port = Integer.parseInt(parts[0].split(":")[1]);
		} catch (NumberFormatException e) {
			String mess = "Identification de service incorrecte '" + actifId
					+ "' le port doit être un entier";
			LOGGER.error(mess);
		}
		return port;
	}

	public static String extractHost(String actifId) throws PortalisException {
		String[] parts = actifId.split("::");
		return parts[0].split(":")[0];
	}

}
