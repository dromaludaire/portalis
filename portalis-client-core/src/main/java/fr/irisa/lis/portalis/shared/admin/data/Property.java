package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

// TODO make Property class abstract
// TODO cleanup Property class after making LisIncrement and LisFeature
@SuppressWarnings("serial")
public class Property implements AdminDataObject {

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}


	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected String name;

	public Property() {
		super();
	}
	
	public Property(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof Property) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final Property that = (Property)aThat;
			return new EqualsBuilder()
			//.appendSuper(super.equals(aThat))
			.append(this.name, that.name)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(name).
				toHashCode();
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
