package fr.irisa.lis.portalis.shared.admin.data;

import javax.validation.Constraint;



@Constraint(validatedBy = {LoginValidator.class})
public @interface LoginConstraint {
    String message() default "{myNotNull}";
    Class<?>[] groups() default { };
}
