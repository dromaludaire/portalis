package fr.irisa.lis.portalis.shared.camelis.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class LisIncrement extends Property implements CamelisDataObject {

	private int card;
	private final String xmlName = CamelisXmlName.INCR;
	private String isLeaf = null;
	
	
	public LisIncrement() {
		super();
	}

	public LisIncrement(String name) {
		super(name);
	}

	public LisIncrement(String name, int card, String isLeaf) {
		super(name);
		this.card = card;
		this.isLeaf = isLeaf;
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisIncrement) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LisIncrement that = (LisIncrement)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.card, that.card)
			.append(this.isLeaf, that.isLeaf)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(card).
				append(isLeaf).
				toHashCode();
	}

	
	public int getCard() {
		return this.card;
	}

	public String getXmlName() {
		return xmlName;
	}


	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}


	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public boolean isLeafUnknown() {
		return isLeaf == null;
	}
	
	public String getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(String val) {
		this.isLeaf = val;
	}
	
}
