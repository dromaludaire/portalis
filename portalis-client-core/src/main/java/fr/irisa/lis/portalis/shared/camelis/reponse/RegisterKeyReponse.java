package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.Date;

import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;

@SuppressWarnings("serial")
public class RegisterKeyReponse extends CamelisUserReponse {


	public RegisterKeyReponse() {
		super();
		elemName = CamelisXmlName.REGISTER_KEY_REPONSE;
	}

	public RegisterKeyReponse(String creator,
			Date dateCreation, Date lastUpdate, int port, int pid, int nbObject){
		this();
	}

	public RegisterKeyReponse(CamelisUser user2) {
		this();
		setUser(user2);
	}

	public RegisterKeyReponse(Err err) {
		super(err);
		elemName = CamelisXmlName.REGISTER_KEY_REPONSE;
	}


}
