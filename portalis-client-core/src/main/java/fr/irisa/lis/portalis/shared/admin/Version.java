package fr.irisa.lis.portalis.shared.admin;

/**
 * Classe pour obtenir en Java la version du module Maven.
 * 
 * <b>Attention</p> : Afin que Maven stock ses informations de version dans ses fichiers <code>jar<c/ode>,
 * il faut modifier l'élément <code>&lt;archive> &lt;configuration></code> du plugin <code>maven-jar-plugin</code>
 * de votre ficher <code>pom.xml</code> . Initialiser les options 
 * <code>addDefaultImplementationEntries</code> et <code>addDefaultSpecificationEntries</code> à <code>true</code>.
 * 
 * @author bekkers
 *
 */
public class Version {
	
	/**
	 * 
	 * @return l'information de version de Maven
	 */
	public static String get() {
		return Version.class.getPackage().getImplementationVersion();
	}

}
