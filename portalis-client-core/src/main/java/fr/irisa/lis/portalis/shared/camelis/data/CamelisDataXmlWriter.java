package fr.irisa.lis.portalis.shared.camelis.data;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMWriter;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

public class CamelisDataXmlWriter extends AdminDataXmlWriter
		implements CamelisDataWriterInterface<Element> {

	static private CamelisDataXmlWriter instance;
	
	public static CamelisDataXmlWriter getInstance() {
		if (instance == null) {
			instance = new CamelisDataXmlWriter();
		}
		return instance;
	}
	
	

	@Override
	public Element visit(LisIntent intent) {
		Element root = DOMWriter.createRootElement(intent.getXmlName());
		for (Property p : intent.getProperties()) {
			DOMWriter.addChild(root, visit(p));
		}
		return root;
	}

	
	@Override
	public Element visit(LisIncrement incr) {
		Element elem = DOMWriter.createRootElement(incr.getXmlName());
		DOMWriter.addAttribute(elem, CamelisXmlName.PROPERTY_NAME,
				incr.getName());
		DOMWriter.addAttribute(elem, CamelisXmlName.PROPERTY_CARD,
				Integer.toString(incr.getCard()));
		if (!incr.isLeafUnknown()) {
			DOMWriter.addAttribute(elem, CamelisXmlName.PROPERTY_IS_LEAF,
					incr.getIsLeaf());
		}
		return elem;
	}
		

	@Override
	public Element visit(LisIncrementSet incrs) {
		Element elem = DOMWriter.createRootElement(incrs.getXmlName());
		for (LisIncrement i : incrs.getIncrements()) {
			DOMWriter.addChild(elem, visit(i));
		}
		return elem;
	}
	

	@Override
	public Element visit(Axiome axiome) {
		Element result = DOMWriter.createRootElement(CamelisXmlName.AXIOME);
		DOMWriter.addAttribute(result, CamelisXmlName.Terme1,
				axiome.getTerme1());
		DOMWriter.addAttribute(result, CamelisXmlName.Terme2,
				axiome.getTerme2());
		return result;
	}

	
	@Override
	public Element visit(LisObject lisObject) {
		Element object = DOMWriter.createRootElement(CamelisXmlName.OBJECT);
		DOMWriter.addAttribute(object,
								  CamelisXmlName.OID,
								  Integer.toString(lisObject.getOid()));
		DOMWriter.addAttribute(object, CamelisXmlName.OBJECT_NAME,
				lisObject.getName());
		String pic = lisObject.getPicture();
		if (pic != "") {
			DOMWriter.addAttribute(object, CamelisXmlName.OBJECT_PICTURE, pic);
		}
		String file = lisObject.getFile();
		if (file != "") {
			DOMWriter.addAttribute(object, CamelisXmlName.OBJECT_FILE, file);
		}
		return object;
	}
	
	
	@Override
	public Element visit(LisExtent lisExtent) {
		Element root = DOMWriter.createRootElement(CamelisXmlName.EXTENT);
		for (LisObject obj : lisExtent.getLisObjects()) {
			DOMWriter.addChild(root, visit(obj));
		}
		return root;
	}

	
	@Override
	public Element visit(PropertyTree tree) {
		Element propertiesElement = DOMWriter.createRootElement(CamelisXmlName.PROPERTY_TREE);
		DOMWriter.addAttribute(propertiesElement, CamelisXmlName.FEATURE,
				tree.getFeature());
		DOMWriter.addAttribute(propertiesElement, CamelisXmlName.SUPP, tree.getSupp() + "");

		StringBuffer buff = new StringBuffer();
			buff.append(tree.getOids());
		DOMWriter.addTextChild(propertiesElement, CamelisXmlName.OIDS,
				buff.toString());

		// modification ici
		int length = 0;
		if (tree.getChildren() != null)
			length = tree.getChildren().length;
		if (length == 0) {
			return propertiesElement;
		}
		for (int i = 0; i < length; i++) {
			DOMWriter.addChild(propertiesElement, visit(tree.getChildren()[i]));
		}
		return propertiesElement;
	}


	@Override
	public Element visit(CamelisUser camelisUser) {
		Element reponse = DOMWriter.createRootElement(CamelisXmlName.USER);
		DOMWriter.addAttribute(reponse, CamelisXmlName.KEY, camelisUser.getKey());
		DOMWriter.addAttribute(reponse, CamelisXmlName.ROLE,
						camelisUser.getRole().toString());
		DOMWriter.addAttribute(reponse, CamelisXmlName.EXPIRES,
						Util.writeDate(camelisUser.getExpires()));
		return reponse;
	}


	@Override
	public Element visit(CamelisHttp camelisHttp) {
		Element reponse = DOMWriter.createRootElement(CamelisXmlName.CAMELIS_HTTP);
		DOMWriter.addChild(reponse, visit(camelisHttp.getActiveUser()));
		DOMWriter.addChild(reponse, visit(camelisHttp.getActiveService()));
		return reponse;
	}

}
