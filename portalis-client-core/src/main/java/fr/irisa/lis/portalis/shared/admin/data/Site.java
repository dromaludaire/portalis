package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.jquery.TreetableNode;
import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;

@SuppressWarnings("serial")
public class Site implements AdminDataObject, SiteInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(Site.class
			.getName());
	
	protected SparqlApplication sparqlEndPoints = new SparqlApplication();
	// key is applicationId
	protected Map<String, LisApplication> lisApplications = new ConcurrentHashMap<String, LisApplication>();
	// key is email
	protected Map<String, PasswordUser> users = new ConcurrentHashMap<String, PasswordUser>();
	// key is
	protected Map<String, Role> roles = new ConcurrentHashMap<String, Role>();

	public Site(SparqlApplication sparqlEndPoints, ConcurrentHashMap<String, LisApplication> applications2) {
		this.sparqlEndPoints = sparqlEndPoints;
		this.lisApplications = applications2;
	}

	public Site() {
	}
	
	@Override
	public Set<LisServiceCore> getLesServices() {
		Set<LisServiceCore> result = new HashSet<LisServiceCore>();
		for (LisApplication appli : lisApplications.values()) {
			for (LisServiceCore service : appli.getServices()) {
				result.add(service);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof Site))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final Site that = (Site) aThat;
		return new EqualsBuilder().
		// appendSuper(super.equals(that)).
				append(this.lisApplications, that.lisApplications).
				append(this.sparqlEndPoints, that.sparqlEndPoints).
				append(this.users, that.users).
				append(this.roles, that.roles).
				isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// if deriving: appendSuper(super.hashCode()).
				append(lisApplications).
				append(sparqlEndPoints).
				append(users).
				append(roles).
				toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.shared.admin.data.dataBase#getApplications()
	 */
	@Override
	public LisApplication[] getApplications() {
		Collection<LisApplication> values = lisApplications.values();
		return values.toArray(new LisApplication[values.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#getApplication(java.
	 * lang.String)
	 */
	@Override
	public LisApplication getApplication(String id) {
		return this.lisApplications.get(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#addApplication(fr.irisa
	 * .lis.portalis.shared.admin.data.Application)
	 */
	@Override
	public void addApplication(LisApplication appli) {
		this.lisApplications.put(appli.getId(), appli);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.shared.admin.data.dataBase#getServices()
	 */
	@Override
	public LisServiceCore[] getServices() {
		List<LisServiceCore> services = new ArrayList<LisServiceCore>();
		for (LisApplication appli : lisApplications.values()) {
			services.addAll(appli.getServices());
		}
		return services.toArray(new LisServiceCore[services.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#getService(java.lang
	 * .String)
	 */
	@Override
	public LisServiceCore getService(String fullName) throws PortalisException {
		if (fullName.equals(PortalisService.getInstance().getFullName())) {
			return null;
		}
		String appliId = LisServiceCore.extractAppliName(fullName);
		String serviceName = LisServiceCore.extractServiceName(fullName);
		LisApplication appli = getApplication(appliId);
		if (appli == null) {
			String mess = ErrorMessages.TEMOIN_UNKNOWN_APPLICATION_NAME + appliId;
			LOGGER.warn(mess);
			LOGGER.warn(this.toString());
			throw new PortalisException(mess);
		}
		LisServiceCore service = appli.getService(serviceName);
		if (service == null) {
			String mess = serviceName
					+ ErrorMessages.TEMOIN_UNKNOWN_SERVICE_NAME_FOR_APPLICATION + appliId;
			LOGGER.warn(mess);
			LOGGER.warn(this.toString());
			throw new PortalisException(mess);
		}
		return service;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.shared.admin.data.dataBase#getUsers()
	 */
	@Override
	public PasswordUser[] getUsers() {
		Collection<PasswordUser> values = users.values();
		return values.toArray(new PasswordUser[values.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#getUser(java.lang.String
	 * )
	 */
	@Override
	public PasswordUser getUser(String email) {
		LOGGER.debug("users.size() = " + users.size());
		return users.get(email);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#addUser(fr.irisa.lis
	 * .portalis.shared.admin.data.PasswordUser)
	 */
	@Override
	public void addUser(PasswordUser user) {
		this.users.put(user.getEmail(), user);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.shared.admin.data.dataBase#getRoles()
	 */
	@Override
	public Role[] getRoles() {
		Collection<Role> values = roles.values();
		return values.toArray(new Role[values.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#getRole(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public Role getRole(String email, String fullName) {
		LOGGER.debug("email = " + email + " fullName = " + fullName);
		PasswordUser user = getUser(email);
		if (user instanceof SuperUser) {
			return new Role(email, fullName, RightValue.SUPER_ADMIN);
		} else if (user instanceof StandardUser) {
			if (fullName.equals(PortalisService.getInstance().getFullName()))
				return new Role(email, fullName,
						((StandardUser) user).getPortalisAdminRole());
			else {
				String key = Role.buildId(email, fullName);
				Role role = roles.get(key);
				if (role == null) {
					return new Role(email, fullName, RightValue.READER);
				} else {
					return role;
				}
			}
		} else { // anonymous users
			return new Role(email, fullName, RightValue.READER);
		}
	}

	@Override
	public void addRole(String email, String fullName, RightValue rightValue) {
		String key = Role.buildId(email, fullName);
		this.roles.put(key, new Role(email, fullName, rightValue));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.shared.admin.data.dataBase#addRole(fr.irisa.lis
	 * .portalis.shared.admin.data.Role)
	 */
	@Override
	public void addRole(Role role) {
		this.roles.put(role.getId(), role);
	}

	@Override
	public Map<String, PasswordUser> getUsersMap() {
		return users;
	}

	@Override
	public Map<String, LisApplication> getApplicationsMap() {
		return lisApplications;
	}

	@Override
	public Map<String, Role> getRolesMap() {
		return roles;
	}

	public static Site clone(SiteInterface site1) throws RequestException,
			PortalisException {
		Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(site1);
		return ClientConstants.ADMIN_DATA_XML_READER.getSite(elem);
	}

	@Override
	public SparqlApplication getSparqlEndPoints() {
		return sparqlEndPoints;
	}

	@Override
	public void setSparqlEndPoints(SparqlApplication sparqlEndPoints) {
		this.sparqlEndPoints = sparqlEndPoints;
	}

	@Override
	public TreetableNode lisApplicationsToNode() {
		TreetableNode result = new TreetableNode("lisApplication");
		for (String key : lisApplications.keySet()) {
			result.addChild(lisApplications.get(key).toTreetableNode());
		}
		return result;
	}
}
