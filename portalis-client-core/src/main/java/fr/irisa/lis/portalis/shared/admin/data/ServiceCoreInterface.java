package fr.irisa.lis.portalis.shared.admin.data;


public interface ServiceCoreInterface {

	public abstract String getFullName();

	public abstract String getAppliName();

	public abstract String getServiceName();
	
	public abstract RequestLanguage getReqLanguage();


}