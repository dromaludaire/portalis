package fr.irisa.lis.portalis.shared.admin.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

@SuppressWarnings("serial")
public abstract class PasswordUser extends UserData {

	protected String md5Password;

	public PasswordUser() {
		super();
	}

	public PasswordUser(String email, String pseudo, String password) {
		super(email, pseudo);
		md5Password = password;
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		final PasswordUser that = (PasswordUser) aThat;
		return new EqualsBuilder().
				appendSuper(super.equals(aThat)).
				append(this.md5Password, that.md5Password).
				isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.shared.admin.data.userCore#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(md5Password).toHashCode();
	}

	@NotNull
	@Size(min = 8, max = 50)
	public String getPassword() {
		return md5Password;
	}

	public void setPassword(String md5Password) {
		this.md5Password = md5Password;
	}

}