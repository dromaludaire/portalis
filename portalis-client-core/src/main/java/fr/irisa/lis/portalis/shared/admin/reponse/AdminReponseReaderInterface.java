package fr.irisa.lis.portalis.shared.admin.reponse;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;

public interface AdminReponseReaderInterface<T> {

	public abstract SiteReponse getSiteReponse(T root)
			throws PortalisException, RequestException;

	public abstract ActiveSiteReponse getActiveSiteReponse(T root)
			throws PortalisException, RequestException;

	public abstract PortsActifsReponse getPortsActifsReponse(T root)
			throws PortalisException, RequestException;

	public abstract PidReponse getPidReponse(T root)
			throws PortalisException, RequestException;

	public abstract LoginReponse getLoginReponse(T root)
			throws RequestException, PortalisException;

	public abstract VersionReponse getVersionReponse(T root)
			throws RequestException, PortalisException;

	public abstract VoidReponse getVoidReponse(T root)
			throws PortalisException, RequestException;

	public abstract LogReponse getLogReponse(T root)
			throws PortalisException, RequestException;

	public abstract BooleanReponse getBooleanReponse(Element elem)
			throws PortalisException, RequestException;

	public abstract ServiceCoreReponse getServiceCoreReponse(Element elem)
			throws PortalisException, RequestException;

}