package fr.irisa.lis.portalis.shared.admin.data;

@SuppressWarnings("serial")
public class UnauthorizedException extends RuntimeException {
	public UnauthorizedException() {
		super();
	}

	public UnauthorizedException(String mess) {
		super(mess);
	}

	public UnauthorizedException(String mess, Throwable t) {
		super(mess, t);
	}

}
