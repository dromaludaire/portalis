package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;


@SuppressWarnings("serial")
public class ImportLisReponse extends LisReponse implements CamelisReponseObject {

	private String lisFile;
	
	
	public ImportLisReponse() {
		super();
	};

	public ImportLisReponse(String lisFile) {
		super();
		this.lisFile = lisFile;
	};

	public ImportLisReponse(Err err) {
		super(err);
	}

	public String getLisFile() {
		return lisFile;
	}

	public void setLisFile(String lisFile) {
		this.lisFile = lisFile;
	}


	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ImportLisReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ImportLisReponse that = (ImportLisReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.lisFile, that.lisFile).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(lisFile).toHashCode();
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}


	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

}
