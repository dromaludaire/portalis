package fr.irisa.lis.portalis.shared.admin.data;

public interface UserDataContent {

	public abstract UserData getUserCore();

	public abstract void setUserCore(UserData userCore);

}