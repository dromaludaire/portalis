package fr.irisa.lis.portalis.shared.camelis;

import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;


public class CamelisXmlName {

	public static final String OK = "ok";
	public static final String ERROR = "error";
	public static final String INFO = "info";
	public static final String STATUS = "status";

	public static final String PROPERTY_TREE = "propertyTree";
	public static final String X_PROPERTY_TREE = "xPropertyTree";
	public static final String SUPP = "supp";
	public static final String OIDS = "oids";
	public static final String EXTENT = "extent";
	public static final String EXTENT_REPONSE = "extentResponse";
	public static final String OBJECT = "object";
	public static final String OID = "oid";
	public static final String OBJECT_NAME = "name";
	public static final String NEW_REQ = "newReq";
	public static final String PORT = "port";
	public static final String CREATOR = "creator";

	public static final String INCR = "increment";
	public static final String INCRS = "increments";
	public static final String INTENT = "intent";
    public static final String INTENT_REPONSE = "intentResponse";

   	public static final String PROPERTIES = INCRS;
	public static final String PROPERTY_NAME = XmlIdentifier.NAME();
	public static final String PROPERTY_CARD = "card";
	public static final String PROPERTY_VALUE = "value";
	public static final String FEATURE = XmlIdentifier.FEATURE();
	public static final String PROPERTY = FEATURE;



	public static final String PING_USERS_REPONSE = "pingUsersResponse";
	public static final String RESET_CREATOR_REPONSE = "resetCreatorResponse";
	public static final String SET_ROLE_REPONSE = "setRoleResponse";
	public static final String REGISTER_KEY_REPONSE = "registerKeyResponse";
	public static final String ZOOM_REPONSE = "zoomResponse";
	public static final String PROPERTY_TREE_REPONSE = "propTreeResponse";
	public static final String IMPORT_CTX_REPONSE = "importCtxResponse";
	public static final String IMPORT_LIS_REPONSE = "importLisResponse";
	public static final String CTX_FILE = "ctxfile";
	public static final String LIS_FILE = "lisfile";
	public static final String CONTEXT_NAME = "contextName";

	public static final String AXIOME = "axiome";
	public static final String Terme1 = "terme1";
	public static final String Terme2 = "terme2";
	public static final String KEY = "key";
	public static final String EXPIRES = "expires";
	public static final String ROLES = "roles";
	public static final String ROLE = "role";
	public static final String USER = "user";
	public static final String ACTIF_USER_KEY = "userKey";
	public static final String ACTIF_ADMIN_KEY = "adminKey";
	public static final String TIMEOUT = "timeout";

    public static final String QUERY = "query";
	public static final String WQ = "wq";

	public static final String NEW_KEY = "newKey";
	public static final String EMAIL = "email";
	public static final String DEL_OBJECTS_REPONSE = "delObjectsResponse";
	public static final String ADD_AXIOM_REPONSE = "addAxiomResponse";
	public static final String AXIOM_PREMISE = "premise";
	public static final String AXIOM_CONCLUSION = "conclusion";
	public static final String RESET_CAMELIS_REPONSE = "resetCamelisResponse";
	public static final String DEL_KEY_REPONSE = "delKeyResponse";
	public static final String DEL_FEATURE_REPONSE = "delFeatureResponse";
	public static final String GET_VALUED_FEATURES_REPONSE = "getValuedFeaturesResponse";
	public static final String LOAD_REPONSE = "loadResponse";
	public static final String OBJECT_PICTURE = "picture";
	public static final String OBJECT_FILE = "file";
	public static final String RESOURCE_NAME = "file";
	public static final String THUMBNAIL_NAME = "file";
	public static final String PAGE = "page";
	public static final String PAGE_SIZE = "pageSize";
	public static final String NB_OBJECTS = "nbObjects";
	public static final String EXTENT_NB_OBJECTS = NB_OBJECTS;
	public static final String NB_INCRS = "nbIncrs";
	public static final String NEW_WQ = "newWq";
	public static final String PROPERTY_IS_LEAF = "isLeaf";
	public static final String CAMELIS_HTTP = "camelisHttp";
	public static final String SHOW_LEAF = "showLeaf";
}
