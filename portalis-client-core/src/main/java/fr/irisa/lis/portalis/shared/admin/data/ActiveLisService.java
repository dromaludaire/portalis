package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

@SuppressWarnings("serial")
public class ActiveLisService extends ActivePortalisService {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ActiveLisService.class.getName());
	
	private String activeId;
	protected int pid;
	protected Date lastUpdate;
	protected int nbObject;
	private Set<String> features = new HashSet<String>();

	public ActiveLisService() {
		super();
	}

	public ActiveLisService(String creator, String host1, int port,
			Date dateCreation, Date lastUpdate2, int pid2, int nbObject2, String fullName) {
		super(creator, host1, port, dateCreation);
		this.lastUpdate = lastUpdate2;
		this.pid = pid2;
		this.nbObject = nbObject2;
		this.activeId = host1+":"+port+"::"+fullName;
	}
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;
	
		if (!(aThat instanceof ActiveLisService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;
	
		final ActiveLisService that = (ActiveLisService) aThat;
	    return new EqualsBuilder().
	            appendSuper(super.equals(aThat)).
	            append(this.lastUpdate, that.lastUpdate).
	            append(this.nbObject, that.nbObject).
	            append(this.pid, that.pid).
	            append(this.features, that.features).
	            isEquals();
	}

	@Override
	public int hashCode() {
	    return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
	        	appendSuper(super.hashCode()).
	            append(lastUpdate).
	            append(nbObject).
	            append(pid).
	            append(features).
	        toHashCode();
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getPid() {
		return pid;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setNbObject(int nbObject) {
		this.nbObject = nbObject;
	}

	public int getNbObject() {
		return this.nbObject;
	}

	public boolean isContextLoaded() {
		return nbObject>0;
	}

	@Override
	public String getFullName() {
		return this.activeId.split("::")[1];
	}

	public String getAppliName()  {
		return extractAppliName(this.activeId);
	}

	public String getServiceName() {
		return extractServiceName(this.activeId);
	}
		

	@Override
	public String getActiveId() {
		return this.activeId;
	}

	public void setActiveId(String activeId) {
		this.activeId = activeId;
	}

	public Set<String> getFeatures(ActiveUser activeUser) throws PortalisException, Exception {
		if (features.size()==0) {
			new CamelisHttp(activeUser, this).setFeatures();
		}
		return features;
	}
	
	public Set<String> getFeatures() {
		return features;		
	}

	public void addFeature(String feature) {
		features.add(feature);
	}

	public void setFeatures(Set<String> features) {
		this.features = features;
	}

}
