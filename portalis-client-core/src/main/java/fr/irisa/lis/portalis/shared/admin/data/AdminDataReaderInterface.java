package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;

public interface AdminDataReaderInterface<T> {

	public abstract Property getProperty(T root)
			throws PortalisException, RequestException;

	public abstract Site getSite(T root)
			throws RequestException, PortalisException;

	public abstract ActiveSite getActiveSite(T root)
			throws RequestException, PortalisException;

	public abstract LisApplication getLisApplication(T root)
			throws RequestException, PortalisException;

	public abstract SparqlApplication getSparqlApplication(T root)
			throws RequestException, PortalisException;

	public abstract ActiveLisService getActiveLisService(T root) throws PortalisException;

	public abstract UserDataContent getLoginResult(T root) throws PortalisException, RequestException;

	public abstract LisServiceCore getServiceCore(T root) throws PortalisException,
			RequestException;

	public abstract PortActif getPortActif(T root)
			throws RequestException, PortalisException;

	public abstract Date getLastUdate(T root) throws PortalisException;

	public abstract Role getRightPropertyForUser(T root,
			String userEmail) throws PortalisException, RequestException;

	public abstract Role getRightProperty(T root)
			throws PortalisException, RequestException;

	public abstract StandardUser getStandardUser(T root) throws PortalisException,
			RequestException;

	public abstract AnonymousUser getAnonymousUser(T root) throws PortalisException,
			RequestException;

	public abstract SuperUser getSuperUser(T root) throws PortalisException, RequestException;

	public abstract UserData getUserData(T root) throws PortalisException,
	RequestException;

	public abstract UIPreference getUIPreference(T root) throws PortalisException,
	RequestException;

	public abstract SparqlService getSparqlService(T root) throws PortalisException,
	RequestException;

	public abstract Query getQuery(T root) throws PortalisException,
	RequestException;

	public abstract void setSite(Element root, Site site) throws RequestException,
			PortalisException;



}