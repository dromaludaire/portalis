package fr.irisa.lis.portalis.shared.camelis.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlReader;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

public class CamelisDataXmlReader extends AdminDataXmlReader implements
		CamelisDataReaderInterface<Element> {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CamelisDataXmlReader.class.getName());

	static private CamelisDataXmlReader instance;

	public static CamelisDataXmlReader getInstance() {
		if (instance == null) {
			instance = new CamelisDataXmlReader();
		}
		return instance;
	}

	protected CamelisDataXmlReader() {
		super();
	}

	public LisObject getLisObject(Element root) throws CamelisException,
			PortalisException, RequestException {
		checkElement(root, CamelisXmlName.OBJECT);

		String soid = getAttribute(root, CamelisXmlName.OID);
		if (soid == null || soid.length() == 0) {
			throw new CamelisException("Attribut " + CamelisXmlName.OID
					+ " incorrect dans un élément " + CamelisXmlName.OBJECT);
		}
		int oid = Integer.parseInt(soid);

		String name = getAttribute(root, CamelisXmlName.OBJECT_NAME);
		if (name == null || name.length() == 0) {
			throw new CamelisException("Attribut " + CamelisXmlName.OBJECT_NAME
					+ " incorrect dans un élément " + CamelisXmlName.OBJECT);
		}

		// picture attribute is optional
		String picture = getAttribute(root, CamelisXmlName.OBJECT_PICTURE);
		if (picture == null) {
			picture = "";
		}

		// file attribute is optional
		String file = getAttribute(root, CamelisXmlName.OBJECT_FILE);
		if (file == null) {
			file = "";
		}

		return new LisObject(oid, name, picture, file);
	}

	public LisExtent getLisExtent(Element root) throws CamelisException,
			PortalisException, RequestException {
		checkElement(root, CamelisXmlName.EXTENT);
		ArrayList<LisObject> lisObjects = new ArrayList<LisObject>();
		List<Element> lisObjectElems = getChildElements(root,
				CamelisXmlName.OBJECT);
		for (Element e : lisObjectElems) {
			lisObjects.add(getLisObject(e));
		}
		return new LisExtent(lisObjects);
	}

	public CamelisUser getCamelisUser(Element root) throws PortalisException,
			RequestException {
		LOGGER.debug("CamelisUser(\n" + DOMUtil.prettyXmlString(root) + ")");
		checkElement(root, CamelisXmlName.USER);
		String key = getStringAttribute(root, CamelisXmlName.KEY);
		RightValue role = getRightValueAttribute(root, CamelisXmlName.ROLE);
		Date expires = getDateAttribute(root, CamelisXmlName.EXPIRES);
		return new CamelisUser(key, role, expires);
	}

	public LisIncrement getIncrement(Element root) throws PortalisException,
			RequestException {
		checkElement(root, CamelisXmlName.INCR);
		String name = getAttribute(root, XmlIdentifier.NAME());
		String sCard = getAttribute(root, CamelisXmlName.PROPERTY_CARD);
		String isLeaf = getAttribute(root, CamelisXmlName.PROPERTY_IS_LEAF);
		int card = Integer.parseInt(sCard);
		return new LisIncrement(name, card, isLeaf);
	}

	public LisIntent getLisIntent(Element root) throws PortalisException,
			RequestException {
		LisIntent res = new LisIntent();
		checkElement(root, res.getXmlName());

		List<Element> propertyElems = getChildElements(root,
				CamelisXmlName.PROPERTY);
		for (Element e : propertyElems) {
			res.add(getProperty(e));
		}
		return res;
	}

	public LisIncrementSet getLisIncrementSet(Element root)
			throws PortalisException, RequestException {
		LisIncrementSet res = new LisIncrementSet();
		checkElement(root, res.getXmlName());
		List<Element> incrs = getChildElements(root, CamelisXmlName.INCR);
		for (Element e : incrs) {
			res.add(getIncrement(e));
		}
		return res;
	}

	public PropertyTree getPropertyTree(Element root) throws PortalisException {
		PropertyTree tree = new PropertyTree();
		tree.setFeature(getAttribute(root, CamelisXmlName.FEATURE));
		tree.setOids(getStringElement(root, CamelisXmlName.OIDS));
		List<Element> tabE = getChildElements(root,
				CamelisXmlName.PROPERTY_TREE);
		if (!tabE.isEmpty()) {
			PropertyTree[] tab = new PropertyTree[tabE.size()];
			int i = 0;
			for (Element e : tabE) {
				tab[i] = getPropertyTree(e);
				i++;
			}
			tree.setChildren(tab);
		}
		return tree;
	}

}
