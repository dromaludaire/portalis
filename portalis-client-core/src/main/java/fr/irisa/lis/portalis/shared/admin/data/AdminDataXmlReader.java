package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

public class AdminDataXmlReader extends AdminXmlReader implements
		AdminDataReaderInterface<Element> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AdminDataXmlReader.class.getName());

	static private AdminDataXmlReader instance;

	public static AdminDataXmlReader getInstance() {
		if (instance == null) {
			instance = new AdminDataXmlReader();
		}
		return instance;
	}

	public AdminDataXmlReader() {
		super();
	}

	public Property getProperty(Element root) throws PortalisException,
			RequestException {
		return getProperty(root, CamelisXmlName.FEATURE);
	}

	// TODO remove after switch to LisIncrement and LisFeature
	public Property getProperty(Element root, String rootName)
			throws PortalisException, RequestException {
		checkElement(root, rootName);
		String name = getAttribute(root, XmlIdentifier.NAME());
		Property res = new Property(name);
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getPortalisSite(org.dom4j.Element)
	 */
	@Override
	public Site getSite(Element root) throws RequestException,
			PortalisException {
		Site site = new Site();
		checkElement(root, XmlIdentifier.SITE());
		setSite(root, site);
		return site;
	}

	@Override
	public void setSite(Element root, Site site) throws RequestException,
			PortalisException {
		
		site.setSparqlEndPoints(getSparqlApplication(getChildElement(root, XmlIdentifier.SPARQL_APPLICATION())));
		
		List<Element> appliElems = getChildElements(root,
				XmlIdentifier.LIS_APPLICATION());
		for (Element applicationElem : appliElems) {
			site.addApplication(getLisApplication(applicationElem));
		}

		Map<String, PasswordUser> users = site.getUsersMap();
		NodeList listPasswordUsers = root.getElementsByTagName(XmlIdentifier
				.STANDARD_USER());
		for (int i = 0; i < listPasswordUsers.getLength(); i++) {
			StandardUser user = getStandardUser((Element) listPasswordUsers
					.item(i));
			users.put(user.getEmail(), user);
		}

		NodeList listUsers = root.getElementsByTagName(XmlIdentifier
				.SUPER_USER());
		for (int i = 0; i < listUsers.getLength(); i++) {
			SuperUser user = getSuperUser((Element) listUsers.item(i));
			users.put(user.getEmail(), user);
		}

		Map<String, Role> roles = site.getRolesMap();
		NodeList listRoles = root.getElementsByTagName(XmlIdentifier.ROLE());
		for (int i = 0; i < listRoles.getLength(); i++) {
			Role prop = getRightProperty((Element) listRoles.item(i));
			roles.put(prop.getUserEmail() + "::" + prop.getServiceFullName(),
					prop);
		}
	}

	@Override
	public ActiveSite getActiveSite(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.ACTIVE_SITE());
		ActiveSite site = new ActiveSite();
		setSite(root, site);

		List<Element> serviceElems = getChildElements(root,
				XmlIdentifier.CAMELIS());
		for (Element serviceElem : serviceElems) {
			site.addActiveLisService(getActiveLisService(serviceElem));
		}
		return site;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getApplication(org.dom4j.Element)
	 */
	@Override
	public LisApplication getLisApplication(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.LIS_APPLICATION());
		LisApplication result = new LisApplication();
		return setLisApplication(root, result);
	}

	private LisApplication setLisApplication(Element root, LisApplication result)
			throws PortalisException, RequestException {
		result.setId(getStringAttribute(root, XmlIdentifier.APPLICATION_ID()));
		List<LisServiceCore> services = new ArrayList<LisServiceCore>();
		List<Element> serviceElems = getChildElements(root,
				XmlIdentifier.SERVICE_CORE());
		for (Element serviceElem : serviceElems) {
			services.add(getServiceCore(serviceElem));
		}
		result.setServices(services);
		return result;
	}

	@Override
	public SparqlApplication getSparqlApplication(Element root)
			throws RequestException, PortalisException {
		checkElement(root, XmlIdentifier.SPARQL_APPLICATION());
		SparqlApplication result = new SparqlApplication();
		List<SparqlService> services = new ArrayList<SparqlService>();
		List<Element> serviceElems = getChildElements(root,
				XmlIdentifier.SPARQL_SERVICE());
		for (Element serviceElem : serviceElems) {
			services.add(getSparqlService(serviceElem));
		}
		result.setServices(services);

		return result;
	}

	@Override
	public ActiveLisService getActiveLisService(Element root)
			throws PortalisException {
		ActiveLisService result = new ActiveLisService();
		result.setLastUpdate(getDateAttribute(root, XmlIdentifier.LAST_UPDATE()));
		result.setActivationDate(getDateAttribute(root,
				XmlIdentifier.ACTIVATION_DATE()));
		result.setCreator(getStringAttribute(root, XmlIdentifier.CREATOR()));
		result.setPid(getIntAttribute(root, XmlIdentifier.PID()));
		result.setNbObject(getIntAttribute(root, XmlIdentifier.NB_OBJECT()));
		result.setActiveId(getStringAttribute(root,
				XmlIdentifier.ACTIVE_SERVICE_ID()));
		String host = root.getAttribute(XmlIdentifier.HOST());
		if (host != null && host.length() > 0) {
			result.setHost(host);
			result.setPort(getIntAttribute(root, XmlIdentifier.PORT()));
		}
		List<Element> features = getChildElements(root, XmlIdentifier.FEATURE());
		for (Element feature : features) {
			result.addFeature(getStringAttribute(feature, XmlIdentifier.NAME()));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getPortActif(org.dom4j.Element)
	 */
	@Override
	public PortActif getPortActif(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.PORT_ACTIF());
		int port = getIntAttribute(root, XmlIdentifier.PORT());
		int pid = getIntAttribute(root, XmlIdentifier.PID());
		String server = getStringAttribute(root, XmlIdentifier.HOST());
		return new PortActif(server, port, pid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getServiceCore(org.dom4j.Element)
	 */
	@Override
	public LisServiceCore getServiceCore(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.SERVICE_CORE());
		LisServiceCore reponse = null;
		String serviceId = getStringAttribute(root,
				XmlIdentifier.SERVICE_NAME());
		RightValue rightValue = getRightValueAttribute(root,
				XmlIdentifier.ROLE());
		String appliId = getStringAttribute(root,
				XmlIdentifier.APPLICATION_ID());

		RequestLanguage reqLanguage = getRequestLanguageAttribute(root);
		switch (reqLanguage) {
		case CAMELIS :
			reponse = new CamelisServiceCore(appliId, serviceId, rightValue);
			break;
		default :
			String mess = "Langage de requête "+reqLanguage.toString()+" inconnu pour un système Lis";
			throw new PortalisException(mess);
		}


		reponse.setUiPreference(getUIPreference(getChildElement(root,
				XmlIdentifier.UI_PREFERENCE(), true)));

		return reponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getLastUdate(org.dom4j.Element)
	 */
	@Override
	public Date getLastUdate(Element root) throws PortalisException {
		return getDateAttribute(root, XmlIdentifier.LAST_UPDATE());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#getSession
	 * (org.dom4j.Element)
	 */
	private List<String> getCookies(Element root) {
		NodeList cookiesElems = root.getElementsByTagName(XmlIdentifier
				.COOKIE());
		List<String> cookies = new ArrayList<String>();
		for (int i = 0; i < cookiesElems.getLength(); i++) {
			Element cookieElem = (Element) cookiesElems.item(i);
			cookies.add(cookieElem.getTextContent());
		}
		return cookies;
	}

	public ActiveUser getActiveUser(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.ACTIF_USER());
		ActiveUser activeUser = new ActiveUser();
		setUserDataChild(root, activeUser);
		activeUser.setHttpSessionId(getStringAttribute(root,
				XmlIdentifier.ACTIF_USER_KEY()));
		activeUser.setCookies(getCookies(root));

		return activeUser;
	}

	private void setUserDataChild(Element root, UserDataContent activeUser)
			throws PortalisException, RequestException {
		List<Element> list = getChildElements(root);
		if (list.size() == 0 || list.size() > 2) {
			String mess = "incorrect activeUser element : "
					+ DOMUtil.prettyXmlString(root);
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
		activeUser.setUserCore(getUserData((Element) list.get(0)));
	}

	@Override
	public UserData getUserData(Element root) throws PortalisException,
			RequestException {
		if (root.getNodeName().equals(XmlIdentifier.STANDARD_USER())) {
			StandardUser user = getStandardUser(root);
			return user;
		} else if (root.getNodeName().equals(XmlIdentifier.ANONYMOUS_USER())) {
			AnonymousUser user = getAnonymousUser(root);
			return user;
		} else if (root.getNodeName().equals(XmlIdentifier.SUPER_USER())) {
			PasswordUser user = getSuperUser(root);
			return user;
		}

		return null;
	}

	@Override
	public SuperUser getSuperUser(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.SUPER_USER());
		SuperUser user = new SuperUser();
		setUserData(user, root);
		user.setPassword(getStringAttribute(root, XmlIdentifier.PASSWORD()));
		return user;
	}

	@Override
	public AnonymousUser getAnonymousUser(Element root)
			throws PortalisException, RequestException {
		checkElement(root, XmlIdentifier.ANONYMOUS_USER());
		String pseudo = getStringAttribute(root, XmlIdentifier.PSEUDO());
		AnonymousUser user = new AnonymousUser(pseudo);
		return user;
	}

	@Override
	public StandardUser getStandardUser(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.STANDARD_USER());
		StandardUser user = new StandardUser();
		setUserData(user, root);
		user.setPortalisADminRole(getRightValueAttribute(root,
				XmlIdentifier.PORTALIS_ADMIN_ROLE()));
		user.setPassword(getStringAttribute(root, XmlIdentifier.PASSWORD()));
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getRightPropertyForUser(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public Role getRightPropertyForUser(Element root, String userEmail)
			throws PortalisException, RequestException {
		checkElement(root, XmlIdentifier.ROLE());
		Role prop = new Role();
		prop.setUserEmail(userEmail);
		prop.setServiceFullName(getStringAttribute(root,
				XmlIdentifier.SERVICE_NAME()));
		prop.setRight(getRightValueAttribute(root, XmlIdentifier.VALUE()));
		return prop;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getRightProperty(org.dom4j.Element)
	 */
	@Override
	public Role getRightProperty(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.ROLE());
		String userEmail = getStringAttribute(root, XmlIdentifier.EMAIL());
		return getRightPropertyForUser(root, userEmail);
	}

	private void setUserData(UserData user, Element root)
			throws PortalisException {
		user.setEmail(getStringAttribute(root, XmlIdentifier.EMAIL()));
		user.setPseudo(getStringAttribute(root, XmlIdentifier.PSEUDO()));
	}

	@Override
	public LoginResult getLoginResult(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.LOGIN_RESULT());
		LoginResult result = new LoginResult();
		result.setLoginErr(getLoginErrAttribute(root, XmlIdentifier.STATUS()));
		setUserDataChild(root, result);
		return result;
	}

	/*
	 * private List<String> hiddenAttribute = new ArrayList<String>(); private
	 * Map<String, String> extraColumn = new HashMap<String, String>(); private
	 * boolean hasPictures = false;
	 * 
	 * @see fr.irisa.lis.portalis.shared.admin.data.AdminDataReaderInterface#
	 * getUIPreference(java.lang.Object)
	 */
	@Override
	public UIPreference getUIPreference(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.UI_PREFERENCE());
		UIPreference uiPreference = new UIPreference();
		uiPreference.setMinimumSupport(getIntAttribute(root,
				XmlIdentifier.MINIMUM_SUPPORT()));
		uiPreference.setEntryShown(getIntAttribute(root,
				XmlIdentifier.ENTRY_SHOWN()));
		uiPreference.setHasPictures(getBooleanAttribute(root,
				XmlIdentifier.HAS_PICTURE()));
		List<Element> hiddenAttrs = getChildElements(root,
				XmlIdentifier.HIDDEN_ATTRIBUTE());
		for (Element hiddenAttr : hiddenAttrs) {
			uiPreference.addHiddenAttribute(hiddenAttr
					.getAttribute(XmlIdentifier.NAME()));
		}
		List<Element> extraColumns = getChildElements(root,
				XmlIdentifier.EXTRA_COLUMN());
		for (Element extraColumn : extraColumns) {
			uiPreference.addExtraColumn(
					extraColumn.getAttribute(XmlIdentifier.NAME()),
					extraColumn.getAttribute(XmlIdentifier.VALUE()));
		}
		return uiPreference;
	}

	@Override
	public SparqlService getSparqlService(Element root)
			throws PortalisException, RequestException {
		checkElement(root, XmlIdentifier.SPARQL_SERVICE());
		SparqlService sparqlService = new SparqlService();
		sparqlService.setServiceName(getStringAttribute(root,
				XmlIdentifier.SERVICE_NAME()));
		sparqlService.setUrl(getStringAttribute(root,
				XmlIdentifier.URL()));
		List<Element> queries = getChildElements(root,
				XmlIdentifier.QUERY());
		for (Element query : queries) {			
			sparqlService.addQuery(getQuery(query));
		}
		return sparqlService;
	}

	@Override
	public Query getQuery(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.QUERY());
		Query query = new Query();
		
		query.setId(getStringAttribute(root, XmlIdentifier.ID()));
		query.setLabel(getStringAttribute(root, XmlIdentifier.LABEL()));
		query.setReqLanguage(getRequestLanguageAttribute(root));
		
		query.setQuery(getCharacterDataFromElement(
				getChildElement(root, XmlIdentifier.VALUE())));
		
		List<Element> params = getChildElements(root,
				XmlIdentifier.PARAM());
		for (Element param : params) {
			String name = getStringAttribute(param, XmlIdentifier.NAME());
			String val = getStringAttribute(param, XmlIdentifier.VALUE());
			query.addParam(name, val);
		}
		
		return query;
	}
	
	public RequestLanguage getRequestLanguageAttribute(Element root)
			throws PortalisException {
		String reqLanguageAttr = getStringAttribute(root,
				XmlIdentifier.REQUEST_LANGUAGE());
		RequestLanguage reqLanguage = null;
		try {
			reqLanguage = RequestLanguage
					.valueOf(reqLanguageAttr.toUpperCase());
		} catch (IllegalArgumentException e) {
			String mess = "Valeur d'attribut "
					+ XmlIdentifier.REQUEST_LANGUAGE()
					+ " incorrecte dans une description de serviceCore : "+reqLanguageAttr;
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
		// never return null
		return reqLanguage;
	}

}
