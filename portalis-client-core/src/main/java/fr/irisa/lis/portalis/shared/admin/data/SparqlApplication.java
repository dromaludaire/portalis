package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.jquery.TreetableNode;
import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class SparqlApplication extends Application<SparqlService> implements
		AdminDataObject {

	public SparqlApplication() {
		super();
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof SparqlApplication))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final SparqlApplication that = (SparqlApplication) aThat;
		return new EqualsBuilder()
				.
				// appendSuper(super.equals(aThat)).
				append(this.services, that.services).append(this.id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// appendSuper(super.hashCode()).
				append(services).append(id).toHashCode();
	}

	@Override
	public void setId(String id) {
		throw new UnauthorizedException();
	}

	@Override
	public String getId() {
		return "sparqlApplication";
	}

	@Override
	public TreetableNode toTreetableNode() {
		final String[] noProps = {"language", "url"};
		TreetableNode result = new TreetableNode(id);
		for (SparqlService sparqlService : services) {
			TreetableNode child = new TreetableNode(sparqlService.getServiceName());
			child.addProperty("language", sparqlService.getReqLanguage().name());
			child.addProperty("url", sparqlService.getUrl());
			for (Query query : sparqlService.getQuery()) {
				TreetableNode queryNode = new TreetableNode(String.format("<span><em><b>%s</b></em> <a href=\".\" onclick=\"return setQuery('%s','%s')\">%s</a></span>", 
						query.getId(),
						sparqlService.getServiceName(),
						query.getId(),
						query.getLabel()));
				child.addChild(queryNode);
				child.addProperty("label", query.getLabel());
				child.addProperty("query",query.getQuery());
				queryNode.setColspan(3);
				queryNode.setNoProps(noProps);
			}
			result.addChild(child);
		}
		return result;
	}

}
