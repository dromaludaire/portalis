package fr.irisa.lis.portalis.shared.camelis.data;

import java.util.ArrayList;
import java.util.Arrays;

import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class LisIntent implements CamelisDataObject {

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected ArrayList<Property> properties;
	private static String xmlName = CamelisXmlName.INTENT;

	public LisIntent() {
		super();
		this.properties = new ArrayList<Property>();
	}

	public LisIntent(Property[] props) {
		super();
		this.setProperties(props);
	}

	public void setProperties(Property[] props) {
		this.properties = new ArrayList<Property>(Arrays.asList(props));
	}

	public ArrayList<Property> getProperties() {
		return this.properties;
	}
	
	public Property[] toArray() {
		return properties.toArray(new Property[properties.size()]);
	}

	public void add(Property p) {
		this.properties.add(p);
	}
	
	public int card() {
		return this.properties.size();
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisIntent) ) return false;

		  final LisIntent that = (LisIntent)aThat;
		  return this.properties.size() == that.properties.size()
				  && this.properties.containsAll(that.properties);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(properties).
				toHashCode();
	}

	public String getXmlName() {
		return xmlName;
	}
		

}
