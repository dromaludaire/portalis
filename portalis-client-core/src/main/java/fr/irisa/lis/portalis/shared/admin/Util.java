package fr.irisa.lis.portalis.shared.admin;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.LogManager;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.shared.camelis.CamelisException;

public class Util {

	static final Logger LOGGER = LoggerFactory.getLogger(Util.class
			.getName());

	static final String simpleFormat = "dd MMM yyyy 'at' HH:mm:ss";
	static final SimpleDateFormat prettyFormatter = new SimpleDateFormat(
			simpleFormat);

	private static String PARSE_NO_MILLIS = AdminProprietes.portalis
			.getProperty("admin.parseNoMillis");
	private static String NULL_DATE = AdminProprietes.ioConstants
			.getProperty("NULL_DATE");

	private static String fmtNoMillis = "yyyy-MM-dd'T'HH:mm:ssZ";
	private static DateTimeFormatter formaterNoMillis = DateTimeFormat
			.forPattern(fmtNoMillis);

	private static boolean IS_PARSE_NO_MILLIS = (PARSE_NO_MILLIS == null
			|| PARSE_NO_MILLIS.equals("false") ? false : true);

	public static Date parseNoMillis(String text) {
		return formaterNoMillis.parseDateTime(text).toDate();
	}

	static public String writeDate(Date date) {
		if (date == null)
			return XmlIdentifier.NONE();
		else
			return format(date);
	}

	static public String prettyDate(Date date) {
		if (date == null)
			return XmlIdentifier.NONE();
		else
			return prettyFormatter.format(date);
	}

	static public Date readDate(String s) throws PortalisException {
		if (s == null) {
			throw new PortalisException("impossible de convertir la date");
		} else if (s.equals(XmlIdentifier.NONE())) {
			return null;
		} else {
			return Util.parse(s);
		}
	}

	/**
	 * ISO8601/RFC333true9 date formatter
	 * 
	 * @param d
	 *            the date to be formatted
	 * @return the formatted string representing the date
	 */
	private static String format(Date d) {
		DateTime dt = new DateTime(d);
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		return fmt.print(dt);
	}

	/**
	 * ISO8601/RFC3339 date parser
	 * 
	 * @param text
	 *            the text to parse
	 * @return the date represented by the string
	 */
	private static Date parse(String text) {
		if (text.equals(NULL_DATE))
			return null;
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Date result = null;
		try {
			result = fmt.parseDateTime(text).toDate();
		} catch (IllegalArgumentException e) {
			if (IS_PARSE_NO_MILLIS) {
				LOGGER.debug("parseNoMillis(" + text + ")");
				result = parseNoMillis(text);
			} else {
				throw e;
			}

		}
		return result;
	}

	public static void clientInitLogPropertiesFile() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		String fileName = "logging.properties";
		InputStream inputStream = cl.getResourceAsStream(fileName);

		Util.initLogPropertiesFile(inputStream, "classpath(" + fileName + ")",
				"java.util.logging.FileHandler.level");

		LOGGER.info("On vient de lire le fichier des propriétes de log pour le client");
	}

	public static void initLogPropertiesFile(InputStream inputStream,
			String message, String propName) {
		if (inputStream == null) {
			String mess = "pas de fichier de log";
			LOGGER.error(mess);
			throw new PortalisError(mess);
		}
		try {
			LogManager.getLogManager().readConfiguration(inputStream);
			String prop = LogManager.getLogManager().getProperty(propName);
			LOGGER.info(new StringBuffer(
					"logging.properties have been correctly read from '")
					.append(message).append("' ").append(propName)
					.append(" = ").append(prop)
					.append("\n==============================> start log")
					.toString());

		} catch (final Exception e) {
			String mess = "Could not load logging.properties file "
					+ e.getMessage();
			LOGGER.error(mess, e);
			throw new PortalisError(mess);
		}
	}

	/**
	 * Checks to see if a specific port is available.
	 * 
	 * @param port
	 *            the port to check for availability
	 * @throws CamelisException
	 */
	public static boolean available(int port) {

		ServerSocket ss = null;
		DatagramSocket ds = null;
		try {
			ss = new ServerSocket(port);
			ss.setReuseAddress(true);
			ds = new DatagramSocket(port);
			ds.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (ds != null) {
				ds.close();
			}

			if (ss != null) {
				try {
					ss.close();
				} catch (IOException e) {
					/* should not be thrown */
				}
			}
		}

		return false;

	}
}
