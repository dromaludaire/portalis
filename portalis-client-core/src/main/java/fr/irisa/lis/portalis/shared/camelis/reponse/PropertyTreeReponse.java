package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.data.PropertyTree;

@SuppressWarnings("serial")
public class PropertyTreeReponse extends LisReponse implements CamelisReponseObject   {

	protected PropertyTree tree;

	public PropertyTreeReponse() {
		super();
	}

	public PropertyTreeReponse(Err err) {
		super(err);
	}
	
	public void setTree(PropertyTree tree) {
		this.tree = tree;
	}

	public PropertyTree getTree() {
		return tree;
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PropertyTreeReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PropertyTreeReponse that = (PropertyTreeReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.tree, that.tree)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(tree).
				toHashCode();
	}

}
