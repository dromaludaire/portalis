package fr.irisa.lis.portalis.shared.admin;


@SuppressWarnings("serial")
public class PortalisException extends Exception implements
		java.io.Serializable {

	public PortalisException(String mess) {
		super(mess);
	}

	public PortalisException() {
	}

	public PortalisException(String mess, Throwable e) {
		super(mess, e);
	}

	public PortalisException(Throwable e) {
		super(e);
	}
}
