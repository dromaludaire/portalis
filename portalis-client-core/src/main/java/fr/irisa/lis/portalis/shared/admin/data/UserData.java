package fr.irisa.lis.portalis.shared.admin.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

@SuppressWarnings("serial")
public abstract class UserData implements AdminDataObject {

	private String email;
	private String pseudo;

	protected UserData() {
		super();
	}

	protected UserData(String email, String pseudo) {
		super();
		this.email = email;
		this.pseudo = pseudo;
	}

	@Override
	public abstract String toString();

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof UserData))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final UserData that = (UserData) aThat;
		return new EqualsBuilder()
				.
				// appendSuper(super.equals(aThat)).
				append(this.email, that.email).append(this.pseudo, that.pseudo)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// appendSuper(super.hashCode()).
				append(email).append(pseudo).toHashCode();
	}

	@NotNull
	@Size(min = 2, max = 50)
	public String getPseudo() {
		return this.pseudo;
	}

	@NotNull
	@Size(min = 3)
	@Pattern(regexp = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", message = "Ceci n'est pas une adresse email")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public abstract boolean isGeneralAdmin();

}