package fr.irisa.lis.portalis.shared.admin.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.BooleanReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.ServiceCoreReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VersionReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class AdminHttp {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AdminHttp.class.getName());

	public final static String WEBAPP_PATH = AdminProprietes.portalis
			.getProperty("admin.webappsDirPath");
	public static final String LOG_FILE_PATH = AdminProprietes.portalis
			.getProperty("admin.camelis.logFilePath");

	public static SiteReponse getSite(ActiveUser activeUser) {
		if (activeUser == null) {
			String mess = "activeUser "+ErrorMessages.TEMOIN_IS_MISSING;
			LOGGER.error(mess);
			return new SiteReponse(new Err(mess));
		}

		Element reponseElem;
		try {
			reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "getSite.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getSiteReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new SiteReponse(new Err(e));
		}

	}

	public static ActiveSiteReponse getActiveSite(ActiveUser activeUser) {
		if (activeUser == null) {
			String mess = "activeUser "+ErrorMessages.TEMOIN_IS_MISSING;
			LOGGER.error(mess);
			return new ActiveSiteReponse(new Err(mess));
		}

		Element reponseElem;
		try {
			reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "getActiveSite.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getActiveSiteReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new ActiveSiteReponse(new Err(e));
		}

	}

	public static SiteReponse clearLog(ActiveUser activeUser) {
		if (activeUser == null) {
			String mess = "erreur activeUser nulle";
			LOGGER.error(mess);
			return new SiteReponse(new Err(mess));
		}

		Element reponseElem;
		try {
			reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "clearLog.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getSiteReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new SiteReponse(new Err(e));
		}

	}

	public static LoginReponse login(String email, String password) {
		
		return login(email, password, null);
		
	}

		public static LoginReponse login(String email, String password, String maxInactiveInterval) {

		// appelée pour un test via http ou par les clients Java
		LOGGER.info(String.format("login(%s, %s)", email, password));

		try {
			String[][] httpReqArgs = null;

			if (maxInactiveInterval==null) {
				String[][] array = { { XmlIdentifier.EMAIL(), email },
					{ XmlIdentifier.PASSWORD(), password } };
				httpReqArgs = array;
			} else {
				String[][] array = { 
						{ XmlIdentifier.MAX_INACTIVE_INTERVAL(), maxInactiveInterval },
						{ XmlIdentifier.EMAIL(), email },
						{ XmlIdentifier.PASSWORD(), password } };
					httpReqArgs = array;
			}
			Element reponseElem = new HttpPortalisUrlConnection()
					.doFirstConnection("login.jsp", httpReqArgs);
			LOGGER.info("elem = " + DOMUtil.prettyXmlString(reponseElem));

			LoginReponse reponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLoginReponse(reponseElem);

			return reponse;
		} catch (PortalisException e) {
			String mess = "login error : " + e.getMessage();
			LOGGER.error(mess, e);
			return new LoginReponse(new Err(mess));
		} catch (Throwable t) {
			String mess = "login error";
			LOGGER.error(mess, t);
			return new LoginReponse(new Err(t));
		}
	}

		public static LoginReponse anonymousLogin() {
			return anonymousLogin(null);
			
		}
		
		public static LoginReponse anonymousLogin(String maxInactiveInterval) {

		// appelée pour un test via http ou par les clients Java
		LOGGER.debug("anonymousLogin()");

		try {
			Element reponseElem = null;
			if (maxInactiveInterval==null) {
				reponseElem = new HttpPortalisUrlConnection()
				.doFirstConnection("anonymousLogin.jsp");
			} else {
				String[][] httpReqArgs = { 
						{ XmlIdentifier.MAX_INACTIVE_INTERVAL(), maxInactiveInterval } };
				reponseElem = new HttpPortalisUrlConnection()
				.doFirstConnection("anonymousLogin.jsp", httpReqArgs);
			}

			LoginReponse reponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLoginReponse(reponseElem);

			return reponse;
		} catch (Throwable t) {
			Throwable e = t.getCause();
			String mess = t.getMessage();
			if (e == null) {
				e = t;
			} else {
				mess = mess + e.getMessage();
			}
			LOGGER.error(t.getMessage());
			return new LoginReponse(new Err(e));
		}
	}

	public static PortsActifsReponse getActifsPorts(ActiveUser activeUser) {
		if (activeUser == null) {
			String mess = "erreur activeUser nulle";
			LOGGER.error(mess);
			return new PortsActifsReponse(new Err(mess));
		}

		try {

			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "getActifsPorts.jsp");

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getPortsActifsReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PortsActifsReponse(new Err(e));
		}
	}

	public static PingReponse getActiveLisServicesOnPortalis(
			ActiveUser activeUser) {
		PingReponse pingReponse = new PingReponse();
		try {
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "getActiveCamelisServices.jsp");

			pingReponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getPingReponse(reponseElem);
			return pingReponse;

		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PingReponse(new Err(e));
		}
	}

	public static PidReponse getCamelisProcessId(ActiveUser activeUser,
			ActiveLisService service) {

		try {
			LOGGER.debug("getCamelisProcessId(" + activeUser + ", "
					+ service.getActiveId() + ")");
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, String.format("getCamelisProcessId.jsp?%s=%s",
							XmlIdentifier.PORT(), service.getPort()));

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getPidReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PidReponse(new Err(e));
		}
	}

	public static PidReponse getCamelisProcessId(ActiveUser activeUser,
			String serviceFullName) {
		LOGGER.debug("getCamelisProcessId(" + activeUser + ", "
				+ serviceFullName + ")");

		try {
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, String.format("getCamelisProcessId.jsp?%s=%s",
							XmlIdentifier.SERVICE_NAME(), serviceFullName));

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getPidReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PidReponse(new Err(e));
		}
	}

	public static PidReponse getCamelisProcessId(ActiveUser activeUser, int port) {
		LOGGER.debug("getCamelisProcessId(" + activeUser + ", " + port + ")");

		try {
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, String.format("getCamelisProcessId.jsp?%s=%d",
							XmlIdentifier.PORT(), port));

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getPidReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PidReponse(new Err(e));
		}
	}

	/**
	 * 
	 * @return l'information de version de Maven
	 */
	public static VersionReponse getImplementationVersion(ActiveUser activeUser) {
		return new VersionReponse(AdminHttp.class.getPackage()
				.getImplementationVersion());
	}

	public static VoidReponse logout(ActiveUser activeUser) {
		try {
			// appelée pour un test via http ou par les clients Java
			LOGGER.debug(String.format("logout(%s)", activeUser));
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "logout.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getVoidReponse(reponseElem);
		} catch (Throwable t) {
			String mess = t.getMessage();
			LOGGER.error(mess + CommonsUtil.stack2string(t));
			return new VoidReponse(new Err(mess));
		}

	}

	public static VoidReponse propertyFilesCheck(ActiveUser loginActiveUser) {
		try {
			LOGGER.debug(String.format("adminPropertyFilesCheckHttp(%s)",
					loginActiveUser));
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					loginActiveUser, "propertiesFileCheck.jsp");

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getVoidReponse(reponseElem);
		} catch (Throwable t) {
			String mess = t.getMessage();
			LOGGER.error(mess + CommonsUtil.stack2string(t));
			return new VoidReponse(new Err(mess));
		}
	}

	public static StartReponse startCamelis(ActiveUser activeUser,
			String serviceName) {
		return startCamelis(activeUser, serviceName, WEBAPP_PATH, LOG_FILE_PATH);
	}

	public static StartReponse startCamelis(ActiveUser activeUser,
			String serviceName, String webApplicationDataPath, String logFile) {
		String[][] httpReqArgs;
		httpReqArgs = new String[][] {
				{ XmlIdentifier.CREATOR(), activeUser.getUserCore().getEmail() },
				{ XmlIdentifier.KEY(), activeUser.getPortalisSessionId() },
				{ XmlIdentifier.SERVICE_NAME(), serviceName },
				{ XmlIdentifier.LOG(), logFile },
				{ XmlIdentifier.DATADIR(), webApplicationDataPath } };
		return startCamelis(activeUser, httpReqArgs);
	}

	public static StartReponse startCamelis(ActiveUser activeUser,
			String serviceName, int port) {
		return startCamelis(activeUser, serviceName, port, WEBAPP_PATH,
				LOG_FILE_PATH);
	}

	public static StartReponse startCamelis(ActiveUser activeUser,
			String serviceName, int port, String webApplicationDataPath,
			String logFile) {

		LOGGER.debug(String.format("startCamelis(%d, %s)\n%s))", port,
				serviceName, activeUser));

		String[][] httpReqArgs = new String[][] {
				{ XmlIdentifier.CREATOR(), activeUser.getUserCore().getEmail() },
				{ XmlIdentifier.PORT(), port + "" },
				{ XmlIdentifier.KEY(), activeUser.getPortalisSessionId() },
				{ XmlIdentifier.SERVICE_NAME(), serviceName },
				{ XmlIdentifier.LOG(), logFile },
				{ XmlIdentifier.DATADIR(), webApplicationDataPath } };

		return startCamelis(activeUser, httpReqArgs);
	}

	private static StartReponse startCamelis(ActiveUser activeUser,
			String[][] httpReqArgs) {
		StartReponse resultReponse = null;

		try {
			Element elem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "startCamelis.jsp", httpReqArgs);

			resultReponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getStartReponse(elem);
		} catch (Throwable e) {
			String mess = e.getMessage();
			LOGGER.error(mess);
			return new StartReponse(new Err(mess));
		}

		return resultReponse;
	}

	/*
	 * ------------------------------ stopCamelis
	 */
	/**
	 * stop all camelis activeUsers
	 * 
	 * @param activeUser
	 * @return
	 */
	public static PingReponse stopCamelis(ActiveUser activeUser,
			ActiveService activeService) {
		try {
			String[][] httpReqArgs = { { XmlIdentifier.ACTIVE_SERVICE_ID(),
				activeService.getActiveId() } };
			Element elem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "stopCamelis.jsp", httpReqArgs);
			return ClientConstants.CAMELIS_REPONSE_XML_READER
					.getPingReponse(elem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PingReponse(new Err(e));
		}
	}

	public static PingReponse killAllCamelisProcess(ActiveUser activeUser) {
		try {
			Element elem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "stopCamelis.jsp");
			return ClientConstants.CAMELIS_REPONSE_XML_READER
					.getPingReponse(elem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new PingReponse(new Err(e));
		}
	}

	public static VoidReponse clearActifUsers(ActiveUser activeUser)
			throws PortalisException, RequestException {
		Element elem = new HttpPortalisUrlConnection().doConnection(activeUser,
				"clearActifUsers.jsp");

		return ClientConstants.ADMIN_REPONSE_XML_READER.getVoidReponse(elem);
	}

	public static BooleanReponse portIsBusy(ActiveUser activeUser, int port) {
		try {
			String[][] httpReqArgs = { { XmlIdentifier.PORT(),
				port+"" } };
			Element elem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "portIsBusy.jsp", httpReqArgs);

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getBooleanReponse(elem);
		} catch (PortalisException e) {
			String mess = "portIsBusy error : " + e.getMessage();
			LOGGER.error(mess, e);
			return new BooleanReponse(new Err(mess));
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new BooleanReponse(new Err(e));
		}
	}
	
	public static ServiceCoreReponse getServiceCore(ActiveUser activeUser, String serviceId) {
		if (activeUser == null) {
			String mess = "activeUser "+ErrorMessages.TEMOIN_IS_MISSING;
			LOGGER.error(mess);
			return new ServiceCoreReponse(new Err(mess));
		}

		Element reponseElem;
		try {
			String[][] httpReqArgs = new String[][] {
					{ XmlIdentifier.ACTIVE_SERVICE_ID(), serviceId }};

			reponseElem = new HttpPortalisUrlConnection().doConnection(
					activeUser, "getServiceCore.jsp", httpReqArgs);
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getServiceCoreReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage());
			return new ServiceCoreReponse(new Err(e));
		}

	}



}
