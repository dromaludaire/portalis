package fr.irisa.lis.portalis.shared.admin.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ActiveServiceIdValidator implements PortalisValidator<String> {

	private Pattern pattern;
	private Matcher matcher;
	private static ActiveServiceIdValidator instance;

	private static final String SERVICEID_PATTERN = "^[\\.\\-_A-Za-z0-9-]+:[0-9]+::[\\._A-Za-z0-9-]+:[\\.\\-_A-Za-z0-9-]+$";

	/**
	 * Singleton class use getInstance() to get the singleton instance of this class
	 */
	private ActiveServiceIdValidator() {
		pattern = Pattern.compile(SERVICEID_PATTERN);
	}
	
	/**
	 * 
	 * @return the singleton instance of this class validator
	 */
	public static ActiveServiceIdValidator getInstance() {
		if (instance==null)
			instance = new ActiveServiceIdValidator();
		return instance;
	}

	/**
	 * Validate ServiceId with ServiceId_PATTERN regular expression
	 * 
	 * @param serviceId
	 *            ServiceId for validation
	 * @return true valid ServiceId, false invalid ServiceId
	 */
	public boolean validate(final String serviceId) {
		if (serviceId == null)
			return false;

		matcher = pattern.matcher(serviceId);
		return matcher.matches() && Integer.parseInt(serviceId.split("::")[0].split(":")[1]) != 0;

	}
}
