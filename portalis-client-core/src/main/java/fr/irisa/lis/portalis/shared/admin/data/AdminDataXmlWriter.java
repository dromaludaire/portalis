package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMWriter;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.PortalisError;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

public class AdminDataXmlWriter extends DOMWriter implements
		AdminDataWriterInterface<Element> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AdminDataXmlWriter.class.getName());

	static private AdminDataXmlWriter instance;

	public static AdminDataXmlWriter getInstance() {
		if (instance == null) {
			instance = new AdminDataXmlWriter();
		}
		return instance;
	}

	protected static Element buildRootElement(String name, VoidReponse reponse) {
		Element rootElem = createRootElement(name);
		if (reponse.isOk()) {
			addAttribute(rootElem, XmlIdentifier.STATUS(), XmlIdentifier.OK);
		} else {
			List<String> messages = reponse.getMessages();
			errors(rootElem, messages);
			return rootElem;
		}
		return rootElem;
	}

	protected static Element error(Element reponseElem, String message) {
		reponseElem.setAttribute(XmlIdentifier.STATUS(), XmlIdentifier.ERROR);
		addTextChild(reponseElem, XmlIdentifier.MESSAGE(), message);
		LOGGER.warn("visitor error = " + message);
		return reponseElem;
	}

	protected static Element errors(Element reponseElem, List<String> messages) {
		reponseElem.setAttribute(XmlIdentifier.STATUS(), XmlIdentifier.ERROR);
		addAttribute(reponseElem, XmlIdentifier.STATUS(), XmlIdentifier.ERROR);
		for (String mess : messages) {
			addTextChild(reponseElem, XmlIdentifier.MESSAGE(),
					mess == null ? "null" : mess);
		}
		LOGGER.warn("visitor errors = "
				+ Arrays.toString(messages.toArray(new String[messages.size()])));
		return reponseElem;
	}

	@Override
	public Element visit(Property property) {
		// TODO check if Property should be visited or not
		Element elem = DOMWriter.createRootElement(CamelisXmlName.PROPERTY);
		DOMWriter.addAttribute(elem, XmlIdentifier.NAME(), property.getName());
		return elem;
	}

	@Override
	public Element visit(LoginBean loginBean) {
		Element elem = createRootElement(XmlIdentifier.USER());
		addAttribute(elem, XmlIdentifier.EMAIL(), loginBean.getId());
		addAttribute(elem, XmlIdentifier.PASSWORD(),
				CommonsUtil.sha(loginBean.getPassword()));
		return elem;
	}

	@Override
	public Element visit(LisApplication application) {
		Element rootElement = createRootElement(XmlIdentifier.LIS_APPLICATION());
		return setLisApplication(rootElement, application);
	}

	public Element setLisApplication(Element rootElement,
			LisApplication application) {

		// set attribute to status element
		addAttribute(rootElement, XmlIdentifier.APPLICATION_ID(),
				application.getId());

		for (LisServiceCore service : application.getServices()) {
			addChild(rootElement, visit(service));
		}
		return rootElement;
	}

	@Override
	public Element visit(SparqlApplication sparqlApplication) {
		Element rootElement = createRootElement(XmlIdentifier
				.SPARQL_APPLICATION());

		for (SparqlService service : sparqlApplication.getServices()) {
			addChild(rootElement, visit(service));
		}
		return rootElement;
	}

	@Override
	public Element visit(SiteInterface site) {
		Element siteElem = createRootElement(XmlIdentifier.SITE());
		setSiteInterface(siteElem, site);
		return siteElem;
	}

	private void setSiteInterface(Element siteElem, SiteInterface site) {

		addChild(siteElem, visit(site.getSparqlEndPoints()));

		for (LisApplication appli : site.getApplications()) {
			addChild(siteElem, visit(appli));
		}

		for (UserData user : site.getUsers()) {
			addChild(siteElem, getUserData(user));
		}

		for (Role role : site.getRoles()) {
			addChild(siteElem, visit(role));
		}
	}

	@Override
	public Element visit(ActiveSite activeSite) {
		Element siteElem = createRootElement(XmlIdentifier.ACTIVE_SITE());
		setSiteInterface(siteElem, activeSite);
		List<ActiveLisService> activeServices = activeSite
				.getActiveLisServices();
		for (ActiveLisService activeLisService : activeServices) {
			addChild(siteElem, visit(activeLisService));
		}
		return siteElem;
	}

	@Override
	public Element visit(boolean bool) {
		Element elem = createRootElement(XmlIdentifier.BOOLEAN());

		elem.setTextContent(Boolean.toString(bool));
		return elem;
	}

	@Override
	public Element visit(String s) {
		String ss = s;
		if (ss == null) {
			ss = "";
		}
		Element elem = createRootElement(XmlIdentifier.STRING());

		elem.setTextContent(ss);
		return elem;
	}

	@Override
	public Element visit(Integer[] intTable) {
		Element elem = createRootElement(XmlIdentifier.INTEGER_LIST());
		for (Integer integer : intTable) {
			addTextChild(elem, XmlIdentifier.INTEGER(), integer + "");
		}
		return null;
	}

	@Override
	public Element visit(Role userRights) {
		Element right = createRootElement(XmlIdentifier.ROLE());
		addAttribute(right, XmlIdentifier.EMAIL(), userRights.getUserEmail());
		addAttribute(right, XmlIdentifier.SERVICE_NAME(),
				userRights.getServiceFullName());
		addAttribute(right, XmlIdentifier.VALUE(), userRights.getRight()
				.toString());
		return right;
	}

	@Override
	public Element visit(ActiveLisService lisService) {
		Element reponseElem = createRootElement(XmlIdentifier.CAMELIS());
		setActiveService(reponseElem, lisService);
		addAttribute(reponseElem, XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(lisService.getLastUpdate()));
		addAttribute(reponseElem, XmlIdentifier.ACTIVATION_DATE(),
				Util.writeDate(lisService.getActivationDate()));
		addAttribute(reponseElem, XmlIdentifier.CREATOR(),
				lisService.getCreator());
		addAttribute(reponseElem, XmlIdentifier.PID(), lisService.getPid() + "");
		addAttribute(reponseElem, XmlIdentifier.NB_OBJECT(),
				lisService.getNbObject() + "");
		addAttribute(reponseElem, XmlIdentifier.ACTIVE_SERVICE_ID(),
				lisService.getActiveId());
		addAttribute(reponseElem, XmlIdentifier.HOST(), lisService.getHost());
		addAttribute(reponseElem, XmlIdentifier.PORT(), lisService.getPort()
				+ "");
		for (String feature : lisService.getFeatures()) {
			Element child = DOMWriter.createAddChild(reponseElem,
					XmlIdentifier.FEATURE());
			child.setAttribute(XmlIdentifier.NAME(), feature);
		}
		return reponseElem;
	}

	@Override
	public Element visit(PortActif portActif) {
		Element elem = createRootElement(XmlIdentifier.PORT_ACTIF());
		addAttribute(elem, XmlIdentifier.PORT(), portActif.getPort() + "");
		addAttribute(elem, XmlIdentifier.PID(), portActif.getPid() + "");
		addAttribute(elem, XmlIdentifier.HOST(), portActif.getServer());
		return elem;
	}

	@Override
	public Element visit(PortalisService portalisService) {
		Element reponseElem = createRootElement(XmlIdentifier
				.PORTALIS_SERVICE());
		addAttribute(reponseElem, XmlIdentifier.ACTIVE_SERVICE_ID(),
				portalisService.getActiveId());
		setActiveService(reponseElem, portalisService);
		return reponseElem;
	}

	protected Element setActiveService(Element reponseElem,
			ActivePortalisService lisService) {
		addAttribute(reponseElem, XmlIdentifier.CREATOR(),
				lisService.getCreator());
		addAttribute(reponseElem, XmlIdentifier.PORT(), lisService.getPort()
				+ "");
		addAttribute(reponseElem, XmlIdentifier.HOST(), lisService.getHost());

		addAttribute(reponseElem, XmlIdentifier.ACTIVATION_DATE(),
				Util.writeDate(lisService.getActivationDate()));
		return reponseElem;
	}

	@Override
	public Element visit(ActiveUser user) {
		Element elem = createRootElement(XmlIdentifier.ACTIF_USER());
		addAttribute(elem, XmlIdentifier.ACTIF_USER_KEY(),
				user.getHttpSessionId());
		addChild(elem, getUserData(user.getUserCore()));

		for (String cookie : user.getCookies()) {
			addTextChild(elem, XmlIdentifier.COOKIE(), cookie);
		}
		return elem;
	}

	@Override
	public Element visit(ActiveUser[] loggedInUsers) {
		Element elem = createRootElement(XmlIdentifier.ACTIF_USER_LIST());
		for (ActiveUser user : loggedInUsers) {
			addChild(elem, visit(user));
		}
		return elem;
	}

	private Element getUserData(UserData userData) {
		Element userElement = null;
		if (userData instanceof StandardUser) {
			userElement = visit((StandardUser) userData);
		} else if (userData instanceof AnonymousUser) {
			userElement = visit((AnonymousUser) userData);
		} else if (userData instanceof SuperUser) {
			userElement = visit((SuperUser) userData);
		} else {
			// should not append
			throw new PortalisError("not possible");
		}
		return userElement;
	}

	@Override
	public Element visit(StandardUser user) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier
				.STANDARD_USER());
		addAttribute(elem, XmlIdentifier.PASSWORD(), user.getPassword());
		addAttribute(elem, XmlIdentifier.PORTALIS_ADMIN_ROLE(), user
				.getPortalisAdminRole().name());
		setUserData(user, elem);

		return elem;
	}

	@Override
	public Element visit(AnonymousUser user) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier
				.ANONYMOUS_USER());
		addAttribute(elem, XmlIdentifier.PSEUDO(), user.getPseudo());
		return elem;
	}

	@Override
	public Element visit(SuperUser user) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier.SUPER_USER());
		addAttribute(elem, XmlIdentifier.PASSWORD(), user.getPassword());
		setUserData(user, elem);
		return elem;
	}

	private void setUserData(UserData user, Element elem) {
		addAttribute(elem, XmlIdentifier.EMAIL(), user.getEmail());
		addAttribute(elem, XmlIdentifier.PSEUDO(), user.getPseudo());
	}

	@Override
	public Element visit(StandardUser[] users) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier.USER_LIST());
		for (StandardUser user : users) {
			addChild(elem, visit(user));
		}
		return elem;
	}

	@Override
	public Element visit(LisServiceCore service) {
		Element elem = createRootElement(XmlIdentifier.SERVICE_CORE());
		addAttribute(elem, XmlIdentifier.SERVICE_NAME(),
				service.getServiceName());
		addAttribute(elem, XmlIdentifier.APPLICATION_ID(),
				service.getAppliName());
		addAttribute(elem, XmlIdentifier.ROLE(), service.getDefaultRightValue()
				.name());
		addChild(elem, visit(service.getUiPreference()));
		addAttribute(elem, XmlIdentifier.REQUEST_LANGUAGE(), service
				.getReqLanguage().toString());

		return elem;
	}

	@Override
	public Element visit(LoginResult loginResult) {
		Element reponseElem = createRootElement(XmlIdentifier.LOGIN_RESULT());
		addAttribute(reponseElem, XmlIdentifier.STATUS(), loginResult
				.getLoginErr().toString());
		addChild(reponseElem, getUserData(loginResult.getUserCore()));

		return reponseElem;
	}

	@Override
	public Element visit(UIPreference uiPreference) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier
				.UI_PREFERENCE());
		addAttribute(elem, XmlIdentifier.ENTRY_SHOWN(),
				uiPreference.getEntryShown() + "");
		addAttribute(elem, XmlIdentifier.MINIMUM_SUPPORT(),
				uiPreference.getMinimumSupport() + "");
		addAttribute(elem, XmlIdentifier.HAS_PICTURE(),
				Boolean.toString(uiPreference.isHasPictures()));
		Set<String> hiddenAttrs = uiPreference.getHiddenAttribute();
		for (String hidenAttr : hiddenAttrs) {
			Element child = createAddChild(elem,
					XmlIdentifier.HIDDEN_ATTRIBUTE());
			child.setAttribute(XmlIdentifier.NAME(), hidenAttr);
		}
		Map<String, String> extraColumns = uiPreference.getExtraColumn();
		for (Entry<String, String> extraColumn : extraColumns.entrySet()) {
			Element child = createAddChild(elem, XmlIdentifier.EXTRA_COLUMN());
			child.setAttribute(XmlIdentifier.NAME(), extraColumn.getKey());
			child.setAttribute(XmlIdentifier.VALUE(), extraColumn.getValue());
		}
		return elem;
	}

	@Override
	public Element visit(SparqlService sparqlService) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier
				.SPARQL_SERVICE());
		addAttribute(elem, XmlIdentifier.SERVICE_NAME(),
				sparqlService.getServiceName());
		addAttribute(elem, XmlIdentifier.URL(), sparqlService.getUrl());
		for (Query query : sparqlService.getQuery()) {
			addChild(elem, visit(query));
		}

		return elem;
	}

	@Override
	public Element visit(Query query) {
		Element elem = DOMWriter.createRootElement(XmlIdentifier.QUERY());
		addAttribute(elem, XmlIdentifier.LABEL(), query.getLabel());
		addAttribute(elem, XmlIdentifier.ID(), query.getId());
		addAttribute(elem, XmlIdentifier.REQUEST_LANGUAGE(), query
				.getReqLanguage().toString());
		Element value = createAddChild(elem, XmlIdentifier.VALUE());
		CDATASection cdata = elem.getOwnerDocument().createCDATASection(
				query.getQuery());
		value.appendChild(cdata);

		Map<String, String> params = query.getParam();
		for (Entry<String, String> extraColumn : params.entrySet()) {
			Element child = createAddChild(elem, XmlIdentifier.PARAM());
			child.setAttribute(XmlIdentifier.NAME(), extraColumn.getKey());
			child.setAttribute(XmlIdentifier.VALUE(), extraColumn.getValue());
		}
		return elem;
	}

}
