package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;


@SuppressWarnings("serial")
public class LoginReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(LoginReponse.class.getName());
	
	private ActiveUser activeUser;
	
	public LoginReponse() {}

	public LoginReponse(ActiveUser activeUser) {
		this.activeUser = activeUser;
	}

	public LoginReponse(Err err) {
		super(err);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LoginReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LoginReponse that = (LoginReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.activeUser, that.activeUser)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(activeUser).
				toHashCode();
	}
	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	public ActiveUser getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(ActiveUser activeUser) {
		this.activeUser = activeUser;
	}



}
