package fr.irisa.lis.portalis.shared.camelis.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;


@SuppressWarnings("serial")
public class Axiome implements CamelisDataObject {

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private String terme1;
	private String terme2;
	
	public Axiome() {}

	public Axiome(String terme1, String terme2) {
		this.terme1 = terme1;
		this.terme2 = terme2;
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof Axiome) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final Axiome that = (Axiome)aThat;
			return new EqualsBuilder()
			//.appendSuper(super.equals(aThat))
			.append(this.terme1, that.terme1)
			.append(this.terme2, that.terme2)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(terme1).
				append(terme2).
				toHashCode();
	}

	public String getTerme1() {
		return terme1;
	}

	public String getTerme2() {
		return terme2;
	}

	public void setTerme1(String terme1) {
		this.terme1 = terme1;
	}

	public void setTerme2(String terme2) {
		this.terme2 = terme2;
	}

}
