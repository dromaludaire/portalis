package fr.irisa.lis.portalis.shared.admin.data;

import java.util.List;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public interface AdminReaderInterface<T> {

	public abstract RightValue getRightValueAttribute(T root, String name)
			throws PortalisException;

	public abstract List<String> getMessageElements(T root)
			throws PortalisException;


	public abstract boolean checkReponseElement(T root, String name,
			VoidReponse reponse) throws RequestException, PortalisException;


}