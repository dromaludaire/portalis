package fr.irisa.lis.portalis.shared.camelis.data;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;

public interface CamelisDataReaderInterface<T> {

	public abstract LisObject getLisObject(T root)
			throws CamelisException, PortalisException, RequestException;

	public abstract LisExtent getLisExtent(T root)
			throws CamelisException, PortalisException, RequestException;

	public abstract CamelisUser getCamelisUser(T root)
			throws PortalisException, RequestException;

	public abstract LisIncrement getIncrement(T root)
			throws PortalisException, RequestException;

	public abstract PropertyTree getPropertyTree(T root) 
			throws PortalisException;

	public abstract LisIntent getLisIntent(T root)
			throws PortalisException, RequestException;

	public abstract LisIncrementSet getLisIncrementSet(T root)
			throws PortalisException, RequestException;

}