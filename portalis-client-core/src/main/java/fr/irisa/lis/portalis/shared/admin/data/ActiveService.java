package fr.irisa.lis.portalis.shared.admin.data;


@SuppressWarnings("serial")
public abstract class ActiveService implements AdminDataObject {

	public static String extractFullName(String actifId) {
		String[] parts = actifId.split("::");
		return parts[1];
	}

	@Override
	public abstract String toString();

	@Override
	public abstract <T> T accept(AdminDataWriterInterface<T> visitor);

	public abstract String getFullName();

	public abstract String getActiveId();

	public abstract String getActivePartOfId() ;
	
	public String getUrl() {
		return "http://"+getActivePartOfId();
	}

	public static String extractAppliName(String activeServiceId) {
				String fullName = extractFullName(activeServiceId);
				return fullName.split(":")[0];
			}

	public static String extractServiceName(String activeServiceId) {
				String fullName = extractFullName(activeServiceId);
				return fullName.split(":")[1];
			}

	public ActiveService() {
		super();
	}

}