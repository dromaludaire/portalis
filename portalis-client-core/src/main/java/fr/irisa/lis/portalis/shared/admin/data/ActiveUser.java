package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class ActiveUser implements AdminDataObject, UserDataContent {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ActiveUser.class.getName());

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private UserData userCore;
	private String httpSessionId;
	private List<String> cookies = new ArrayList<String>();

	public ActiveUser() {

	}

	public ActiveUser(UserData user, String httpSessionId) {
		this.userCore = user;
		this.httpSessionId = httpSessionId;
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ActiveUser))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ActiveUser that = (ActiveUser) aThat;
		return new EqualsBuilder()
				// if deriving: .appendSuper(super.equals(that))
				.append(this.userCore, that.userCore)
				.append(this.httpSessionId, that.httpSessionId)
				.append(this.cookies, that.cookies).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				// two randomly chosen prime numbers
				// if deriving: .appendSuper(super.hashCode())
				.append(userCore).append(httpSessionId).append(cookies)
				.toHashCode();
	}

	public static String portalisSessionIdFromHttpSessionId(String key) {
		return CommonsUtil.sha(key);
	}

	public UserData getUserCore() {
		return userCore;
	}

	public void setUserCore(UserData userCore) {
		this.userCore = userCore;
	}

	public String getPortalisSessionId() {
		return portalisSessionIdFromHttpSessionId(httpSessionId
				+ userCore.getEmail());
	}

	public String toShortString() {
		return new StringBuffer(httpSessionId).append(":")
				.append(userCore.getEmail()).toString();
	}

	public String getHttpSessionId() {
		return (httpSessionId == null ? "null" : httpSessionId);
	}

	public void setHttpSessionId(String httpSessionId) {
		if (httpSessionId.equals("null"))
			this.httpSessionId = null;
		else
			this.httpSessionId = httpSessionId;
	}

	public void setCookies(List<String> cookies) {
		this.cookies = cookies;
	}

	public void resetCookies() {
		this.cookies = new ArrayList<String>();
	}

	public List<String> getCookies() {
		return cookies;
	}

	public static ActiveUser createAnonymousActiveUser() {
		return new ActiveUser(new AnonymousUser(), new Date().toString());
	}

}
