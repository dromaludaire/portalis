package fr.irisa.lis.portalis.shared.admin.data;

import java.net.InetAddress;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisError;

@SuppressWarnings("serial")
public class PortalisService extends ActivePortalisService {

	private String appliName;

	private static final Logger LOGGER = LoggerFactory.getLogger(PortalisService.class
			.getName());

	public void init(String hostName, int port, String appliName) {
		this.host = hostName;
		this.port = port;
		this.appliName = appliName;
	}
	

	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static PortalisService instance = new PortalisService();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static PortalisService getInstance() {
		return SingletonHolder.instance;
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private PortalisService() {
		this.activationDate = new Date();
		this.creator = "%none%";
		try {
			host = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (java.net.UnknownHostException e) {
			
		}
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof PortalisService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final PortalisService that = (PortalisService) aThat;
		return new EqualsBuilder().
				appendSuper(super.equals(aThat)).
	            append(this.appliName, that.appliName).
				isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(appliName).
				toHashCode();
	}

	@Override
	public String getFullName() {
		if (this.appliName == null || this.appliName.length() == 0) {
			String mess = "PortalisService n'a pas encore été initialisé";
			LOGGER.error(mess);
			throw new PortalisError(mess);
		}
		return String.format("%s:_", this.appliName);
	}

	public String getAppliName() {
		return this.appliName;
	}

	@Override
	public String getActiveId() {
		return host + ":" + port + "::" + getFullName();
	}

}
