package fr.irisa.lis.portalis.shared.admin.reponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.LisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestAdminReponse {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestAdminReponse.class.getName());
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		PortalisService.getInstance().init(host, 8080, "portalis");
		Util.clientInitLogPropertiesFile();
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @BeforeClass : TestAdminCore        |"
				+ "\n            ----------------------------------------------------\n");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @AfterClass : TestAdminCore         |"
				+ "\n            ----------------------------------------------------\n");
	}

	@Test
	public void portIsBusy() {
		try {
			LOGGER.info("--------------> @Test : portIsBusy\n");
			BooleanReponse in = new BooleanReponse(false);
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK,
					elem.getAttribute(XmlIdentifier.STATUS()));

			BooleanReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getBooleanReponse(elem);

			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertTrue("les deux objets SiteReponse doivent être égaux",
					in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testReponseAvecErreurs() {
		try {
			LOGGER.info("--------------> @Test : testReponseAvecErreurs\n");

			List<String> messages = new ArrayList<String>();
			String mess1 = "message N°1";
			messages.add(mess1);
			String mess2 = new LogReponse(new Err("blabla")).toString();
			messages.add(mess2);
			String mess3 = "service is :<camelis activationDate=\"2013-10-01T21:39:28.433+02:00\" creator=\"bekkers@irisa.fr\" host=\"locbbalhost\" lastUpdate=\"none\" nbObject=\"0\" pid=\"4787\" port=\"8060\" serviceName=\"localhost:8060::planets:planetsPhoto\"/>\n";
			messages.add(mess3);

			// ---------------------------------------------------------------------
			// LogReponse
			LogReponse inLogReponse = new LogReponse(new Err(messages));

			Element elemLogReponse = ClientConstants.ADMIN_REPONSE_XML_WRITER
					.visit(inLogReponse);
			assertNotNull(elemLogReponse);
			assertEquals(XmlIdentifier.ERROR, inLogReponse.getStatus());
			LOGGER.info("elemLogReponse\n"
					+ DOMUtil.prettyXmlString(elemLogReponse));

			LogReponse outLogReponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLogReponse(elemLogReponse);
			assertTrue("in et ou doivent être égaux\nin=" + inLogReponse
					+ "ou=\n" + outLogReponse,
					inLogReponse.equals(outLogReponse));

			// ---------------------------------------------------------------------
			// ActiveSiteReponse
			ActiveSiteReponse inActiveSiteReponse = new ActiveSiteReponse(
					new Err(messages));

			Element elemActiveSiteReponse = ClientConstants.ADMIN_REPONSE_XML_WRITER
					.visit(inActiveSiteReponse);
			assertNotNull(elemActiveSiteReponse);
			assertEquals(XmlIdentifier.ERROR, inActiveSiteReponse.getStatus());
			LOGGER.info("elemActiveSiteReponse\n"
					+ DOMUtil.prettyXmlString(elemActiveSiteReponse));

			ActiveSiteReponse outActiveSiteReponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getActiveSiteReponse(elemActiveSiteReponse);
			assertTrue("in et ou doivent être égaux\nin=" + inActiveSiteReponse
					+ "ou=\n" + outActiveSiteReponse,
					inActiveSiteReponse.equals(outActiveSiteReponse));

			// ---------------------------------------------------------------------
			// LoginReponse
			LoginReponse inLoginReponse = new LoginReponse(new Err(messages));

			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER
					.visit(inLoginReponse);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.ERROR, inLoginReponse.getStatus());
			LOGGER.info("elemLogReponse\n" + DOMUtil.prettyXmlString(elem));

			LoginReponse outLoginReponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLoginReponse(elem);
			assertTrue("in et ou doivent être égaux\nin=" + inLoginReponse
					+ "ou=\n" + outLoginReponse,
					inLoginReponse.equals(outLoginReponse));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSiteReponse() {
		try {
			LOGGER.info("--------------> @Test : testSiteReponse\n");
			Site site = CoreTestConstants.PORTALIS_SITE;
			SiteReponse in = new SiteReponse(site);
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK,
					elem.getAttribute(XmlIdentifier.STATUS()));

			SiteReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getSiteReponse(elem);

			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertTrue("les deux objets SiteReponse doivent être égaux",
					in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testServiceCoreReponse() {
		try {
			LOGGER.info("--------------> @Test : testSiteReponse\n");
			LisServiceCore serviceCore = CoreTestConstants.SC1;
			ServiceCoreReponse in = new ServiceCoreReponse(serviceCore);
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK,
					elem.getAttribute(XmlIdentifier.STATUS()));

			ServiceCoreReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getServiceCoreReponse(elem);

			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertTrue("les deux objets ServiceCoreReponse doivent être égaux",
					in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testActiveSiteReponse() {
		try {
			LOGGER.info("--------------> @Test : testActiveSiteReponse\n");
			ActiveSite site = CoreTestConstants.ACTIVE_SITE;
			ActiveSiteReponse in = new ActiveSiteReponse(site);
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK,
					elem.getAttribute(XmlIdentifier.STATUS()));

			ActiveSiteReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getActiveSiteReponse(elem);

			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertTrue("les deux objets ActiveSiteReponse doivent être égaux",
					in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPidReponse() {
		try {
			LOGGER.info("--------------> @Test : testPidReponse\n");
			HashSet<Integer> pidSet1 = new HashSet<Integer>();
			pidSet1.add(new Integer(15));
			pidSet1.add(new Integer(16));
			PidReponse in = new PidReponse(pidSet1);
			HashSet<Integer> pidSet2 = new HashSet<Integer>();
			pidSet2.add(new Integer(16));
			pidSet2.add(new Integer(15));
			PidReponse in1 = new PidReponse(pidSet2);
			assertTrue("les deux objets PidReponse doivent être égaux",
					in.equals(in1));
			assertTrue("les deux objets PidReponse doivent être différents",
					!in.equals(new PidReponse(new HashSet<Integer>())));

			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK,
					elem.getAttribute(XmlIdentifier.STATUS()));

			PidReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getPidReponse(elem);
			LOGGER.info("\nout = " + out);

			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertEquals(in, out);

			assertTrue("Les deux hashSet doivent égaux",
					in.getPids().equals(out.getPids()));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testVoidReponse() {
		LOGGER.info("--------------> @Test : testVoidReponse\n");
		try {
			VoidReponse in = new VoidReponse();
			LOGGER.info("\nin = " + in);
			VoidReponse in1 = new VoidReponse();
			assertTrue("les deux objets VoidReponse doivent être égaux\nin = "
					+ in + "\nin1 = " + in1, in.equals(in1));

			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			VoidReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getVoidReponse(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("les deux objets VoidReponse doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLoginReponse() {
		try {
			LOGGER.info("--------------> @Test : testLoginReponse\n");

			LoginReponse in = new LoginReponse(
					CoreTestConstants.SuperActiveUser);
			LOGGER.info("\nin = " + in);

			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK, in.getStatus());
			LOGGER.info("elem= " + DOMUtil.prettyXmlString(elem));

			LoginReponse out = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLoginReponse(elem);
			LOGGER.info("\nout = " + out);
			assertTrue(out != null);
			assertTrue("in et ou doivent être égaux\nin=" + in + "ou=\n" + out,
					in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

}
