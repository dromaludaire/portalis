package fr.irisa.lis.portalis.test;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.CamelisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;

public class CamelisTestConstants extends CoreTestConstants {
	
	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(CamelisTestConstants.class.getName());
	
	public static ActiveLisService CS1;
	public static ActiveLisService CS2;

	public static final LisObject OBJ1 = new LisObject(42, "plop42", "/images/no-image.jpg", "filename");
	public static final LisObject OBJ2 = new LisObject(1, "plop1");
	public static final LisObject OBJ3 = new LisObject(2, "plop2");

	public static final LisObject[] OBJS = { OBJ1, OBJ2, OBJ3 };

	// TODO remove PROPS after switching to LisINcrments LisFeature
	public static final Property PROP1 = new Property("fooProp1");
	public static final Property PROP2 = new Property("fooProp2");
	public static final Property PROP3 = new Property("fooProp3");
	public static final Property[] PROPS = { PROP1, PROP2, PROP3 };

	public static final LisIncrement INCR1 = new LisIncrement("fooIncr1");
	public static final LisIncrement INCR2 = new LisIncrement("fooIncr2");
	public static final LisIncrement INCR3 = new LisIncrement("fooIncr3");
	public static final LisIncrement[] INCRS = { INCR1, INCR2, INCR3 };
	public static final ServiceCoreInterface SERV_CORE1 = new CamelisServiceCore("planets:planets", RightValue.READER);
	public static final ServiceCoreInterface SERV_CORE2 = new CamelisServiceCore("planets:planetPhoto", RightValue.READER);


	static {
		String portalisHost = null;
			portalisHost = PortalisService.getInstance().getHost();
			CS1 = new ActiveLisService(MAIL_YVES,
					portalisHost, PORT1, DATE1, DATE2, PID1,
					NB_OBJECT1, SC1.getFullName());
			CS2 = new ActiveLisService(MAIL_BENJAMIN,
					portalisHost, PORT2, DATE1, DATE2, PID2,
					NB_OBJECT2, SC2.getFullName());
	}

}
