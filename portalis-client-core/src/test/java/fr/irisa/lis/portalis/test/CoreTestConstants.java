package fr.irisa.lis.portalis.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.CamelisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.LisApplication;
import fr.irisa.lis.portalis.shared.admin.data.Query;
import fr.irisa.lis.portalis.shared.admin.data.RequestLanguage;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.SparqlApplication;
import fr.irisa.lis.portalis.shared.admin.data.SparqlService;
import fr.irisa.lis.portalis.shared.admin.data.StandardUser;
import fr.irisa.lis.portalis.shared.admin.data.SuperUser;


public class CoreTestConstants {

	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(CoreTestConstants.class.getName());
	
	public static final String ADMIN_KEY = "12AE43BC58F";
    public static final String USER_KEY = "1234567890ABCDEF";

	public static final String MAIL_YVES = "bekkers@irisa.fr";
	public static final String PSEUDO_YVES = "bekkers";
	public static final String PASSWORD_YVES = "yves";
	
    public static final String MAIL_BENJAMIN = "benjamin@dromaludaire.info";
	public static final String PSEUDO_BENJAMIN = "sigonneau";
	public static final String PASSWORD_BENJAMIN = "benjamin";

    public static final String BAD_MAIL = "test";

	public static final String APPLICATION_PLANETS = "planets";
	public static final String SERVICE_PLANETS_DISTANCE_ERR = "planets:photo";
	public static final String SERVICE_PLANETS_DISTANCE = "planets:planetsDistance";
	public static final String SERVICE_PLANETS_PLANETS = "planets:planets";
	public static final String SERVICE_THABOR_ARBRES_THABOR = "svt:arbres_thabor";
	public static final String APPLI_ID1 = "appli1";
	public static final String APPLI_ID2 = "appli2";
	public static final String SERVICE_FULL_NAME1 = "appli:service1";
	public static final String SERVICE_FULL_NAME2 = "appli:service2";
	public static final String SERVICE_NAME1 = "service1";
	public static final String SERVICE_NAME2 = "service2";
	public static final String HOST = "tradewind";
	public static final int PORT = 8090;
	public static final int PORT1 = 8888;
	public static final int PORT2 = 8889;
	public static final Date DATE1 = new Date();
	public static final Date DATE2 = null;
	public static final int PID1 = 14256;
	public static final int PID2 = 99999;
	public static final int NB_OBJECT1 = 32;
	public static final int NB_OBJECT2 = 0;
	public static final String SESS_ID1 = "123321";
	public static final String SESS_ID2 = "ABCCBA";

	public static final CamelisServiceCore SC1 = new CamelisServiceCore(APPLI_ID1,
			SERVICE_NAME1, RightValue.READER);
	public static final ActiveLisService cam1 = new ActiveLisService(MAIL_BENJAMIN, HOST, PORT, new Date(), null, PID1, 15, SC1.getFullName());
	public static final ActiveLisService cam2 = new ActiveLisService(MAIL_YVES, HOST, PORT1, new Date(), null, PID2, 1500, SC1.getFullName());
	
	public static final CamelisServiceCore SC2 = new CamelisServiceCore(APPLI_ID2,
			SERVICE_NAME2, RightValue.NONE);
	public static final ActiveLisService cam3 = new ActiveLisService(MAIL_YVES, HOST, PORT2, new Date(), null, 4321, 9999, SC2.getFullName());

	public static final SuperUser SUPER_USER = new SuperUser(MAIL_YVES, PSEUDO_YVES,
			PASSWORD_YVES);
	public static final StandardUser STANDARD_USER = new StandardUser(MAIL_BENJAMIN, PSEUDO_BENJAMIN,
			PASSWORD_BENJAMIN);

	public static final ActiveUser SuperActiveUser = new ActiveUser(SUPER_USER, "session1");
	public static final ActiveUser StandardActiveUser = new ActiveUser(STANDARD_USER, "session2");

	public static final String PLANETS_ID = "planets";
	public static final LisApplication PLANETS = new LisApplication(PLANETS_ID);
	public static final LisApplication APPLI1 = new LisApplication(APPLI_ID1);
	public static final LisApplication APPLI2 = new LisApplication(APPLI_ID2);
	public static final CamelisServiceCore PLANETS_PLANETS = new CamelisServiceCore(PLANETS.getId(),
			"planets", RightValue.COLLABORATOR);
	public static final Role ROLE = new Role(StandardActiveUser.getUserCore().getEmail(), PLANETS_PLANETS.getFullName(), RightValue.ADMIN);

	public static final ConcurrentHashMap<String, LisApplication> applications = new ConcurrentHashMap<String, LisApplication>();
	
	public static final String QUERY1 = "prefix skos: <http://www.w3.org/2004/02/skos/core#> "
			+ "select ?libelle from <http://rdf.insee.fr/graphes/codes/nafr2> "
			+ "where {?s skos:notation \"62.02A\" . "
			+ "?s skos:prefLabel ?libelle filter(lang(?libelle) = 'fr') ."
			+ "}";
	public static final String QUERY2 = "select distinct ?Concept where {[] a ?Concept} LIMIT 100";
	public static final String LABEL1 = "Name of a french activity by its NAF code";
	public static final String LABEL2 = "liste des concepts";
	public static final RequestLanguage LANGUAGE = RequestLanguage.SPARQL;
	public static Query QUERY_1 = new Query("q1", LABEL1, QUERY1, LANGUAGE);
	public static Query QUERY_2 = new Query("q2", LABEL2, QUERY2, LANGUAGE);
	public static SparqlService SPARQL_SERVICE = new SparqlService("http://rdf.insee.fr/sparql", "rdf insee");
	public static SparqlApplication SPARQL_APPLICATION = new SparqlApplication();



	public static final Site PORTALIS_SITE = new Site(SPARQL_APPLICATION, applications);
	public static ActiveSite ACTIVE_SITE = null;
	
	public static final String WEBAPPS_DIR =
			AdminProprietes.portalis.getProperty("admin.webappsDirPath");


	
	static {
		STANDARD_USER.setPortalisADminRole(RightValue.ADMIN);
		PLANETS.add(PLANETS_PLANETS);
		applications.put(PLANETS.getId(), PLANETS);
		APPLI1.add(SC1);
		APPLI2.add(SC2);
		PORTALIS_SITE.addApplication(APPLI1);
		PORTALIS_SITE.addApplication(APPLI2);
		PORTALIS_SITE.addRole(ROLE);
		List<ActiveLisService> activeServices = new ArrayList<ActiveLisService>();
		activeServices.add(CoreTestConstants.cam1);
		activeServices.add(CoreTestConstants.cam2);
		activeServices.add(CoreTestConstants.cam3);

		QUERY_1.addParam("timeout", 0);
		SPARQL_SERVICE.addQuery(QUERY_1);
		SPARQL_SERVICE.addQuery(QUERY_2);
		SPARQL_APPLICATION.add(SPARQL_SERVICE);

		ACTIVE_SITE = new ActiveSite(PORTALIS_SITE, activeServices);
		PORTALIS_SITE.addUser(SUPER_USER);
		PORTALIS_SITE.addUser(STANDARD_USER);

	}
	
}
