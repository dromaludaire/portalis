package fr.irisa.lis.portalis.shared.camelis.reponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveService;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.test.CamelisTestConstants;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisReponse {private static final Logger LOGGER = LoggerFactory.getLogger(TestCamelisReponse.class.getName());


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		PortalisService.getInstance().init(host, 8080, "portalis");
		Util.clientInitLogPropertiesFile();
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @BeforeClass : TestCamelisReponse       |"
				+ "\n            ---------------------------------------------------\n");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @AfterClass : TestCamelisReponse        |"
				+ "\n            ---------------------------------------------------\n");

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/*
	 * ------------------------------------- ping
	 * ------------------------------------------
	 */

	@Test
	public void testActiveLisService() {
		try {
			LOGGER.info("--------------> @Test : testActiveLisService\n");
			// CoreTestConstants.CS1 = new ActiveLisService(SC1, YVES, HOST_NAME,
			// PORT1, DATE1, DATE2, PID1, NB_OBJECT1)
			ActiveLisService in = CamelisTestConstants.CS1;
			LOGGER.info("\nin  = " + in);
			String host = PortalisService.getInstance().getHost();
			ActiveLisService in1 = new ActiveLisService(
					CoreTestConstants.MAIL_YVES, host,
					CoreTestConstants.PORT1, null, CoreTestConstants.DATE1,
					CoreTestConstants.PID2, 9999, CoreTestConstants.SC1.getFullName());
			ActiveLisService in2 = new ActiveLisService(
					CoreTestConstants.MAIL_YVES, "toto",
					CoreTestConstants.PORT1, CoreTestConstants.DATE1,
					CoreTestConstants.DATE2, CoreTestConstants.PID1,
					CoreTestConstants.NB_OBJECT1, CoreTestConstants.SC1.getFullName());
			ActiveLisService in3 = new ActiveLisService(
					CoreTestConstants.MAIL_YVES, host,
					CoreTestConstants.PORT1, CoreTestConstants.DATE2, null,
					14444, 52, CoreTestConstants.SC1.getFullName());
			assertTrue(
					String.format(
							"les deux objets ActiveLisService doivent être différents\nin1=%s\nin2=%s",
							in1, in2), !in1.equals(in2));
			assertTrue(
					String.format(
							"les deux objets ActiveLisService doivent être différents\nin1=%s\nin=%s",
							in1, in), !in1.equals(in));
			assertTrue(
					String.format(
							"les deux objets ActiveLisService doivent être différents\nin=%s\nin1=%s",
							in, in1), !in.equals(in1));
			assertTrue(
					String.format(
							"les deux objets ActiveLisService doivent être différents\nin=%s\nin3=%s",
							in, in3), !in.equals(in3));
			assertTrue(					String.format(
					"les deux objets ActiveLisService doivent être différents\nin=%s\nin3=%s",
					in, in3),
					!in3.equals(in));
			assertTrue(
					"les deux objets ActiveLisService doivent être différents",
					!in.equals(in2));
			assertTrue(
					"les deux objets ActiveLisService doivent être différents",
					!in1.equals(in2));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			LOGGER.info(DOMUtil.prettyXmlString(elem));
			ActiveService out = ClientConstants.ADMIN_DATA_XML_READER.getActiveLisService(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("les deux objets ActiveLisService doivent être égaux",
					in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testDelKeyReponse() {
		try {
			LOGGER.info("--------------> @Test : testDelKeyReponse\n");
			DelKeyReponse in = new DelKeyReponse();
			in.setLastUpdate(new Date());
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			DelKeyReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelKeyReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testReponseErr() {
		try {
			LOGGER.info("--------------> @Test : testReponseErr\n");
			
			String messErr = "essai erreur";
			
			// DelKeyReponse
			DelKeyReponse inDelKeyReponse = new DelKeyReponse(new Err(messErr));
			LOGGER.info("\nin  = " + inDelKeyReponse);

			Element elemDelKeyReponse = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(inDelKeyReponse);
			assertNotNull(elemDelKeyReponse);

			DelKeyReponse outDelKeyReponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelKeyReponse(elemDelKeyReponse);
			LOGGER.info("\nout = " + outDelKeyReponse);
			assertEquals(inDelKeyReponse, outDelKeyReponse);
			
			// ResetCreatorReponse
			ResetCreatorReponse in = new ResetCreatorReponse(new Err(messErr));
			LOGGER.info("\nin  = " + in);

			Element elemResetCreatorRepons = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elemResetCreatorRepons);

			ResetCreatorReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getResetCreatorReponse(elemResetCreatorRepons);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testResetCreatorReponse() {
		try {
			LOGGER.info("--------------> @Test : testResetCreatorReponse\n");
			ResetCreatorReponse in = new ResetCreatorReponse();
			in.setLastUpdate(new Date());
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			ResetCreatorReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getResetCreatorReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testStartReponse() {
		try {
			LOGGER.info("--------------> @Test : testStartReponse\n");
			StartReponse in = new StartReponse(CamelisTestConstants.CS2);
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			StartReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getStartReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPingReponse() {
		try {
			LOGGER.info("--------------> @Test : testPingReponse\n");
			PingReponse in = new PingReponse(CamelisTestConstants.CS1);
			in.setLastUpdate(new Date());
			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			LOGGER.info("inElem = "+DOMUtil.prettyXmlString(elem));

			PingReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getPingReponse(elem);

			assertTrue(String.format("les deux objets PingReponse doivent être égaux\nin=%s\nout=%s", in, out), in.equals(out));
			ActiveLisService serviceIn = in.getFirstService();
			ActiveLisService serviceOut = out.getFirstService();
			assertEquals(serviceIn, serviceOut);

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportCtxReponse() {
		LOGGER.info("--------------> @Test : testPortActif\n");
		try {
			String ctxFile = "myFile.ctx";
			ImportCtxReponse in = new ImportCtxReponse(ctxFile);
			in.setLastUpdate(new Date());
			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			ImportCtxReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getImportCtxReponse(elem);
			LOGGER.info("\nout = " + out);

			assertTrue(
					"les deux objets ImportCtxReponse doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			assertTrue(
					"les deux objets ImportCtxReponse doivent avoir le même fichier cxtFile\nin = "
							+ in + "\nout = " + out,
					ctxFile.equals(out.getCtxFile()));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportLisReponse() {
		String methodName = Thread.currentThread().getStackTrace()[1]
				.getMethodName();
		LOGGER.info("--------------> @Test : " + methodName + "\n");
		try {
			String lisFile = "myFile.lis";
			ImportLisReponse in = new ImportLisReponse(lisFile);
			in.setLastUpdate(new Date());
			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			ImportLisReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getImportLisReponse(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("toString outputs should be the same", in.toString()
					.equals(out.toString()));

			assertTrue("hashCodes should be the same",
					in.hashCode() == out.hashCode());

			assertTrue(
					"les deux objets ImportLisReponse doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPingReponseMulti() {
		try {
			LOGGER.info("--------------> @Test : testPingReponseMulti\n");
			PingReponse in = new PingReponse(CamelisTestConstants.CS1);
			in.addActiveLisService(CamelisTestConstants.CS2);
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			LOGGER.info("testPingReponse(\nelem = "
					+ DOMUtil.prettyXmlString(elem) + ")");
			assertNotNull(elem);

			PingReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getPingReponse(elem);
			LOGGER.info("\nout = " + out);

			assertEquals(in, out);

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testExtentReponse() {
		LOGGER.info("--------------> @Test : testExtentReponse\n");
		try {
			ExtentReponse in = new ExtentReponse();
			in.setLastUpdate(new Date());
			in.setExtent(CamelisTestConstants.OBJS);
			in.setNbObjects(42);
			
			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			ExtentReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getExtentReponse(elem);
			LOGGER.info("\nout = " + out);

			assertTrue(
					"les deux objets ExtentReponse doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			// // bypass ExtentReponse.equals, so as to test it...
			// assertTrue("les deux objets ExtentReponse doivent avoir la même extension\nin = "
			// + in + "\nout = " + out,
			// Arrays.equals(CamelisTestConstants.OBJS, out.getExtent()));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testIntentReponse() {
		LOGGER.info("--------------> @Test : testIntentReponse\n");
		try {
			IntentReponse in = new IntentReponse();
			in.setLastUpdate(new Date());
			in.setIntent(CamelisTestConstants.PROPS);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			IntentReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getIntentReponse(elem);
			LOGGER.info("\nout = " + out);

			assertTrue(
					"les deux objets IntentReponse doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testZoomReponse() {
		LOGGER.info("--------------> @Test : testZoomReponse\n");
		try {
			ZoomReponse in = new ZoomReponse();
			in.setLastUpdate(new Date());
			in.setNbObjects(42);
			in.setIncrements(CamelisTestConstants.INCRS);
			in.setNewWorkingQuery("some query");
			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			ZoomReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getZoomReponse(elem);
			LOGGER.info("\nout = " + out);
			assertTrue("les deux objets ZoomReponse doivent être égaux\nin = "
					+ in + "\nout = " + out,
					in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	
	@Test
	public void testDelObjectsReponse() {
		try {
			LOGGER.info("--------------> @Test : testDelObjectsReponse\n");
			DelObjectsReponse in = new DelObjectsReponse();
			in.setLastUpdate(new Date());
			in.setExtent(new LisExtent(CamelisTestConstants.OBJS));
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			DelObjectsReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getDelObjectsReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	

		@Test
	public void testDelFeatureReponse() {
		try {
			LOGGER.info("--------------> @Test : testDelFeatureReponse\n");
			DelFeatureReponse in = new DelFeatureReponse();
			in.setLastUpdate(new Date());
			in.setIncrements(new LisIncrementSet(CamelisTestConstants.INCRS));
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			DelFeatureReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getDelFeatureReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test
	public void testAddAxiomReponse() {
		try {
			LOGGER.info("--------------> @Test : testAddAxiomReponse\n");
			AddAxiomReponse in = new AddAxiomReponse();
			in.setLastUpdate(new Date());
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			AddAxiomReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getAddAxiomReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	
	@Test
	public void testResetCamelisReponse() {
		try {
			LOGGER.info("--------------> @Test : testResetCamelisReponse\n");
			ResetCamelisReponse in = new ResetCamelisReponse();
			in.setLastUpdate(new Date());
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			ResetCamelisReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getResetCamelisReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	
	@Test
	public void testGetValuedFeaturesReponse() {
		try {
			LOGGER.info("--------------> @Test : testGetValuedFeaturesReponse\n");
			GetValuedFeaturesReponse in = new GetValuedFeaturesReponse();
			in.setLastUpdate(new Date());
			in.setValue(1, "foo", "toto");
			in.setValue(1, "bar", "42");
			in.setValue(1, "bar", "54");  // multi-value
			in.setValue(2, "foo", "tata");
			in.setValue(2, "bar", "1618");
			LOGGER.info("\nin  = " + in);

			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			GetValuedFeaturesReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getGetValuedFeaturesReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	
	@Test
	public void testLoadContextReponse() {
		try {
			LOGGER.info("--------------> @Test : testLoadContextReponse\n");
			LoadContextReponse in = new LoadContextReponse();
			in.setLastUpdate(new Date());
			LOGGER.info("\nin  = " + in);
			in.setContextName("foo");
			
			Element elem = ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			LoadContextReponse out = ClientConstants.CAMELIS_REPONSE_XML_READER.getLoadContextReponse(elem);
			LOGGER.info("\nout = " + out);
			assertEquals(in, out);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
}
