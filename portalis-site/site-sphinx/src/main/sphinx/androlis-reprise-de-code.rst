.. -*- mode: rst -*-

==========================
Androlis - Reprise de code
==========================

.. sectionauthor:: Vincent Piet <vincent.piet@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/androlis-repriseDeCode.pdf>`
   
.. admonition:: Androlis est en cours de développement, voici sa TODO list :

  * **Implémenter** communication serveur.
  * Durant le développement et pour la mise au point : **utiliser** un système de log à base de la librairie log4j_
  * **utiliser** l'asynchronisme des requêtes HTTP/XML pour laisser l'interface acive pendant l'interprétation d'une requête.

.. _Camelis:        http://www.irisa.fr/LIS/ferre/camelis/
.. _`Objective Caml`: http://caml.inria.fr/ocaml/
.. _`concept formel`: http://en.wikipedia.org/wiki/Formal_concept_analysis
.. _log4j:            http://code.google.com/p/android-logging-log4j/
   
Présentation
============

Portalis
--------

**Portalis** est une application WEB répartie qui permet à des utilisateurs distants de se partager,
de manière collaboratrive, des serveurs d'information logique LIS. Chaque serveur d'information met en oeuvre un **service**.
Un service est un ensemble d'objets et d'attributs d'un domaine d'intérêt particulier. Portalis permet de naviger intelligemment
au sein des services. Une étape de navigation sélectionne un ensemble d'objets ainsi que les attributs qui les caractérisent. Il s'agit d'un `concept formel`_

Concrêtement, **Portalis** est composé d'un serveur d'administration, appelé **serveur Portalis**, et d'un ensemble de serveurs d'information,
à raison de un serveur d'information par service considéré.
Pour l'instant, les serveurs d'information accessibles sont des serveurs de type **Camelis_**. Ces derniers sont implémentés en `Objective Caml`_. 
Voici le schéma de cette application :

.. image:: images/schemaGeneralPortalis.jpg
   :width: 16 cm

Androlis
--------

**Androlis** est un client Androïd pour l'application répartie **Portalis** ainsi qu'à ses serveurs d'information.

Les clients Androlis communiquent avec le serveur Portalis ainsi qu'avec les serveurs Camelis, à travers **internet** via le protocole **HTTP**.
Les réponses des serveurs sont retournées en XML.

Caractéristiques d'Androlis
===========================

Androlis est une interface usager composée de pages. Les pages permettent une navigation intelligente au sein de l'information maintenu par les serveurs.
Les pages principales d'Androlis sont :

- Page de login
- Page de choix d'un service
- Pages d'affichage des objets couramment sélectionnés
  - affichage liste
  - affichage icon ou image
  - affichage carte
  - affichage détail (de type fiche)
- Pages d'affichages des attributs
  - affichage liste
  - affichage nuage de points
- Page d'affichage des requêtes

La plupart des pages sont aussi des pages d'édition.

Maven
=====
Maven est un outil de gestion de librairie. Il permet entre autres la récupération automatique de librairies.
Cette opération s'effectue au moyen d'un fichier pom.xml dans lequel sont spécifiés ces dites librairies.

Conversion d'un projet Android sous éclipse
-------------------------------------------
Afin de "maveniser" un projet Android sous éclipse, il est nécessaire d'avoir au préalable installé le plug-in maven m2e 
ainsi que le plug-in m2e-android. 

pom.xml
```````
Il faudra ensuite créer un fichier pom.xml. Voici un exemple de pom minimal :

<project>
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mycompany.app</groupId>
  <artifactId>my-app</artifactId>
  <version>1</version>
</project>

modelVersion : version du pom.xml utilisée
groupeID : entitée à l'origine du projet
artifactId : nom des ressources générées par maven
version : version des ressources générées

plus d'informations sur le pom.xml : 
http://maven.apache.org/guides/mini/guide-naming-conventions.html
http://java.developpez.com/faq/maven/?page=terminologie
http://stackoverflow.com/questions/2487485/what-is-maven-artifact


Afin d'ajouter des librairies au projet grâce à maven, il faut les déclarer dans le fichier pom.xml. 
Afin d'obtenir les informations nécessaires, on peut par exemple consulter le site :
http://mvnrepository.com/

Conversion
``````````
Pour convertir le projet, il ne reste plus qu'à cliquer droit sur le projet, configure,
convert to maven project...

Problème rencontré
``````````````````
L'ajout de librairies peut ne pas fonctionner : les librairies ajoutés ne sont pas retrouvées dans l'application
compilée. Une solution à ce problème se trouve dans la partie concernant les librairies tierces.


Librairies Tierces
==================
Une librairie tierce (Third-party library) est une librairie développée par une entitée autre que sun. 
Elles proposent des services et évitent donc de les réimplémenter.

Le cas d'Android
----------------
A la compilation, en utilisant maven, ces librairies peuvent ne pas être retrouvées causant une erreur de type classNotFound.
Une solution est de les ajouter, "à la main", dans le dossier libs du projet Android.


Log4j
=====
Log4j est une librairie permettant entre autres de définir le format, le degré d'importance 
ou encore un endroit ou l'on souhaite rediriger les logs. Les logs sont les messages émis à la compilation ou
à l'exécution d'un programme. Ils peuvent donc être des messages d'erreur, d'avertissement, ou encore de simples informations.

Log4j et Android
----------------
Afin de fonctionner correctement, Log4j a besoin d'un fichier de propriétés (.properties) au format xml.
Cependant, Android utilise déjà le format xml. Il est donc nécessaire de les déclarer différemment.

Un autre problème entre Log4j et android est lié aux java beans. Plus d'explications sur ce lien :
http://mindpipe.blogspot.de/2011/11/android-log4j-exception-properties.html 

Une solution est l'utilisation de la librairie : android-logging-log4j. 
Cette librairie définie le concept de LogConfigurator en java, permettant ainsi d'écrire un fichier de propriétés en java.

Pour pouvoir utiliser les logs, il faut donc créer un fichier java de configuration que l'on placera dans le package défini
lors de la création de l'application. Il faut également appeler cette configuration dans la première activité appelée par l'application.

Il faut également donner la permission de lire et d'écrire un fichier externe si l'on souhaite rediriger les logs dans un fichier.
Pour se faire il faut les ajouter au fichier AndroidManifest.xml :
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />

Plus d'information sur l'utilisation de Log4j pour Android sont donnés à l'adresse :
http://brucebilyeu.com/blog/content.php?/archives/8-Logging-in-Android.html

Utilisation de log4j
--------------------
L'utilisation des logs se fait au moyen d'un ojet de type Logger :
   private final Logger logger = Logger.getLogger(Login.class);
   
On peut alors l'utiliser pour émettre un message auquel on spécifiera un degré d'importance.

Les différentes utilisations de Log4j sont décrites à l'adresse :
http://beuss.developpez.com/tutoriels/java/jakarta/log4j/

Avertissement dans la console
`````````````````````````````
L'utilisation de la librairie m2e-android peut provoquer un certain nombre d'avertissement dans la console
de type :
Dx warning: Ignoring InnerClasses attribute for an anonymous inner class

Ces avertissements sont bénins, il n'est cependant pas possible des les supprimer, plus d'informations sont données 
à cette adresse :
http://code.google.com/p/android-logging-log4j/issues/detail?id=1

EGit
====
EGit est un plugin eclipse permettant une utilisation simple de projets Git. 

Importer un projet
------------------
Une fois votre projet git disponible sur un serveur hote, vous pouvez l'importer facilement dans eclipse 
en utilisant la fonctionnalité "clone a git" de EGit.

Une fenetre s'ouvre contenant entre autres le champ URI. Collez l'adresse web de votre projet, les deux champs suivant 
devraient alors se remplir automatiquement. Ajoutez enfin votre nom d'utilisateur et votre mot de passe pour pouvoir
mettre à jour votre projet en local ou sur le serveur simplement.

Sur la page suivante choisissez la branche que vous voulez (master si vous êtes seul).

Enfin sur la dernière page choisissez l'endroit ou vous souhaitez enregistrer le projet, 
vérifiez que la checkbox "import all existing projects after clone finishes" soit cochez puis cliquez sur finish.

Utilisation des différentes fonctionnalitées
--------------------------------------------
Les principales fonctionnalitées sont présentent dans le menu clic droit sur le projet, Team. La fonctionnalitée
commit donne accès à un commit and push permettant d'automatiser la procédure.

Les principales fonctionnalitées sont définies à cette adresse:
http://eclipsesource.com/blogs/tutorials/egit-tutorial/

Working Set
===========
L'importation d'un nombre important de projets dans eclipse pour encombrer votre espace de travail au point de ne plus vous
y retrouver dans vos projets. Afin d'y remédier, eclipse propose une notion de Working Set, permettant de séparer vos projets 
entre différents espaces de travail.

Création d'un Working Set
-------------------------
Dans la vue Package Explorer cliquez sur la flèche et sélectionnez "Select Working Set". 
Dans la fenêtre qui viens de s'ouvrir cliquez sur New. Selectionnez alors "Ressources" afin de pouvoir
ajouter tous types de fichier dans votre Working Set. Entrez alors le nom que vous souhaitez associer à votre Working Set,
sélectionner les projets que vous souhaiter lier à cette vue, cliquez sur finish, puis double cliquez sur le Working Set
que vous venez de créer. Votre package explorer vous montre maintenant les projets de votre Working Set

Navigation
----------
Pour naviguer entre vos différents Working Set, cliquez sur la flèche de votre package explorer puis sélectionner
le Working Set sur lequel vous souhaiter travailler. Pour voir l'intégralité de vos projets choisissez le Working Set
Windows Working Set.

android-maps-extensions
=======================
Android maps extensions est une librairie pour google maps, permettant entre autres d'agréger les différents marqueurs
présents sur une carte afin de gagner en lisibilité. Cette librairie est trouvable à l'adresse :
https://code.google.com/p/android-maps-extensions/

Contenu
-------
Le téléchargement de l'archive du projet met à disposition de l'utilisateur la librairie, une démo de l'utilisation de cette librairie
et une démo de fonctionnalitées de google play services. Elle contient également un projet lib-installer dont le but est de placer
dans le fichier lib de l'application deux librairies utilisées.

Problème rencontré
------------------
Cette librairie est donc un multi projet maven android. Son utilisation sous l'IDE android studio se fait facilement, 
en effet, elle a été programmé grâce à cet IDE.
En revanche, l'importation peut poser problème sous Eclipse, les fichiers .projet n'étant pas à jour, les projets maven android
peuvent s'importer comme des projets maven java. Afin de solutionner le problème il faut aller modifier la nature du projet du fichier .projet
de chacuns des projets android et y ajouter :
<nature>com.android.ide.eclipse.adt.AndroidNature</nature>
<nature>org.eclipse.m2e.core.maven2Nature</nature>

Utilisation
-----------
Une fois le Multi projet maven importé, il faut utiliser maven/update projet, afin d'installer les librairies mentionnées précédement.
Il faut ensuite ajouter android-maps-extensions comme librairie à notre projet utilisateur :
Clic droit sur le projet/properties/android puis dans l'encadré Library Add et ajouter la librarie.

Si elle n'apparait pas dans les choix de libraries, il faut luivre la même procédure en partant de la librarie, 
puis cliquer sur is Library, plutôt que Add. Elle devrait alors apparaitre correctement.

Il faut enfin ajouter un certain nombre de permissions ainsi que clé API délivrée par google, propre à chaque application, dans l'application
destinnée à utiliser la librarie. La clef API nécéssite de connaitre sa clef SHA 1, présente dans Eclipse sous Window/Preferences/Android/Build. 
Ces ajouts sont requis par google pour l'utilisation de google maps.

Plus d'informations sur les permissions sont données ici:
https://developers.google.com/maps/documentation/android/start

Les clefs API étant délivrées ici:
https://code.google.com/apis/console/