package fr.irisa.lis.portalis.site;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;


import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.UtilCargo;
import fr.irisa.lis.portalis.core.CoreTestConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.irisa.lis.portalis.shared.ErrorMessages;
import fr.irisa.lis.portalis.shared.PortalisException;
import fr.irisa.lis.portalis.shared.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.CamelisService;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.SetRoleReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.util.CamelisXmlName;
import fr.irisa.lis.portalis.shared.http.admin.AdminHttp;
import fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp;
import fr.irisa.lis.portalis.shared.util.XmlIdentifier;
import fr.irisa.lis.portalis.system.linux.LinuxCmd;
import fr.irisa.lis.portalis.system.linux.PortService;
import fr.irisa.lis.portalis.camelis.http.UtilTest;

public class AutoDocCamelis {private static final Logger LOGGER = LoggerFactory.getLogger(AutoDocCamelis.class.getName());

	static String titre = DocumentationProprietes.documentation
			.getProperty("camelis-titre");
	static String docId = DocumentationProprietes.documentation
			.getProperty("camelis-id");
	static Documentation doc;
	private static Session camelisSession;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			
			doc = new Documentation(titre, docId);
			if (CargoLauncher.tomcatIsActif()) {
				CargoLauncher.stop();
			}
			CargoLauncher.start();

			startCamelis_jsp1();
			startCamelis_jsp2();
			startCamelis_jsp3();
			startCamelis_jsp4();

			stopCamelis_jsp1();

			ping1();
			ping2();
			ping3();

			registerKey1();
			registerKey2();
			registerKey3();
			registerKey4();

			setRole1();
			setRole2();
			setRole3();
			setRole4();
			setRole5();
			setRole6();
			setRole7();
			setRole8();

			// ----------- s t a r t S e s s i o n ( )
			startSession();
			String host = camelisSession.getHost();
			int port = camelisSession.getPort();

			// ------------- i m p o r t C t x 1
			String ctxFile = "planets/planets/planets1.ctx";
			String url = String.format("http://%s:%d/importCtx?%s=%s&%s=%s",
					host, port, XmlIdentifier.KEY(),
					camelisSession.getActiveUser().getPortalisSessionId(), CamelisXmlName.CTX_FILE,
					ctxFile);
			String comment = String
					.format("ImportCtx sur le port %s du serveur %s où un dépot Camelis est lancé, nom du fichier ctx %s",
							port, host, ctxFile);
			doc.pushDoc("importCtx", comment, url, importCtx1(ctxFile));

			// ----------- s t a r t S e s s i o n ( )
			startSession();
			host = camelisSession.getHost();
			port = camelisSession.getPort();

			// ------------- i m p o r t C t x 2
			url = String.format("http://%s:%d/importCtx?%s=%s", host,
					port, XmlIdentifier.KEY(), camelisSession.getActiveUser().getPortalisSessionId());
			comment = String
					.format("ImportCtx sur le port %s du serveur %s où un dépot Camelis est lancé, nom du fichier ctx par défaut",
							port, host);
			doc.pushDoc("importCtx", comment, url, importCtx2());

			// ----------- s t a r t S e s s i o n ( )
			startSession();
			host = camelisSession.getHost();
			port = camelisSession.getPort();

			// ------------- e x t e n t 1
			String lisQuery = "medium";
			ExtentReponse reponse = getExtent(lisQuery);
			url = String.format("http://%s:%d/extent?%s=%s&%s=%s", host, port,
					CamelisXmlName.ACTIF_USER_KEY, camelisSession.getActiveUser().getPortalisSessionId(),
					CamelisXmlName.QUERY, lisQuery);
			doc.pushDoc("extent", "On cherche les deux planètes médium ",
					url, reponse);

			// ------------- e x t e n t 2
			lisQuery = "medium-tt";
			reponse = CamelisHttp.extent(camelisSession, lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension doit doit être en erreur :" + reponse,
					!reponse.isOk());
			url = String.format("http://%s:%d/extent?%s=%s&%s=%s", host, port,
					CamelisXmlName.ACTIF_USER_KEY, camelisSession.getActiveUser().getPortalisSessionId(),
					CamelisXmlName.QUERY, lisQuery);
			doc.pushDoc("extent", "Erreur de syntaxe dans la requete Camelis ",
					url, reponse);
			
			// ------------- e x t e n t 2
			lisQuery = "pouet";
			reponse = CamelisHttp.extent(camelisSession, lisQuery);
			url = String.format("http://%s:%d/extent?%s=%s&%s=%s", host, port,
					CamelisXmlName.ACTIF_USER_KEY, camelisSession.getActiveUser().getPortalisSessionId(),
					CamelisXmlName.QUERY, lisQuery);
			doc.pushDoc("extent", "Extention nulle ",
					url, reponse);
			

			// ----------- f i n   d e   g e n e r a t i o n
			doc.generate();

			CargoLauncher.stop();
			doc = null;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				CargoLauncher.stop();
			}
		}
	}

	private static ExtentReponse getExtent(String lisQuery)
			throws PortalisDocumentException {
		LOGGER.fine("getExtent("+lisQuery+")");
		ExtentReponse reponse = CamelisHttp.extent(camelisSession,
				lisQuery);
		assertNotNull(reponse);
		assertTrue("l'extension ne doit doit pas être en erreur :" + reponse,
				reponse.isOk());
		LOGGER.fine("reponse = \n"+reponse);
		LisExtent receivedExtent = reponse.getExtent();
		assertTrue("l'extension ne doit doit pas être vide :" + reponse,
				receivedExtent != null && receivedExtent.card() == 2);
		
		// TODO voir pourquoi les assertEquals ne marche pas ici alors qu'ils marchent dans les tests
		// extent(medium) should contain two objects: neptune and uranus
//		LisObject[] expectedExtent = { new LisObject("5", "neptune"),
//				new LisObject("8", "uranus") };
//		assertTrue("Received extent does not match expected extent",
//					expectedExtent.equals(receivedExtent));
		//assertArrayEquals(expectedExtent, receivedExtent);

		return reponse;
	}

	private static void stopCamelis_jsp1() throws PortalisException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
				email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ******************* startCamelis ****************
		StartReponse startReponse = AdminHttp.startCamelis(sessUser,
				serviceName, null, null);
		assertTrue("La réponse doit être sans erreur, startReponse ="
				+ startReponse, startReponse.isOk());
		LOGGER.info("StartReponse = " + startReponse.toString());
		CamelisService camelisService = startReponse.getCamelisService();
		int port1 = camelisService.getPort();

		// *********************** ping ********************
		Session camelisSession = Session.createCamelisSession(sessUser.getActiveUser(), camelisService);
		PingReponse pingReponse = CamelisHttp.ping(camelisSession);
		assertTrue("La réponse ne doit pas être en erreur, pingReponse ="
				+ pingReponse, pingReponse.isOk());
		assertEquals(camelisService, pingReponse.getOneCamelisService());

		// ******************* stopCamelis ****************
		PingReponse pingReponse1 = AdminHttp.stopCamelis(sessUser, port1);
		assertTrue("La réponse doit être sans erreur, pingReponse1 ="
				+ pingReponse1, pingReponse1.isOk());
		LOGGER.info("pingReponse1 = " + pingReponse1.toString());

		String url = String.format("stopCamelis.jsp?%s=%d",
				XmlIdentifier.PORT(), port1);
		doc.pushDoc("stopCamelis.jsp", "stopCamelis on port " + port1, url,
				pingReponse1);

		// *********************** ping ********************
		PingReponse pingReponse2 = CamelisHttp.ping(camelisSession);
		assertTrue("La réponse doit être en erreur, pingReponse ="
				+ pingReponse2, !pingReponse2.isOk());
		final String TEMOIN = ErrorMessages.TEMOIN_IO_FAILURE;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + pingReponse2,
				pingReponse2.messagesContains(TEMOIN));
	}

	private static void startCamelis_jsp4() throws PortalisException,
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// *************************** login ***********************
		LOGGER.fine("\n\n              ===========================================> login <============================");
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
				email, password);
		Session sessUser = loginReponse.getSession();

		// ************************ startCamelis ********************
		LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
		StartReponse startReponse1 = UtilTest.startCamelis(sessUser, 1);
		LOGGER.info("StartReponse1 = " + startReponse1);
		int port1 = startReponse1.getCamelisService().getPort();

		// ************************ startCamelis ********************
		LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
		StartReponse startReponse2 = AdminHttp.startCamelis(sessUser, port1,
				CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
		LOGGER.info("StartReponse2 = " + startReponse2);

		final String[] parameters = { XmlIdentifier.KEY(),
				CoreTestConstants.ADMIN_KEY, XmlIdentifier.CREATOR(),
				CoreTestConstants.MAIL_YVES, XmlIdentifier.SERVICE_NAME(),
				CoreTestConstants.SERVICE_PLANETS_PHOTO + 0,
				XmlIdentifier.PORT(), CoreTestConstants.PORT + "" };
		String url1 = UtilCargo.buildURL("startCamelis.jsp", parameters);
		doc.pushDoc("startCamelis.jsp", "Port déjà occupé "
				+ CoreTestConstants.PORT + ".", url1, startReponse2);

		assertTrue("La réponse doit être en erreur, elle vaut :"
				+ startReponse2, !startReponse2.isOk());

		Set<PortActif> actifs = LinuxCmd.getActifsCamelisPortsOnPortalis();
		assertEquals(1, actifs.size());
		PortActif actif = new PortActif(PortalisService.getPORTALIS_HOST(), port1,
				startReponse1.getCamelisService().getPid());
		assertTrue(actif + " devrait être dans la liste des actifs",
				actifs.contains(actif));
	}

	private static void startCamelis_jsp3() throws PortalisDocumentException,
			PortalisException {
		LinuxCmd.killAllCamelisProcess();
		String serviceName = XmlIdentifier.SERVICE_NAME();
		StartReponse reponse = AdminHttp.startCamelis(Session.getPortalisSession(CoreTestConstants.AU1),
				null, serviceName, null, null);

		LOGGER.info("StartReponse = " + reponse);

		String[] parameters = { serviceName,
				CoreTestConstants.SERVICE_PLANETS_PHOTO };
		String url = UtilCargo.buildURL("startCamelis.jsp", parameters);
		doc.pushDoc("startCamelis.jsp", "*Erreur* : Paramètres manquants",
				url, reponse);

		assertTrue("La réponse doit être en erreur, elle vaut :" + reponse,
				!reponse.isOk());
		Set<PortActif> actifs = LinuxCmd.getActifsCamelisPortsOnPortalis();
		assertTrue("Liste d'actifs null", actifs != null);
		assertEquals(0, actifs.size());
	}

	private static void startCamelis_jsp2() throws PortalisException,
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
				email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ******************* startCamelis ****************
		String serviceFullName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 12;
		int port1 = PortService.choosePort();
		StartReponse startReponse = AdminHttp.startCamelis(sessUser, port1,
				serviceFullName, null, null);
		assertTrue("La réponse doit être sans erreur, startReponse ="
				+ startReponse, startReponse.isOk());
		CamelisService camelisService = startReponse.getCamelisService();
		assertTrue("Il doit y avoir un camelisService", camelisService != null);
		assertEquals(host, camelisService.getHost());

		final String[] params = { XmlIdentifier.KEY(),
				CoreTestConstants.ADMIN_KEY, XmlIdentifier.CREATOR(), email,
				XmlIdentifier.SERVICE_NAME(), serviceFullName,
				XmlIdentifier.PORT(), port1 + "" };
		String url = UtilCargo.buildURL("startCamelis.jsp", params);
		doc.pushDoc("startCamelis.jsp", "Lancement sur le port " + port1
				+ ".", url, startReponse);

		assertTrue("La réponse ne doit pas être en erreur, elle vaut :"
				+ startReponse, startReponse.isOk());

		Set<PortActif> actifs = LinuxCmd.getActifsCamelisPortsOnPortalis();
		assertTrue("La liste d'actifs actifs ne doit pas être nulle",
				actifs != null);
		assertEquals(1, actifs.size());
		PortActif actif = new PortActif(host, port1, camelisService.getPid());
		assertTrue(actif + " devrait être dans la liste des actifs",
				actifs.contains(actif));

		assertEquals(port1, camelisService.getPort());
		assertEquals(serviceFullName, camelisService.getServiceCore()
				.getFullName());
		assertEquals(CoreTestConstants.UC1.getEmail(),
				camelisService.getCreator());
	}

	private static void setRole8() throws PortalisException,
			IOException, InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		Session sess1 = UtilTest.startOneCamelis(sessUser);

		RightValue rightValue = RightValue.ADMIN;

		SetRoleReponse reponse = CamelisHttp.setRole(sess1,
				CoreTestConstants.USER_KEY, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur", !reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s&%s=%s",
				host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				sess1.getActiveUser().getPortalisSessionId(), // même key que celui qui a lancé
									// Camelis
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ROLE, rightValue.toString());
		String comment = String
				.format("setRole, port=%s, serveur host=%s, tous les paramètres sont présents",
						sess1.getPort(), host);
		doc.pushDoc("setRole", comment, url, reponse);

		final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void setRole7() throws PortalisException,
			IOException, InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		String userKey = "123456654321";
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		Session sess1 = UtilTest.startOneCamelis(sessUser);

		RightValue rightValue = RightValue.NONE;

		SetRoleReponse reponse = CamelisHttp
				.setRole(sess1, userKey, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur", !reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s&%s=%s",
				host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.ADMIN_KEY, // même key que celui qui a lancé
												// Camelis
				CamelisXmlName.ACTIF_USER_KEY, userKey, CamelisXmlName.ROLE,
				rightValue.toString());
		String comment = String
				.format("setRole, port=%s, serveur host=%s, tous les paramètres sont présents",
						sess1.getPort(), host);
		doc.pushDoc("setRole", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void setRole6() throws PortalisException,
			IOException, InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		Session sess1 = UtilTest.startOneCamelis(sessUser);

		RightValue rightValue = RightValue.READER;
		SetRoleReponse reponse = CamelisHttp.setRole(sess1,
				CoreTestConstants.USER_KEY, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur", !reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s&%s=%s",
				host, sess1.getPort(), CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.ADMIN_KEY, CamelisXmlName.ACTIF_USER_KEY,
				CoreTestConstants.USER_KEY, CamelisXmlName.ROLE,
				rightValue.toString());
		String comment = String
				.format("setRole, port=%s, serveur host=%s, tous les paramètres sont présents",
						sess1.getPort(), host);
		doc.pushDoc("setRole", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void setRole5() throws PortalisException,
			IOException, InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		Session sess1 = UtilTest.startOneCamelis(sessUser);

		RightValue rightValue0 = RightValue.COLLABORATOR;
		String userKey = CoreTestConstants.USER_KEY;

		UtilTest.registerUser(sess1, userKey, rightValue0);

		String role = "noRole";
		SetRoleReponse reponse = CamelisHttp.setRole(sess1,
				CoreTestConstants.USER_KEY, role);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur\nreponse = " + reponse,
				!reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s",
				sess1.getHost(), sess1.getPort(),
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ACTIF_ADMIN_KEY, CoreTestConstants.ADMIN_KEY,
				CamelisXmlName.ROLE, role);
		String comment = String
				.format("*Erreur* : setRole avec port=%s et serveur host=%s, la valeur du paramètre %s est incorrecte,"
						+ " elle vaut %s, les valeurs autorisées sont %s",
						sess1.getHost(), sess1.getPort(), CamelisXmlName.ROLE,
						role, Arrays.toString(RightValue.values()));
		doc.pushDoc("setRole", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		final String TEMOIN = ErrorMessages.TEMOIN_IS_INCORRECT;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void setRole4() throws PortalisException,
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		String role = null;
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		StartReponse startReponse = AdminHttp.startCamelis(sessUser,
				CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
		assertTrue("Le retour ne doit pas être en erreur : " + startReponse,
				startReponse.isOk());

		CamelisService camelisService = startReponse.getCamelisService();
		int port2 = camelisService.getPort();
		ActiveUser activeUser0 = sessUser.getActiveUser();
		Session sess = Session.createCamelisSession(activeUser0, camelisService);

		// ********************* ping *********************
		PingReponse pingReponse = CamelisHttp.ping(sess);
		LOGGER.fine(pingReponse.toString());
		assertTrue("Le retour ne doit pas être en erreur", pingReponse.isOk());

		assertEquals(camelisService, pingReponse.getOneCamelisService());

		RightValue rightValue0 = RightValue.COLLABORATOR;
		String userKey = CoreTestConstants.USER_KEY;

		UtilTest.registerUser(sess, userKey, rightValue0);

		SetRoleReponse reponse = CamelisHttp.setRole(sess,
				CoreTestConstants.USER_KEY, role);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur", !reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s", host,
				port2, CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.ADMIN_KEY, CamelisXmlName.ACTIF_USER_KEY,
				CoreTestConstants.USER_KEY);
		String comment = String
				.format("*Erreur* : setRole avec port=%s et serveur host=%s, manque le paramètre String %s",
						port2, host, CamelisXmlName.ROLE);
		doc.pushDoc("setRole", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		final String TEMOIN = ErrorMessages.TEMOIN_IS_INCORRECT;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void setRole3() throws PortalisException, 
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		StartReponse startReponse = AdminHttp.startCamelis(sessUser,
				CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
		assertTrue("Le retour ne doit pas être en erreur : " + startReponse,
				startReponse.isOk());

		CamelisService camelisService = startReponse.getCamelisService();
		int port1 = camelisService.getPort();
		ActiveUser activeUser0 = sessUser.getActiveUser();
		Session sess0 = Session.createCamelisSession(activeUser0, camelisService);

		RightValue rightValue0 = RightValue.READER;
		String userKey = CoreTestConstants.USER_KEY;

		UtilTest.registerUser(sess0, userKey, rightValue0);

		ActiveUser activeUser = new ActiveUser(
				CoreTestConstants.AU2.getUserCore(), "EAF123");
		Session sess = Session.createCamelisSession(activeUser, camelisService);

		PingReponse pingReponse = CamelisHttp.ping(sess);
		assertTrue("Le retour ne doit pas être en erreur\nreponse ="
				+ pingReponse, pingReponse.isOk());
		assertEquals(camelisService, pingReponse.getOneCamelisService());

		RightValue rightValue = RightValue.COLLABORATOR;
		SetRoleReponse reponse = CamelisHttp.setRole(sess, null, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur\nreponse =" + reponse,
				!reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s", host, port1,
				CamelisXmlName.ROLE, rightValue.toString());
		String comment = String
				.format("*Erreur* : setRole avec port=%s et serveur host=%s, manque les paramètres %s et %s",
						port1, host, CamelisXmlName.ACTIF_USER_KEY,
						CamelisXmlName.ACTIF_ADMIN_KEY);
		doc.pushDoc("setRole", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void setRole1() throws PortalisException,
			IOException, InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		Session sess1 = UtilTest.loginAndStartCamelis(port, host, 
				email, password);

		RightValue rightValue0 = RightValue.NONE;
		String userKey = CoreTestConstants.USER_KEY;

		UtilTest.registerUser(sess1, userKey, rightValue0);

		RightValue rightValue1 = RightValue.ADMIN;

		SetRoleReponse setRoleReponse = CamelisHttp.setRole(sess1,
				CoreTestConstants.USER_KEY, rightValue1);
		LOGGER.info("SetRoleReponse = " + setRoleReponse);
		assertTrue("Le retour ne doit pas être en erreur",
				setRoleReponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n"
				+ setRoleReponse, setRoleReponse.getUser() != null);

		assertEquals(rightValue1, setRoleReponse.getUser().getRole());

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s&%s=%s",
				host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.ADMIN_KEY, // même key que celui qui a lancé
												// Camelis
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ROLE, rightValue1.toString());
		String comment = String
				.format("setRole, port=%s, serveur host=%s, tous les paramètres sont présents",
						sess1.getPort(), host);
		doc.pushDoc("setRole", comment, url, setRoleReponse);

		assertEquals(CoreTestConstants.USER_KEY, setRoleReponse.getUser()
				.getKey());
		assertEquals(rightValue1, setRoleReponse.getUser().getRole());
	}

	private static void registerKey4() throws 
			PortalisException, IOException, InterruptedException,
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		Session sess1 = UtilTest.loginAndStartCamelis(port, host, 
				email, password);

		LOGGER.fine("testRegisterKey2() session = " + sess1);

		LOGGER.fine("\n\n              ===========================================> registerKey 1 <============================");
		RightValue rightValue = RightValue.ADMIN;
		RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
				CoreTestConstants.USER_KEY, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour ne doit pas être en erreur, reponse =" + reponse,
				reponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n" + reponse,
				reponse.getUser() != null);

		assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
		assertEquals(rightValue, reponse.getUser().getRole());

		LOGGER.fine("\n\n              ===========================================> registerKey 2 <============================");
		RightValue rightValue1 = RightValue.NONE;
		RegisterKeyReponse reponse1 = CamelisHttp.registerKey(sess1,
				CoreTestConstants.USER_KEY, rightValue1);
		LOGGER.info("SetRoleReponse = " + reponse1);
		assertTrue("Le retour doit être en erreur. reponse = " + reponse1,
				!reponse1.isOk());
		assertTrue("Un utilisateur ne pas doit être là. La réponse est :\n"
				+ reponse1, reponse1.getUser() == null);

		String url = String.format(
				"http://%s:%s/registerKey?%s=%s&%s=%s&%s=%s", host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.AU1.getPortalisSessionId(), // même key que celui qui a
				// lancé Camelis
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ROLE, rightValue1.toString());
		String comment = String
				.format("registerKey, port=%s, serveur host=%s, la clé %s est déjà enregistrée",
						sess1.getPort(), host, CoreTestConstants.USER_KEY);
		doc.pushDoc("registerKey", comment, url, reponse);

		final String TEMOIN = ErrorMessages.TEMOIN_IS_ALREADY_REGISTERED;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse1,
				reponse1.messagesContains(TEMOIN));
	}

	private static void setRole2() throws PortalisException,
			IOException, InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		Session sess1 = UtilTest.loginAndStartCamelis(port, host, 
				email, password);

		RightValue rightValue0 = RightValue.NONE;
		String userKey = CoreTestConstants.USER_KEY;

		UtilTest.registerUser(sess1, userKey, rightValue0);

		RightValue rightValue = RightValue.EDITOR;
		SetRoleReponse reponse = CamelisHttp.setRole(sess1, null, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour doit être en erreur", !reponse.isOk());
		assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
				+ reponse, reponse.getUser() == null);

		String url = String.format("http://%s:%s/setRole?%s=%s&%s=%s", host,
				sess1.getPort(), CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.ADMIN_KEY, CamelisXmlName.ROLE,
				rightValue.toString());
		String comment = String
				.format("*Erreur* : setRole avec port=%s et serveur host=%s, manque le paramètre %s",
						sess1.getPort(), host, CamelisXmlName.ACTIF_USER_KEY);
		doc.pushDoc("setRole", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));
	}

	private static void registerKey3() throws PortalisException,
			InterruptedException, IOException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		Session sess1 = UtilTest.loginAndStartCamelis(port, host, 
				email, password);

		LOGGER.fine("testRegisterKey2() session = " + sess1);

		LOGGER.fine("\n\n              ===========================================> registerKey 1 <============================");
		RightValue rightValue = RightValue.ADMIN;
		RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
				CoreTestConstants.USER_KEY, rightValue);
		LOGGER.info("SetRoleReponse = " + reponse);
		assertTrue("Le retour ne doit pas être en erreur, reponse =" + reponse,
				reponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n" + reponse,
				reponse.getUser() != null);

		assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
		assertEquals(rightValue, reponse.getUser().getRole());

		LOGGER.fine("\n\n              ===========================================> registerKey 2 <============================");
		RightValue rightValue1 = RightValue.NONE;
		RegisterKeyReponse reponse1 = CamelisHttp.registerKey(sess1,
				CoreTestConstants.USER_KEY, rightValue1);
		LOGGER.info("SetRoleReponse = " + reponse1);
		assertTrue("Le retour doit être en erreur. reponse = " + reponse1,
				!reponse1.isOk());
		assertTrue("Un utilisateur ne pas doit être là. La réponse est :\n"
				+ reponse1, reponse1.getUser() == null);

		String url = String.format(
				"http://%s:%s/registerKey?%s=%s&%s=%s&%s=%s", host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.AU1.getPortalisSessionId(), // même key que celui qui a
				// lancé Camelis
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ROLE, rightValue1.toString());
		String comment = String
				.format("registerKey, port=%s, serveur host=%s, la clé %s est déjà enregistrée",
						sess1.getPort(), host, CoreTestConstants.USER_KEY);
		doc.pushDoc("registerKey", comment, url, reponse);

		final String TEMOIN = ErrorMessages.TEMOIN_IS_ALREADY_REGISTERED;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse1,
				reponse1.messagesContains(TEMOIN));
	}

	private static void registerKey1() throws PortalisException,
			IOException,
			InterruptedException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		Session session = loginReponse.getSession();
		Session sess1 = UtilTest.startOneCamelis(session);

		RightValue rightValue = RightValue.NONE;

		// ******************* registerKey ****************
		RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
				CoreTestConstants.USER_KEY, rightValue);
		LOGGER.info("testRegisterKeyNoPort()" + reponse);

		assertTrue("Le retour ne doit pas être en erreur", reponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n" + reponse,
				reponse.getUser() != null);

		String url = String.format(
				"http://%s:%s/registerKey?%s=%s&%s=%s&%s=%s", host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.AU1.getPortalisSessionId(), // même key que celui qui a
				// lancé Camelis
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ROLE, rightValue.toString());
		String comment = String
				.format("registerKey, port=%s, serveur host=%s, tous les paramètres sont présents",
						sess1.getPort(), host);
		doc.pushDoc("registerKey", comment, url, reponse);

		assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
		assertEquals(rightValue, reponse.getUser().getRole());
	}

	private static void registerKey2() throws 
			PortalisException, IOException, InterruptedException,
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ********************* startCamelis *********************
		Session session = loginReponse.getSession();
		Session sess1 = UtilTest.startOneCamelis(session);

		RightValue rightValue = RightValue.NONE;

		// ******************* registerKey ****************
		RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
				CoreTestConstants.USER_KEY, rightValue);
		LOGGER.info("testRegisterKeyNoPort()" + reponse);

		assertTrue("Le retour ne doit pas être en erreur", reponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n" + reponse,
				reponse.getUser() != null);

		String url = String.format(
				"http://%s:%s/registerKey?%s=%s&%s=%s&%s=%s", host,
				sess1.getPort(),
				CamelisXmlName.ACTIF_ADMIN_KEY,
				CoreTestConstants.AU1.getPortalisSessionId(), // même key que celui qui a
				// lancé Camelis
				CamelisXmlName.ACTIF_USER_KEY, CoreTestConstants.USER_KEY,
				CamelisXmlName.ROLE, rightValue.toString());
		String comment = String
				.format("registerKey, port=%s, serveur host=%s, tous les paramètres sont présents",
						sess1.getPort(), host);
		doc.pushDoc("registerKey", comment, url, reponse);

		assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
		assertEquals(rightValue, reponse.getUser().getRole());
	}

	private static void startCamelis_jsp1() throws PortalisDocumentException,
			PortalisException {
		LinuxCmd.killAllCamelisProcess();
		String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
		String[] parameters = { XmlIdentifier.KEY(),
				CoreTestConstants.ADMIN_KEY, XmlIdentifier.CREATOR(),
				CoreTestConstants.MAIL_YVES, XmlIdentifier.SERVICE_NAME(),
				serviceName };
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
				email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);

		// ******************* startCamelis ****************
		StartReponse startReponse = AdminHttp.startCamelis(sessUser,
				serviceName, null, null);
		assertTrue("La réponse doit être sans erreur, startReponse ="
				+ startReponse, startReponse.isOk());
		LOGGER.info("StartReponse = " + startReponse.toString());

		String url = UtilCargo.buildURL("startCamelis.jsp", parameters);
		doc.pushDoc("startCamelis.jsp",
				"Pas de port spécifié. C'est le serveur Portalis qui choisit",
				url, startReponse);

		Set<PortActif> actifs = LinuxCmd.getActifsCamelisPortsOnPortalis();
		assertTrue("Liste d'actifs null", actifs != null);
		assertEquals(1, actifs.size());
		CamelisService camelisService = startReponse.getCamelisService();
		PortActif portActif = new PortActif(host, startReponse
				.getCamelisService().getPort(), camelisService.getPid());
		assertTrue("le port actif " + portActif
				+ " doit être dans la liste des actfs",
				actifs.contains(portActif));
		assertEquals(serviceName, camelisService.getServiceCore().getFullName());
		assertEquals(CoreTestConstants.UC1.getEmail(),
				camelisService.getCreator());
	}

	private static void ping3() throws 
			PortalisException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = 0;
		String host = null;

		PingReponse reponse = CamelisHttp.ping(Session.getUnidentifiedSession(host, port));
		LOGGER.info("testPingErrorPort0HostNull()" + reponse);

		String url = String.format("http://%s:%s/ping", host, port);
		String comment = String.format(
				"Ping avec deux problèmes : port = %s et serveur = %s", port,
				host);
		doc.pushDoc("ping", comment, url, reponse);

		assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
		String mess = ErrorMessages.TEMOIN_PORT_POSITIF;
		assertTrue("le message " + mess + " devrait être présent",
				reponse.messagesContains(mess));
	}

	private static void ping2() throws PortalisException, 
			PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortService.choosePort();
		String host = PortalisService.getPORTALIS_HOST();
		PingReponse reponse = CamelisHttp.ping(Session.getUnidentifiedSession(host, port));
		assertTrue("La réponse de être en erreur, elle vaut : " + reponse,
				!reponse.isOk());
		final String TEMOIN = ErrorMessages.TEMOIN_IO_FAILURE;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + reponse,
				reponse.messagesContains(TEMOIN));

		String url = String.format("http://%s:%s/ping", host, port);
		String comment = String.format(
				"Ping sur un port=%s et un serveur=%s, mais"
						+ " il n'y a pas de processus à l'écoute de ce port",
				port, host);
		doc.pushDoc("ping", comment, url, reponse);
	}

	private static void ping1() throws PortalisException, PortalisDocumentException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		LOGGER.fine("\n\n              ===========================================> login <============================");
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());

		LOGGER.fine("\n\n              ===========================================> startCamelis <============================");
		Session sess = loginReponse.getSession();
		StartReponse startReponse = AdminHttp.startCamelis(sess,
				CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
		assertTrue("Le retour ne doit pas être en erreur : " + startReponse,
				startReponse.isOk());

		CamelisService camelisService = startReponse.getCamelisService();
		assertTrue("camelisService ne doit pas être null, startReponse ="
				+ startReponse, camelisService != null);

		PingReponse pingReponse = CamelisHttp.ping(Session.createCamelisSession(sess.getActiveUser(), camelisService));
		LOGGER.info("testPingSuccessDefaultPort()" + pingReponse);

		assertTrue("Le retour ne doit pas être en erreur", pingReponse.isOk());
		assertEquals(camelisService, pingReponse.getOneCamelisService());

		int port2 = camelisService.getPort();
		String url = String.format("http://%s:%d/ping", host, port2);
		String comment = String
				.format("Ping sur le port %s du serveur %s où un dépot Camelis est lancé",
						port2, host);
		doc.pushDoc("ping", comment, url, pingReponse);

		assertEquals(LinuxCmd.getCamelisProcessId(port2), pingReponse
				.getOneCamelisService().getPid());
	}

	private static ImportCtxReponse importCtx1(String ctxFile) throws PortalisException,
			PortalisDocumentException {

		LOGGER.fine("\n\n              ===========================================> importCtx <============================");
		ImportCtxReponse importReponse = CamelisHttp.importCtx(camelisSession,
				ctxFile);
		assertTrue("La réponse doit être sans erreur, importReponse ="
				+ importReponse, importReponse.isOk());
		return importReponse;
	}

	private static ImportCtxReponse importCtx2() throws PortalisException,
			PortalisDocumentException {

		LOGGER.fine("\n\n              ===========================================> importCtx <============================");
		ImportCtxReponse importReponse = CamelisHttp.importCtx(camelisSession);
		assertTrue("La réponse doit être sans erreur, importReponse ="
				+ importReponse, importReponse.isOk());
		return importReponse;
	}

	private static void startSession() throws PortalisException {
		LinuxCmd.killAllCamelisProcess();
		int port = PortalisService.getPORTALIS_PORT();
		String host = PortalisService.getPORTALIS_HOST();
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		LOGGER.fine("\n\n              ===========================================> login <============================");
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(host, port,
				 email, password);
		LOGGER.fine("\n\n              ===========================================> startCamelis <============================");
		int camelisPort = CoreTestConstants.PORT1;
		Session sess1 = loginReponse.getSession();
		StartReponse startReponse = AdminHttp.startCamelis(sess1,
				camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS, null,
				null);
		assertTrue("Le démarrage de Camelis n'a pas marché",
				startReponse != null);
		CamelisService camelisService = startReponse.getCamelisService();
		assertTrue("pas de service en retour du démarage de Camelis"
				+ startReponse, camelisService != null);

		LOGGER.fine("\n\n              ===========================================> importCtx <============================");
		camelisSession = Session.createCamelisSession(sess1.getActiveUser(), camelisService);
		String ctxFile = "planets/planets/planets.ctx";
		ImportCtxReponse importReponse = CamelisHttp.importCtx(camelisSession,
				ctxFile);
		assertTrue("L'import n'a pas marché", importReponse != null);
		assertTrue("problème d'importCtx : " + importReponse,
				importReponse.isOk());

	}
}
