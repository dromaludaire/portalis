package fr.irisa.lis.portalis.site;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.UtilCargo;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.core.CoreTestConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.irisa.lis.portalis.shared.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.http.admin.AdminHttp;
import fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp;
import fr.irisa.lis.portalis.shared.util.XmlIdentifier;
import fr.irisa.lis.portalis.system.linux.LinuxCmd;
import fr.irisa.lis.portalis.system.linux.PortService;

public class AutoDocPortalis {private static final Logger LOGGER = LoggerFactory.getLogger(AutoDocPortalis.class.getName());

	private static String titre = DocumentationProprietes.documentation
			.getProperty("portalis-titre");
	private static String docId = DocumentationProprietes.documentation
			.getProperty("portalis-id");
	private static Documentation doc;

	private static Session currentUserSession;
	private static int currentPort;
	private static String currentHost;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			doc = new Documentation(titre, docId);
			LinuxCmd.killAllCamelisProcess();
			CargoLauncher.start();
			
			LOGGER.fine("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ début de génération de la documentation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			
			// ******************** teste de la présence des properties ******************
			VoidReponse reponse = AdminHttp.adminPropertyFilesCheck();
			String url = UtilCargo.buildURL("propertiesFileCheck.jsp");
			doc.pushDoc(
					"propertiesFileCheck.jsp",
					"On chercher à voir si les fichiers de properties admin sont accessibles sur le serveur Portalis",
					url, reponse);


			currentPort = PortService.choosePort();
			currentUserSession = logAndStartCamelis(currentPort);
			currentHost = currentUserSession.getHost();

			LOGGER.fine("\n\n              ========================================> getSite <=========================");

			SiteReponse siteReponse = AdminHttp.getSite(currentUserSession);
			
			url = UtilCargo.buildURL("getSite.jsp");
			doc.pushDoc("getSite.jsp", "Interrogation de Portalis", url,
					siteReponse);

			LOGGER.fine("\n\n              ========================================> lsof <=========================");

			url = UtilCargo.buildURL("lsof.jsp", 
					new String[] { XmlIdentifier.PORT(), currentPort + "" });
			PidReponse pidReponse = AdminHttp.lsof(currentUserSession, currentPort);

			doc.pushDoc("lsof.jsp",
					"Rechercher les processus à l'écoute du port " + currentPort,
					url, pidReponse);
		
		
			LOGGER.fine("\n\n              ========================================> lsof Pas de processus à l'écoute du port <=========================");

			int anyPort = 8989;
			url = UtilCargo.buildURL("lsof.jsp", 
					new String[] { XmlIdentifier.PORT(), anyPort + "" });

			Session sess = Session.getUnidentifiedSession(PortalisService.getPORTALIS_HOST(), anyPort);
			PidReponse in = AdminHttp.lsof(sess);

			doc.pushDoc("lsof.jsp", "Pas de processus à l'écoute du port "
					+ anyPort, url, in);

			LOGGER.fine("\n\n              ========================================> ping currentPort <=========================");

			PingReponse pingReponse = CamelisHttp.ping(currentUserSession);
			url = UtilCargo.buildURL("ping.jsp", 
					new String[] { XmlIdentifier.PORT(), currentPort + "",
					XmlIdentifier.HOST(), currentHost});
			doc.pushDoc("ping.jsp", "Test sur le port " + CoreTestConstants.PORT
					+ " occupé par un processus Camelis", url, pingReponse);

			LOGGER.fine("\n\n              ========================================> ping not a Camelis service listening <=========================");
			Session badSession = Session.getPortalisSession(currentUserSession.getActiveUser());
			pingReponse = CamelisHttp.ping(badSession);

			doc.pushDoc(
					"ping.jsp",
					"Test sur le port "
							+ badSession.getPort()
							+ " qui a un processus à l'écoute mais qui n'est pas un processus Camelis",
					url, pingReponse);


			LOGGER.fine("\n\n              ========================================> startCamelis no port specified <=========================");

			String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
			StartReponse startReponse = AdminHttp.startCamelis(currentUserSession,
					serviceName, null, null);

			url = UtilCargo.buildURL("startCamelis.jsp", 
					new String[] { XmlIdentifier.KEY(), CoreTestConstants.ADMIN_KEY,
					XmlIdentifier.CREATOR(), CoreTestConstants.MAIL_YVES,
					XmlIdentifier.SERVICE_NAME(), serviceName });

			doc.pushDoc(
					"startCamelis.jsp",
					"Pas de port spécifié. C'est le serveur Portalis qui choisit",
					url, startReponse);

			LOGGER.fine("\n\n              ========================================> startCamelis on a specified port <=========================");

			String serviceFullName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 12;
			int port1 = PortService.choosePort();
			startReponse = AdminHttp.startCamelis(currentUserSession, port1,
					serviceFullName, null, null);
			url = UtilCargo.buildURL("startCamelis.jsp", 
					new String[] { 
					XmlIdentifier.KEY(), CoreTestConstants.ADMIN_KEY,
					XmlIdentifier.CREATOR(), "anyBody@france.fr",
					XmlIdentifier.SERVICE_NAME(), serviceFullName,
					XmlIdentifier.PORT(), port1 + "" });
			doc.pushDoc("startCamelis.jsp", "Lancement sur le port " + port1
					+ ".", url, startReponse);
			
			LOGGER.fine("\n\n              ========================================> startCamelis on a specified port already in use par un processus non Camelis <=========================");

			serviceFullName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 12;
			port1 = PortalisService.getPORTALIS_PORT();
			startReponse = AdminHttp.startCamelis(currentUserSession, port1,
					serviceFullName, null, null);
			url = UtilCargo.buildURL("startCamelis.jsp", 
					new String[] { 
					XmlIdentifier.KEY(), CoreTestConstants.ADMIN_KEY,
					XmlIdentifier.CREATOR(), "anyBody@france.fr",
					XmlIdentifier.SERVICE_NAME(), serviceFullName,
					XmlIdentifier.PORT(), port1 + "" });
			doc.pushDoc("startCamelis.jsp", "Lancement sur le port " + port1 + " qui est déjà occupé"
					+ ".", url, startReponse);
			
			LOGGER.fine("\n\n              ========================================> startCamelis arguments needed <=========================");

			String serviceName1 = CoreTestConstants.SERVICE_PLANETS_PHOTO;
			startReponse = AdminHttp.startCamelis(Session.getPortalisSession(CoreTestConstants.AU1), null,
					serviceName1, null, null);

			url = UtilCargo.buildURL("startCamelis.jsp", 
					new String[] { XmlIdentifier.SERVICE_NAME(), serviceName1 });
			doc.pushDoc("startCamelis.jsp",
					"*Erreur* : Paramètres manquants", url, reponse);
			
			LOGGER.fine("\n\n              ========================================> startCamelis port déjà occupé par un processus Camelis <=========================");

			startReponse = AdminHttp.startCamelis(currentUserSession,
					currentPort, CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);

			String url1 = UtilCargo.buildURL("startCamelis.jsp", 
					new String[] { XmlIdentifier.KEY(),
							CoreTestConstants.ADMIN_KEY, XmlIdentifier.CREATOR(),
							CoreTestConstants.MAIL_YVES, XmlIdentifier.SERVICE_NAME(),
							CoreTestConstants.SERVICE_PLANETS_PHOTO + 0, XmlIdentifier.PORT(),
							currentPort+ "" });
			doc.pushDoc("startCamelis.jsp", "Port déjà occupé "
					+ currentPort + ".", url1, startReponse);
			
			LOGGER.fine("\n\n              ========================================> GetCamelisProcessId <=========================");

			pidReponse = AdminHttp.getCamelisProcessId(currentUserSession, null);
			
			url = UtilCargo.buildURL("GetCamelisProcessId.jsp");
			doc.pushDoc("getCamelisProcessId.jsp", String.format(
					"Port par default (port de session  = %s)",
					currentPort), url, pidReponse);
			
			LOGGER.fine("\n\n              ========================================> getActifsPorts <=========================");

			AdminHttp.startCamelis(sess,
					CoreTestConstants.SERVICE_PLANETS_PHOTO + 1, null, null);
			AdminHttp.startCamelis(sess, CoreTestConstants.PORT,
					CoreTestConstants.SERVICE_PLANETS_PHOTO + 2, null, null);
			AdminHttp.startCamelis(sess,
					CoreTestConstants.SERVICE_PLANETS_PHOTO + 3, null, null);
			url = UtilCargo.buildURL("getActifsPorts.jsp");
			PortsActifsReponse paReponse = AdminHttp.getActifsPorts(currentUserSession);
			doc.pushDoc("getActifsPorts.jsp",
					"Trois serveurs Camelis actifs", url, paReponse);
			
			LOGGER.fine("\n\n              ========================================> stop one Camelis service <=========================");

			pingReponse = AdminHttp.stopCamelis(currentUserSession, 
					CoreTestConstants.PORT);
			url = UtilCargo.buildURL("stopCamelis.jsp",
					new String [] {XmlIdentifier.PORT(), CoreTestConstants.PORT + ""});
			doc.pushDoc("stopCamelis.jsp", "stopCamelis on port " + port1,
					url, pingReponse);
			
			LOGGER.fine("\n\n              ========================================> stop all Camelis service <=========================");

			pingReponse = AdminHttp.stopCamelis(currentUserSession);
			url = UtilCargo.buildURL("stopCamelis.jsp");
			doc.pushDoc("stopCamelis.jsp", "Arrêt de tous les service Camelis actifs",
					url, pingReponse);
			
			LOGGER.fine("\n\n              ========================================> getActifsPorts (sans camelis actifs) <=========================");
			paReponse = AdminHttp.getActifsPorts(currentUserSession);

			url = UtilCargo.buildURL("getActifsPorts.jsp");
			doc.pushDoc("getActifsPorts.jsp",
					"Aucun serveur Camelis actif", url, paReponse);

			LOGGER.fine("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Fin de génération de la documentation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			doc.generate();
			CargoLauncher.stop();
			doc = null;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (doc!=null) {
				CargoLauncher.stop();
			}
			try {
				LinuxCmd.killAllCamelisProcess();
			} catch (PortalisException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}

	private static Session logAndStartCamelis(int port1) {
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();

		// ********************* login *********************
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email, password);
		Session sessUser = null;
		try {
			sessUser = loginReponse.getSession();
		} catch (PortalisException e) {
			fail("Il doit y avoir une session");
		}

		// ******************* startCamelis ****************
		StartReponse startReponse = AdminHttp.startCamelis(sessUser, port1,
				CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
		assertTrue("La réponse doit être sans erreur, startReponse ="
				+ startReponse, startReponse.isOk());
		
		return sessUser;
	}

}
