package fr.irisa.lis.portalis.service.utilities;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.File2DocumentXmlParser;
import fr.irisa.lis.portalis.commons.geoloc.LambertConformalConicProjection;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune;


public class Eval {
	

	static final XPathFactory FACTORY = XPathFactory.newInstance();
	static final XPath xpath = FACTORY.newXPath();
	static XPathExpression exprSTATUS;
	static XPathExpression exprREF;
	static XPathExpression exprID;
	static XPathExpression exprDATE_STAMP;
	static XPathExpression exprZONE;
	static XPathExpression exprX;
	static XPathExpression exprY;
	static XPathExpression exprCI_TYPE;
	static XPathExpression exprDESI;
	static XPathExpression exprLOCA;
	static XPathExpression exprHIST;

	static boolean first = true;
	private static StringBuffer buff;

	private static enum DOS_Attibutes {
		COPY, DBOR, DENQ, DOSS, ETUD, NOMS, RENV, TYPE;
	}

	private static enum DESI_Elements {
		COLL, DENO, NART, TICO, PDEN, VOCA, PART;
	}

	private static enum LOCA_Elements {
//		INSEE, 
		AIRE, CANT, IMPL;
	}
	
	private static enum HIST_Elements {
		SCLE, EXEC, SCLD, DATE, JDAT, AUTR, JATT, ORIG;
	}


	static {
		try {
			exprSTATUS = xpath.compile("header/@status='deleted'");
			exprREF = xpath.compile("normalize-space(metadata/CI/@REF)");
			exprID = xpath.compile("normalize-space(header/identifier)");
			exprDATE_STAMP = xpath.compile("normalize-space(header/datestamp)");
			exprZONE = xpath
					.compile("normalize-space(metadata/CI/DOSARCH/GEOARCHI/ZONE)");
			exprX = xpath
					.compile("normalize-space(metadata/CI/DOSARCH/GEOARCHI/COOR/X)");
			exprY = xpath
					.compile("normalize-space(metadata/CI/DOSARCH/GEOARCHI/COOR/Y)");

			exprCI_TYPE = xpath
					.compile("metadata/CI/DOSARCH | metadata/CI/DOSOBJT | metadata/CI/DOSCOLL");

			exprDESI = xpath.compile("NOTIARCH/DESIARCH | NOTIOBJT/DESIOBJT");
			exprLOCA = xpath.compile("NOTIARCH/LOCAARCH | NOTIOBJT/LOCAOBJT");
			exprHIST = xpath.compile("NOTIARCH/HISTARCH | NOTIOBJT/HISTOBJT");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// String zone=args[0];
		// String X=args[1];
		// String Y=args[2];
		// System.out.println(toWGS84(zone, X, Y));
		File root = new File("/local/bekkers/patrimoineBreton");
		for (String fileName : root.list()) {
			try {
				if (fileName.endsWith(".xml")) {
					Document doc = new File2DocumentXmlParser(new FileInputStream(new File(root, fileName))).parse();
					NodeList records = doc.getElementsByTagName("record");
					System.out.println(fileName + " : " + records.getLength());
					for (int i = 0; i < records.getLength(); i++) {
						String result = getRecord(records.item(i));
						System.out.println(result == null ? "null" : result);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static String getRecord(Node item) throws XPathExpressionException {
		Boolean deleted = (Boolean) exprSTATUS.evaluate(item,
				XPathConstants.BOOLEAN);
		if (!deleted) {
			String ref = ((String) exprREF
					.evaluate(item, XPathConstants.STRING));
			buff = new StringBuffer("mk \"").append(ref).append("\"");
			first = true;

			String id = ((String) exprID.evaluate(item, XPathConstants.STRING));
			push("id", id);

			String dateStamp = ((String) exprDATE_STAMP.evaluate(item,
					XPathConstants.STRING));
			push("datestamp", dateStamp);

			pushDOS(item);

			String zone = ((String) exprZONE.evaluate(item,
					XPathConstants.STRING));
			String X = ((String) exprX.evaluate(item, XPathConstants.STRING));
			String Y = ((String) exprY.evaluate(item, XPathConstants.STRING));
			push("gps", toWGS84(zone, X, Y));
			return buff.toString();
		}
		return null;
	}

	private static void pushDOS(Node item) throws XPathExpressionException {
		Node ciType = ((Node) exprCI_TYPE.evaluate(item, XPathConstants.NODE));
		push("ciType", ciType.getNodeName());

		for (DOS_Attibutes att : DOS_Attibutes.values()) {
			XPathExpression expr = xpath.compile("@" + att.toString());
			String[] values = ((String) expr.evaluate(ciType,
					XPathConstants.STRING)).split(";");
			for (String val : values) {
				push(att.toString(), val);
			}
		}
		
		NodeList desiList = (NodeList) exprDESI.evaluate(ciType,
				XPathConstants.NODESET);
		for (int i = 0; i < desiList.getLength(); i++) {
			for (DESI_Elements elem : DESI_Elements.values()) {
				XPathExpression expr = xpath.compile(elem.toString());
				String[] values = ((String) expr.evaluate(desiList.item(i),
						XPathConstants.STRING)).split(";");
				for (String val : values) {
					push(elem.toString(), val);
				}
			}
		}
		
		NodeList locaList = (NodeList) exprLOCA.evaluate(ciType,
				XPathConstants.NODESET);
		for (int i = 0; i < locaList.getLength(); i++) {
			for (LOCA_Elements elem : LOCA_Elements.values()) {
				XPathExpression expr = xpath.compile(elem.toString());
				String[] values = ((String) expr.evaluate(locaList.item(i),
						XPathConstants.STRING)).split(";");
				for (String val : values) {
					push(elem.toString(), val);
				}
			}
		}
		
		NodeList histList = (NodeList) exprHIST.evaluate(ciType,
				XPathConstants.NODESET);
		for (int i = 0; i < locaList.getLength(); i++) {
			for (HIST_Elements elem : HIST_Elements.values()) {
				XPathExpression expr = xpath.compile(elem.toString());
				String[] values = ((String) expr.evaluate(histList.item(i),
						XPathConstants.STRING)).split(";");
				for (String val : values) {
					push(elem.toString(), val);
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private static void pushDESI(Node item) throws XPathExpressionException {
		for (DESI_Elements elem : DESI_Elements.values()) {
			XPathExpression expr = xpath.compile(elem.toString());
			String[] values = ((String) expr.evaluate(item,
					XPathConstants.STRING)).split(";");
			for (String val : values) {
				push(elem.toString(), val);
			}
		}
	}

	private static void push(String name, String id) {
		if (id != null && id.length() != 0) {
			if (first) {
				first = false;
			} else {
				buff.append(",");
			}
			buff.append(" ")
					.append(name)
					.append(" is \"")
					.append(id.replaceFirst("^\\b*", "")
							.replaceFirst("\\b*$", "").replaceAll("\"", "\\\""))
					.append("\"");
		}
	}

	private static String toWGS84(String zone, String X, String Y) {
		Double XX = Double.parseDouble(X.split(" ")[0].replace(",", "."));
		Double YY = Double.parseDouble(Y.split(" ")[0].replace(",", "."));

		double[] coordonnee = null;

		if (zone != null && zone.length() != 0 && X != null && X.length() != 0
				&& Y != null && Y.length() != 0) {
			if (zone.equals("Lambert0")) {
				coordonnee = LambertConformalConicProjection
						.lambertIIEtendutoWGS84(YY, XX);
			} else if (zone.equals("Lambert1")) {
				coordonnee = LambertConformalConicProjection.lambertItoWGS84(
						YY, XX);
			} else if (zone.equals("Lambert2")) {
				coordonnee = LambertConformalConicProjection.lambertIItoWGS84(
						YY, XX);
			} else if (zone.equals("Lambert93")) {
				coordonnee = LambertConformalConicProjection.lambert93toWGS84(
						YY, XX);
			}
		}
		return coordonnee == null ? "" : coordonnee[1] + " ; " + coordonnee[0];
	}

}
