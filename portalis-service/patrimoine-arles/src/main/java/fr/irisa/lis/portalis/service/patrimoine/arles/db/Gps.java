package fr.irisa.lis.portalis.service.patrimoine.arles.db;

public class Gps {
	
	private double longitude;
	private double latitude;
	private boolean isApproximated;
	

	
	public Gps(String longitude, String latitude) {
        this.longitude = Double.parseDouble(longitude.replace(',', '.').replaceAll("null", "0.0"));
        this.latitude = Double.parseDouble(latitude.replace(',', '.').replaceAll("null", "0.0"));
        this.isApproximated = false;
}
   
public Gps(String longitude, String latitude, boolean isApproximated) {
        this.longitude = Double.parseDouble(longitude.replace(',', '.').replaceAll("null", "0.0"));
        this.latitude = Double.parseDouble(latitude.replace(',', '.').replaceAll("null", "0.0"));
        this.isApproximated = isApproximated;
}
	
	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public boolean isApproximated() {
		return isApproximated;
	}
	
	public String toString() {
		return new StringBuffer()
			.append(Double.toString(longitude))
			.append(", ")
			.append(Double.toString(latitude))
			.append(" - ")
			.append(Boolean.toString(isApproximated))
			.toString();
	}


}
