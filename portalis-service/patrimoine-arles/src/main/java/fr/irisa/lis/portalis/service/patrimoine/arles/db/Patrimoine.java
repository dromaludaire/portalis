/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class Patrimoine.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Patrimoine extends PatrimoineType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Patrimoine() {
        super();
    }

}
