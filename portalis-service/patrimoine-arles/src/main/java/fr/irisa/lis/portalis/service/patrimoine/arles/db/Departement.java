/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class Departement.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Departement extends DepartementType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Departement() {
        super();
    }

    public Departement(final java.lang.String defaultValue) {
        super(defaultValue);
    }

}
