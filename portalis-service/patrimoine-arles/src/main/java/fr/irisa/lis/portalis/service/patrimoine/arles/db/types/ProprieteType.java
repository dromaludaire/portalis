/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db.types;

/**
 * Enumeration ProprieteType.
 * 
 * @version $Revision$ $Date$
 */
public enum ProprieteType {


      //------------------/
     //- Enum Constants -/
    //------------------/

    /**
     * Constant VALUE_0
     */
    VALUE_0("Etat"),
    /**
     * Constant VALUE_1
     */
    VALUE_1("Communale"),
    /**
     * Constant VALUE_2
     */
    VALUE_2("Privée"),
    /**
     * Constant VALUE_3
     */
    VALUE_3("Eglise (Catholique ou Réformée)"),
    /**
     * Constant VALUE_4
     */
    VALUE_4("Ier siècle avant JC"),
    /**
     * Constant VALUE_5
     */
    VALUE_5("Religion");

      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field value.
     */
    private final java.lang.String value;

    /**
     * Field enumConstants.
     */
    private static final java.util.Map<java.lang.String, ProprieteType> enumConstants = new java.util.HashMap<java.lang.String, ProprieteType>();


    static {
        for (ProprieteType c: ProprieteType.values()) {
            ProprieteType.enumConstants.put(c.value, c);
        }

    };


      //----------------/
     //- Constructors -/
    //----------------/

    private ProprieteType(final java.lang.String value) {
        this.value = value;
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method fromValue.
     * 
     * @param value
     * @return the constant for this value
     */
    public static fr.irisa.lis.portalis.service.patrimoine.arles.db.types.ProprieteType fromValue(
            final java.lang.String value) {
        ProprieteType c = ProprieteType.enumConstants.get(value);
        if (c != null) {
            return c;
        }
        throw new IllegalArgumentException(value);
    }

    /**
     * 
     * 
     * @param value
     */
    public void setValue(
            final java.lang.String value) {
    }

    /**
     * Method toString.
     * 
     * @return the value of this constant
     */
    public java.lang.String toString(
    ) {
        return this.value;
    }

    /**
     * Method value.
     * 
     * @return the value of this constant
     */
    public java.lang.String value(
    ) {
        return this.value;
    }

}
