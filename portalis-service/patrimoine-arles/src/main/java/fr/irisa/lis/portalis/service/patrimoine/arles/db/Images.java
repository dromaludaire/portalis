/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class Images.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Images extends ImagesType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Images() {
        super();
    }

}
