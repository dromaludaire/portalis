/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class ImagesType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class ImagesType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _imageList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Image> _imageList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImagesType() {
        super();
        this._imageList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Image>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vImage
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addImage(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Image vImage)
    throws java.lang.IndexOutOfBoundsException {
        this._imageList.addElement(vImage);
    }

    /**
     * 
     * 
     * @param index
     * @param vImage
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addImage(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Image vImage)
    throws java.lang.IndexOutOfBoundsException {
        this._imageList.add(index, vImage);
    }

    /**
     * Method enumerateImage.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Image elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.Image> enumerateImage(
    ) {
        return this._imageList.elements();
    }

    /**
     * Method getImage.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Image at the
     * given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Image getImage(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._imageList.size()) {
            throw new IndexOutOfBoundsException("getImage: Index value '" + index + "' not in range [0.." + (this._imageList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Image) _imageList.get(index);
    }

    /**
     * Method getImage.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Image[] getImage(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.Image[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.Image[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Image[]) this._imageList.toArray(array);
    }

    /**
     * Method getImageCount.
     * 
     * @return the size of this collection
     */
    public int getImageCount(
    ) {
        return this._imageList.size();
    }

    /**
     */
    public void removeAllImage(
    ) {
        this._imageList.clear();
    }

    /**
     * Method removeImage.
     * 
     * @param vImage
     * @return true if the object was removed from the collection.
     */
    public boolean removeImage(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Image vImage) {
        boolean removed = _imageList.remove(vImage);
        return removed;
    }

    /**
     * Method removeImageAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Image removeImageAt(
            final int index) {
        java.lang.Object obj = this._imageList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Image) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vImage
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setImage(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Image vImage)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._imageList.size()) {
            throw new IndexOutOfBoundsException("setImage: Index value '" + index + "' not in range [0.." + (this._imageList.size() - 1) + "]");
        }

        this._imageList.set(index, vImage);
    }

    /**
     * 
     * 
     * @param vImageArray
     */
    public void setImage(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Image[] vImageArray) {
        //-- copy array
        _imageList.clear();

        for (int i = 0; i < vImageArray.length; i++) {
                this._imageList.add(vImageArray[i]);
        }
    }

}
