/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class CirconscriptionEuropeenne.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CirconscriptionEuropeenne extends CirconscriptionEuropeenneType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public CirconscriptionEuropeenne() {
        super();
    }

    public CirconscriptionEuropeenne(final java.lang.String defaultValue) {
        super(defaultValue);
    }

}
