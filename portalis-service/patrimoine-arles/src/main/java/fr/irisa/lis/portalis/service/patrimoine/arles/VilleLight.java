package fr.irisa.lis.portalis.service.patrimoine.arles;


public class VilleLight {

	/**
	 * Field _idville.
	 */
	private byte _idville;
	/**
	 * keeps track of state for field: _idville
	 */
	private boolean _has_idville;
	/**
	 * Field _nomville.
	 */
	private java.lang.String _nomville;
	/**
	 * Field _site.
	 */
	private java.lang.String _site;

	public VilleLight() {
		super();
	}

	/**
	 */
	public void deleteIdville() {
	    this._has_idville= false;
	}

	/**
	 * Returns the value of field 'idville'.
	 * 
	 * @return the value of field 'Idville'.
	 */
	public byte getIdville() {
	    return this._idville;
	}

	/**
	 * Returns the value of field 'nomville'.
	 * 
	 * @return the value of field 'Nomville'.
	 */
	public java.lang.String getNomville() {
	    return this._nomville;
	}

	/**
	 * Returns the value of field 'site'.
	 * 
	 * @return the value of field 'Site'.
	 */
	public java.lang.String getSite() {
	    return this._site;
	}

	/**
	 * Method hasIdville.
	 * 
	 * @return true if at least one Idville has been added
	 */
	public boolean hasIdville() {
	    return this._has_idville;
	}

	/**
	 * Sets the value of field 'idville'.
	 * 
	 * @param idville the value of field 'idville'.
	 */
	public void setIdville(final byte idville) {
	    this._idville = idville;
	    this._has_idville = true;
	}

	/**
	 * Sets the value of field 'nomville'.
	 * 
	 * @param nomville the value of field 'nomville'.
	 */
	public void setNomville(final java.lang.String nomville) {
	    this._nomville = nomville;
	}

	/**
	 * Sets the value of field 'site'.
	 * 
	 * @param site the value of field 'site'.
	 */
	public void setSite(final java.lang.String site) {
	    this._site = site;
	}

}