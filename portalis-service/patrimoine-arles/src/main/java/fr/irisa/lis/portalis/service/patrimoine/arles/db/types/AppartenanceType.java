/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db.types;


/**
 * Enumeration AppartenanceType.
 * 
 * @version $Revision$ $Date$
 */
public enum AppartenanceType {


      //------------------/
     //- Enum Constants -/
    //------------------/

    /**
     * Constant VALUE_0
     */
    VALUE_0("Monument historique classé"),
    /**
     * Constant VALUE_1
     */
    VALUE_1("Monument historique inscrit"),
    /**
     * Constant VALUE_2
     */
    VALUE_2("Civil"),
    /**
     * Constant VALUE_3
     */
    VALUE_3("Site classé"),
    /**
     * Constant VALUE_4
     */
    VALUE_4("Site inscrit"),
    /**
     * Constant VALUE_5
     */
    VALUE_5("Architecture privée     ");

      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field value.
     */
    private final java.lang.String value;

    /**
     * Field enumConstants.
     */
    private static final java.util.Map<java.lang.String, AppartenanceType> enumConstants = new java.util.HashMap<java.lang.String, AppartenanceType>();


    static {
        for (AppartenanceType c: AppartenanceType.values()) {
            AppartenanceType.enumConstants.put(c.value, c);
        }

    };


      //----------------/
     //- Constructors -/
    //----------------/

    private AppartenanceType(final java.lang.String value) {
        this.value = value;
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method fromValue.
     * 
     * @param value
     * @return the constant for this value
     */
    public static fr.irisa.lis.portalis.service.patrimoine.arles.db.types.AppartenanceType fromValue(
            final java.lang.String value) {
        AppartenanceType c = AppartenanceType.enumConstants.get(value);
        if (c != null) {
            return c;
        }
        throw new IllegalArgumentException(value);
    }

    /**
     * 
     * 
     * @param value
     */
    public void setValue(
            final java.lang.String value) {
    }

    /**
     * Method toString.
     * 
     * @return the value of this constant
     */
    public java.lang.String toString(
    ) {
        return this.value;
    }

    /**
     * Method value.
     * 
     * @return the value of this constant
     */
    public java.lang.String value(
    ) {
        return this.value;
    }

}
