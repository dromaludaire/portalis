/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class VillesType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class VillesType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _status.
     */
    private java.lang.String _status;

    /**
     * Field _villeList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville> _villeList;


      //----------------/
     //- Constructors -/
    //----------------/

    public VillesType() {
        super();
        this._villeList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vVille
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addVille(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville vVille)
    throws java.lang.IndexOutOfBoundsException {
        this._villeList.addElement(vVille);
    }

    /**
     * 
     * 
     * @param index
     * @param vVille
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addVille(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville vVille)
    throws java.lang.IndexOutOfBoundsException {
        this._villeList.add(index, vVille);
    }

    /**
     * Method enumerateVille.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Ville elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville> enumerateVille(
    ) {
        return this._villeList.elements();
    }

    /**
     * Returns the value of field 'status'.
     * 
     * @return the value of field 'Status'.
     */
    public java.lang.String getStatus(
    ) {
        return this._status;
    }

    /**
     * Method getVille.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Ville at the
     * given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville getVille(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._villeList.size()) {
            throw new IndexOutOfBoundsException("getVille: Index value '" + index + "' not in range [0.." + (this._villeList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville) _villeList.get(index);
    }

    /**
     * Method getVille.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville[] getVille(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville[]) this._villeList.toArray(array);
    }

    /**
     * Method getVilleCount.
     * 
     * @return the size of this collection
     */
    public int getVilleCount(
    ) {
        return this._villeList.size();
    }

    /**
     */
    public void removeAllVille(
    ) {
        this._villeList.clear();
    }

    /**
     * Method removeVille.
     * 
     * @param vVille
     * @return true if the object was removed from the collection.
     */
    public boolean removeVille(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville vVille) {
        boolean removed = _villeList.remove(vVille);
        return removed;
    }

    /**
     * Method removeVilleAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville removeVilleAt(
            final int index) {
        java.lang.Object obj = this._villeList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville) obj;
    }

    /**
     * Sets the value of field 'status'.
     * 
     * @param status the value of field 'status'.
     */
    public void setStatus(
            final java.lang.String status) {
        this._status = status;
    }

    /**
     * 
     * 
     * @param index
     * @param vVille
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setVille(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville vVille)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._villeList.size()) {
            throw new IndexOutOfBoundsException("setVille: Index value '" + index + "' not in range [0.." + (this._villeList.size() - 1) + "]");
        }

        this._villeList.set(index, vVille);
    }

    /**
     * 
     * 
     * @param vVilleArray
     */
    public void setVille(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Ville[] vVilleArray) {
        //-- copy array
        _villeList.clear();

        for (int i = 0; i < vVilleArray.length; i++) {
                this._villeList.add(vVilleArray[i]);
        }
    }

}
