/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db.types;

/**
 * Enumeration CategoriedelarchitectureType.
 * 
 * @version $Revision$ $Date$
 */
public enum CategoriedelarchitectureType {


      //------------------/
     //- Enum Constants -/
    //------------------/

    /**
     * Constant VALUE_0
     */
    VALUE_0("Architecture religieuse, funéraire et commémorative"),
    /**
     * Constant VALUE_1
     */
    VALUE_1("Architecture publique     "),
    /**
     * Constant VALUE_2
     */
    VALUE_2("Architecture religieuse, funéraire et      commémorative"),
    /**
     * Constant VALUE_3
     */
    VALUE_3("Urbanisme"),
    /**
     * Constant VALUE_4
     */
    VALUE_4("Architecture publique"),
    /**
     * Constant VALUE_5
     */
    VALUE_5("Architecture privée     "),
    /**
     * Constant VALUE_6
     */
    VALUE_6("Architecture militaire     "),
    /**
     * Constant VALUE_7
     */
    VALUE_7("Architecture industrielle     "),
    /**
     * Constant VALUE_8
     */
    VALUE_8("Architecture fortifiée");

      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field value.
     */
    private final java.lang.String value;

    /**
     * Field enumConstants.
     */
    private static final java.util.Map<java.lang.String, CategoriedelarchitectureType> enumConstants = new java.util.HashMap<java.lang.String, CategoriedelarchitectureType>();


    static {
        for (CategoriedelarchitectureType c: CategoriedelarchitectureType.values()) {
            CategoriedelarchitectureType.enumConstants.put(c.value, c);
        }

    };


      //----------------/
     //- Constructors -/
    //----------------/

    private CategoriedelarchitectureType(final java.lang.String value) {
        this.value = value;
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method fromValue.
     * 
     * @param value
     * @return the constant for this value
     */
    public static fr.irisa.lis.portalis.service.patrimoine.arles.db.types.CategoriedelarchitectureType fromValue(
            final java.lang.String value) {
        CategoriedelarchitectureType c = CategoriedelarchitectureType.enumConstants.get(value);
        if (c != null) {
            return c;
        }
        throw new IllegalArgumentException(value);
    }

    /**
     * 
     * 
     * @param value
     */
    public void setValue(
            final java.lang.String value) {
    }

    /**
     * Method toString.
     * 
     * @return the value of this constant
     */
    public java.lang.String toString(
    ) {
        return this.value;
    }

    /**
     * Method value.
     * 
     * @return the value of this constant
     */
    public java.lang.String value(
    ) {
        return this.value;
    }

}
