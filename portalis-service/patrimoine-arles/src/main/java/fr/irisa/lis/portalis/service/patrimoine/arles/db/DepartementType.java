/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class DepartementType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class DepartementType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _nom.
     */
    private java.lang.String _nom;

    /**
     * Field _numero.
     */
    private byte _numero;

    /**
     * keeps track of state for field: _numero
     */
    private boolean _has_numero;

    /**
     * Field _prefecture.
     */
    private java.lang.String _prefecture;


      //----------------/
     //- Constructors -/
    //----------------/

    public DepartementType() {
        super();
        setContent("");
    }

    public DepartementType(final java.lang.String defaultValue) {
        try {
            setContent( new java.lang.String(defaultValue));
         } catch(Exception e) {
            throw new RuntimeException("Unable to cast default value for simple content!");
         } 
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteNumero(
    ) {
        this._has_numero= false;
    }

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'Content'.
     */
    public java.lang.String getContent(
    ) {
        return this._content;
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    @XmlAttribute
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Returns the value of field 'numero'.
     * 
     * @return the value of field 'Numero'.
     */
    @XmlAttribute
    public byte getNumero(
    ) {
        return this._numero;
    }

    /**
     * Returns the value of field 'prefecture'.
     * 
     * @return the value of field 'Prefecture'.
     */
    @XmlAttribute
    public java.lang.String getPrefecture(
    ) {
        return this._prefecture;
    }

    /**
     * Method hasNumero.
     * 
     * @return true if at least one Numero has been added
     */
    public boolean hasNumero(
    ) {
        return this._has_numero;
    }

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(
            final java.lang.String content) {
        this._content = content;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

    /**
     * Sets the value of field 'numero'.
     * 
     * @param numero the value of field 'numero'.
     */
    public void setNumero(
            final byte numero) {
        this._numero = numero;
        this._has_numero = true;
    }

    /**
     * Sets the value of field 'prefecture'.
     * 
     * @param prefecture the value of field 'prefecture'.
     */
    public void setPrefecture(
            final java.lang.String prefecture) {
        this._prefecture = prefecture;
    }

}
