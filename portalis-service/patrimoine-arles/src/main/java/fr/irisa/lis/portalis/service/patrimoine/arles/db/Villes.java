/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class Villes.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
@XmlRootElement
public class Villes extends VillesType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Villes() {
        super();
    }

}
