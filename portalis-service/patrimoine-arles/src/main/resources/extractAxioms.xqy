declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $root as node() external;


declare function local:getRef($ref as xs:string) as  element()? {
	  $root/descendant::Mot[@nom=$ref]
};

declare function local:genAxiom($prop as xs:string, $mot as element()) as xs:string* {
    if ($mot/@ref) then 
		local:genAxiom($prop,  local:getRef($mot/@ref))
    else 
      for $child in $mot/Mot
      return 
        (concat("axiom ", $prop, " '", if ($child/@nom) then $child/@nom else $child/@ref, "', ", $prop, " '", $mot/@nom, "'
"), local:genAxiom($prop, $child) )
};

<result> {
for $mot in /Mots/Mot
for $child in $mot/Mot
return
    local:genAxiom($mot/@nom, $child)
} </result>