.. -*- mode: rst -*-

=============
Quatiers IRIS
=============

.. _`Insee`: http://www.insee.fr
.. _`iris`: http://www.insee.fr/fr/methodes/default.asp?page=zonages/iris.htm


© `Insee`_

Source
======

Les données et les définitions sur le découpage des communes de France en IRIS ont été extraites
d'un Fichier xls importé depuis le site `iris`_.

Définitions
===========

IRIS
----

L'IRIS constitue la maille de base de la diffusion infra-communale standard.
Il est parfois appelé "quartier IRIS".


Identifiant de l'IRIS
---------------------

L'``identifiant de l'IRIS`` est constitué

- du code département
- du code commune
- du code IRIS à l'intérieur de la commune.

Pour Paris, Lyon et Marseille, le code commune identifie l'arrondissement municipal.

Le code IRIS est '0000' pour les communes non découpées en IRIS.

types d'IRIS
------------

le type d'IRIS permet de distinguer les IRIS d'habitat 'H' des IRIS d'activité 'A' et des IRIS divers 'D' ;
le type d'IRIS est 'Z' pour les communes non découpées en IRIS.
Le type d'IRIS a été déterminé lors de la création des IRIS, en 1999.
Depuis, les caractéristiques de l'IRIS ont pu évoluer (population, nombre d'emplois, …) sans que le type d'IRIS n'ait toujours été modifié.

Détails sur les trois ``types d'IRIS`` :

- les *IRIS d'habitat* : leur population se situe en général entre 1 800 et 5 000 habitants. Ils sont homogènes quant au type d'habitat et leurs limites s'appuient sur les grandes coupures du tissu urbain (voies principales, voies ferrées, cours d'eau, …) ;
- les *IRIS d'activité* : ils regroupent environ 1 000 salariés et comptent au moins deux fois plus d'emplois salariés que de population résidente ;
- les *IRIS divers* : il s'agit de grandes zones spécifiques peu habitées et ayant une superficie importante (parcs de loisirs, zones portuaires, forêts, …).

Les communes ainsi découpées (1 877 communes irisées) regroupent 16 079 IRIS dont 15 429 en France métropolitaine et 650 dans les DOM hors Mayotte. Parmi ces IRIS, 14 843 sont des IRIS d'habitat, 828 des IRIS d'activité et 408 des IRIS divers."

Code modification de l'IRIS
---------------------------

Le ``code modification de l'IRIS`` permet de savoir si les IRIS ont été modifiés depuis 1999.
Les communes de 10 000 habitants ou plus et la plupart des communes de 5 000 à 9 999 habitants ont été découpées en IRIS pour la diffusion des résultats du recensement de 1999. Depuis, les IRIS n'ont été modifiés qu'à la marge pour tenir compte, chaque année, des modifications des limites communales.
Par ailleurs, l'Insee a effectué, au cours du deuxième trimestre 2008, la retouche de quelques IRIS afin de prendre en compte les évolutions importantes de la voierie et de la démographie. Cette retouche a été réalisée en respectant les règles établies en 1999 avec la Commission Nationale de l'Informatique et des Libertés (CNIL) et en préservant au maximum la continuité des séries de diffusion. 
Ces différentes modifications sont identifiées grâce au code modification de l’IRIS présent dans chaque base. Il se décline selon les modalités suivantes :

==== =======================================================================
Code Modalité
==== =======================================================================
00   Pas de modification.
1Z   Scission d'IRIS,
2A   à 2AC Déplacement de limites d'IRIS (du groupe 2A au groupe 2AC),
3A   Fusion de communes irisées,
3B   Rétablissement de communes issues de communes irisées,
4A   Échange de parcelles entre communes irisées,
4B   Échange de parcelles habitées entre communes irisées et non irisées,
4C   Échange de parcelles inhabitées entre communes irisées et non irisées.
==== =======================================================================

TRIRIS
------

Un ``TRIRIS`` est un regroupement d'IRIS (en général 3 IRIS). Son code, sur 6 positions, est composé 

- du code département
- d'un numéro d'ordre sur 3 positions (la dernière position est un indicateur de TRIRIS). 

Le code est à ZZZZZZ si la commune n'est pas découpée en IRIS ou si les IRIS ne sont pas regroupés en TRIRIS.
Le TRIRIS a été créé en 1999 pour la diffusion de variables sensibles du recensement pour lesquelles l'IRIS apparaît insuffisant pour garantir le secret statistique. "

Grand quartier
--------------

Un ``Grand quartier`` est en principe un regroupement d'IRIS.
Le code, sur 7 positions est constitué 

- du code département/commune sur 5 positions
- des deux premières positions du code IRIS (sur quatre positions).

Si la commune n'est pas découpée en IRIS, le code Grand quartier sera composé 

- du code département/commune sur 5 positions
- de '00' (exemple : L'Abergement-Clémenciat a pour code Grand quartier '0100100')
