package fr.irisa.lis.portalis.admin.db;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.UserData;


public interface RoleChecker {

	public void isAuthorized(UserData userCore, String serviceFullName, RightValue role) throws PortalisException;
	
}
