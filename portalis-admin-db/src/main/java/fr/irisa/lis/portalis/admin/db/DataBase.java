package fr.irisa.lis.portalis.admin.db;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.SiteInterface;

public interface DataBase extends SiteInterface, RoleChecker {

	public abstract LoginResult getUserInfo(String email, String password);

	public abstract void load() throws FileNotFoundException, SAXException, IOException,
			ParserConfigurationException, RequestException, PortalisException;

	public abstract void save(String fileName) throws IOException,
	TransformerException;

	public abstract void save() throws IOException, TransformerException;

}