package fr.irisa.lis.portalis.admin.db;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import fr.irisa.lis.portalis.shared.admin.PortalisError;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.LisApplication;
import fr.irisa.lis.portalis.shared.admin.data.LoginErr;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.PasswordUser;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.SparqlService;
import fr.irisa.lis.portalis.shared.admin.data.StandardUser;
import fr.irisa.lis.portalis.shared.admin.data.SuperUser;
import fr.irisa.lis.portalis.shared.admin.data.UserData;

@SuppressWarnings("serial")
public class DataBaseMongoImpl extends Site implements DataBase {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DataBaseMongoImpl.class.getName());
	
	private static String DB_SITE = "portalisSite";
	
	private static final String USER_COLL = "users";
	private static final String LISAPPL_COLL = "lisApplications";
	private static final String SPARQL_COLL = "sparqlEndPoints";
	private static final String ROLE_COLL = "roles";


	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static DataBaseMongoImpl instance = new DataBaseMongoImpl();
	}

	private DataBaseMongoImpl() {
		super();
		try {
			load();
		} catch (Exception e) {
			String mess = "impossible de charger la base de données du site : "+e.getMessage();
			LOGGER.error(mess);
			throw new PortalisError(mess);
		} 
	}
	
	private static DB getMongoDB() throws UnknownHostException {
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		DB db = mongoClient.getDB(DB_SITE);
		return db;
	}
	


	/** Point d'accès pour l'instance unique du singleton */
	public static DataBaseMongoImpl getInstance() {
		return SingletonHolder.instance;
	}


	@Override
	public void load() throws UnknownHostException {
		load(this);		
	}
		
	@SuppressWarnings("unchecked")
	public static void load(Site site) throws UnknownHostException {
		DB db = getMongoDB();
		DBCollection lisApplications = db.getCollection(LISAPPL_COLL);
		System.out.println(lisApplications);
		DBCursor cursor = lisApplications.find();
		while (cursor.hasNext()) {
			Map<String, LisApplication> map = site.getApplicationsMap();
			map.clear();
			map.putAll((Map<String, LisApplication>) cursor.next().toMap());
		}
		
		DBCollection sparqlEndPoints = db.getCollection(SPARQL_COLL);
		cursor = sparqlEndPoints.find();
		while (cursor.hasNext()) {
			Map<String, SparqlService> map = site.getSparqlEndPoints().getServiceMap();
			map.clear();
			map.putAll((Map<String, SparqlService>) cursor.next().toMap());
		}
		
		DBCollection users = db.getCollection(USER_COLL);
		cursor = users.find();
		while (cursor.hasNext()) {
			Map<String, PasswordUser> map = site.getUsersMap();
			map.clear();
			map.putAll((Map<String, PasswordUser>) cursor.next().toMap());
		}
		
		DBCollection roles = db.getCollection(ROLE_COLL);
		cursor = roles.find();
		while (cursor.hasNext()) {
			Map<String, Role> map = site.getRolesMap();
			map.clear();
			map.putAll((Map<String, Role>) cursor.next().toMap());
		}
		
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see fr.irisa.lis.portalis.admin.db.DataB#getUserInfo(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public LoginResult getUserInfo(String email, String password) {
		UserData userCore = users.get(email);
		LoginErr err = null;
		if (userCore == null) {
			err = LoginErr.unknown;
		} else if (userCore instanceof StandardUser) {
			if (((StandardUser) userCore).getPassword().equals(password)) {
				err = LoginErr.ok;
			} else {
				err = LoginErr.incorrectPassword;
			}
		} else if (userCore instanceof SuperUser) {
			if (((PasswordUser) userCore).getPassword().equals(password)) {
				err = LoginErr.ok;
			} else {
				err = LoginErr.incorrectPassword;
			}
		} else {
			err = LoginErr.unexpected;
		}
		return new LoginResult(userCore, err);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.irisa.lis.portalis.admin.db.DataB#save(java.lang.String)
	 */
	@Override
	public void save() throws IOException, TransformerException {
		save(this, DB_SITE);
	}

	@Override
	public void save(String dataBaseName) throws IOException, TransformerException {
		save(this, dataBaseName);
	}

	
	public static void save(Site site, String dataBaseName) throws IOException, TransformerException {
		DB db = getMongoDB();
		db.dropDatabase();
		
		DBCollection lisApplications = db.getCollection(LISAPPL_COLL);
		// remove all objects from the collection
		lisApplications.remove(new BasicDBObject());
		// save lisApplications to the lisApplications collection
		lisApplications.insert(new BasicDBObject(site.getApplicationsMap()));

		DBCollection sparqlEndPoints = db.getCollection(SPARQL_COLL);
		// remove all objects from the collection
		sparqlEndPoints.remove(new BasicDBObject());
		// save sparqlEndPoints to the sparqlEndPoints collection
		sparqlEndPoints.insert(new BasicDBObject(site.getSparqlEndPoints().getServiceMap()));

		DBCollection users = db.getCollection(USER_COLL);
		// remove all objects from the collection
		users.remove(new BasicDBObject());
		// save users to the users collection
		users.insert(new BasicDBObject(site.getUsersMap()));

		DBCollection roles = db.getCollection(ROLE_COLL);
		// remove all objects from the collection
		roles.remove(new BasicDBObject());
		// save roles to the roles collection
		roles.insert(new BasicDBObject(site.getRolesMap()));
	}
	
	@Override
	public void isAuthorized(UserData userData, String serviceFullName,
			RightValue requiredRole) throws PortalisException {
		RightValue max = null;
		if (userData instanceof SuperUser)
			max = RightValue.SUPER_ADMIN;
		else
			max = getRole(userData.getEmail(), serviceFullName).getRight();
		if (!RightValue.greaterEq(max, requiredRole)) {
			String mess = new StringBuffer(requiredRole.name())
					.append(" permission denied for user ")
					.append(userData.getEmail()).append(", it has ")
					.append(max.name()).append(" permission").toString();
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
	}


}
