package fr.irisa.lis.portalis.admin.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.CamelisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.LisApplication;
import fr.irisa.lis.portalis.shared.admin.data.LisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.LoginErr;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.PasswordUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.StandardUser;
import fr.irisa.lis.portalis.shared.admin.data.SuperUser;
import fr.irisa.lis.portalis.shared.admin.data.UIPreference;
import fr.irisa.lis.portalis.shared.admin.data.UserData;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestBd {

	static final Logger LOGGER = LoggerFactory.getLogger(TestBd.class.getName());
	
	private static final SuperUser BEKKERS = new SuperUser("bekkers@irisa.fr",
			"bekkers", "yves");
	private static final StandardUser SIGONNEAU = new StandardUser(
			"benjamin@dromaludaire.info", "sigonneau", "benjamin");
	private static  final StandardUser NONE_USER = createUser(RightValue.NONE);
	private static final LisApplication PLANETS = new LisApplication("planets");
	private static LisServiceCore PLANETS_PLANET_DISTANCE;
	
	private static final String DB_SITE = AdminHttp.WEBAPP_PATH + "/portalisSite.xml";
	private static final String MONGO_DB_SITE = "portalisSite";

	

	static final DataBaseSimpleImpl bd = DataBaseSimpleImpl.getInstance();

	static {
		PLANETS_PLANET_DISTANCE = new CamelisServiceCore(PLANETS.getId(), "planetsDistance",
				RightValue.READER);
		String[][] extraColumn0 = {{"DistanceToEarth_km",null}};
		UIPreference uiPreference0 = new UIPreference(5, 35, null, extraColumn0, true);
		PLANETS_PLANET_DISTANCE.setUiPreference(uiPreference0);
		PLANETS.add(PLANETS_PLANET_DISTANCE);
		setUniformRightValue(SIGONNEAU, RightValue.ADMIN);
		setUniformRightValue(NONE_USER, RightValue.NONE);
	}


	private static StandardUser createUser(RightValue rightValue) {
		String name = rightValue.name();
		String email = name + "@irisa.fr";
		StandardUser result = new StandardUser(email, name, name);
		return result;
	}

	private static void setUniformRightValue(StandardUser userCore,
			RightValue rightValue) {
		for (LisApplication LisApplication : bd.getApplications()) {
			for (LisServiceCore service : LisApplication.getServices()) {
				bd.addRole(userCore.getEmail(), service.getFullName(), rightValue);
			}
		}
		userCore.setPortalisADminRole(rightValue);
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Util.clientInitLogPropertiesFile();
		final String host = InetAddress.getLocalHost().getCanonicalHostName();

		PortalisService.getInstance().init(host, 8080, "portalis");
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @BeforeClass : TestBd       |"
				+ "\n            ---------------------------------------------------\n");
	}
	
	@Test
	public void testAdminPropertiesFilesArePresent() {
			LOGGER.info("--------------> @Test : testAdminPropertiesFilesArePresent\n");
		try {
			assertTrue(AdminProprietes.test());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSaveLoad() {
		LOGGER.info("--------------> @Test : testSaveLoad\n");
		try {
			final String fileName = "/srv/testSite.xml";
			LOGGER.info("bd = "+bd);
			bd.save(fileName);
			Site site = DataBaseSimpleImpl.load(fileName);
			assertTrue("Sites should be equals\nbd = "+bd
					+"\nsite = "+site, bd.equals(site));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testUsers() {
		LOGGER.info("--------------> @Test : testUsers\n");
		try {
			SuperUser in0 = (SuperUser) bd.getUser(CoreTestConstants.MAIL_YVES);
			assertNotNull(in0);
			Element elem0 = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in0);
			
			SuperUser out00 = ClientConstants.ADMIN_DATA_XML_READER.getSuperUser(elem0);
			PasswordUser out0 = BEKKERS;
			assertTrue("les deux objets SuperUser doivent être egaux\nin0 = "
					+ in0 + "\nout0 = " + out0, in0.equals(out0));
			assertTrue("les deux objets SuperUser doivent être egaux\nin0 = "
					+ in0 + "\nout00 = " + out00, in0.equals(out00));
			
			StandardUser in1 = (StandardUser) bd.getUser(CoreTestConstants.MAIL_BENJAMIN);
			assertNotNull(in1);
			Element elem1 = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in1);
			
			StandardUser out10 = ClientConstants.ADMIN_DATA_XML_READER.getStandardUser(elem1);
			StandardUser out1 = SIGONNEAU;
			assertTrue("les deux objets standardUser doivent être egaux\nin1 = "
					+ in1 + "\nout1 = " + out1, in1.equals(out1));
			assertTrue("les deux objets standardUser doivent être egaux\nin1 = "
					+ in1 + "\nout10 = " + out10, in1.equals(out10));
			
			UserData inNONE = bd.getUser(NONE_USER.getEmail());
			assertNotNull(inNONE);
			StandardUser outNONE = NONE_USER;
			assertTrue("les deux objets SuperUser doivent être egaux\nin = "
					+ inNONE + "\nout = " + outNONE, inNONE.equals(outNONE));
						
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Ignore
	@Test
	public void testLoadMongoImpl() {
		LOGGER.info("--------------> @Test : testLoadMongoImpl\n");
		try {
			Site site = new Site();
			DataBaseMongoImpl.load(site);
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Ignore
	@Test
	public void testSaveMongoImpl() {
		LOGGER.info("--------------> @Test : testSaveMongoImpl\n");
		try {
			DataBaseMongoImpl.save(bd, "testSite");
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Ignore
	@Test
	public void testMongodb() {
		LOGGER.info("--------------> @Test : testMongodb\n");
		try {
			// get database "test"
			MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
			DB db = mongoClient.getDB("test");
			// get collection "foo"
			DBCollection coll = db.getCollection("foo");
			assertEquals(2,coll.getCount());
			// get the first document in the collection.
			DBObject myDoc = coll.findOne();
			System.out.println(myDoc);
			
			BasicDBObject query = new BasicDBObject("a", new BasicDBObject("$ne", 1));

			DBCursor cursor = coll.find(query);
			try {
			   while(cursor.hasNext()) {
				   DBObject o = cursor.next();
				   int fieldA = (int)((Double) o.get("a")).doubleValue();
				   assertEquals(2, fieldA);
			   }
			} finally {
			   cursor.close();
			}
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	

	@Test
	public void testGetService() {
		LOGGER.info("--------------> @Test : testGetService\n");
		try {
			String fullName =PLANETS_PLANET_DISTANCE.getFullName();
			LisServiceCore in = bd.getService(fullName);
			assertNotNull(in);
			
			LisServiceCore out = new CamelisServiceCore(fullName,PLANETS_PLANET_DISTANCE.getDefaultRightValue());
			String[][] extraColumn0 = {{"DistanceToEarth_km",null}};
			UIPreference uiPreference0 = new UIPreference(5, 35, null, extraColumn0, true);
			out.setUiPreference(uiPreference0);
			assertTrue("les deux objets LisServiceCore doivent être egaux\nin = "
					+ in + "\nout = " + out, in.equals(out));
			

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

			
	@Test
	public void testGetLisApplication() {
		LOGGER.info("--------------> @Test : testGetLisApplication\n");
		try {
			String appliId = PLANETS.getId();
			LisApplication in = bd.getApplication(appliId);
			assertNotNull(in);
			
			LisApplication out = new LisApplication(appliId);
			out.setServices(in.getServices());
			
			assertTrue("les deux objets LisApplication doivent être egaux\nin = "
					+ in + "\nout = " + out, in.equals(out));
			

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

			
	@Test
	public void testGetUserInfo() {
		LOGGER.info("--------------> @Test : testGetUserInfo\n");
		try {
			LoginResult in = bd.getUserInfo(BEKKERS.getEmail(), BEKKERS.getPassword());
			LoginResult out = new LoginResult(BEKKERS, LoginErr.ok);
			assertTrue("les deux objets LoginResult doivent être egaux\nin = "
					+ in + "\nout = " + out, in.equals(out));
			
			LoginResult in1 = bd.getUserInfo(BEKKERS.getEmail(), "toto");
			assertNotNull(in1);
			
			LoginResult out1 = new LoginResult(BEKKERS, LoginErr.incorrectPassword);
			assertTrue("les deux objets LoginResult doivent être egaux\nin1 = "
					+ in1 + "\nout1 = " + out1, in1.equals(out1));
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testRoles() {
		LOGGER.info("--------------> @Test : testRoles\n");
		try {
			Role roleYvesPlanet = bd.getRole(CoreTestConstants.MAIL_YVES, CoreTestConstants.PLANETS_PLANETS.getFullName());
			assertTrue(roleYvesPlanet.getRight()+" should be equals to "+RightValue.SUPER_ADMIN, roleYvesPlanet.getRight().equals(RightValue.SUPER_ADMIN));

			Role roleBenjaminPlanet = bd.getRole(CoreTestConstants.MAIL_BENJAMIN, CoreTestConstants.PLANETS_PLANETS.getFullName());
			assertTrue(roleBenjaminPlanet.getRight()+" should be equals to "+RightValue.ADMIN, roleBenjaminPlanet.getRight().equals(RightValue.ADMIN));

			Role roleNonePlanet = bd.getRole(NONE_USER.getEmail(), CoreTestConstants.PLANETS_PLANETS.getFullName());
			assertTrue(roleNonePlanet.getRight()+" should be equals to "+RightValue.NONE, roleNonePlanet.getRight().equals(RightValue.NONE));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

}
