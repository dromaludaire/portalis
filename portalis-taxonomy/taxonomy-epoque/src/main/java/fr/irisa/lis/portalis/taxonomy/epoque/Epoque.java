package fr.irisa.lis.portalis.taxonomy.epoque;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.jkee.gtree.Tree;

import com.google.common.collect.Lists;

public class Epoque {

	private static final TaxonomyTreeBuilder.Funnel urlFunnel = new TaxonomyTreeBuilder.Funnel() {
		@Override
		public List<String> getPath(String value) {
			return Lists.newArrayList(value.split("/"));
		}
	};

	private static final TaxonomyTreeBuilder pathBuilder = new TaxonomyTreeBuilder(
			urlFunnel);
	
	private static final List<String> leaves = new ArrayList<String>();
	private static final List<String> nodes = new ArrayList<String>();
	private static final List<String> sameNodes = new ArrayList<String>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {

			final String txtFileName = "/local/bekkers/portalis/portalis-taxonomy/taxonomy-epoque/src/test/resources/epoque.txt";
			FileInputStream in = new FileInputStream(new File(txtFileName));
			// InputStream in =
			// Thread.currentThread().getContextClassLoader().getResourceAsStream(txtFileName);
			String ctx = genAxioms(in);
			Writer out = new OutputStreamWriter(
					new BufferedOutputStream(
							new FileOutputStream(
									"/srv/webapps/patrimoine/epoque.ctx")),
					"UTF-8");
			out.append(ctx);
			out.close();
			in = new FileInputStream(new File(txtFileName));
			String graph = genGraph(in);
			out = new OutputStreamWriter(
					new BufferedOutputStream(
							new FileOutputStream(
									"/srv/webapps/patrimoine/epoque.graph")),
					"UTF-8");
			out.append(graph);
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String genAxioms(InputStream in) throws IOException {
		List<String> list = new ArrayList<String>();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(in, "UTF-8"));
		String line = bufferedReader.readLine();
		while (line != null) {
			list.add(line);
			line = bufferedReader.readLine();
		}

		Tree<String> PRESHITOIRE_TREE = pathBuilder.build(list, "Datation");
		System.out.println(PRESHITOIRE_TREE.toStringTree());
		StringBuffer buff = new StringBuffer();
		genAxioms(buff, PRESHITOIRE_TREE);
		return buff.toString();

	}

	public static void genAxioms(StringBuffer buff,
			Tree<String> PRESHITOIRE_TREE) {
		String root = PRESHITOIRE_TREE.getValue();
		for (Tree<String> child : PRESHITOIRE_TREE.getChildren()) {
			if (!child.getValue().equals("''")) {
				if (!root.equals("Datation")) {
					buff.append("axiom datation ").append(child.getValue()).append(", datation ")
						.append(root).append("\n");
				}

				if (child.getChildren().size() > 0) {
					genAxioms(buff, child);
				}
			}
		}
	}

	public static String genGraph(InputStream in) throws IOException {
		List<String> list = new ArrayList<String>();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(in, "UTF-8"));
		String line = bufferedReader.readLine();
		while (line != null) {
			list.add(line);
			line = bufferedReader.readLine();
		}

		Tree<String> PRESHITOIRE_TREE = pathBuilder.build(list, "Datation");
		System.out.println(PRESHITOIRE_TREE.toStringTree());
		StringBuffer buff1 = new StringBuffer();
		genGraph(buff1, PRESHITOIRE_TREE);

		StringBuffer buff = new StringBuffer("digraph{\n");
		buff.append("node [shape=plaintext, fontsize=16];\n");
		
		StringBuffer buff3 = new StringBuffer("{rank=same; ");
		for (String leave : leaves) {
			buff.append(leave).append("\n");
			buff3.append(leave).append("; ");
		}
		buff3.append("};\n");
		buff.append(buff3.toString());
		
		for (String node : nodes) {
			buff.append(node).append("\n");
		}
		
		for (int i = 0; i<sameNodes.size(); i++) {
			String node = sameNodes.get(i);
			buff.append(node).append("\n");
		}
		
		buff.append(buff1);
		buff.append("fontsize=28;\n label=\"From Past to Future...\";\n");
		return buff.append("}").toString();

	}

	private static String replaceApos(String s) {
		if (s.startsWith("'"))
			return "\"" + s.substring(1, s.length() - 1) + "\"";
		else
			return s;
	}

	public static void genGraph(StringBuffer buff,
			Tree<String> PRESHITOIRE_TREE) {
		String root = replaceApos(PRESHITOIRE_TREE.getValue());
		StringBuffer buff3 = new StringBuffer("//{rank=same; ");
		int count = 0;
		for (Tree<String> child : PRESHITOIRE_TREE.getChildren()) {
			if (!child.getValue().equals("''")) {
				if ( !root.equals("Datation")) {
				buff.append(root).append(" -> ")
						.append(replaceApos(child.getValue())).append(";\n");
				}
				if (child.getChildren().size() > 0) {
					count++;
					String val = replaceApos(child.getValue());
					if (!nodes.contains(val)) {
						nodes.add(val);
					}
					genGraph(buff, child);
					buff3.append(val).append("; ");
				} else {
					String val = replaceApos(child.getValue());
					if (!leaves.contains(val)) {
						leaves.add(val);
					}
				}
			}
		}
		if (count > 1) {
			buff3.append("};\n");
			sameNodes.add(buff3.toString());
		}
	}

}
