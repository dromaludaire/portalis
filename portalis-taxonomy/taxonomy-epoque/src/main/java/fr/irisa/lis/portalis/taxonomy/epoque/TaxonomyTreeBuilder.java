package fr.irisa.lis.portalis.taxonomy.epoque;

import java.util.ArrayList;
import java.util.List;

import org.jkee.gtree.Tree;
import org.jkee.gtree.builder.PathTreeBuilder;

import com.google.common.collect.Lists;

public class TaxonomyTreeBuilder {

	public static interface Funnel {
		public List<String> getPath(String value);
	}

	private final Funnel funnel;

	public TaxonomyTreeBuilder(Funnel funnel) {
		this.funnel = funnel;
	}

	public Tree<String> build(Iterable<String> values, String rootValue) {
		return build(values, rootValue, false);
	}

	public Tree<String> build(Iterable<String> values, String rootValue,
			boolean fold) {
		/*
		 * 1. split by words : append nodes
		 */

		Tree<String> root = new Tree<String>(rootValue,
				new ArrayList<Tree<String>>());
		for (String value : values) {
			List<String> path = funnel.getPath(value);
			Tree<String> current = root;

			for (String name : path) {
				if (name.length() != 0) {
					List<Tree<String>> children = current.getChildren();
					boolean found = false;
					for (Tree<String> child : children) {
						if (child.getValue().equals(name)) {
							current = child;
							found = true;
							break;
						}
					}
					if (!found) {
						Tree<String> newTree = new Tree<String>(name,
								new ArrayList<Tree<String>>());
						current.addChild(newTree);
						current = newTree;
					}
				}
			}
		}

		if (fold)
			return fold(root.getValue(), root);
		else
			return root;

	}

	public Tree<String> fold(String rootValue, Tree<String> root) {
		Tree<String> result = new Tree<String>(rootValue,
				new ArrayList<Tree<String>>());
		List<Tree<String>> children = root.getChildren();
		if (children == null || children.size() == 0) {
			return result;
		} else if (children.size() == 1) {
			Tree<String> singletonChild = children.get(0);
			return fold(String.format("%s %s", rootValue,
					singletonChild.getValue()), singletonChild);
		} else {
			for (Tree<String> newChild : children) {
				result.addChild(fold(newChild.getValue(), newChild));
			}
			return result;
		}

	}

	public static void main(String[] args) {
		PathTreeBuilder.Funnel<String, String> urlFunnel = new PathTreeBuilder.Funnel<String, String>() {
			@Override
			public List<String> getPath(String value) {
				return Lists.newArrayList(value.split("/"));
			}
		};
		List<String> urls = Lists.newArrayList("/home/url1/suburl",
				"/home/url1", "/home/url2", "/basket", "/basket/url1",
				"/basket/url2", "/home/url1/suburl/more", "/about",
				"/home/url3/more");
		PathTreeBuilder<String, String> builder = new PathTreeBuilder<String, String>(
				urlFunnel);
		List<Tree<String>> build = builder.build(urls);
		System.out.println(build.size());
	}

}
