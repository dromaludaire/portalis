package fr.irisa.lis.portalis.taxonomy;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class GetVille {
	
	private static final String URL_INSEE = "http://rdf.insee.fr/sparql";
	private static final String URL_IGN = "http://data.ign.fr";
	
	private static final String json_format = "application/json";
	private static final String rdf_format = "application/sparql-results+xml";
	private static final String turtle_format = "text/turtle";
	private static final String ntriples_format = "application/n-triples";
	private static final String trig_format = "application/x-trig";
	private static final String trix_format = "application/trix";
	private static final String csv_format = "text/csv";

	private static final String QUERY_INSEE = 
		"prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> "			
		+"prefix igeo:<http://rdf.insee.fr/def/geo#> "			
		+"select * where {"			
			+"?commune igeo:codeINSEE ?c . "			
			+"?commune igeo:nom \"Vern-sur-Seiche\" . "			
			+"?commune igeo:nom ?nomCommune . "			
			+"?commune igeo:subdivisionDe ?canton . "			
			+"?canton rdf:type igeo:Canton . "			
			+"?canton igeo:nom ?nomCanton . "			
			+"?canton igeo:subdivisionDe ?arrondissement . "			
			+"?arrondissement igeo:subdivisionDe ?departement . "			
			+"?arrondissement igeo:nom ?nomArrondissement . "			
			+"?departement igeo:subdivisionDe ?region . "			
			+"?departement igeo:nom ?nomDepartement . "			
			+"?region igeo:nom ?nomRegion . "			
		+"}";
	
	public static void main (String[] args) throws UnsupportedEncodingException {
        final String FORMAT = rdf_format;
        String urlParameters =
                "?query=" + URLEncoder.encode(QUERY_INSEE, "UTF-8") +
                "&format=" + URLEncoder.encode(FORMAT, "UTF-8");
		String result = get(URL_INSEE, urlParameters, FORMAT);
		System.out.println(result);
	}
	
	private static String get(String endPoint, String urlParameters, String format){
        HttpURLConnection connection = null;  
	    try {
	        
	        URL url = new URL(endPoint+urlParameters);

	        //Create connection
	        connection = (HttpURLConnection)url.openConnection();
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Accept", format);
	  			
	        connection.setUseCaches (false);
	        connection.setDoInput(true);
	        connection.setDoOutput(true);
//	        System.out.println(connection.getURL().toString());

	        //Send request
	        DataOutputStream wr = new DataOutputStream (
	                    connection.getOutputStream ());
	        wr.writeBytes (urlParameters);
	        wr.flush ();
	        wr.close ();

	        //Get Response	
	        InputStream is = connection.getInputStream();
	        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	        String line;
	        StringBuffer response = new StringBuffer(); 
	        while((line = rd.readLine()) != null) {
	          response.append(line);
	          response.append('\r');
	        }
	        rd.close();
	        return response.toString();

	      } catch (Exception e) {

	        e.printStackTrace();
	        return null;

	      } finally {

	        if(connection != null) {
	          connection.disconnect(); 
	        }
	      }
	    }

}
