/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class CirconscriptionType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CirconscriptionType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numero.
     */
	@XmlAttribute
    private byte _numero;

    /**
     * keeps track of state for field: _numero
     */
    private boolean _has_numero;

    /**
     * Field _communeList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune> _communeList;


      //----------------/
     //- Constructors -/
    //----------------/

    public CirconscriptionType() {
        super();
        this._communeList = new java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vCommune
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCommune(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune vCommune)
    throws java.lang.IndexOutOfBoundsException {
        this._communeList.addElement(vCommune);
    }

    /**
     * 
     * 
     * @param index
     * @param vCommune
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCommune(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune vCommune)
    throws java.lang.IndexOutOfBoundsException {
        this._communeList.add(index, vCommune);
    }

    /**
     */
    public void deleteNumero(
    ) {
        this._has_numero= false;
    }

    /**
     * Method enumerateCommune.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Commune
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune> enumerateCommune(
    ) {
        return this._communeList.elements();
    }

    /**
     * Method getCommune.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Commune at
     * the given index
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune getCommune(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._communeList.size()) {
            throw new IndexOutOfBoundsException("getCommune: Index value '" + index + "' not in range [0.." + (this._communeList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune) _communeList.get(index);
    }

    /**
     * Method getCommune.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune[] getCommune(
    ) {
        fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune[] array = new fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune[0];
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune[]) this._communeList.toArray(array);
    }

    /**
     * Method getCommuneCount.
     * 
     * @return the size of this collection
     */
    public int getCommuneCount(
    ) {
        return this._communeList.size();
    }

    /**
     * Returns the value of field 'numero'.
     * 
     * @return the value of field 'Numero'.
     */
    public byte getNumero(
    ) {
        return this._numero;
    }

    /**
     * Method hasNumero.
     * 
     * @return true if at least one Numero has been added
     */
    public boolean hasNumero(
    ) {
        return this._has_numero;
    }

    /**
     */
    public void removeAllCommune(
    ) {
        this._communeList.clear();
    }

    /**
     * Method removeCommune.
     * 
     * @param vCommune
     * @return true if the object was removed from the collection.
     */
    public boolean removeCommune(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune vCommune) {
        boolean removed = _communeList.remove(vCommune);
        return removed;
    }

    /**
     * Method removeCommuneAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune removeCommuneAt(
            final int index) {
        java.lang.Object obj = this._communeList.remove(index);
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCommune
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCommune(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune vCommune)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._communeList.size()) {
            throw new IndexOutOfBoundsException("setCommune: Index value '" + index + "' not in range [0.." + (this._communeList.size() - 1) + "]");
        }

        this._communeList.set(index, vCommune);
    }

    /**
     * 
     * 
     * @param vCommuneArray
     */
    public void setCommune(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune[] vCommuneArray) {
        //-- copy array
        _communeList.clear();

        for (int i = 0; i < vCommuneArray.length; i++) {
                this._communeList.add(vCommuneArray[i]);
        }
    }

    /**
     * Sets the value of field 'numero'.
     * 
     * @param numero the value of field 'numero'.
     */
    public void setNumero(
            final byte numero) {
        this._numero = numero;
        this._has_numero = true;
    }

}
