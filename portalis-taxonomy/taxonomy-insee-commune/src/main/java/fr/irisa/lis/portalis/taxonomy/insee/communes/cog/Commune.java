package fr.irisa.lis.portalis.taxonomy.insee.communes.cog;


/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class Commune.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Commune extends CommuneType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Commune() {
        super();
    }

}
