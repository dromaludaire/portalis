package fr.irisa.lis.portalis.taxonomy.insee.communes.cog;
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class CommuneType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CommuneType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _CODGEO.
     */
    private java.lang.String _CODGEO;

    /**
     * Field _LIBGEO.
     */
    private java.lang.String _LIBGEO;

    /**
     * Field _REG.
     */
    private java.lang.String _REG;

    /**
     * Field _DEP.
     */
    private java.lang.String _DEP;

    /**
     * Field _ARR.
     */
    private java.lang.String _ARR;

    /**
     * Field _CV.
     */
    private java.lang.String _CV;

    /**
     * Field _ZE2010.
     */
    private java.lang.String _ZE2010;

    /**
     * Field _UU2010.
     */
    private java.lang.String _UU2010;

    /**
     * Field _AU2010.
     */
    private java.lang.String _AU2010;

    /**
     * Field _TUU2010.
     */
    private java.lang.String _TUU2010;

    /**
     * Field _TDUU2010.
     */
    private java.lang.String _TDUU2010;

    /**
     * Field _TAU2010.
     */
    private java.lang.String _TAU2010;

    /**
     * Field _CATAEU2010.
     */
    private java.lang.String _CATAEU2010;

    /**
     * Field _EPCI.
     */
    private java.lang.String _EPCI;

    /**
     * Field _NATURE_EPCI.
     */
    private java.lang.String _NATURE_EPCI;

    /**
     * Field _POP2010.
     */
    private java.lang.String _POP2010;

    /**
     * Field _POP1999.
     */
    private java.lang.String _POP1999;

    /**
     * Field _CAT_ZDIFF.
     */
    private java.lang.String _CAT_ZDIFF;

    /**
     * Field _MODE_COLLECTE.
     */
    private java.lang.String _MODE_COLLECTE;

    /**
     * Field _GR_ROTATION.
     */
    private java.lang.String _GR_ROTATION;


      //----------------/
     //- Constructors -/
    //----------------/

    public CommuneType() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'ARR'.
     * 
     * @return the value of field 'ARR'.
     */
    public java.lang.String getARR(
    ) {
        return this._ARR;
    }

    /**
     * Returns the value of field 'AU2010'.
     * 
     * @return the value of field 'AU2010'.
     */
    public java.lang.String getAU2010(
    ) {
        return this._AU2010;
    }

    /**
     * Returns the value of field 'CATAEU2010'.
     * 
     * @return the value of field 'CATAEU2010'.
     */
    public java.lang.String getCATAEU2010(
    ) {
        return this._CATAEU2010;
    }

    /**
     * Returns the value of field 'CAT_ZDIFF'.
     * 
     * @return the value of field 'CAT_ZDIFF'.
     */
    public java.lang.String getCAT_ZDIFF(
    ) {
        return this._CAT_ZDIFF;
    }

    /**
     * Returns the value of field 'CODGEO'.
     * 
     * @return the value of field 'CODGEO'.
     */
    public java.lang.String getCODGEO(
    ) {
        return this._CODGEO;
    }

    /**
     * Returns the value of field 'CV'.
     * 
     * @return the value of field 'CV'.
     */
    public java.lang.String getCV(
    ) {
        return this._CV;
    }

    /**
     * Returns the value of field 'DEP'.
     * 
     * @return the value of field 'DEP'.
     */
    public java.lang.String getDEP(
    ) {
        return this._DEP;
    }

    /**
     * Returns the value of field 'EPCI'.
     * 
     * @return the value of field 'EPCI'.
     */
    public java.lang.String getEPCI(
    ) {
        return this._EPCI;
    }

    /**
     * Returns the value of field 'GR_ROTATION'.
     * 
     * @return the value of field 'GR_ROTATION'.
     */
    public java.lang.String getGR_ROTATION(
    ) {
        return this._GR_ROTATION;
    }

    /**
     * Returns the value of field 'LIBGEO'.
     * 
     * @return the value of field 'LIBGEO'.
     */
    public java.lang.String getLIBGEO(
    ) {
        return this._LIBGEO;
    }

    /**
     * Returns the value of field 'MODE_COLLECTE'.
     * 
     * @return the value of field 'MODE_COLLECTE'.
     */
    public java.lang.String getMODE_COLLECTE(
    ) {
        return this._MODE_COLLECTE;
    }

    /**
     * Returns the value of field 'NATURE_EPCI'.
     * 
     * @return the value of field 'NATURE_EPCI'.
     */
    public java.lang.String getNATURE_EPCI(
    ) {
        return this._NATURE_EPCI;
    }

    /**
     * Returns the value of field 'POP1999'.
     * 
     * @return the value of field 'POP1999'.
     */
    public java.lang.String getPOP1999(
    ) {
        return this._POP1999;
    }

    /**
     * Returns the value of field 'POP2010'.
     * 
     * @return the value of field 'POP2010'.
     */
    public java.lang.String getPOP2010(
    ) {
        return this._POP2010;
    }

    /**
     * Returns the value of field 'REG'.
     * 
     * @return the value of field 'REG'.
     */
    public java.lang.String getREG(
    ) {
        return this._REG;
    }

    /**
     * Returns the value of field 'TAU2010'.
     * 
     * @return the value of field 'TAU2010'.
     */
    public java.lang.String getTAU2010(
    ) {
        return this._TAU2010;
    }

    /**
     * Returns the value of field 'TDUU2010'.
     * 
     * @return the value of field 'TDUU2010'.
     */
    public java.lang.String getTDUU2010(
    ) {
        return this._TDUU2010;
    }

    /**
     * Returns the value of field 'TUU2010'.
     * 
     * @return the value of field 'TUU2010'.
     */
    public java.lang.String getTUU2010(
    ) {
        return this._TUU2010;
    }

    /**
     * Returns the value of field 'UU2010'.
     * 
     * @return the value of field 'UU2010'.
     */
    public java.lang.String getUU2010(
    ) {
        return this._UU2010;
    }

    /**
     * Returns the value of field 'ZE2010'.
     * 
     * @return the value of field 'ZE2010'.
     */
    public java.lang.String getZE2010(
    ) {
        return this._ZE2010;
    }

    /**
     * Sets the value of field 'ARR'.
     * 
     * @param ARR the value of field 'ARR'.
     */
    public void setARR(
            final java.lang.String ARR) {
        this._ARR = ARR;
    }

    /**
     * Sets the value of field 'AU2010'.
     * 
     * @param AU2010 the value of field 'AU2010'.
     */
    public void setAU2010(
            final java.lang.String AU2010) {
        this._AU2010 = AU2010;
    }

    /**
     * Sets the value of field 'CATAEU2010'.
     * 
     * @param CATAEU2010 the value of field 'CATAEU2010'.
     */
    public void setCATAEU2010(
            final java.lang.String CATAEU2010) {
        this._CATAEU2010 = CATAEU2010;
    }

    /**
     * Sets the value of field 'CAT_ZDIFF'.
     * 
     * @param CAT_ZDIFF the value of field 'CAT_ZDIFF'.
     */
    public void setCAT_ZDIFF(
            final java.lang.String CAT_ZDIFF) {
        this._CAT_ZDIFF = CAT_ZDIFF;
    }

    /**
     * Sets the value of field 'CODGEO'.
     * 
     * @param CODGEO the value of field 'CODGEO'.
     */
    public void setCODGEO(
            final java.lang.String CODGEO) {
        this._CODGEO = CODGEO;
    }

    /**
     * Sets the value of field 'CV'.
     * 
     * @param CV the value of field 'CV'.
     */
    public void setCV(
            final java.lang.String CV) {
        this._CV = CV;
    }

    /**
     * Sets the value of field 'DEP'.
     * 
     * @param DEP the value of field 'DEP'.
     */
    public void setDEP(
            final java.lang.String DEP) {
        this._DEP = DEP;
    }

    /**
     * Sets the value of field 'EPCI'.
     * 
     * @param EPCI the value of field 'EPCI'.
     */
    public void setEPCI(
            final java.lang.String EPCI) {
        this._EPCI = EPCI;
    }

    /**
     * Sets the value of field 'GR_ROTATION'.
     * 
     * @param GR_ROTATION the value of field 'GR_ROTATION'.
     */
    public void setGR_ROTATION(
            final java.lang.String GR_ROTATION) {
        this._GR_ROTATION = GR_ROTATION;
    }

    /**
     * Sets the value of field 'LIBGEO'.
     * 
     * @param LIBGEO the value of field 'LIBGEO'.
     */
    public void setLIBGEO(
            final java.lang.String LIBGEO) {
        this._LIBGEO = LIBGEO;
    }

    /**
     * Sets the value of field 'MODE_COLLECTE'.
     * 
     * @param MODE_COLLECTE the value of field 'MODE_COLLECTE'.
     */
    public void setMODE_COLLECTE(
            final java.lang.String MODE_COLLECTE) {
        this._MODE_COLLECTE = MODE_COLLECTE;
    }

    /**
     * Sets the value of field 'NATURE_EPCI'.
     * 
     * @param NATURE_EPCI the value of field 'NATURE_EPCI'.
     */
    public void setNATURE_EPCI(
            final java.lang.String NATURE_EPCI) {
        this._NATURE_EPCI = NATURE_EPCI;
    }

    /**
     * Sets the value of field 'POP1999'.
     * 
     * @param POP1999 the value of field 'POP1999'.
     */
    public void setPOP1999(
            final java.lang.String POP1999) {
        this._POP1999 = POP1999;
    }

    /**
     * Sets the value of field 'POP2010'.
     * 
     * @param POP2010 the value of field 'POP2010'.
     */
    public void setPOP2010(
            final java.lang.String POP2010) {
        this._POP2010 = POP2010;
    }

    /**
     * Sets the value of field 'REG'.
     * 
     * @param REG the value of field 'REG'.
     */
    public void setREG(
            final java.lang.String REG) {
        this._REG = REG;
    }

    /**
     * Sets the value of field 'TAU2010'.
     * 
     * @param TAU2010 the value of field 'TAU2010'.
     */
    public void setTAU2010(
            final java.lang.String TAU2010) {
        this._TAU2010 = TAU2010;
    }

    /**
     * Sets the value of field 'TDUU2010'.
     * 
     * @param TDUU2010 the value of field 'TDUU2010'.
     */
    public void setTDUU2010(
            final java.lang.String TDUU2010) {
        this._TDUU2010 = TDUU2010;
    }

    /**
     * Sets the value of field 'TUU2010'.
     * 
     * @param TUU2010 the value of field 'TUU2010'.
     */
    public void setTUU2010(
            final java.lang.String TUU2010) {
        this._TUU2010 = TUU2010;
    }

    /**
     * Sets the value of field 'UU2010'.
     * 
     * @param UU2010 the value of field 'UU2010'.
     */
    public void setUU2010(
            final java.lang.String UU2010) {
        this._UU2010 = UU2010;
    }

    /**
     * Sets the value of field 'ZE2010'.
     * 
     * @param ZE2010 the value of field 'ZE2010'.
     */
    public void setZE2010(
            final java.lang.String ZE2010) {
        this._ZE2010 = ZE2010;
    }

}
