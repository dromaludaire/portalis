package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import net.sf.saxon.s9api.XdmNode;

import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.XmlBeanDAO;
import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.commons.csv.File2DocumentCsvParser;
import fr.irisa.lis.portalis.taxonomy.insee.communes.CommunesInfo;

public class HierachieEurope extends File2DocumentCsvParser {

	private static final String xqyFileName = "communesGpsToCirconscription.xqy";

	private static final String rowName = "commune";
	private static final String csvSplitBy = ";";
	private static final String encoding = CommonsUtil.DEFAULT_ENCODING;
	private static final int longHeadersPos = -1;
	private static final int headersPos = 0;
	
	private Document newDoc;
	
	private XmlBeanDAO<CirconscriptionsEuropeennes> bean = new XmlBeanDAO<CirconscriptionsEuropeennes>(new CirconscriptionsEuropeennes());

	public HierachieEurope(FileInputStream inputStream) throws Exception {
		super(inputStream, rowName, csvSplitBy, headersPos, longHeadersPos,
				encoding);
		// parse
		Document doc = parse();
		DOMUtil.prettyPrintXml(doc, "euroCirco0.xml");
	// transform
		newDoc = transformDocument(doc);
		DOMUtil.prettyPrintXml(newDoc, "euroCirco.xml");
		// compile (unmarshall)
		bean.loadBeanFromDom(newDoc);
		// serialisenewDoc (marshall)
		bean.storeBeanToFile(new File("euroCircoBean.xml"));
	}

	private Document transformDocument(Document doc) throws Exception {

		XqueryUtil xqueryUtil = new XqueryUtil();
		XdmNode xmlNode = xqueryUtil.xqueryTransform(doc, xqyFileName);
		Document result = XqueryUtil.XdmNodeToDomDocument(xmlNode);


		return result;
	}

	public static void main(String[] args) throws Exception {
		FileInputStream inputStream = new FileInputStream(CommunesInfo.circonscriptionGpsCsvFileName);
		new HierachieEurope(inputStream);
	}

	public XmlBeanDAO<CirconscriptionsEuropeennes> getBean() {
		return bean;
	}

	public Document getDocument() {
		return newDoc;
	}


}
