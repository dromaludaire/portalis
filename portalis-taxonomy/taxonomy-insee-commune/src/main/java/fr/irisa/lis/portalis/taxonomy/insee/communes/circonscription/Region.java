/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

/**
 * Class Region.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Region extends RegionType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Region() {
        super();
    }

}
