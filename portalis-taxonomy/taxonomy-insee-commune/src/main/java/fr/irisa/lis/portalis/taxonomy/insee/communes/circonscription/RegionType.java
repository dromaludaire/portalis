/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class RegionType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class RegionType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _chefLieu.
     */
	@XmlAttribute
    private java.lang.String _chefLieu;

    /**
     * Field _code.
     */
	@XmlAttribute
    private byte _code;

    /**
     * keeps track of state for field: _code
     */
    private boolean _has_code;

    /**
     * Field _nom.
     */
	@XmlAttribute
    private java.lang.String _nom;

    /**
     * Field _departementList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement> _departementList;


      //----------------/
     //- Constructors -/
    //----------------/

    public RegionType() {
        super();
        this._departementList = new java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vDepartement
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addDepartement(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement vDepartement)
    throws java.lang.IndexOutOfBoundsException {
        this._departementList.addElement(vDepartement);
    }

    /**
     * 
     * 
     * @param index
     * @param vDepartement
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addDepartement(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement vDepartement)
    throws java.lang.IndexOutOfBoundsException {
        this._departementList.add(index, vDepartement);
    }

    /**
     */
    public void deleteCode(
    ) {
        this._has_code= false;
    }

    /**
     * Method enumerateDepartement.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Departement
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement> enumerateDepartement(
    ) {
        return this._departementList.elements();
    }

    /**
     * Returns the value of field 'chefLieu'.
     * 
     * @return the value of field 'ChefLieu'.
     */
    public java.lang.String getChefLieu(
    ) {
        return this._chefLieu;
    }

    /**
     * Returns the value of field 'code'.
     * 
     * @return the value of field 'Code'.
     */
    public byte getCode(
    ) {
        return this._code;
    }

    /**
     * Method getDepartement.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Departement
     * at the given index
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement getDepartement(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._departementList.size()) {
            throw new IndexOutOfBoundsException("getDepartement: Index value '" + index + "' not in range [0.." + (this._departementList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement) _departementList.get(index);
    }

    /**
     * Method getDepartement.Returns the contents of the collection
     * in an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement[] getDepartement(
    ) {
        fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement[] array = new fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement[0];
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement[]) this._departementList.toArray(array);
    }

    /**
     * Method getDepartementCount.
     * 
     * @return the size of this collection
     */
    public int getDepartementCount(
    ) {
        return this._departementList.size();
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Method hasCode.
     * 
     * @return true if at least one Code has been added
     */
    public boolean hasCode(
    ) {
        return this._has_code;
    }

    /**
     */
    public void removeAllDepartement(
    ) {
        this._departementList.clear();
    }

    /**
     * Method removeDepartement.
     * 
     * @param vDepartement
     * @return true if the object was removed from the collection.
     */
    public boolean removeDepartement(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement vDepartement) {
        boolean removed = _departementList.remove(vDepartement);
        return removed;
    }

    /**
     * Method removeDepartementAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement removeDepartementAt(
            final int index) {
        java.lang.Object obj = this._departementList.remove(index);
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement) obj;
    }

    /**
     * Sets the value of field 'chefLieu'.
     * 
     * @param chefLieu the value of field 'chefLieu'.
     */
    public void setChefLieu(
            final java.lang.String chefLieu) {
        this._chefLieu = chefLieu;
    }

    /**
     * Sets the value of field 'code'.
     * 
     * @param code the value of field 'code'.
     */
    public void setCode(
            final byte code) {
        this._code = code;
        this._has_code = true;
    }

    /**
     * 
     * 
     * @param index
     * @param vDepartement
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setDepartement(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement vDepartement)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._departementList.size()) {
            throw new IndexOutOfBoundsException("setDepartement: Index value '" + index + "' not in range [0.." + (this._departementList.size() - 1) + "]");
        }

        this._departementList.set(index, vDepartement);
    }

    /**
     * 
     * 
     * @param vDepartementArray
     */
    public void setDepartement(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement[] vDepartementArray) {
        //-- copy array
        _departementList.clear();

        for (int i = 0; i < vDepartementArray.length; i++) {
                this._departementList.add(vDepartementArray[i]);
        }
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

}
