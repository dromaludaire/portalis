/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

/**
 * Class CirconscriptionsEuropeennesType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CirconscriptionsEuropeennesType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _circonscriptionEuropeenneList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne> _circonscriptionEuropeenneList;


      //----------------/
     //- Constructors -/
    //----------------/

    public CirconscriptionsEuropeennesType() {
        super();
        this._circonscriptionEuropeenneList = new java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vCirconscriptionEuropeenne
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCirconscriptionEuropeenne(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne vCirconscriptionEuropeenne)
    throws java.lang.IndexOutOfBoundsException {
        this._circonscriptionEuropeenneList.addElement(vCirconscriptionEuropeenne);
    }

    /**
     * 
     * 
     * @param index
     * @param vCirconscriptionEuropeenne
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCirconscriptionEuropeenne(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne vCirconscriptionEuropeenne)
    throws java.lang.IndexOutOfBoundsException {
        this._circonscriptionEuropeenneList.add(index, vCirconscriptionEuropeenne);
    }

    /**
     * Method enumerateCirconscriptionEuropeenne.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.CirconscriptionEuropeenne
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne> enumerateCirconscriptionEuropeenne(
    ) {
        return this._circonscriptionEuropeenneList.elements();
    }

    /**
     * Method getCirconscriptionEuropeenne.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.CirconscriptionEuropeenne
     * at the given index
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne getCirconscriptionEuropeenne(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._circonscriptionEuropeenneList.size()) {
            throw new IndexOutOfBoundsException("getCirconscriptionEuropeenne: Index value '" + index + "' not in range [0.." + (this._circonscriptionEuropeenneList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne) _circonscriptionEuropeenneList.get(index);
    }

    /**
     * Method getCirconscriptionEuropeenne.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne[] getCirconscriptionEuropeenne(
    ) {
        fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne[] array = new fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne[0];
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne[]) this._circonscriptionEuropeenneList.toArray(array);
    }

    /**
     * Method getCirconscriptionEuropeenneCount.
     * 
     * @return the size of this collection
     */
    public int getCirconscriptionEuropeenneCount(
    ) {
        return this._circonscriptionEuropeenneList.size();
    }

    /**
     */
    public void removeAllCirconscriptionEuropeenne(
    ) {
        this._circonscriptionEuropeenneList.clear();
    }

    /**
     * Method removeCirconscriptionEuropeenne.
     * 
     * @param vCirconscriptionEuropeenne
     * @return true if the object was removed from the collection.
     */
    public boolean removeCirconscriptionEuropeenne(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne vCirconscriptionEuropeenne) {
        boolean removed = _circonscriptionEuropeenneList.remove(vCirconscriptionEuropeenne);
        return removed;
    }

    /**
     * Method removeCirconscriptionEuropeenneAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne removeCirconscriptionEuropeenneAt(
            final int index) {
        java.lang.Object obj = this._circonscriptionEuropeenneList.remove(index);
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCirconscriptionEuropeenne
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCirconscriptionEuropeenne(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne vCirconscriptionEuropeenne)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._circonscriptionEuropeenneList.size()) {
            throw new IndexOutOfBoundsException("setCirconscriptionEuropeenne: Index value '" + index + "' not in range [0.." + (this._circonscriptionEuropeenneList.size() - 1) + "]");
        }

        this._circonscriptionEuropeenneList.set(index, vCirconscriptionEuropeenne);
    }

    /**
     * 
     * 
     * @param vCirconscriptionEuropeenneArray
     */
    public void setCirconscriptionEuropeenne(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne[] vCirconscriptionEuropeenneArray) {
        //-- copy array
        _circonscriptionEuropeenneList.clear();

        for (int i = 0; i < vCirconscriptionEuropeenneArray.length; i++) {
                this._circonscriptionEuropeenneList.add(vCirconscriptionEuropeenneArray[i]);
        }
    }

}
