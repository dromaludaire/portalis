package fr.irisa.lis.portalis.server.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;


public final class ServerConstants {

	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(ServerConstants.class.getName());
	
	public final static String CAMELIS_APPLICATION_NAME = AdminProprietes.portalis
			.getProperty("camelis.application.name");
	
		
}
