package fr.irisa.lis.portalis.server.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.system.linux.LinuxInteractor;
import fr.irisa.lis.portalis.system.linux.PortalisSystemException;

public class LinuxCmd {private static final Logger LOGGER = LoggerFactory.getLogger(LinuxCmd.class.getName());

	public static String CAMELIS_APPLICATION_NAME = AdminProprietes.portalis
			.getProperty("camelis.application.name");


	

	/**
	 * 
	 * @return l'information de version de Maven
	 */
	public static String getPortalisVersion() {
		return LinuxCmd.class.getPackage().getImplementationVersion();
	}

	static final String PROCESS_PATTERN = "^[\\w\\-]*\\s([\\d]+)\\s.*$";
	static final String PORT_PATTERN = "^.*\\sTCP\\s\\*:([\\d]+)\\s.*$";
	
	

	public static int getMyJavaPid() throws PortalisException {
		String pid = ManagementFactory.getRuntimeMXBean().getName()
				.replaceFirst("@.*$", "");
		int n = 0;
		try {
			n = Integer.parseInt(pid);
		} catch (Exception e) {
			String mess = "Erreur interne : impossible de calculer le N° de processus Java, la chaîne d'entrée est '"
					+ pid + "'";
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		LOGGER.info("==== Linux ==== >> getMyJavaPid() = " + n);
		return n;
	}

	public static int getCamelisProcessId(int port) throws PortalisException {
		LOGGER.info("==== Linux ==== >> getCamelisPid(" + port + ")");

		Set<PortActif> actifs = getActifsCamelisPortsOnPortalis();
		for (PortActif actif : actifs) {
			if (actif.getPort()==port) {
				if (Util.available(port)) { // should never append
					String mess = "Erreur système : pour command 'ps' le port "+ port +" est actif mais pas pour java socket";
					LOGGER.error(mess);
					throw new PortalisException(mess);
				}
				return actif.getPid();
			}
		}
		if (!Util.available(port)) {
			String mess = "Port "+ port +" is active but, the process which is using it, is not a CamelisProcess'";
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		return 0;
	}

	public static VoidReponse clearLog(File logFile) {
		VoidReponse voidReponse = new VoidReponse();
		final String logFileName = logFile.getName();
		final File logDir = logFile.getParentFile();

		Calendar calendar = new GregorianCalendar();
		StringBuffer buff = new StringBuffer();
		buff.append(calendar.get(Calendar.DAY_OF_MONTH) + "-");
		buff.append(calendar.get(Calendar.MONTH) + "-");
		buff.append(calendar.get(Calendar.YEAR) + "_");
		buff.append(calendar.get(Calendar.HOUR) + ":");
		buff.append(calendar.get(Calendar.MINUTE) + ":");
		buff.append(calendar.get(Calendar.SECOND) + ":");
		buff.append(calendar.get(Calendar.MILLISECOND) + "_");

		String prefixe = buff.toString();
		for (File file : logDir.listFiles()) {
			if (file.getName().startsWith(logFileName)) {
				String cp_cmd = String.format("cp %s %s/%s%s", file.getPath(),
						logDir.getPath(), prefixe, file.getName());
				try {
					LinuxInteractor.executeCommand(cp_cmd, true);
				} catch (PortalisSystemException e) {
					String mess = String.format(
							"Impossible d'exécuter la commande '%s' : %s",
							cp_cmd, e.getMessage());
					LOGGER.error(CommonsUtil.stack2string( e));
					return new VoidReponse(new Err(mess));
				}
				if (file.getName().length() == logFileName.length()) {
					BufferedWriter out;
					try {
						out = new BufferedWriter(new FileWriter(file));
						out.write("");
						out.close();
					} catch (IOException e) {
						String mess = String.format(
								"Impossible d'exécuter la commande '%s' : %s",
								cp_cmd, e.getMessage());
						LOGGER.error(CommonsUtil.stack2string( e));
						return new VoidReponse(new Err(mess));
					}
				} else {
					file.delete();
				}
			}
		}
		return voidReponse;
	}

	public static Set<PortActif> getActifsCamelisPortsOnPortalis()
			throws PortalisException {
		final String cmd = "ps -ef | grep "
				+ CAMELIS_APPLICATION_NAME;
		LOGGER.info("==== Linux ==== >> getActifsCamelisPorts()");
		HashSet<PortActif> result = new HashSet<PortActif>();
		String reponse;
		try {
			reponse = LinuxInteractor.executeCommand(cmd, true);
		} catch (PortalisSystemException e) {
			throw new PortalisException(e);
		}

		String[] lignes = reponse.split("\n");
		for (String inputLine : lignes) {
			int i = inputLine.indexOf("-port");
			if (i != -1) {
				String portNum = inputLine.substring(i + 6, i + 10);

				Matcher m = ClientConstants.PID_PATTERN.matcher(inputLine);
				m.find();
				String processNum = m.group(1);
				PortActif actif = new PortActif(
						PortalisService.getInstance().getHost(),
						new Integer(portNum), new Integer(processNum));
				LOGGER.debug(actif.toString());
				result.add(actif);
			}
		}
		return result;
	}

	public static void killCamelisProcessByPid(int pid)
			throws PortalisException {
		LOGGER.info("==== Linux ==== >> killCamelisProcessByPid(" + pid + ")");
		try {
			final String cmd = "kill -9 " + pid;
			LinuxInteractor.executeCommand(cmd, true);
			LOGGER.debug("killProcess(" + pid + ")");
		} catch (Exception e) {
			String mess = e.getClass().getName()
					+ " : impossible de tuer le processus " + pid + ", "
					+ e.getMessage();
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
	}

}
