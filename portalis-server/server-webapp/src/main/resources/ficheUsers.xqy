declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $root as node() external;

declare function local:getFicheSite() as  element()* {
		<section> 
				<h1 class="header">Utilisateurs</h1>
		{
	for $user in $root/site/superUser | $root/site/standardUser
	order by $user/@email
	return(
		<h3 class="name">{concat('',$user/@email)}</h3>,
		<ul class="list">
		<li><em>Pseudo</em>{concat(' : ',$user/@pseudo)}</li>
		<li><em>Password</em>{concat(' : ',$user/@password)}</li>
		
		{if (name($user)='superUser') then
		    <li><em>Roles</em> : Super User</li> else 
		    <li><em>Admin role</em>{concat(' : ',$user/@portalisAdminRole)}</li>
		}
		</ul>,
		if (name($user)='standardUser') then (
		<div class="interTitre">Other roles</div>,
		<ul class="list">
		{for $role in $root/site/role[@email=$user/@email]
		return
		    <li><em>{concat('',$role/@serviceName)}</em>{concat(' : ',$role/@value)}</li> 
		    }
		  </ul>) else ()
		)
		}
		
	<div id="divLogout">
		<form action="editSite.jsp" method="post">
			<fieldset id="formLogin">
				<input type="submit" name="connectUser" value="Edit Site" />
			</fieldset>
		</form>
		<form action="clientLogout.jsp" method="post">
			<fieldset id="formLogin">
				<input type="submit" name="connectUser" value="Logout" />
			</fieldset>
		</form>
	</div>
				
		</section>
};

()