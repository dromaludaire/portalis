<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.Config"%>
<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.BooleanReponse"%>

<%@ page import="java.lang.NumberFormatException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ErrorMessages"%>

<%!private static final String jspName = "portIsBusy.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);%>

<%
	response.setContentType(Config.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.debug("\nPortalisCtx ====== " + jspName + " ====== "
			+ session.getId());

	BooleanReponse reponse = null;
	String portNum = request.getParameter(XmlIdentifier.PORT());
		String camelisSessionId = (String) session
				.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		if (camelisSessionId == null) {
			String mess = jspName + " : "
					+ ErrorMessages.TEMOIN_NO_USER_LOGGED_IN;
			LOGGER.debug(mess);
			reponse = new BooleanReponse(new Err(mess));
		} else if (portNum == null) {
			String mess = "manque le parametre " + XmlIdentifier.PORT();
			LOGGER.debug(mess);
			reponse = new BooleanReponse(new Err(mess));
		} else {
			try {
				int port = Integer.parseInt(portNum);
				reponse = AdminCmd.portIsBusy(camelisSessionId,
						port);
			} catch (NumberFormatException e) {
				String mess = "error "+jspName+"Port parameter in not numeric, its value is : "
						+ portNum;
				LOGGER.debug(mess);
				reponse = new BooleanReponse(new Err(mess));
			}
		}

%>
<%=reponse%>
