<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.Config"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.PortalisService"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse"%>


<%!private static final String jspName = "anonymousLogin.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);%>

<%
	response.setContentType(Config.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.debug("\nPortalisCtx ====== " + jspName + " ====== "
			+ session.getId());

	LoginReponse reponse = null;
	String camelisSessionId = null;
	try {
		camelisSessionId = (String) session
				.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		reponse = AdminCmd.anonymousLogin(session);
		if (reponse.isOk()) {
			camelisSessionId = reponse.getActiveUser()
					.getPortalisSessionId();
			LOGGER.debug(new StringBuffer(jspName)
					.append("(")
					.append(reponse.getActiveUser().getUserCore()
							.getEmail()).append(")")
					.append(" portalisID = ").append(camelisSessionId)
					.toString());
		}
// 	} catch (PortalisException e) {
// 		String mess = "Error " + jspName;
// 		LOGGER.warn(mess, e);
// 		reponse = new LoginReponse(new Err(mess, e.getMessage()));
	} catch (Throwable t) {
		String mess = "Error in ", jspName;
		LOGGER.error(mess, t);
		reponse = new LoginReponse(new Err(mess, t));
	}
%>

<%=reponse%>
