<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.Config"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ErrorMessages"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse"%>

<%!private static final String jspName = "getSite.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);%>

<%
	response.setContentType(Config.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.debug("\nPortalisCtx ====== " + jspName + " ====== "
			+ session.getId());

	ActiveSiteReponse reponse = null;
	try {
		String camelisSessionId = (String) session
				.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.debug("CamelisSessionId = " + camelisSessionId);
		if (camelisSessionId == null) {
			String mess = jspName + " : " + ErrorMessages.TEMOIN_NO_USER_LOGGED_IN;
			LOGGER.warn(mess);
			reponse = new ActiveSiteReponse(new Err(mess));
		} else {
			reponse = AdminCmd.getActiveSite(camelisSessionId);
		}
// 	} catch (PortalisException e) {
// 		String mess = "Error "+jspName;
// 		LOGGER.warn(mess, e);
// 		reponse = new SiteReponse(new Err(mess, e.getMessage()));
	} catch (Throwable t) {
		String mess = "Error "+jspName;
		LOGGER.error(mess, t);
		reponse = new ActiveSiteReponse(new Err(mess, t));
	}
%>
<%=reponse%>
