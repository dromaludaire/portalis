<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.Config"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ErrorMessages"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse"%>

<%!private static final String jspName = "startCamelis.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);%>

<%
	response.setContentType(Config.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.debug("\nPortalisCtx ====== " + jspName + " ====== "
			+ session.getId());

	StartReponse reponse = null;
	try {
		String serviceName = AdminCmd.checkServiceName(request
				.getParameter(XmlIdentifier.SERVICE_NAME()));
		String portNum = request.getParameter(XmlIdentifier.PORT());
		String datadir = request.getParameter(XmlIdentifier.DATADIR());
		String logFile = request.getParameter(XmlIdentifier.LOG());

		String camelisSessionId = (String) session
				.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.debug("CamelisSessionId = " + camelisSessionId);
		if (camelisSessionId == null) {
			String mess = "Error " + jspName + " : " + ErrorMessages.TEMOIN_NO_USER_LOGGED_IN;
			LOGGER.debug(mess);
			reponse = new StartReponse(new Err(mess));
		} else if (portNum != null) {
			// give a choosen port number
			reponse = AdminCmd.startCamelis(camelisSessionId, portNum,
					serviceName, datadir, logFile);
		} else {
			// leave portalis choosing port number

			reponse = AdminCmd.startCamelis(camelisSessionId,
					serviceName, datadir, logFile);
		}

	} catch (PortalisException e) {
		String mess = "Error " + jspName + " : ";
		LOGGER.warn(mess, e);
		reponse = new StartReponse(new Err(mess, e.getMessage()));
	} catch (Throwable t) {
		String mess = "Error " + jspName;
		LOGGER.error(mess, t);
		reponse = new StartReponse(new Err(mess, t));
	}
	LOGGER.debug("reponse = " + reponse);
%>
<%=reponse%>
