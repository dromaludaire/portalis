var data = [ {
	"id" : "Distance",
	"label" : "Distance (9)",
	"load_on_demand" : "true",
	"color" : "gray",
	"children" : [ {
		"id" : "Far",
		"label" : "Far (5)",
		"color" : "black"
	}, {
		"id" : "Near",
		"label" : "Near (4)",
		"color" : "black"
	} ]
}, {
	"id" : "Size",
	"label" : "Size (9)",
	"load_on_demand" : "true",
	"color" : "gray",
	"children" : [ {
		"id" : "Small",
		"label" : "Small (5)",
		"color" : "black"
	}, {
		"id" : "Jovian",
		"label" : "Jovian (4)",
		"load_on_demand" : "true",
		"color" : "black",
		"children" : [ {
			"id" : "Big",
			"label" : "Big (2)",
			"color" : "black"
		}, {
			"id" : "Medium",
			"label" : "Medium (2)",
			"color" : "black"
		} ]
	} ]
}, {
	"id" : "satellite",
	"label" : "satellite (7)",
	"color" : "black"
} ];

$('#attributs').tree(
		{
			data : data,
			autoOpen : true,
			onCreateLi : function(node, $li) {
				if (node.color) {
					var $title = $li.find('.jqtree-title');
					$title.addClass(node.color);
				}
				if (node.color != 'gray') {
					$($li.children().last().children().last()).before(
							'<input type="checkbox" name="choisir"/>');
				}
			}
		});

var newQuery;

window["zoom"] = function () {
	var $tree = $('#attributs');
	document.selectedNodes = [];
	getSelected($tree.tree('getTree'));
    console.log(newQuery);
};

function getSelected(node) {
    console.log('nb children = '+node.children.length);
	console.log($(node));
    var id = $(node).attr('id');
    if (id)
        console.log('id = '+id);
    var li = node.element;
    if (li) {
         console.log(li);
        var input = $(li).children('div').children('input').first();
        if (input) {
            console.log(input);
            console.log(input.prop('checked'));
            if (input.prop('checked')) {
                if (newQuery) {
                    newQuery = newQuery+' and '+ id;
                } else {
                    newQuery = id;
                }
            }
        }
    }
	for (var i=0; i < node.children.length; i++) {
		getSelected(node.children[i]);
	    
	}
};
