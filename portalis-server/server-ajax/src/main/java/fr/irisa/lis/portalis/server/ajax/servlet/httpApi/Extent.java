// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   AttributModel.java

package fr.irisa.lis.portalis.server.ajax.servlet.httpApi;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveService;
import fr.irisa.lis.portalis.shared.admin.data.LisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.UIPreference;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.ServiceCoreReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.GetValuedFeaturesReponse;

@SuppressWarnings("serial")
public class Extent extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(Extent.class
			.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			LOGGER.debug("1");

			String callback = request.getParameter("callback");
			String aaData = request.getParameter("aaData");

			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("2");
			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("3");
			ActiveService activeService = camelisHttp.getActiveService();
			if (activeService == null) {
				String mess = "Pas de service actif";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("4");
			ServiceCoreReponse serviceCoreReponse = AdminHttp.getServiceCore(
					camelisHttp.getActiveUser(), activeService.getActiveId());
			if (!serviceCoreReponse.isOk()) {
				String mess = "Impossible d'obtenir le serviceCore : "
						+ serviceCoreReponse.getMessagesAsString();
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("serviceCoreReponse=" + serviceCoreReponse);
			LOGGER.debug("activeService=" + camelisHttp.getActiveService());

			// Lecture des préférences

			UIPreference uiPreferences = (UIPreference) session
					.getAttribute(Constant.PREFERENCE
							+ activeService.getActiveId());
			if (uiPreferences == null) {
				LisServiceCore serviceCore = serviceCoreReponse.getServiceCore();
				uiPreferences = serviceCore.getUiPreference();
			}


			String req = request.getParameter("request");
			if (req == null) {
				String mess = "Pas de paramêtre 'request'";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			String s_iDisplayStart = request.getParameter("iDisplayStart");
			String s_iDisplayLength = request.getParameter("iDisplayLength");
			String s_sEcho = request.getParameter("sEcho");
			LOGGER.debug(String.format(
					"req=%s, iDisplayStart=%s iDisplayLength=%s", req,
					s_iDisplayStart, s_iDisplayLength));

			ExtentReponse extentReponse = null;
			if (s_iDisplayStart != null && s_iDisplayLength != null) {

				try {
					int iDisplayStart = Integer.parseInt(s_iDisplayStart);
					int iDisplayLength = Integer.parseInt(s_iDisplayLength);

					int page = iDisplayStart / iDisplayLength;

					extentReponse = camelisHttp.extent(req, page,
							iDisplayLength);
				} catch (NumberFormatException e) {
					String mess = "Pas de paramêtre 'request'";
					LOGGER.error(mess);
					response.sendError(
							HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
					return;
				}
			} else {
				extentReponse = camelisHttp.extent(req);
				if (!extentReponse.isOk()) {
					String mess = extentReponse.getMessagesAsString();
					LOGGER.error(mess);
					response.sendError(
							HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
					return;
				}
			}
			if (extentReponse == null) {
				String mess = "impossible) d'obtenir l'extension";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			Integer nbObjects = extentReponse.getNbObjects();
			int iTotalRecords = nbObjects;
			int iTotalDisplayRecords = nbObjects;

			Set<String> feats = uiPreferences.getCamelisFeatures();

			GetValuedFeaturesReponse featsReponse = null;
			if (feats.size() > 0) {
				featsReponse = camelisHttp.getValuedFeatures(extentReponse
						.getExtent().getOids(), feats.toArray(new String[feats
						.size()]));
				if (!featsReponse.isOk()) {
					String mess = featsReponse.getMessagesAsString();
					LOGGER.error(mess);
					response.sendError(
							HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
					return;

				}
			}
			LOGGER.debug("featsReponse=" + featsReponse);

			response.setContentType(Constant.CONTENT_TYPE_JSON);

			PrintWriter out = response.getWriter();
			if (callback != null) {
				out.println(callback + "(");
			}
			if (aaData != null && aaData.equals("true")) {
				withAaData(camelisHttp, uiPreferences, nbObjects,
						extentReponse, out, req, featsReponse, s_sEcho,
						iTotalRecords, iTotalDisplayRecords);
			} else {
				withoutAaData(camelisHttp, uiPreferences, nbObjects,
						extentReponse, out, featsReponse, s_sEcho,
						iTotalRecords, iTotalDisplayRecords);
			}
			if (callback != null) {
				out.println(")");
			}

		} catch (Throwable t) {
			String mess = t.getMessage() + CommonsUtil.stack2string(t);
			LOGGER.error(mess);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					mess);
			return;
		}

	}


	private void withAaData(CamelisHttp camelisHttp,
			UIPreference uiPreferences, Integer nbObjects,
			ExtentReponse extentReponse, PrintWriter out, String request,
			GetValuedFeaturesReponse featsReponse, String sEcho,
			int iTotalRecords, int iTotalDisplayRecords)
			throws PortalisException {
		out.println("{");

		if (sEcho != null) {
			out.println("  \"sEcho\": " + sEcho + ",");
			out.println("  \"iTotalRecords\": " + iTotalRecords + ",");
			out.println("  \"iTotalDisplayRecords\": " + iTotalDisplayRecords
					+ ",");
		}

		out.println("  \"aaData\": [");
		List<String> camelisInfo = uiPreferences.getCamelisInfo();
		Set<String> feats = uiPreferences.getCamelisFeatures();
		int j = 0;
		for (LisObject lisObject : extentReponse.getExtent().getLisObjects()) {
			out.print(j++ == 0 ?"":",");
			out.print("   [");

			int i = 0;
			int oid = lisObject.getOid();

			for (String featname : camelisInfo) {
				if (featname.equals("ID")) {
					out.print(i++ == 0 ?"":",");
					out.print("\"");
					out.print(oid);
					out.print("\"");
				} else if (featname.equals("Label")) {
					out.print(i++ == 0 ?"":",");
					out.print("\"");
					out.print(lisObject.getName().replace("\"", "\\\""));
					out.print("\"");
				} else if (featname.equals("Picture")) {
					out.print(i++ == 0 ?"":",\n");
					out.print("");
					String image = lisObject.getPicture();
					if (image == null || image.length() == 0 )
						image = "images/no-image-100.jpg";
					else
						image = camelisHttp.getThumbnail(image, "60", "60").toString();
					out.println(String.format("\"<img src=\\\"%s\\\"  width=\\\"60\\\" height=\\\"60\\\" />\"", image));
				}
			}

			for (String featname : feats) {
				out.print(i++ == 0 ?"":",");
				Set<String> values = featsReponse.getValues(oid, featname);
				StringBuffer buff = new StringBuffer("\"<ul>");
				for (String value : values) {
					buff.append("<li>").append(value.replace("\"", "\\\"")).append("</li>");
				}
				out.println( buff.append("</ul>\"").toString());
			}

			out.println("]");
		}
		out.println("  ]\n}");

	}



	private void withoutAaData(CamelisHttp camelisHttp,
			UIPreference uiPreferences, Integer nbObjects,
			ExtentReponse extentReponse, PrintWriter out,
			GetValuedFeaturesReponse featsReponse, String sEcho,
			int iTotalRecords, int iTotalDisplayRecords)
			throws PortalisException {
		out.println("[");
		int j = nbObjects;
		for (LisObject lisObject : extentReponse.getExtent().getLisObjects()) {
			out.print("   { \"id\" : \"");
			out.print(lisObject.getOid());
			out.print("\", \"label\" : \"");
			out.print(lisObject.getName().replace("\"", "\\\""));
			if (uiPreferences.isHasPictures()) {
				out.print("\", \"picture\" : \"");
				String image = lisObject.getPicture();
				image = (image == null || image.length() == 0 ? "/no-image-100.jpg"
						: image);

				out.print(camelisHttp.getThumbnail(image, "", "").toString());
			} else {
				out.print("\"");
			}

			j--;
			if (j == 0) {
				out.println("\" }");
			} else {
				out.println("\" },");
			}
		}
		out.println("]");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
