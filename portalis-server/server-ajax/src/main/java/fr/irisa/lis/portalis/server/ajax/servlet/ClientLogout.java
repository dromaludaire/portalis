package fr.irisa.lis.portalis.server.ajax.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;


@SuppressWarnings("serial")
public class ClientLogout extends HttpServlet {


	private static final Logger LOGGER = LoggerFactory.getLogger(ClientLogout.class
			.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}


			ActiveUser activeUser = camelisHttp.getActiveUser();
			if (activeUser == null) {
				String mess = "Sorry can't find activeUser";
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}

			VoidReponse voidReponse = AdminHttp.logout(activeUser);
			if (!voidReponse.isOk()) {
				String mess = "Impossible d'arrêter la sessiod de  "
						+ activeUser.getUserCore().getEmail() + " : "
						+ voidReponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}
			
			response.sendRedirect(request.getContextPath()+"/stopPortalis.html");

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
