package fr.irisa.lis.portalis.server.ajax.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.server.core.AdminCmd;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

@SuppressWarnings("serial")
public class StartCamelis extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(StartCamelis.class.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {


			String serviceName = AdminCmd.checkServiceName(request
					.getParameter(XmlIdentifier.SERVICE_NAME()));
			if (serviceName == null) {
				String mess = "Manque le paramètre "
						+ XmlIdentifier.SERVICE_NAME();
				LOGGER.error(mess);
				throw new ServletException(mess);
			}

			HttpSession session = request.getSession();
			if (session == null) {
				String mess = "Pas de session utilisateur en cours";
				LOGGER.error(mess);
				throw new ServletException(mess);
			}
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

						
			ActiveUser activeUser = camelisHttp.getActiveUser();

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName);
			if (!startReponse.isOk()) {
				throw new ServletException(startReponse.getMessagesAsString());
			}

			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			LoadContextReponse loadReponse = new CamelisHttp(activeUser,
					activeLisService).loadContext();
			if (!loadReponse.isOk()) {
				String mess = "Error : " + loadReponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new ServletException(mess);

			}

			camelisHttp = new CamelisHttp(activeUser,
					activeLisService);
			userContext.setCamelisHttp(camelisHttp);



			response.sendRedirect("editSite.jsp");

		} catch (ServletException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
