package fr.irisa.lis.portalis.server.ajax.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

@SuppressWarnings("serial")
public class TestBenchUpload extends HttpServlet {

	private static final String jspName = "admin.patronUpload.jsp";

	private static final String PEGGY = "peggy.cellier@irisa.fr";
	private static final String PEGGY_PASSWORD = "peggy";

	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);
	private static final String UPLOAD_DIRECTORY = Constant.WEBAPPS_DIR
			+ "linguistique/patron/";
	private static final String SERVICE_NAME = "linguistique:patron";

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			if (session == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				throw new ServletException(mess);

			}
			// process only if its multipart content
			String uploadedFileName = null;
			if (ServletFileUpload.isMultipartContent(request)) {
				try {
					List<FileItem> multiparts = (List<FileItem>) new ServletFileUpload(
							new DiskFileItemFactory()).parseRequest(request);

					for (FileItem item : multiparts) {
						if (!item.isFormField()) {
							// get the last name of the path
							uploadedFileName = new File(item.getName())
									.getName();
							item.write(new File(UPLOAD_DIRECTORY
									+ uploadedFileName));
						}
					}

				} catch (Exception ex) {
					throw new ServletException("File Upload Failed due to " + ex);
				}

			} else {
				throw new ServletException(
						"Sorry this Servlet only handles file upload request");
			}

			LOGGER.info("\nPortalisCtx ========================================> "
					+ jspName + " <============================");
			LoginReponse loginReponse = AdminHttp.login(PEGGY, PEGGY_PASSWORD);
			if (!loginReponse.isOk()) {
				throw new ServletException(loginReponse.getMessagesAsString());
			}

			ActiveUser activeUser = loginReponse.getActiveUser();

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					SERVICE_NAME);
			if (!startReponse.isOk()) {
				throw new ServletException(startReponse.getMessagesAsString());
			}

			ActiveLisService activeLisService = startReponse
					.getActiveLisService();

			CamelisHttp camelisHttp = new CamelisHttp(activeUser,
					activeLisService);
			session.setAttribute(Constant.USER_CONTEXT, new UserContext(camelisHttp));

			ImportCtxReponse importReponse = camelisHttp
					.importCtx(uploadedFileName);
			if (!importReponse.isOk()) {
				throw new ServletException(importReponse.getMessagesAsString());
			}


			response.sendRedirect("camelis.jsp");

		} catch (ServletException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
