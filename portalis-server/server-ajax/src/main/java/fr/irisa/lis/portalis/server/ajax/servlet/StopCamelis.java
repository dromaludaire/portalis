package fr.irisa.lis.portalis.server.ajax.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;

@SuppressWarnings("serial")
public class StopCamelis extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(StopCamelis.class.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}


			ActiveUser activeUser = camelisHttp.getActiveUser();
			String activeServiceId = request.getParameter(XmlIdentifier
					.ACTIVE_SERVICE_ID());
			if (activeServiceId == null) {
				String mess = "Error : parameter "
						+ XmlIdentifier.ACTIVE_SERVICE_ID() + " is missing";
				LOGGER.error(mess);
				throw new ServletException(mess);
			}

			PingReponse pingReponse = AdminHttp
					.getActiveLisServicesOnPortalis(activeUser);
			if (!pingReponse.isOk()) {
				String mess = "Impossible d'arrêter le service "
						+ activeServiceId + " : "
						+ pingReponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new ServletException(mess);
			}

			ActiveLisService activeLisService = pingReponse
					.getService(activeServiceId);

			PingReponse stopReponse = AdminHttp.stopCamelis(activeUser,
					activeLisService);
			if (stopReponse == null) {
				String mess = "Impossible d'arrêter le service "
						+ activeServiceId;
				LOGGER.error(mess);
				throw new ServletException(mess);
			}
			if (!stopReponse.isOk()) {
				String mess = "Impossible d'arrêter le service "
						+ activeServiceId + " : "
						+ stopReponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}
			response.sendRedirect("editSite.jsp");

		} catch (ServletException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
