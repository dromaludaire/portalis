<%@ page errorPage="showError.jsp"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.Constant"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.UserContext"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.http.AdminHttp"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse"%>

<%!private static final String jspName = "admin.clientlogout.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);
	private PortalisCtx camelisCtx = PortalisCtx.getInstance();%>

<%
	LOGGER.debug("\nPortalisCtx ====== " + jspName + " ====== "
			+ session.getId());

	UserContext userContext = (UserContext) session
			.getAttribute(Constant.USER_CONTEXT);
	if (userContext == null) {
		String mess = "Pas de session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	CamelisHttp camelisHttp = userContext.getCamelisHttp();
	if (camelisHttp == null) {
		String mess = "Pas de camelis dans la session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	ActiveUser activeUser = camelisHttp.getActiveUser();
	if (activeUser == null) {
		String mess = jspName + " : Sorry can't find activeUser";
		LOGGER.error(mess);
		throw new PortalisException(mess);
	}

	VoidReponse voidReponse = AdminHttp.logout(activeUser);
	if (!voidReponse.isOk()) {
		String mess = "Impossible d'arr�ter la sessiod de  "
				+ activeUser.getUserCore().getEmail() + " : "
				+ voidReponse.getMessagesAsString();
		LOGGER.error(mess);
		throw new PortalisException(mess);
	}

	response.sendRedirect(request.getContextPath() + "/index.jsp");
%>
