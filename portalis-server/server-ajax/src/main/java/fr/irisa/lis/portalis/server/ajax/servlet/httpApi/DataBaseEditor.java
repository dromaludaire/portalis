package fr.irisa.lis.portalis.server.ajax.servlet.httpApi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

@SuppressWarnings("serial")
public class DataBaseEditor extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zoom.class
			.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}


			ActiveUser activeUser = camelisHttp.getActiveUser();
			if (activeUser == null) {
				String mess = "Sorry can't find activeUser";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			SiteReponse siteReponse = AdminHttp.getSite(activeUser);
			if (!siteReponse.isOk()) {
				String mess = "Impossible d'obtenir le site : "+siteReponse.getMessagesAsString();
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			Site site = siteReponse.getSite();
			if (site == null) {
				String mess = "la réponse de getSite ne contient pas de siteActif";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(site);

			response.setHeader("Expires", "0");
			response.setContentType(Constant.CONTENT_TYPE_HTML);

			PrintWriter out = response.getWriter();
			out.println(new XqueryUtil().toStringEval(elem.getOwnerDocument(),
							"ficheUsers.xqy", "getFicheSite"));
		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, t.getMessage());
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
