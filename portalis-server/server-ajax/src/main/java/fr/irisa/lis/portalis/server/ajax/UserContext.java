package fr.irisa.lis.portalis.server.ajax;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.admin.db.DataBaseSimpleImpl;
import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.UIPreference;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

public class UserContext {
	private CamelisHttp camelisHttp;
	private UIPreference uiPreference = new UIPreference();

	public UserContext(CamelisHttp camelisHttp2) {
		this.camelisHttp = camelisHttp2;
	}

	public CamelisHttp getCamelisHttp() {
		return camelisHttp;
	}

	public void setCamelisHttp(CamelisHttp camelisHttp) {
		this.camelisHttp = camelisHttp;
	}

	public UIPreference getUiPreference() {
		return uiPreference;
	}

	public void setUiPreference(UIPreference uiPreference) {
		this.uiPreference = uiPreference;
	}
	
	public static void main(String[] args) throws Exception {
		Document doc = DOMUtil.readFile(new FileInputStream("activeSite.xml"));
		System.out.println(new XqueryUtil().toStringEval(doc, "jsonSpaqlEndPoints.xqy","getSite"));
	}
	
	
	@SuppressWarnings("unused")
	private static void tryContext() throws JsonGenerationException, JsonMappingException, IOException {
		UIPreference uiPreference = new UIPreference();
		ObjectMapper mapper = new ObjectMapper();
		String[] hiddenAttr = {"toto", "titi ?"};
		uiPreference.addHiddenAttribute(hiddenAttr);
		System.out.println(mapper.writeValueAsString(uiPreference));
		
		DataBaseSimpleImpl bd = DataBaseSimpleImpl.getInstance();
		System.out.println(mapper.writeValueAsString(bd));
		
		System.out.println(	DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(bd)));
	}



}
