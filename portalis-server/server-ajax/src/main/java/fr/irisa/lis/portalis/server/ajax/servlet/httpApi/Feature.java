package fr.irisa.lis.portalis.server.ajax.servlet.httpApi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

@SuppressWarnings("serial")
public class Feature extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(Feature.class
			.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {

			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			String callback = request.getParameter("callback");

			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			ActiveLisService activeLisService = camelisHttp.getActiveService();
			if (activeLisService == null) {
				String mess = "Pas de serviceActif dans camelis";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			
			
			PrintWriter out = response.getWriter();
			if (callback != null) {
				out.println(callback + "(");
			}
			out.println("[");
			for (String feature : activeLisService.getFeatures(camelisHttp.getActiveUser())) {
				out.print("\"");
				out.print(feature.replace("\"", "\\\""));
				out.print("\",\n");
			}
			out.println("]");
			if (callback != null) {
				out.println(")");
			}

		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					t.getMessage());
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		LOGGER.debug("post");
		doGet(request, response);
	}

}
