<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true" %>

<%@ page import="java.io.StringWriter"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="fr.irisa.lis.portalis.commons.core.CommonsUtil"%>

<!doctype html>
<html>
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="css/camelis.css">
</head>
<body>
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label id="logo-label">Portalis</label>
		</div>
		<div id="header-login-title">
			<h4>Opps ! ... Erreur Portalis</h4>
		</div>
	</header>
	<section id="errorSection">
	<table border="1">
<tr valign="top">
<td><b>Message</b></td>
<td><%=exception.getMessage()%></td>
</tr>
<tr valign="top">
<td><b>URI</b></td>
<td>${pageContext.errorData.requestURI}</td>
</tr>
<tr valign="top">
<td><b>Stack trace</b></td>
<td>
<pre><%=CommonsUtil.stack2string(exception)%></pre> 
</td>
</tr>
</table>
	</section>
</body>
</html>