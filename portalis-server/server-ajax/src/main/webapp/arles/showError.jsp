<%@ page isErrorPage="true" contentType="text/html"%>

<%@ page import="java.io.StringWriter"%>
<%@ page import="java.io.PrintWriter"%>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="../../css/portalis.css">
</head>
<body>
	<jsp:include page="${request.getContextPath()}/header.jsp" />
	<h1>Erreur Portalis</h1>
<em>Message :</em>
<%=exception.getMessage()%><br/>

<em>StackTrace :</em>
<br/>
<%
	StringWriter stringWriter = new StringWriter();
	PrintWriter printWriter = new PrintWriter(stringWriter);
	exception.printStackTrace(printWriter);
	out.println(stringWriter);
	printWriter.close();
	stringWriter.close();
%></body>
</html>