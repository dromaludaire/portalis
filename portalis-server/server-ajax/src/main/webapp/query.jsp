<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="showError.jsp"%>


<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%@ page import="fr.irisa.lis.portalis.server.ajax.Constant"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.UserContext"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.ActiveLisService"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.SparqlApplication"%>
<%@ page import="fr.irisa.lis.jquery.TreetableNode"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.ActivePortalisService"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp"%>

<%@ page import="org.w3c.dom.Element"%>

<%@ page import="fr.irisa.lis.portalis.commons.XqueryUtil"%>
<%@ page import="fr.irisa.lis.portalis.commons.DOMUtil"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveSite"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.http.AdminHttp"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse"%>


<%@ page import="org.codehaus.jackson.JsonGenerationException"%>
<%@ page import="org.codehaus.jackson.map.JsonMappingException"%>
<%@ page import="org.codehaus.jackson.map.ObjectMapper"%>



<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="stylesheet" type="text/css" href="css/portalis.css">
<title>Portalis query</title>
<link rel="stylesheet" type="text/css" href="css/camelis.css"
	media="screen" />
<link rel="stylesheet" type="text/css" href="./css/jquery.treetable.css" />
<link rel="stylesheet" type="text/css" href="./css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />

<title>Insert title here</title>
<!--[if lt IE 9]-->
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.2.js"></script>
	
<!--[endif]-->
<!--[if gte IE 9]-->
<script type="text/javascript"
	src="http://code.jquery.com/jquery-2.0.3.js"></script>
<!--[endif]-->
<script type="text/javascript" src="./js/camelis.js"></script>
<script type="text/javascript" src="./js/jQuery/jquery.treetable.js"></script>
<script type="text/javascript" src="./js/jQuery/grid.locale-en.js" ></script>
<script type="text/javascript" src="./js/jQuery/jquery-ui-1.10.3.custom.js" ></script>
<script type="text/javascript" src="./js/jQuery/jquery.jqGrid.min.js"></script>

</head>
<%!private static final String jspName = "query.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);%>
<%
	String serviceName = request.getParameter("serviceName");
	if (serviceName==null) {
		String mess = "Pas de parametre 'serviceName'";
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	String url = request.getParameter("url");
	if (url==null) {
		String mess = "Pas de parametre 'url'";
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	String query = request.getParameter("query");
	if (query==null) {
		String mess = "Pas de parametre 'query'";
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	String label = request.getParameter("label");
	if (label==null) {
		String mess = "Pas de parametre 'label'";
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	LoginReponse loginReponse = AdminHttp.anonymousLogin();
	if (!loginReponse.isOk()) {
		String mess = "la réponse de getActifSite est en erreur "
		+ loginReponse.getMessagesAsString();
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	ActiveUser activeUser = loginReponse.getActiveUser();

	ActiveSiteReponse siteRep = AdminHttp.getActiveSite(activeUser);

	LOGGER.info("siteRep = " + siteRep.toString());
	if (!siteRep.isOk()) {
		String mess = "la réponse de getActifSite est en erreur "
		+ siteRep.getMessagesAsString();
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	ActiveSite activeSite = siteRep.getSite();
	if (activeSite == null) {
		String mess = "la réponse de getActifSite ne contient pas de siteActif";
		LOGGER.error(mess);
		response.sendError(
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER
	.visit(activeSite);

	final String TABLE_ID = "sparqlEndPoints";
	SparqlApplication sparqlApplication = activeSite
	.getSparqlEndPoints();
	
	String[] props = {"language", "url"};
	String endPointsTree = DOMUtil.prettyXmlString(sparqlApplication.toTreetableNode()
	.toHtmlTable("", TABLE_ID, props));
	LOGGER.debug("html\n"+endPointsTree);
	
	ObjectMapper mapper = new ObjectMapper();
	String sparqlApplicationAsJson = mapper.writeValueAsString(sparqlApplication);
	LOGGER.debug("sparqlApplicationAsJson = " + sparqlApplicationAsJson);
%>
<body>
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label
				id="logo-label">Portalis</label>
		</div>
		<table style="width:100%">
			<caption>
				Sparql url <input type="text" id="url" value="<%=url %>" style="width: 500px"/>
			</caption>
			<tr>
				<td><span> <textarea style="width: 91%;" name="requete"
							rows="10" cols="2" id="request"><%=query%></textarea>
				</span> <span class="rightAlign" style="width: 8%;"> 
				<em><b>Not connected</b></em> <br/><em><b>Service</b></em>&nbsp;:
						<br />
					<span id="serviceName"><%=serviceName%></span>

				</span></td>
			</tr>
			<tr>
				<td><span id="buttonsForRequest"> <img id="refresh"
						src="images/page-refresh-reload-icone-5333-16.png" alt=""
						onclick="send()" /> <span id="requestLabel"><%=label %></span>
						
				</span> <span style="float: right;"> <img style="display: none;" id="annuler"
						src="images/annuler-icone-24.png" alt="annuler" title="Annuler"
						onclick="showMain()" /> <img
						src="images/login-computer-button-24.jpg" alt="login"
						title="Login" onclick="showLoginDiv()" />
						<a id="login" href="accueil.jsp"><img style="vertical-align:top"
				src="images/home-24.jpg" alt="" title="Retour à la page d'accueil" /></a>
				</span></td>
			</tr>
		</table>

	</header>

	<section id="logError" style="display: none;">
		<div class="log" style="background-color: #FAC5D2;"></div>
		<button onclick="$('#logError').hide()">Hide</button>
	</section>


	<section id="main-content">
		<section id="left-section">
			<header id="sub-header">
					<img id="minize-left-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" onclick="toggleCamelisJsp('left')"/>
				<span style="position: absolute; top: 2px; left-margin: 3px;">
					<label id="left-label">Sparql end points</label> 
				</span>

			</header>
			<article>
				<section id="table"><%= endPointsTree%></section>
			</article>
		</section>

		<section id="right-section">

			<header id="sub-header">
					<img id="minize-right-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" onclick="toggleCamelisJsp('right')"/>
				<label id="right-label">Result</label>
			</header>
			<article id="rightSectionContainer">
				<div id="resultat">
				</div>
			</article>
		</section>
		<section id="divLogin" style="display: none;">
			<form action="execLogin" method="post">
				<div class="centered">
					<table>
						<tr>
							<th colspan="2">Accéder aux services lis</th>
						</tr>
						<tr>
							<td><label for="email">Identifiant</label></td>
							<td><input type="email" id="email" name="email" /></td>
						</tr>
						<tr>
							<td><label for="password">Mot de passe</label></td>
							<td><input type="password" id="password" name="password" /></td>
						</tr>
						<tr>
							<td colspan="2"><div>
									<input type="radio" name="admin" value="camelis"
										checked="checked" />Utiliser Camelis <input type="radio"
										name="admin" value="site" />Admin Site <input type="radio"
										name="admin" value="users" />Admin Users
								</div></td>
						</tr>
						<tr>
							<td colspan="2"><span class="centrage"><input
									type="submit" name="connexionType" value="anonymousConnexion"
									id="valid-form" /><input type="submit" name="connexionType"
									value="userConnexion" id="valid-form" /></span></td>
						</tr>
					</table>
				</div>
			</form>
		</section>
		<section id="UIPreference">
		</section>
	</section>
	<script type="text/javascript">
		//<![CDATA[
		var sparqlApplication = <%=sparqlApplicationAsJson%>;
		display("sparqlApplication", sparqlApplication);
		
		$("#sparqlEndPoints").treetable({
			expandable : true
		});
		
		function send() {
			var query = $('#request').val();
			var url = $('#url').val();
			$.ajax({
				url: url,
				data: {
					"query" : query,
					"format" : "text/html",
					"timeout" : 30000,
					"debug" : "on"
				},
				success: function(data) {
					$('#resultat').empty();
					$('#resultat').append(data);
				},
				error: function() {
					alert("error");
				},
				dataType: "html"
				});
		}
		
		function showLoginDiv() {
			document.getElementById("right-section").style.display = "none";
			document.getElementById("left-section").style.display = "none";
			document.getElementById('divLogin').style.display = "table-cell";
			document.getElementById('annuler').style.display = "table-cell";
			document.getElementById('login').style.display = "none";
		}
		
		function showMain() {
			document.getElementById("right-section").style.display = "table-cell";
			document.getElementById("left-section").style.display = "table-cell";
			document.getElementById('divLogin').style.display = "none";
			document.getElementById('annuler').style.display = "none";
			document.getElementById('login').style.display = "table-cell";
		}

		function setQuery(serviceName, queryId) {
			$('#url').val(getServiceUrl(serviceName));
			$('#requestLabel').text(getQueryLabel(serviceName, queryId));
			$('#request').val(getQueryText(serviceName, queryId));
			send();

			// to avoid going on to the link's url
			return false;
		}
		
		function getServiceUrl(serviceName) {
			var services = sparqlApplication.services;
			for (var i = 0; i < services.length; i = i + 1) {
				if (services[i].serviceName==serviceName) {
					return services[i].url;
				}
			};
			return "";
		}

		function getQueryLabel(serviceName, queryId) {
			var services = sparqlApplication.services;
			for (var i = 0; i < services.length; i = i + 1) {
				if (services[i].serviceName==serviceName) {
					var query = services[i].query;
					for (var j= 0; j < query.length; j = j+1) {
						if (query[j].id==queryId) {
							return query[j].label;
						}
					}
				}
			};
			return "";
		}

		function getQueryText(serviceName, queryId) {
			var services = sparqlApplication.services;
			for (var i = 0; i < services.length; i = i + 1) {
				if (services[i].serviceName==serviceName) {
					var query = services[i].query;
					for (var j= 0; j < query.length; j = j+1) {
						if (query[j].id==queryId) {
							return query[j].query;
						}
					}
				}
			};
			return "";
		}
		
		$(document).ready(function() {
			send();
		});

		//]]>
	</script>
</body>
</html>