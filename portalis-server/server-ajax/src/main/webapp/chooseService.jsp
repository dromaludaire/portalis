<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="showError.jsp"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.Constant"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.UserContext"%>

<%!private static final String jspName = "admin.camelis.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);
%>

<%
	UserContext userContext = (UserContext) session
			.getAttribute(Constant.USER_CONTEXT);
	if (userContext == null) {
		String mess = "Pas de session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	CamelisHttp camelisHttp = userContext.getCamelisHttp();
	if (camelisHttp == null) {
		String mess = "Pas de camelis dans la session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	ActiveUser activeUser = camelisHttp.getActiveUser();
	if (activeUser.getUserCore().isGeneralAdmin()) {
		response.sendRedirect("editSite.jsp");
	}

	String userPseudo = activeUser.getUserCore().getPseudo();
%>

<!doctype html>
<html>
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="stylesheet" type="text/css" href="css/portalis.css">
<link rel="stylesheet" type="text/css" href="css/camelis.css"
	media="screen" />
<title>Insert title here</title>
<!--[if lt IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
<!--[endif]-->
<!--[if gte IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.js"></script>
<!--[endif]-->
<script type="text/javascript" src="./js/camelis.js"></script>
</head>
<body>
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label
				id="logo-label">Portalis</label>
		</div>
		<div id="header-title">
			<h4>Choisir un service</h4>
		</div>
		<div class="rightAlign" >
			<label>User : <%=userPseudo%></label> <a id="logout"
				href="clientLogout" title="Logout"> <img
				src="images/logout-icone-6625-24.png" alt="Logout" />
			</a>
		</div>

	</header>

	<section id="main-content">
		<section id="left-section">
			<header id="sub-header">
					<img class="toggler" onclick="toggle('left')" id="minize-left-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" />
				<label id="left-label">Modèles de service</label>
			</header>
			<article>
				<div id="staticSite"></div>
			</article>
		</section>
		<section id="right-section">
			<header id="sub-header">
					<img class="toggler" onclick="toggle('right')" id="minize-right-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" />
				<label id="right-label">Services actifs</label>
				<div class="rightAlign" >
					<form action="startTestBench.jsp?parcours=default">
						<input type="submit" value="Charger et tester votre service" />
					</form>
				</div>
			</header>
			<article>
				<div id="actifSite"></div>
			</article>
		</section>
	</section>

	<script type="text/javascript">
		//<[CDATA[
		$(document).ready(function() {
			$('#staticSite').load('showSite?site=static');
			$('#actifSite').load('showSite?site=actifChoose');
		});

		$body = $("body");

		$(document).on({
			ajaxStart : function() {
				$body.addClass("loading");
			},
			ajaxStop : function() {
				$body.removeClass("loading");
			}
		});
		//]]>
	</script>
	<div class="modal"></div>

</body>
</html>