// Le servlet renvoie une reponse de type texte indiquant si on doit recharger la page
// = Si la requete change, on recharge la page pour actualiser les objets/attributs etc

// Appele le servlet requestmanager qui change l'etat de la requete suivant la direction dir
function changeRequest (dir) {
	$.ajax({ 
		url: "requestsmanager",
		method: "POST",
		data: {direction: dir},
		success: function(text) {if (text=='reload') {location.assign(location.href);} else if (text=='login') {$(location).attr('href', "login");}}
	});
};

// Appele le servlet requestmanager pour ajouter une nouvelle requete
function addRequest (req) {
	$.ajax({ 
		url: "requestsmanager",
		method: "POST",
		data: {newquerry: req},
		success: function(text) {if (text=='reload') {location.assign(location.href);} else if (text=='login') {$(location).attr('href', "login");}}
	});
};

// Appels des fonctions au clic sur les boutons
$(document).ready(function() {
	$('#first').click(function() {
		changeRequest("first");
	});
	$('#previous').click(function() {
		changeRequest("previous");
	});
	$('#next').click(function() {
		changeRequest("next");
	});
	$('#last').click(function() {
		changeRequest("last");
	});
	$('#addReq').click(function() {
		addRequest($('#requete').val());
	});
}); 