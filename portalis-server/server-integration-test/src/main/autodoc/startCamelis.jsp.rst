Démarrer un service.

Fonction Java
-------------

**Class** : ``fr.irisa.lis.portalis.camelis.http.CamelisHttp``

**Fonction static** : ``startCamelis``

.. code-block:: java

   public static StartReponse startCamelis(Session sess,
         String serviceName, String webApplicationDataPath, String logFile) {};
   public static StartReponse startCamelis(Session sess, String port,
         String serviceName, String webApplicationDataPath, String logFile) {};
   public static StartReponse startCamelis(Session sess, int port,
         String serviceName, String webApplicationDataPath, String logFile) {};
         
Paramètres
----------

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
session       Session avec son utilisateur et son service courant                  *yes*
port          port number where to start the service                               *no*      *Choosen by the launcher*
serviceName   service identification string (e.g. "planet:planet")                 *yes*
datadir       path to the directory holding context data on server                 *no*      ``/srv``
log           path to the log file                                                 *no*      ``/srv/portalis.log``
============  ===================================================================  ========  =========================

Paramêtres http
'''''''''''''''

Pour chaque requête Java une requête http est envoyée dans laquelle le paramêtre ``session`` est
remplacé automatiquement par les deux paramètre suivants (les autres paramêtres sont identiques) :

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
creator       email of the service creator (that's YOU)                            *yes*
key           current key of the creator. He has admin right for this service      *yes*
============  ===================================================================  ========  =========================

Résultat
--------

``StartReponse`` **extends** ``VoidReponse``

.. image:: ../images/StartReponse.gif


