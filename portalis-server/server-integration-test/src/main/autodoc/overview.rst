Présentation
============

Cette documentation présente l'interface d'utilisation d'un dépos de données **Camelis**

Dépendances d'un module client distant
--------------------------------------

Ci-dessous voici les dépendences du module ``androlis-exemple`` :

.. image:: ../images/androlis-example.png

Tout module client distant doit diposer de ces dépendences.

Structure d'une session
-----------------------

Ci-dessous voici la structure d'une session :

.. image:: ../images/session.gif


