Pour le service considéré considéré, change les droits d'un utilisateur. Au préalable
l'utilisateur doit avoir été enregistré par la requète ``registerKey``

Paramêtres
----------

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
adminKey      admin's key                                                          *yes*
userKey       user's key to which a role is assigned                               *yes*
role          the assigned role                                                    *yes*
============  ===================================================================  ========  =========================

Résultat
--------

``SetRoleReponse`` **extends**``CamelisUserReponse`` **extends** ``LisReponse`` **extends** ``VoidReponse``


.. image:: ../images/CamelisUserReponse.gif
