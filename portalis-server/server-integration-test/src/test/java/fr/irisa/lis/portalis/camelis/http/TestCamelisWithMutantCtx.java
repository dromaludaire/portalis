package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.AddAxiomReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelFeatureReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelObjectsReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCamelisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisWithMutantCtx {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestCamelisWithCtx.class.getName());

	private static ActiveLisService activeLisService;
	private static ActiveUser activeUser;
	private static CamelisHttp camelisHttp;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);
		Util.clientInitLogPropertiesFile();
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisWithMutantCtx ************");

		if (CargoLauncher.tomcatIsActif()) {
			CargoLauncher.stop();
		}
		CargoLauncher.start();

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> Login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("Le démarrage de Camelis n'a pas marché",
					startReponse != null);
			activeLisService = startReponse
					.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis"
					+ startReponse, activeLisService != null);

			camelisHttp = new CamelisHttp(activeUser, activeLisService);

			installFresfPlanetCtx();
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	private static void installFresfPlanetCtx()
			throws Exception {
		LOGGER.info("\nPortalisCtx ========================================> installFreshCtx <============================");
		// We need to initially import the context here
		// otherwise, the contextLoaded property won't be set
		// and most xml-lis-server operations will fail with a No_context
		// exception
		// thus making the tests fail miserably
		UtilTest.installResourcesForFreshPlanetsCtx();

		String ctxFile = "planets.ctx";

		LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
		ImportCtxReponse importReponse = camelisHttp.importCtx(ctxFile);
		assertTrue("Import failed", importReponse != null);
		assertTrue("importCtx problem: " + importReponse,
				importReponse.isOk());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		resetTestContext();
		UtilTest.killAllCamelisProcessAndClearUsers();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisWithMutantCtx **********");
	}

	public static void resetTestContext() throws Exception {
		LOGGER.info("\nPortalisCtx ========================================> resetTestContext <============================");
		ResetCamelisReponse resetCamelisReponse = camelisHttp.resetCamelis();
		assertNotNull("Reseting failed", resetCamelisReponse != null);
		assertTrue("Problem during resetCamelis, resetCamelisReponse is :\n" + resetCamelisReponse,
				resetCamelisReponse.isOk());

		UtilTest.installResourcesForFreshPlanetsCtx();

		LOGGER.info("\nPortalisCtx ========================================> importCtx planets/planets/planets.ctx <============================");
		String ctxFile = "planets.ctx";
		ImportCtxReponse importReponse = camelisHttp.importCtx(ctxFile);
		assertTrue("Ctx import failed", importReponse != null);
		assertTrue("problem during importCtx: " + importReponse,
				importReponse.isOk());
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
		resetTestContext();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testResetCamelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testResetCamelis ***********************************\n");
		try {
			ResetCamelisReponse resetReponse = camelisHttp.resetCamelis();
			assertNotNull(resetReponse);
			assertTrue("resetCamelis should not return an error: "
					+ resetReponse, resetReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> check empty extent <============================");
			ExtentReponse extReponse = camelisHttp.extent("all");
			LisExtent receivedExt = extReponse.getExtent();
			assertTrue("Extent should be empty: " + extReponse,
					receivedExt != null && receivedExt.card() == 0);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testDelObjects() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testDelObjects ***********************************\n");
		try {
			int[] oids = { 1, 6 };
			DelObjectsReponse reponse = camelisHttp.delObjects(oids);
			assertNotNull(reponse);
			assertTrue("delObjects should not return an error: " + reponse,
					reponse.isOk());
			// -------------- check extent -----------------
			LisExtent receivedExt = reponse.getExtent();
			assertNotNull(receivedExt);
			assertTrue("Object with oid == 1 not deleted",
					!receivedExt.contains(1));
			assertTrue("Object with oid == 6 not deleted",
					!receivedExt.contains(6));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testDelFeature() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testDelFeature ***********************************\n");
		try {
			String query = "all";
			String[] features = { "Distance", "Size" };
			DelFeatureReponse reponse = camelisHttp.delFeature(query, features);
			assertNotNull(reponse);
			assertTrue("delFeature should not return an error: " + reponse,
					reponse.isOk());
			// -------------- check increments -----------------
			LisIncrementSet receivedIncrs = reponse.getIncrements();
			assertNotNull(receivedIncrs);
			for (String feat : features) {
				assertTrue("Feature " + feat + " not deleted",
						!receivedIncrs.contains(feat));
			}
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testAddAxiom() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testAddAxiom ***********************************\n");
		try {
			AddAxiomReponse reponse = camelisHttp.addAxiom("satellite", "Misc");
			assertNotNull(reponse);
			assertTrue("addAxiom should not return an error: " + reponse,
					reponse.isOk());
			ZoomReponse zrep = camelisHttp.zoom("all", "all");
			LisIncrementSet incrs = zrep.getIncrements();
			assertTrue("Increment satellite not masked",
					!incrs.contains("satellite"));
			assertTrue("Increment Misc not listed", incrs.contains("Misc"));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


}
