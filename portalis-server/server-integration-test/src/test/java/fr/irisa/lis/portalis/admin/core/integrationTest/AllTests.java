package fr.irisa.lis.portalis.admin.core.integrationTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestCamelisLauncherHttpRequest0.class,
		TestCamelisLauncherHttpRequest1.class,
		TestCamelisLauncherHttpRequest2.class})
public class AllTests {

}
