package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoProprietes;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;

public class TestCamelisLauncherHttpRequest0 {
	
	

	private static final String tomcatContainerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String tomcatContainerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String tomcatContainer = tomcatContainerPath + tomcatContainerName;private static final Logger LOGGER = LoggerFactory.getLogger(TestCamelisLauncherHttpRequest0.class.getName());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);
		Util.clientInitLogPropertiesFile();
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisLauncherHttpRequest0 ************");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx ************************ @Before starting Cargo ****************************");
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @After ***********************************");
	}

	@Test
	public void testTomcatIsNotActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testTomcatIsNotActif ***********************************\n");
		try {
			assertTrue(tomcatContainer+" ne doit pas être actif", !CargoLauncher.tomcatIsActif());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartTomcat() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartTomcat ***********************************\n");
		try {
			CargoLauncher.start();
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());
			CargoLauncher.stop();

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


}
