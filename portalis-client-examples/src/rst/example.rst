.. -*- mode: rst -*-

=============================
Un exemple de client Portalis
=============================

:Author: Yves Bekkers <bekkers@irisa.fr>

Résumé
======

Portalis est une API pour construire des clients WEB pour les systèmes LIS. Cette API offre deux types de commandes :

- Des commandes d'administration de serveurs LIS ainsi que d'utilisateurs;
- Des commandes de navigation dans les systèmes LIS.

L'API Portalis est utilisable à deux niveaux :

- Au niveau bas, l'API consiste un ensemble de requètes HTTP appelables
  depuis n'importe quel environnement. Chaque requète HTTP retourne son résultat en une sérialisation XML.
- Au niveau haut, l'API Portalis offre un ensemble de librairies jar qui permettent de programmer
  des clients Portalis directement en Java. Dans ces librairies,
  chaque requète XML/HTTP du niveau bas est enveloppée dans une méthode Java et le résultat XML de la requète HTTP
  est desérialisé automatiquement en un objet Java.

Dans ce document nous donnons un exemple de client programmé en Java. Ce client consiste en une séquence d'appel à des fonctions Java
où on récupère les résutats en tant qu'objets Java.
Chaque fonction Java cache en son sein l'appel à une requète HTTP ainsi que la désèrialisation de son résultat.
Nous montrons cependant ici la requéte HTTP appelée ainsi que son résultat XML caché par la couche Java.

Dans ce document, un utilisateur désirant programmer un client dans un autre
langage que Java, par exemple en C, en php, etc. trouvera ici la séquence équivalente d'appels aux requètes HTTP
ainsi qu'une visualisation des résultats XML qu'il devra désérialiser.

Avertissement
-------------

Dans ce document la machine serveur utilisée s'appelle ``localhost``. Ce que le lecteur de ce document ne voit pas ici, c'est
qu'un serveur d'administration portalis a déjà été lancé sur le port 8080 de cette machine.
Le client s'adresse à lui pour lancer un nouveau serveur camelis. C'est Portalis qui choisit le port sur lequel le serveur Camelis sera lancé.
Comme le lecteur peut le constater dans les requètes HTTP visualisées, le serveur Camelis va être lancé sur le port 8060.

Le lecteur peut donc voir ici les deux sortes de requètes Portalis, celles qui concerne l'administration (port 8080) et
celles qui concernent le service (port 8060). Remarquer que l'on ne voit pas ici le cas général où un serveur d'administration Portalis gére
plusieurs serveurs Camelis, chacun sur son propre port.



Séquence de démarrage
=====================

On suppose ici que l'email de l'utilisateur ``PEGGY_EMAIL`` et son mot de passe ``PEGGY_PASSWORD`` sont déjà initialisés.
On suppose aussi que le nom du service qui va être lancé par cet utilisateur est déjà initialisé dans la
variable ``SERVICE_NAME``.

Dans un premier temps dans cette séquence de démarrage on initialiser les deux variables à fournir pour toute commande d'utilisation de service
Camelis : l'utilisateur (variable ``activeUser``), le service (variable ``activeLisService``).

La première commande ``AdminHttp.login(PEGGY_EMAIL, PEGGY_PASSWORD)`` permet à l'utilisateur de se connecter à l'application 
Portalis. L'objet ``ActiveUser activeUser`` récupéré à la sortie de cette commande représente dans toutes les autres commande
l'utilisateur qui vient de se connecter.

Par la seconde commande ``AdminHttp.startCamelis(activeUser, SERVICE_NAME);``
l'utilisateur qui vient de se connecter lance le nouveau service. A la sortie de cette seconde commade on récupère 
l'objet ``ActiveLisService activeLisService`` qui représente le service que l'on vient de lancer.

Ces deux objets sont maintenus dans un objet ``CamelisHttp`` par l'affectation :

::

 CamelisHttp camelisHttp = new CamelisHttp(activeUser, activeLisService);
 
Les deux commandes qui suivent ces deux première commandes pemettent permmettent de charger un context sur le nouveau service.
Sucessivement on charger sur le serveur le fichier ``.ctx`` représentant ce contexte. Puis on lancer
le nouveau serveur portalis sur ce fichier ``.ctx``. Ainsi le serveur Camelis est prêt à être utiliser pour effectuer des navigations.

Login
-----

Initialisation de la variable ``activeUser`` à la suite de la connection de l'utilisateur.
La commande utilisée est ``AdminHttp.login``.

Commande(s) en Java :

.. code-block:: java

 LoginReponse loginReponse = AdminHttp.login(PEGGY_EMAIL, PEGGY_PASSWORD);
 ActiveUser activeUser = loginReponse.getActiveUser();

Requète HTTP sous jascente :

::

 http://localhost:8080/portalis/login.jsp?email=peggy.cellier%40irisa.fr&password=peggy

Résultat XML sous jascent :

.. code-block:: xml

 <loginResponse>
   <actifUser userKey="7F17C0ED2D967CA74C789DB53FF7D21F">
      <standardUser email="peggy.cellier@irisa.fr" password="peggy" portalisAdminRole="ADMIN" 
            pseudo="cellier"/>
      <cookie>JSESSIONID=7F17C0ED2D967CA74C789DB53FF7D21F; Path=/portalis/; HttpOnly</cookie>
   </actifUser>
 </loginResponse>

Démarrer un serveur Camelis
---------------------------

Initialisation de la variable ``activeLisService`` grâce au lancement d'un nouveau serveur Camelis.
La commande utilisée est ``AdminHttp.startCamelis``.

Remarquer que pour l'instant, ce serveur n'a pas de contexte. Remarquez aussi l'initialisation,
in fine, de la variable ``camelisHttp``. C'est elle qui porte pour la suite l'état du programme client.
Elle contient l'identité de l'utilisateur et le service utilisé.


Commande(s) en Java :

.. code-block:: java

 StartReponse startReponse = AdminHttp.startCamelis(activeUser, SERVICE_NAME);
 ActiveLisService activeLisService = startReponse.getActiveLisService();
 CamelisHttp camelisHttp = new CamelisHttp(activeUser, activeLisService);
 
Requète HTTP sous jascente :

::

 http://localhost:8080/portalis/startCamelis.jsp?creator=peggy.cellier%40irisa.fr
     &key=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc
     &serviceName=linguistique%3Apatron&log=%2Fsrv%2Flogs%2FcamelisServer.log&datadir=%2Fsrv%2Fwebapps

Résultat XML sous jascent :

.. code-block:: xml

 <startResponse status="ok">
   <camelis activationDate="2013-11-18T09:17:58.660+01:00" creator="peggy.cellier@irisa.fr" 
         host="localhost" lastUpdate="none" nbObject="0" pid="6479" port="8060" 
         serviceId="localhost:8060::linguistique:patron"/>
 </startResponse>


Charger un contexte .ctx sur le serveur Portalis
------------------------------------------------

Ici on fait un upload d'un fichier CTX, sur le serveur supportant Portalis.

Requète HTTP sous jascente :

::

 http://localhost:8080/portalis/admin/patronUpload.jsp


Importer le contexte dans le serveur Camelis
--------------------------------------------

Ici on initialise le serveur Camelis, précédemment lancé, avec le contexte qui vient d'être chargé sur la machine serveur.
La commande utilisée est ``camelisHttp.importCtx``.

Commande(s) en Java :

.. code-block:: java

 ImportCtxReponse importReponse = camelisHttp.importCtx(exampleUploadedFileName);

Requète HTTP sous jascente :

::

 http://localhost:8060/importCtx?ctxfile=planets.ctx
      &userKey=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc

Résultat XML sous jascent :

.. code-block:: xml

 <importCtxResponse ctxfile="planets.ctx" status="ok"/>

Réactivation de la session sur le serveur d'administration de Portalis :

::

 http://localhost:8080/portalis/bip.jsp


Boucle de navigation sur le serveur Camelis
===========================================

Ici on donne un exemple de séquence de navigation sur le serveur Camelis. Cette séquence peut être répétée
autant de fois qu'on le désire. Ici le client accède directement le serveur Camelis,
le serveur d'administration Portalis n'est plus utilisé. Hors, ce dernier en tant que maître de l'application
gère le timeout des utilisateurs. Le client doit donc rafrairir périodiquement sa session sous peine de la voir disparaître
derrière son dos. C'est le rôle des requètes de réactivation "http://localhost:8080/portalis/bip.jsp" que l'on trouve dans ce document.

Initialisation de la boucle
---------------------------

Commande(s) en Java :

.. code-block:: java

 Property currentRequest = new Property("all");
 Property currentFeature = new Property("all");
 int currentPageNum = 0;
 int currentPageSize = DEFAULT_PAGE_SIZE;


Obtenir l'extension courante
----------------------------

Ici on demande à connaître la liste de tous les objets du context que l'on vient d'initialiser.
La commande utilisée est ``camelisHttp.extent``.

Commande(s) en Java :

.. code-block:: java

 ExtentReponse extentReponse = camelisHttp.extent(currentRequest.getName(), 
     currentPageNum, currentPageSize);
 int nbObjects = extentReponse.getNbObjects();
 LisExtent extent = extentReponse.getExtent();

Requète HTTP sous jascente :

::

 http://localhost:8060/extent
     ?userKey=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc
     &query=all&page=0&pageSize=30

Résultat XML sous jascent :

.. code-block:: xml

 <extentResponse lastUpdate="none" nbObjects="9" status="ok">
   <extent>
      <object name="earth" oid="1"/>
      <object name="jupiter" oid="2"/>
      <object name="mars" oid="3"/>
      <object name="mercury" oid="4"/>
      <object name="neptune" oid="5"/>
      <object name="pluto" oid="6"/>
      <object name="saturn" oid="7"/>
      <object name="uranus" oid="8"/>
      <object name="venus" oid="9"/>
   </extent>
 </extentResponse>

Réactivation de la session sur le serveur d'administration de Portalis :

::

 http://localhost:8080/portalis/bip.jsp

Obtenir les attributs courants
------------------------------

Ici on demande à connaître la liste des attributs de premier niveau pour le context courant que l'on vient d'initialiser.
La commande utilisée est ``camelisHttp.zoom``.

Commande(s) en Java :

.. code-block:: java

 ZoomReponse zoomReponse = camelisHttp.zoom(currentRequest.getName(), currentFeature.getName());
 LisIncrement[] increments = zoomReponse.getIncrements().toArray();

Requète HTTP sous jascente :

::


 http://localhost:8060/zoom
     ?userKey=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc
     &wq=all&feature=all&extent=false

Résultat XML sous jascent :

.. code-block:: xml

 <zoomResponse lastUpdate="2013-11-18T08:17:59.686+00:00" nbObjects="9" status="ok">
   <increments>
      <increment card="9" name="Distance"/>
      <increment card="9" name="Size"/>
      <increment card="8" name="satellite"/>
      <increment card="1" name="Basket"/>
   </increments>
 </zoomResponse>

Réactivation de la session sur le serveur d'administration de Portalis :

::

 http://localhost:8080/portalis/bip.jsp

Visiter l'attribut 'Size'
-------------------------

Dans ce contexte, on demande de visiter l'attribut ``Size``

Commande(s) en Java :

.. code-block:: java

 currentFeature = new Property("Size");
 ZoomReponse zoomReponse = camelisHttp.zoom(currentRequest.getName(), currentFeature.getName());
 LisIncrement[] increments = zoomReponse.getIncrements().toArray();

Requète HTTP sous jascente :

::

 http://localhost:8060/zoom
     ?userKey=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc
     &wq=all&feature=Size&extent=false

Résultat XML sous jascent :

.. code-block:: xml

 <zoomResponse lastUpdate="2013-11-18T08:17:59.686+00:00" nbObjects="4" status="ok">
   <increments>
      <increment card="5" name="Small"/>
      <increment card="4" name="Jovian"/>
   </increments>
 </zoomResponse>

Réactivation de la session sur le serveur d'administration de Portalis :

::

 http://localhost:8080/portalis/bip.jsp

Connaître les planetes de la même classe que Jupiter en taille
--------------------------------------------------------------

Ici on demande à connaître les planètes qui sont de la même sorte que Jupiter (``Property Jovian``).
La commande utilisée est ``camelisHttp.extent``.

Commande(s) en Java :

.. code-block:: java

 currentRequest = new Property("Jovian");
 extentReponse = camelisHttp.extent(currentRequest.getName(), currentPageNum, currentPageSize);
 nbObjects = extentReponse.getNbObjects();
 extent = extentReponse.getExtent();

Requète HTTP sous jascente :

::

 http://localhost:8060/extent
      ?userKey=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc
      &query=Jovian&page=0&pageSize=30

Résultat XML sous jascent :

.. code-block:: xml

 <extentResponse lastUpdate="none" nbObjects="4" status="ok">
   <extent>
      <object name="jupiter" oid="2"/>
      <object name="neptune" oid="5"/>
      <object name="saturn" oid="7"/>
      <object name="uranus" oid="8"/>
   </extent>
 </extentResponse>

Réactivation de la session sur le serveur d'administration de Portalis :

::

 http://localhost:8080/portalis/bip.jsp

Visiter l'attribut 'Jovian'
---------------------------

Dans ce contexte contenant quatre planètes, on demande de visiter l'attribut ``Jovian``

Commande(s) en Java :

.. code-block:: java

 currentFeature = new Property("Jovian");
 ZoomReponse zoomReponse = camelisHttp.zoom(currentRequest.getName(), 
       currentFeature.getName());
 LisIncrement[] increments = zoomReponse.getIncrements().toArray();

Requète HTTP sous jascente :

::

 http://localhost:8060/zoom
     ?userKey=1379d2616caa06e2156cebfb035a8c01c0d9cd9f56cd2495fcef098620b605cc
     &wq=Jovian&feature=Size&extent=false

Résultat XML sous jascent :

.. code-block:: xml

 <zoomResponse lastUpdate="2013-11-18T12:12:06.247+00:00" nbObjects="4" status="ok">
   <increments>
      <increment card="2" name="Big"/>
      <increment card="2" name="Medium"/>
   </increments>
 </zoomResponse>

Réactivation de la session sur le serveur d'administration de Portalis :

::

 http://localhost:8080/portalis/bip.jsp

Sortie
======


Stop Camelis
------------

Commande(s) en Java :

.. code-block:: java

 PingReponse pingReponse = AdminHttp.stopCamelis(activeUser, activeLisService);

Requète HTTP sous jascente :

::

 http://localhost:8080/portalis/stopCamelis.jsp?serviceId=localhost%3A8060%3A%3Alinguistique%3Apatron

Résultat XML sous jascent :

.. code-block:: xml

 <pingResponse status="ok"/>


Logout
------

Commande(s) en Java :

.. code-block:: java

 VoidReponse logoutReponse = AdminHttp.logout(activeUser);

Requète HTTP sous jascente :

::

 http://localhost:8080/portalis/logout.jsp

Résultat XML sous jascent :

.. code-block:: xml

 <voidResponse status="ok"/>

