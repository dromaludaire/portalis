package fr.irisa.lis.client.example;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

public class ThaborExtent extends AbstractClient {
	
	public static void main(String[] args) {
		
		initPortalisWithArgs(args);

	
		try {

			System.out.println("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = AdminHttp.login("bekkers@irisa.fr", "yves");
			assertTrue("la réponse doit être OK : "+loginReponse, loginReponse.getStatus().equals(XmlIdentifier.OK));
			ActiveUser activeUser = loginReponse.getActiveUser();

			System.out.println("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser, THABOR_ARBRES_THABOR_FULL_NAME);
			assertTrue("Le démarrage de Camelis n'a pas marché", startReponse!=null);
			assertTrue("la réponse doit être OK : "+startReponse, startReponse.getStatus().equals(XmlIdentifier.OK));
			ActiveLisService activeLisService = startReponse.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis : "+startReponse, activeLisService!=null);
			CamelisHttp camelisHttp = new CamelisHttp(activeUser, activeLisService);

			System.out.println("\nPortalisCtx ========================================> getActiveLisServicesOnPortalis <============================");
			PingReponse pingReponse = AdminHttp.getActiveLisServicesOnPortalis(activeUser);
			assertTrue("la réponse doit être OK : "+pingReponse, pingReponse.getStatus().equals(XmlIdentifier.OK));
			assertTrue("La réponse doit contenir le service actif : "+activeLisService+
					"\nreponse = "+pingReponse, pingReponse.contains(activeLisService));
			
			System.out.println("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "arbres_thabor.ctx";
			ImportCtxReponse importReponse = camelisHttp.importCtx(ctxFile);
			assertTrue("la réponse doit être OK : "+importReponse, importReponse.getStatus().equals(XmlIdentifier.OK));
			
			System.out.println("\nPortalisCtx ========================================> zoom <============================");
			ZoomReponse zoomReponse = camelisHttp.zoom("Magnolia", "gps ?");
			assertTrue("la réponse doit être OK : "+zoomReponse, zoomReponse.getStatus().equals(XmlIdentifier.OK));
			int nbMagnoliaWithGps = 49;
			assertTrue("la reponse doit contenir "+nbMagnoliaWithGps+ " Magnolia avec gps. La reponse est = "+zoomReponse, zoomReponse.getIncrements().getIncrements().size() == nbMagnoliaWithGps);
			
			System.out.println("\nPortalisCtx ========================================> extent <============================");
			ExtentReponse extentReponse = camelisHttp.extent();
			assertTrue("la réponse doit être OK : "+extentReponse, extentReponse.getStatus().equals(XmlIdentifier.OK));
			int nbArbresTotal = 1099;
			assertTrue("la reponse doit contenir "+nbArbresTotal+ " arbres. La reponse est = "+zoomReponse, extentReponse.getExtent().card() == nbArbresTotal);
			
			System.out.println("\nPortalisCtx ========================================> stopCamelis <============================");
			VoidReponse stopReponse = AdminHttp.stopCamelis(activeUser, activeLisService);
			assertTrue("la réponse doit être OK : "+stopReponse, stopReponse.getStatus().equals(XmlIdentifier.OK));

			System.out.println("\n====================================== AdminHttp.logout ====================================");
			VoidReponse pietLogoutReponse = AdminHttp.logout(activeUser);
			assertTrue("la réponse doit être OK : "+pietLogoutReponse, pietLogoutReponse.getStatus().equals(XmlIdentifier.OK));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}

	}
	
}
