package fr.irisa.lis.client.example;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class PatrimoineBreton extends AbstractClient {

	public static void main(String[] args) {

		initPortalisWithArgs(args);

		try {
			ClientConstants.setPrintHttpRequest(true);
			System.out.println("\n====================================== login ====================================");
			LoginReponse loginReponse = AdminHttp.login(BEKKERS_EMAIL,
					BEKKERS_PASSWORD);
			checkResult(loginReponse);
			ActiveUser activeUser = loginReponse.getActiveUser();

			System.out.println("\n====================================== getActiveLisServicesOnPortalis ====================================");
			PingReponse pingReponse = AdminHttp
					.getActiveLisServicesOnPortalis(activeUser);
			checkResult(pingReponse);

			System.out.println("\n====================================== StartCamelis patrimoine bretagne ====================================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					PATRIMOINE_BRETAGNE_FULL_NAME);
			checkResult(startReponse);
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();

//			System.out.println("\n====================================== importCtx  patrimoine/patrimoineBretagne/patrimoineBretagne.ctx ====================================");
//			String ctxFile0 = "patrimoine/patrimoineBretagne/patrimoineBretagne.ctx";
//			ImportCtxReponse importReponse = CamelisHttp.importCtx(
//					portalisSessionId, activeLisService, ctxFile0);
//			checkResult(importReponse);
			

			System.out.println("\n====================================== loadContext patrimoineBretagne ====================================");
			LoadContextReponse loadContextReponse =  new CamelisHttp(activeUser, activeLisService)
					.loadContext();
			checkResult(loadContextReponse);			
			
			System.out.println("\n====================================== ping ====================================");
			PingReponse pingReponse0 = CamelisHttp.ping(activeLisService);
			checkResult(pingReponse0);
			
			System.out.println("\n====================================== stopCamelis 2 ====================================");
			PingReponse pingReponse3 = AdminHttp.stopCamelis(activeUser, activeLisService);
			checkResult(pingReponse3);

			System.out.println("\n====================================== logout ====================================");
			VoidReponse logoutReponse = AdminHttp.logout(activeUser);
			checkResult(logoutReponse);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
