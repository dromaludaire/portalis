package fr.irisa.lis.client.example;

import java.util.List;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class Androlis extends AbstractClient {

	
	enum Load {
		NONE, STOP, LOAD, IMPORT;
	}

	enum Service {
		PLANETS_PLANETS("planets:planets", Load.NONE), PATRIMOINE_BRETAGNE(
				"patrimoine:patrimoineBretagne", Load.NONE), 
				PATRIMOINE_ARLES("patrimoine:patrimoineArles", Load.IMPORT), FORMAL_CONCEPT_NUMBER(
				"formalConcepts:number", Load.NONE), ILLUSTRES_BD(
				"illustres:bd", Load.NONE), ILLUSTRES_COMICS(
				"illustres:comics", Load.NONE), THABOR(
				"thabor:arbres_thabor", Load.NONE), BIBLIS("bibliography:biblis", Load.NONE);

		private String fullName;
		private Load load;

		private Service(String fullName, Load load) {
			this.fullName = fullName;
			this.load = load;
		}

		@Override
		public String toString() {
			return fullName;
		}

		public Load getLoad() {
			return this.load;
		}
	}

	static final String PROCESS_PATTERN = "^[\\w\\-]*\\s([\\d]+)\\s.*$";
	static final String PORT_PATTERN = "^.*\\sTCP\\s\\*:([\\d]+)\\s.*$";
	private static List<ActiveLisService> lesServives;

	/**
	 * @param args
	 */

		public static void main(String[] args) {

		initPortalisWithArgs(args);

		try {
			ClientConstants.setPrintHttpRequest(true);

			System.out.println("\n====================================== login ====================================");
			LoginReponse loginReponse = AdminHttp.login(BEKKERS_EMAIL, BEKKERS_PASSWORD);
			checkResult(loginReponse);
			ActiveUser activeUser = loginReponse.getActiveUser();

			System.out.println("\n====================================== getSite ====================================");
			SiteReponse siteReponse = AdminHttp.getSite(activeUser);
			checkResult(siteReponse);

			System.out.println("\n====================================== getActiveLisServicesOnPortalis ====================================");
			PingReponse pingReponse01 = AdminHttp.getActiveLisServicesOnPortalis(activeUser);
			checkResult(pingReponse01);

			// ==================================================================================
			for (Service service : Service.values()) {
				execCmd(activeUser, service);
			}

			System.out.println("\n====================================== getActiveLisServicesOnPortalis ====================================");
			PingReponse pingReponse03 = AdminHttp.getActiveLisServicesOnPortalis(activeUser);
			checkResult(pingReponse03);


			System.out.println("\n====================================== logout ====================================");
			VoidReponse logoutReponse = AdminHttp.logout(activeUser);
			checkResult(logoutReponse);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		private static void execCmd(ActiveUser activeUser, Service service)
				throws PortalisException {
			String fullName = service.toString();

			switch (service.getLoad()) {
			case LOAD:
			case IMPORT:

				System.out
						.println("\n====================================== StartCamelis "
								+ fullName
								+ " ====================================");
				StartReponse startReponse = AdminHttp.startCamelis(activeUser,
						fullName);
				checkResult(startReponse);
				ActiveLisService activeService = startReponse.getActiveLisService();
				CamelisHttp camelisHttp = new CamelisHttp(activeUser, activeService);

				if (service.load == Load.LOAD) {

					System.out
							.println("\n====================================== loadContext "
									+ fullName
									+ " ====================================");
					LoadContextReponse loadContextReponse = camelisHttp
							.loadContext();
					checkResult(loadContextReponse);
				} else {
					ImportCtxReponse importReponse = camelisHttp.importCtx();
					checkResult(importReponse);
				}

				break;
			case STOP:
				String serviceFullName = service.toString();
				for (ActiveLisService activeLisService : lesServives) {
					if (activeLisService.getFullName().equals(serviceFullName)) {
						System.out
						.println("\n====================================== stopCamelis 1 ====================================");
				PingReponse pingReponse = AdminHttp.stopCamelis(activeUser,
						activeLisService);
				checkResult(pingReponse);
						break;
					}				
				}
				break;
			case NONE:
				break;
			}
		}

}
