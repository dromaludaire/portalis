package fr.irisa.lis.portalis.commons.geoloc;

import java.util.ArrayList;
import java.util.List;

import org.jkee.gtree.Tree;

public class TaxonomyTreeBuilder {

	public static interface Funnel {
		public List<String> getPath(String value);
	}

	private final Funnel funnel;

	public TaxonomyTreeBuilder(Funnel funnel) {
		this.funnel = funnel;
	}

	public Tree<String> build(Iterable<String> values, String rootValue) {
		/*
		 * 1. split by words : append nodes
		 */

		Tree<String> root = new Tree<String>(rootValue,
				new ArrayList<Tree<String>>());
		for (String value : values) {
			List<String> path = funnel.getPath(value);
			Tree<String> current = root;

			for (String name : path) {
				if (name.length() != 0) {
					List<Tree<String>> children = current.getChildren();
					boolean found = false;
					for (Tree<String> child : children) {
						if (child.getValue().equals(name)) {
							current = child;
							found = true;
							break;
						}
					}
					if (!found) {
						Tree<String> newTree = new Tree<String>(name,
								new ArrayList<Tree<String>>());
						current.addChild(newTree);
						current = newTree;
					}
				}
			}
		}

		Tree<String> result = fold(root.getValue(), root);
		System.out.println(result.toStringTree());
		return result;

	}

	public Tree<String> fold(String rootValue, Tree<String> root) {
		Tree<String> result = new Tree<String>(rootValue,
				new ArrayList<Tree<String>>());
		List<Tree<String>> children = root.getChildren();
		if (children == null || children.size() == 0) {
			return result;
		} else if (children.size() == 1) {
			Tree<String> singletonChild = children.get(0);
			return fold(String.format("%s %s", rootValue,
					singletonChild.getValue()), singletonChild);
		} else {
			for (Tree<String> newChild : children) {
				result.addChild(fold(newChild.getValue(), newChild));
			}
			return result;
		}

	}

}
