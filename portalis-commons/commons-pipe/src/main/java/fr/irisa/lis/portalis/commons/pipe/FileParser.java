package fr.irisa.lis.portalis.commons.pipe;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.Charset;
 
public abstract class FileParser<T> extends ICharacterSetable<BufferedReader> {
	private static final Charset DEFAULT_CHARACTER_SET = Charset.forName("UTF-8");
	protected Charset characterSet = DEFAULT_CHARACTER_SET;

	public FileParser(FileInputStream inputStream) {
		super(new BufferedReader(new InputStreamReader(inputStream, DEFAULT_CHARACTER_SET)));
	}
 
	public FileParser(FileInputStream inputStream, Charset charset) {
		super(new BufferedReader(new InputStreamReader(inputStream, charset)));
		this.characterSet = charset;
	}
	
	public FileParser(String fileName) throws FileNotFoundException {
		super(new BufferedReader(new InputStreamReader(new FileInputStream(fileName), DEFAULT_CHARACTER_SET)));
	}

	public FileParser(String fileName, Charset charset) throws FileNotFoundException {
		super(new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charset)));
	}

	public FileParser(URI uri) throws FileNotFoundException {
		super(new BufferedReader(new InputStreamReader(new FileInputStream(new File(uri)), DEFAULT_CHARACTER_SET)));
	}

	public FileParser(URI uri, Charset charset) throws FileNotFoundException {
		super(new BufferedReader(new InputStreamReader(new FileInputStream(new File(uri)), charset)));
	}



	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#getCharacterSet()
	 */
	@Override
	public Charset getCharacterSet() {
		return characterSet;
	}
 
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#setCharacterSet(java.nio.charset.Charset)
	 */
	@Override
	public void setCharacterSet(Charset characterSet) {
		this.characterSet = characterSet;
	}
	
	public void close() throws IOException {
		reader.close();
	}
	
	public abstract T parse() throws IOException;

}