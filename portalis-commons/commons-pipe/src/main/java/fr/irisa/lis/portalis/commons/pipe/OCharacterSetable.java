package fr.irisa.lis.portalis.commons.pipe;

import java.io.IOException;
import java.io.Writer;

public abstract class OCharacterSetable<T extends Writer> implements CharacterSetable {
	
	protected T outputStream;

	public OCharacterSetable(T inputStream) {
		this.outputStream = inputStream;;
	}

	public T getInputStream() {
		return outputStream;
	}
	
	public void close() throws IOException {
		outputStream.close();
	}

	public void write(String s) throws IOException {
		outputStream.write(s);
	}

}
