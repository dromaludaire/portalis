package fr.irisa.lis.jquery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.DOMWriter;

public class TreetableNode {
	private String label;
	private ArrayList<TreetableNode> children = new ArrayList<TreetableNode>();
	private Map<String, String> property = new HashMap<String, String>();
	private int colspan = 1;
	private String[] noProps = {};

	public TreetableNode() {
	}

	public TreetableNode(String label) {
		this.label = label;
	}

	public TreetableNode(String label, Collection<TreetableNode> nodes) {
		this.label = label;
		children = new ArrayList<TreetableNode>(nodes);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ArrayList<TreetableNode> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<TreetableNode> children) {
		this.children = children;
	}

	public void addChild(TreetableNode child) {
		this.children.add(child);
	}

	public Map<String, String> getProperty() {
		return property;
	}

	public void setProperty(Map<String, String> property) {
		this.property = property;
	}

	public void addProperty(String name, String value) {
		this.property.put(name, value);
	}

	public Element toHtmlTable(String nodeId, String htmlTableId, String[] props) {
		Element table = DOMWriter.createRootElement("table");
		table.setAttribute("id", htmlTableId);
		// caption
		Element caption = DOMWriter.createAddChild(table, "caption");
		Element a1 = DOMWriter.createAddChild(caption, "a");
		a1.setAttribute("onclick", "jQuery('#" + htmlTableId
				+ "').treetable('expandAll'); return false;");
		a1.setAttribute("href", "#");
		a1.setTextContent("Expand all");
		Element a2 = DOMWriter.createAddChild(caption, "a");
		a2.setAttribute("onclick", "jQuery('#" + htmlTableId
				+ "').treetable('collapseAll'); return false;");
		a2.setAttribute("href", "#");
		a2.setTextContent("Collapse all");
		// head
		Element head = DOMWriter.createAddChild(table, "head");
		Element trHead = DOMWriter.createAddChild(head, "tr");
		// headers
		DOMWriter.createAddChild(trHead, "th").setTextContent("Label");
		for (String prop : props) {
			DOMWriter.createAddChild(trHead, "th").setTextContent(prop);
		}
		// table body
		Element tableBody = DOMWriter.createAddChild(table, "body");
		ArrayList<TreetableNode> lesEnfants = children;
		nodesToHtml(nodeId, tableBody, lesEnfants, props, true);
		return table;
	}

	private void nodesToHtml(String nodeId, Element tableBody,
			ArrayList<TreetableNode> lesEnfants, String[] props, boolean display) {
		int i = 1;
		for (TreetableNode child : lesEnfants) {
			String childId = (nodeId.length() == 0 ? "" : nodeId + "-") + i++;
			Element trChild = DOMWriter.createAddChild(tableBody, "tr");
			if (!display) {
				trChild.setAttribute("style", "display: none;");
			}
			trChild.setAttribute("data-tt-id", childId);
			trChild.setAttribute("class",
					(child.getChildren().size() == 0 ? "leaf " : "branch ")
							+ "collapsed");
			if (nodeId.length() > 0) {
				trChild.setAttribute("data-tt-parent-id", nodeId);
			}
			// table values
			Element td = null;
			if (child.getChildren().size() == 0) {
				td = DOMWriter.createAddChild(trChild, "td");
				setContent(td, child.getLabel());
			} else {
				td = DOMWriter.createAddChild(trChild, "td");
				Element span = DOMWriter.createAddChild(td, "span");
				span.setAttribute("class", "folder");
				setContent(span, child.getLabel());
			}
			if (child.getColspan()>1) {
				td.setAttribute("colspan", ""+child.getColspan());
			}
			for (String prop : props) {
				if (!child.isNoProp(prop)) {
					String value = child.getProperty().get(prop);
					if (value == null)
						value = "--";
					DOMWriter.createAddChild(trChild, "td").setTextContent(value);
				}
			}
			// descendants
			nodesToHtml(childId, tableBody, child.getChildren(), props, false);
		}
	}

	private void setContent(Element elem, String label2) {
		String inputLabel = label2.trim();
		if (inputLabel.startsWith("<") && inputLabel.endsWith(">")) {
			Element labelElem = DOMUtil.parseXMLString(inputLabel);
			elem.getOwnerDocument().adoptNode(labelElem);
			elem.appendChild(labelElem);
		} else {
			elem.setTextContent(label2);
		}
		
	}

	public static void main(String[] args) {
		TreetableNode root = new TreetableNode("root");
		TreetableNode child1 = new TreetableNode("child1");
		child1.addProperty("prop", "val1");
		root.addChild(child1);
		TreetableNode child2 = new TreetableNode("child2");
		child2.addProperty("prop", "val2");
		root.addChild(child2);
		TreetableNode child3 = new TreetableNode("child3");
		child3.addProperty("prop", "val3");
		child3.setColspan(2);
		String[] noProps = {"prop"};
		child3.setColspan(2);
		child3.setNoProps(noProps);
		child2.addChild(child3);
		String[] props = { "prop" };
		System.out.println(DOMUtil.prettyXmlString(root.toHtmlTable("",
				"example", props)));

	}

	public int getColspan() {
		return colspan;
	}

	public void setColspan(int colspan) {
		this.colspan = colspan;
	}

	public String[] getNoProps() {
		return noProps;
	}

	public void setNoProps(String[] noProps) {
		this.noProps = noProps;
	}

	public boolean isNoProp(String prop) {
		for (String property : noProps) {
			if (prop.equals(property))
				return true;
		}
		return false;
	}

}
