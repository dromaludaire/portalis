package fr.irisa.lis.portalis.commons.rdf;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import fr.irisa.lis.portalis.commons.pipe.FileParser;
import fr.irisa.lis.portalis.commons.pipe.StringOutputStream;

public class RdfUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(RdfUtil.class
			.getName());

	public static String execSparqlQuery(InputStream in, Query query)
			throws IOException {

		// Create an empty in-memory model and populate it from the graph
		Model model = ModelFactory.createMemModelMaker().createDefaultModel();
		model.read(in, null); // null base URI, since model URIs are absolute
		in.close();

		// write it to log
		LOGGER.debug(modelToString(model, "N3"));

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet results = qe.execSelect();

		String text = ResultSetFormatter.asText(results);

		// Important - free up resources used running the query
		qe.close();
		return text;
	}

	public static void modelToFile(FileParser<Model> in, FileOutputStream out) throws IOException {
		Model model = in.parse();
		in.close();
		model.write(out);
	}
	
	public static void modelToFile(Model model, FileOutputStream out) throws IOException {
		model.write(out);
	}

	public static String modelToString(Model model) {
		return modelToString(model, null);
	}

	public static String modelToString(Model model, String lang) {
		return modelToString(model, lang, null);
	}

	public static String modelToString(FileParser<Model> in) throws IOException {
		Model model = in.parse(); // null base URI, since model URIs are absolute
		in.close();
		return modelToString(model, null);
	}

	public static String modelToString(FileParser<Model> in, String lang) throws IOException {
		Model model = in.parse(); // null base URI, since model URIs are absolute
		in.close();
		return modelToString(model, lang, null);
	}

	public static String modelToString(FileParser<Model> in, String lang, String base) throws IOException {
		Model model = in.parse(); // null base URI, since model URIs are absolute
		in.close();
		StringOutputStream out = new StringOutputStream();
		model.write(out, lang, base);
		return out.toString();
	}
	
	public static String modelToString(Model model, String lang, String base) {
		StringOutputStream out = new StringOutputStream();
		model.write(out, lang, base);
		return out.toString();
	}
	
	public static void main (String[] args) throws IOException {
		InputStream in = new FileInputStream("/local/bekkers/workspace/xturtle.topo/topo.owl");
		Model model = ModelFactory.createMemModelMaker()
				.createDefaultModel();
		model.read(in, null); // null base URI, since model URIs are
								// absolute
		in.close();
		
        FileOutputStream fos = new FileOutputStream("/local/bekkers/workspace/xturtle.topo/topo.ttl");
        Writer out = new OutputStreamWriter(fos, "UTF-8");
		model.write(out, "N3");

	}

}
