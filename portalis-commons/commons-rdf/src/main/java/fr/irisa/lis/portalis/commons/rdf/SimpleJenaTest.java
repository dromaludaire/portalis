package fr.irisa.lis.portalis.commons.rdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDBFactory;

public class SimpleJenaTest {
	
	static final String TDB_DIRECTORY = "/srv/TDB/portalis" ;
	
	static final String NS_PORTALIS = "http://portalis.lis.irisa.fr/";
	static final String NS_PORTALIS_SITE = NS_PORTALIS+"site/";
	static final String NS_PORTALIS_APPLICATION = NS_PORTALIS_SITE+"application/";
	static final String NS_PORTALIS_SERVICE = NS_PORTALIS_SITE+"service/";
	static final String NS_PORTALIS_USER = NS_PORTALIS_SITE+"service/";

	static final String NS_PRED = "http://rdf.insee.fr/def/nom";

    public static void main(String[] args) throws FileNotFoundException {

        SimpleJenaTest jenaTest = new SimpleJenaTest();

//        Model model = jenaTest.helloWorld();        
        Model model = jenaTest.connectTDB(TDB_DIRECTORY);        

       	model.write(System.out, "Turtle");

    }
    
    public Model helloWorld() {
        //Creates a model
        Model model = ModelFactory.createDefaultModel();
        // test namespace
     	String NS = "http://bekkers.irisa.fr/test/";
    	Resource r = model.createResource(NS+"r");
    	Property p = model.createProperty(NS+"p");
    	
    	r.addProperty(p, "hello world", XSDDatatype.XSDstring);
    	
    	return model;
    }
    
    public Model connectTDB(String dirPath) {
    	 // Make a TDB-backed dataset
    	  Dataset dataset = TDBFactory.createDataset(dirPath) ;
 
    	  dataset.begin(ReadWrite.READ) ;
    	  // Get model inside the transaction
    	  Model model = dataset.getDefaultModel() ;
    	  dataset.end() ;
    	  
    	  return model;
    }

    public Model listTriples() throws FileNotFoundException {
        //Creates a model
        Model model = ModelFactory.createDefaultModel();
        //Loads the triple files to the model
        File dir = new File(".");

        File[] fileList = dir.listFiles();

            for (File f : fileList) {
                InputStream in = null;

                in = new FileInputStream(f);                                                         

                if(in != null)
                        model.read(in, null, "TURTLE");
            }

        //Create the SPARQL Query 
        String sparqlQueryString =
                        "PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
                        "PREFIX ifgi: <http://ifgi.uni-muenster.de/> " +
                        " " +
                        "SELECT ?friends WHERE { " +
                        "       ifgi:jones foaf:knows ?friends . " +
                        "}";

        Query query = QueryFactory.create(sparqlQueryString);
        ARQ.getContext().setTrue(ARQ.useSAX);        

        //Executing the created query and stores its results in a ResultSet
        QueryExecution qexec = QueryExecutionFactory.create(query, model);
        ResultSet results = qexec.execSelect();

        //Iterating the ResultSet to get all its elements 
        while (results.hasNext()) {

            QuerySolution soln = results.nextSolution();   
            //Displaying on the screen the value of the variable
            //used in the SPARQL Query.
            System.out.println(soln.get("friends"));
        }
        qexec.close();
        
    	return model;
    }
}