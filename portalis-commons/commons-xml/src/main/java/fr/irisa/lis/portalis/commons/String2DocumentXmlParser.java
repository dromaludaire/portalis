package fr.irisa.lis.portalis.commons;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.tools.ant.util.ReaderInputStream;
import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.pipe.StringParser;

public class String2DocumentXmlParser extends StringParser<Document> {

	public String2DocumentXmlParser(String s) {
		super(s);
	}
 
	public String2DocumentXmlParser(String s, Charset charset) {
		super(s, charset);
	}


	@Override
	public Document parse() throws IOException {
		Document doc = null;
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new ReaderInputStream(this.reader));
		} catch (Exception e) {
			throw new IOException(e);
		}
		return doc;
	}

}
