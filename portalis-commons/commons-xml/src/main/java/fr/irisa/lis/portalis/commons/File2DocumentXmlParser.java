package fr.irisa.lis.portalis.commons;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.tools.ant.util.ReaderInputStream;
import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.pipe.FileParser;

public class File2DocumentXmlParser extends FileParser<Document> {

	public File2DocumentXmlParser(FileInputStream inputStream) {
		super(inputStream);
		// TODO Auto-generated constructor stub
	}

	public File2DocumentXmlParser(FileInputStream inputStream, Charset charset) {
		super(inputStream, charset);
		// TODO Auto-generated constructor stub
	}

	public File2DocumentXmlParser(String fileName) throws FileNotFoundException {
		super(fileName);
	}

	public File2DocumentXmlParser(String fileName, Charset charset) throws FileNotFoundException {
		super(fileName, charset);
	}

	public File2DocumentXmlParser(URI uri) throws FileNotFoundException {
		super(uri);
	}

	public File2DocumentXmlParser(URI uri, Charset charset) throws FileNotFoundException {
		super(uri, charset);
	}

	@Override
	public Document parse() throws IOException {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			return db.parse(new ReaderInputStream(this.reader));
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

}
