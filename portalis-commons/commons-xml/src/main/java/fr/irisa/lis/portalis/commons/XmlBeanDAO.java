package fr.irisa.lis.portalis.commons;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.dom.DOMResult;

import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class XmlBeanDAO<O> {

	private boolean loaded = false;
	protected O beanValue;
	
	@SuppressWarnings("unchecked")
	public XmlBeanDAO(O bean) {
		beanValue = bean;
	}

	@SuppressWarnings("unchecked")
	public O loadBeanFromFile(File file) throws FileNotFoundException, JAXBException {
		if (!loaded) {
			loaded = true;
			JAXBContext jaxbContext = JAXBContext.newInstance(beanValue.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			beanValue = (O) jaxbUnmarshaller.unmarshal(file);
		}
		return beanValue;
	}

	@SuppressWarnings("unchecked")
	public O loadBeanFromDom(Document doc) throws JAXBException  {
		if (!loaded) {
			loaded = true;
			JAXBContext jaxbContext = JAXBContext.newInstance(beanValue.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			beanValue = (O) jaxbUnmarshaller.unmarshal(doc);
		}
		return beanValue;
	}

	public void storeBeanToFile(File file) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(beanValue.getClass());
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING,
				CommonsUtil.DEFAULT_ENCODING);

		jaxbMarshaller.marshal(beanValue, file);

	}

	public Document storeBeanToDom() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(beanValue.getClass());
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING,
				CommonsUtil.DEFAULT_ENCODING);

		DOMResult result = new DOMResult();
		jaxbMarshaller.marshal(beanValue, result);
		return result.getNode().getOwnerDocument();
	}

	public O getBeanValue() throws Exception {
		return beanValue;
	}

}
