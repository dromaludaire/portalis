.. -*- mode: rst -*-

==========================
importer des documents csv
==========================

:Author: Yves Bekkers <bekkers@irisa.fr>

.. _`csv wikipedia`: http://fr.wikipedia.org/wiki/Comma-separated_values

Présentation
============

Voici la présentation qu'en fait `csv wikipedia`_ :

``Comma-separated values``, connu sous le sigle ``CSV``, est un format informatique ouvert représentant des données tabulaires sous forme de valeurs séparées 
par des virgules. Ce format n'a jamais vraiment fait l'objet d'une spécification formelle. Toutefois, la RFC 4180 décrit la forme la plus courante et établit
son type MIME « text/csv », enregistré auprès de l'IANA. Un fichier CSV est un fichier texte, par opposition aux formats dits « binaires ».
Chaque ligne du texte correspond à une ligne du tableau et les virgules correspondent aux séparations entre les colonnes. 
Les portions de texte séparées par une virgule correspondent ainsi aux contenus des cellules du tableau.
Une ligne est une suite ordonnée de caractères terminée par un caractère de fin de ligne (line break – CRLF), la dernière ligne pouvant en être exemptée.
Par exemple :

::

  Sexe,Prénom,Année de naissance
  M,Alphonse,1932
  F,Béatrice,1964
  F,Charlotte,1988

représente les données suivantes :

====  ========= ==================
Sexe  Prénom    Année de naissance
====  ========= ==================
M     Alphonse  1932
F     Béatrice  1964
F     Charlotte 1988
====  ========= ==================

Variantes françaises
--------------------

Le fait que les fichiers CSV soient essentiellement utilisés autour du logiciel Microsoft Excel, et que les séparateurs ne soient pas standardisés
(virgules, points-virgules sous certaines localisations dont la française, etc.) rend ce format peu pratique pour une utilisation autre que des
échanges de données ponctuels. Ce format est toutefois assez populaire parce qu'il semble relativement facile à générer.
Les champs texte peuvent également être délimités par des guillemets. Lorsqu'un champ contient lui-même des guillemets,
ils sont doublés afin de ne pas être considérés comme début ou fin du champ. Si un champ contient un signe utilisé comme séparateur
(virgule, point-virgule, etc.), les guillemets sont obligatoires afin que ce signe ne soit pas confondu avec un séparateur.
Exemple, avec des champs séparés par des points-virgules et des guillemets :

::

  Nom;Prénom;Adresse;…
  Robert;Dupont;rue du Verger, 12;…
  "Michel";"Durand";" av. de la Ferme, 89 ";…
  "Michel ""Michele""";"Durand";" av. de la Ferme, 89";…
  "Michel;Michele";"Durand";"av. de la Ferme, 89";…

Ce texte représente les données suivantes :

================= ======== ==================== ===
Prénom            Nom      Adresse              …
================= ======== ==================== ===
Robert            Dupont   rue du Verger, 12    …
Michel            Durand   av. de la Ferme, 89  …
Michel "Michele"  Durand   av. de la Ferme, 89  …
Michel;Michele    Durand   av. de la Ferme, 89  …
================= ======== ==================== ===

Utilisation de la librairie fr.irisa.lis.portalis.commons.csv.CsvReader
=======================================================================

Cette librairie permet de lire un ficher .csv et de le traviure en du XML. La librairie peut être uilisée de deux manières :

- en ligne de commande
- en tant qu'API java.

Utilisation en ligne de commande
--------------------------------

Le seul paramêtre obligatoire de la ligne de commande est le nom du fichier .csv en entrée. les autres paramêtres sont optionnels.
Leur ordre d'apparition sur la ligne de commande est le suivant :

::

  cvsReader csvFile [ouputFile [rowName [csvSplitBy [headersPos [longHeadersPos [Encoding]]]]]]
  
Leur signification et leurs valeurs par défauts sont :

===================== ======================================================================== =====================================================
Nom du paramêtre      Description                                                              Valeur par défaut
===================== ======================================================================== =====================================================
csvFile               Nom du fichier csv d'entrée                                              Paramètre obligatoire
ouputFile             Nom du fichier de sortie                                                 'csvFile'.xml
rowName               Nom de l'élément xml pour les lignes                                     'row', par défaut le nom de l'élément  racine du 
                                                                                               document est 'rows'
csvSplitBy            Caractère séparateur                                                     ','
headersPos            N° de la ligne où se trouvent les entêtes                                '0'
longHeadersPos        N° de la ligne où se trouvent les libellés complêts des pour les entêtes '-1' Signifie qu'il n'y a pas de libellés complêts
Encoding              Encodage du fichier d'entrée                                             'UTF8'
===================== ======================================================================== =====================================================

Voici le message que renvoie le programme en cas de mauvais paramètrage de la ligne de commande :

::

  cvsReader csvFile [
    ouputFile (default is 'csvFile.xml') [
      rowName (default is 'row') [
        csvSplitBy (default is ';') [
          headersPos (default is '0') [
            longHeadersPos (default is '-1', which means 'none') [
              Encoding (default is 'UTF8') 
            ]
          ]
        ]
      ]
    ]
  ]
  
Utilisation en tant qu'api Java
-------------------------------

Voici la description UML de la classe CsvReader

.. image:: csvReader.jpg
  
