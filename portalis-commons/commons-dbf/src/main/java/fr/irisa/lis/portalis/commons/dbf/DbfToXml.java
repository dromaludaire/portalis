package fr.irisa.lis.portalis.commons.dbf;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.linuxense.javadbf.DBFReader;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class DbfToXml {

	private static String rowName = "row";
	private static String rootName = rowName+"s";
	protected String DEFAULT_ENCODING = CommonsUtil.DEFAULT_ENCODING;


	private String[] headers;

	private static String cvsFile = "/local/bekkers/portalis/portalis-commons/commons-dbf/test/reg2013.dbf";
	private static String outputFileName = "/local/bekkers/portalis/portalis-commons/commons-dbf/test/reg2013.rows.xml";

	public static void main(String[] args) throws IOException,
			TransformerException {

		DbfToXml dbfToXml = new DbfToXml();

		if (args.length == 3) {
			cvsFile = args[0];
			outputFileName = args[1];
			rowName = args[2];
		}
		System.out.println(dbfToXml.listHeaders(cvsFile));
		Document doc = dbfToXml.readFile(cvsFile);
		DOMUtil.writeFile(doc, outputFileName);

	}

	public void setHeaders(DBFReader reader) throws IOException {
		int numberOfFields = reader.getFieldCount();
		headers = new String[numberOfFields];
		for (int i = 0; i < numberOfFields; i++) {
			headers[i] = reader.getField(i).getName();
		}
	}

	public String listHeaders(String fileName) throws IOException {
		InputStream inputStream = new FileInputStream(fileName); // take dbf
		DBFReader reader = new DBFReader(inputStream);
		reader.setCharactersetName(DEFAULT_ENCODING);
		StringBuffer buff = new StringBuffer();
		setHeaders(reader);
		for (String header : headers) {
			buff.append(header).append("\n");
		}
		return buff.toString();
	}

	public Document readFile(String fileName) {

		Document doc = DOMUtil.createDocument();
		try {
			InputStream inputStream = new FileInputStream(fileName); // take dbf
			DBFReader reader = new DBFReader(inputStream);
			reader.setCharactersetName(DEFAULT_ENCODING);
			Element root = doc.createElement(rootName);
			doc.appendChild(root);
			// Now, lets us start reading the rows
			//
			Object[] rowObjects;

			while ((rowObjects = reader.nextRecord()) != null) {

				Element rowElem = doc.createElement(rowName);
				root.appendChild(rowElem);
				for (int i = 0; i < rowObjects.length; i++) {
					Element tabElem = doc.createElement(headers[i]);
					rowElem.appendChild(tabElem);
					System.out.println(rowObjects[i].toString()+ ":"+rowObjects[i].getClass().getName());
					tabElem.setTextContent(rowObjects[i].toString());
				}
			}
		} catch (Exception e) {

		}

		return doc;
	}
}
