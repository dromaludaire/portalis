/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.commons.types;

/**
 * Enumeration PrenomType.
 * 
 * @version $Revision$ $Date$
 */
public enum PrenomType {


      //------------------/
     //- Enum Constants -/
    //------------------/

    /**
     * Constant YVES
     */
    YVES("Yves"),
    /**
     * Constant JACQUES
     */
    JACQUES("Jacques"),
    /**
     * Constant MARIE
     */
    MARIE("Marie");

      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field value.
     */
    private final java.lang.String value;

    /**
     * Field enumConstants.
     */
    private static final java.util.Map<java.lang.String, PrenomType> enumConstants = new java.util.HashMap<java.lang.String, PrenomType>();


    static {
        for (PrenomType c: PrenomType.values()) {
            PrenomType.enumConstants.put(c.value, c);
        }

    };


      //----------------/
     //- Constructors -/
    //----------------/

    private PrenomType(final java.lang.String value) {
        this.value = value;
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method fromValue.
     * 
     * @param value
     * @return the constant for this value
     */
    public static PrenomType fromValue(
            final java.lang.String value) {
        PrenomType c = PrenomType.enumConstants.get(value);
        if (c != null) {
            return c;
        }
        throw new IllegalArgumentException(value);
    }

    /**
     * 
     * 
     * @param value
     */
    public void setValue(
            final java.lang.String value) {
    }

    /**
     * Method toString.
     * 
     * @return the value of this constant
     */
    public java.lang.String toString(
    ) {
        return this.value;
    }

    /**
     * Method value.
     * 
     * @return the value of this constant
     */
    public java.lang.String value(
    ) {
        return this.value;
    }

}
