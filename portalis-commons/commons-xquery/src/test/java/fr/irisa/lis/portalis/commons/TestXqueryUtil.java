package fr.irisa.lis.portalis.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TestXqueryUtil {
	

	private static final String input = "personne.xml";
	private static final String xquery = "personne.xqy";

	@Test
	public void testToDomEval() {
		try {
			XdmItem xdmItem = new XdmAtomicValue("Dupond", ItemType.STRING);
			XdmItem[] params = {xdmItem};
			
			Document doc = DOMUtil.readFile(input);
			
			Document result = new XqueryUtil().toDomEval(doc, "functionPersonne2.xqy", "personne", params);
			assertNotNull(result);
			Element root = result.getDocumentElement();
			assertEquals("personne", root.getNodeName());
			Element prenom = (Element) root.getElementsByTagName("prenom").item(0);
			assertEquals("prenom", prenom.getNodeName());
			assertEquals("Jacques", prenom.getTextContent());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testTransform() {
		try {
			XqueryUtil xqueryUtil = new XqueryUtil();

			Document doc = DOMUtil.readFile(input);
			
			XdmNode xdmNode = xqueryUtil.xqueryTransform(doc, xquery);
			assertNotNull(xdmNode);
			assertEquals("html", xdmNode.getNodeName().getLocalName());
			Document newDoc = XqueryUtil.XdmNodeToDomDocument(xdmNode);
			assertEquals("html", newDoc.getDocumentElement().getLocalName());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testTransform1() {
		try {
			XqueryUtil xqueryUtil = new XqueryUtil();

			Document doc = DOMUtil.readFile(input);
			
			List<ParameterValue> params = new ArrayList<ParameterValue>();			
			params.add(new ParameterValue(new QName("salarie"),new XdmAtomicValue("src/test/resources/salarie.xml", ItemType.STRING)));

			XdmNode xdmNode = xqueryUtil.xqueryTransform(doc, "personne1.xqy", params);
			assertNotNull(xdmNode);
			
			xqueryUtil.serialize(xdmNode, System.out);
			assertEquals("personnes", xdmNode.getNodeName().getLocalName());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testFunctionDouble() {
		try {
			QName qName = new QName("http://www.irisa.fr/bekkers", "double");
			XdmItem xdmItem = new XdmAtomicValue("3", ItemType.INTEGER);
			XdmValue result = new XqueryUtil().callFunction(
					"functionDouble.xqy", qName, xdmItem);
			XdmItem resultItem = result.itemAt(0);
			assertEquals("6", resultItem.getStringValue());
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testFunctionPersonne() {
		try {

			QName qName = new QName("http://www.irisa.fr/bekkers", "personne");
			XdmItem xdmItem = new XdmAtomicValue("Dupond", ItemType.STRING);
			XdmItem[] params = {xdmItem};
			
			List<ParameterValue> externalParams = new ArrayList<ParameterValue>();			
			externalParams.add(new ParameterValue(new QName("personne"),new XdmAtomicValue("src/test/resources/personne.xml", ItemType.STRING)));

			XdmValue result = new XqueryUtil().callFunction("functionPersonne.xqy", qName, params, externalParams);
			assertNotNull(result);
			XdmItem resultItem = result.itemAt(0);
			assertTrue(!resultItem.isAtomicValue());
			XdmNode xdmNode = (XdmNode) resultItem;
			assertEquals("prenom", xdmNode.getNodeName().getLocalName());
			assertEquals("Jacques", xdmNode.getStringValue());
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

}
