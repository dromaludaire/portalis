package fr.irisa.lis.portalis.commons;


import fr.irisa.lis.portalis.commons.types.NomType;
import fr.irisa.lis.portalis.commons.types.PrenomType;

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */


/**
 * Class PersonneType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class PersonneType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nom.
     */
    private NomType _nom;

    /**
     * Field _prenom.
     */
    private PrenomType _prenom;


      //----------------/
     //- Constructors -/
    //----------------/

    public PersonneType() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    public NomType getNom(
    ) {
        return this._nom;
    }

    /**
     * Returns the value of field 'prenom'.
     * 
     * @return the value of field 'Prenom'.
     */
    public PrenomType getPrenom(
    ) {
        return this._prenom;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final NomType nom) {
        this._nom = nom;
    }

    /**
     * Sets the value of field 'prenom'.
     * 
     * @param prenom the value of field 'prenom'.
     */
    public void setPrenom(
            final PrenomType prenom) {
        this._prenom = prenom;
    }

}
