package fr.irisa.lis.portalis.commons;

import javax.xml.bind.annotation.XmlRootElement;
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class Personnes.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
@XmlRootElement
public class Personnes extends PersonnesType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Personnes() {
        super();
    }

}
