declare variable $salarie as xs:string+ external;
<personnes>{
for $s in doc($salarie)/salaries/salarie
let $p := /personnes/personne[$s/nom=nom][1]
return
	<personne nom="{$p/nom}" prenom="{$p/prenom}" salaire="{$s/salaire}"/>
}</personnes>