declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $personne as xs:string+ external;
declare function local:personne($nom as xs:string)
   as element() {
      for $p in doc($personne)/personnes/personne[$nom=nom][1]/prenom
      return
      	$p
 };
()