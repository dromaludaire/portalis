<html>
<body>
<h1>Liste des personnes</h1>
<table> 
	<tr>
	<th>Nom</th>
	<th>Prénom</th>
	</tr>
{
for $p in /personnes/personne
order by $p/nom descending
return 
	<tr>
	<td>{$p/nom}</td>
	<td>{$p/prenom}</td>
	</tr>
} </table>
</body>
</html>