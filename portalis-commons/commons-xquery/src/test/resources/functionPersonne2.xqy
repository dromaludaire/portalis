declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $root as node() external;

declare function local:personne($nom as xs:string)
   as element() {
   <personne>{
      for $p in $root/personnes/personne[$nom=nom][1]/prenom
      return
      	$p
   } </personne>
 };
()