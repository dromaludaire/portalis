package fr.irisa.lis.portalis.commons.core;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.util.logging.LogManager;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;




public class CommonsUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CommonsUtil.class.getName());
	
	public static final String DEFAULT_ENCODING = "UTF-8";


	private static final String SHA_CODE = "sha-256";

	/**
	 * String representation of an Exception's stack
	 * 
	 * @param e
	 *            the Exception
	 * @return the string representation of the exception stack
	 */
	public static String stack2string(Throwable e) {
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return "\n------ begin stack -----\r\n" + sw.toString()
					+ "\n------ end stack -----\r\n";
		} catch (Exception e2) {
			return "Bad stack2string";
		}
	}

	public static int evaluateExpr(String s) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			// root elements
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("company");
			doc.appendChild(root);
			XPath xpath = XPathFactory.newInstance().newXPath();
			// XPath Query for showing all nodes value
			XPathExpression expr = xpath.compile(s);
			Double d = (Double) expr.evaluate(root, XPathConstants.NUMBER);
			return d.intValue();
		} catch (Exception e) {
			String message = "Impossible d'évaluer l'expression : " + s;
			throw new Error(message, e);
		}
	}
	
	/* remove leading whitespace */
	public static String ltrim(String source) {
		return source.replaceAll("^\\s+", "");
	}

	/* remove trailing whitespace */
	public static String rtrim(String source) {
		return source.replaceAll("\\s+$", "");
	}

	/* replace multiple whitespaces between words with single blank */
	public static String itrim(String source) {
		return source.replaceAll("\\b\\s{2,}\\b", " ");
	}

	/* remove all superfluous whitespaces in source string */
	public static String trim(String source) {
		return itrim(ltrim(rtrim(source)));
	}


	public static String lrtrim(String source) {
		return ltrim(rtrim(source));
	}

	public static String normalize(String s) {
		String source = s;
		if (s.startsWith("'") && s.endsWith("'")) {
			source = s.substring(1,s.length());
		}
		return trim(source).replace("'", "\\'").replace("\"", "\\\"").replaceAll("\n", "").replaceAll("\\s+", " ");
	}


	public static String sha(String text) {
		MessageDigest md = null;
		byte[] sha1hash = null;
		try {
			md = MessageDigest.getInstance(SHA_CODE);
			sha1hash = new byte[40];
			md.update(text.getBytes(DEFAULT_ENCODING), 0, text.length());
		} catch (Exception e) {
			return "Erreur de codage : " + text;
		}
		sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

	static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static void clientInitLogPropertiesFile() throws Exception {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		String fileName = "logging.properties";
		InputStream inputStream = cl.getResourceAsStream(fileName);

		if (inputStream == null) {
			String mess = "pas de fichier de log";
			LOGGER.error(mess);
			throw new Exception(mess);
		}
			LogManager.getLogManager().readConfiguration(inputStream);
			final String PROP = "java.util.logging.FileHandler.level";
			String prop = LogManager.getLogManager().getProperty(PROP);
			if (prop == null) {
				String mess = "pas de propriété '"+PROP+"' dans le fichier de log";
				LOGGER.error(mess);
				throw new Exception(mess);
			}
	}

	public CommonsUtil() {
		super();
	}

}